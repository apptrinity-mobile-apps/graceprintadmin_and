package indo.com.graceprintingadmin.Adapter

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import indo.com.graceprintingadmin.Helper.CustomTextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import indo.com.graceprintingadmin.Activities.MainActivity
import indo.com.graceprintingadmin.Model.APIResponse.Departments
import indo.com.graceprintingadmin.R


class DeptDetailsRecyclerAdapter(val list_status: Boolean, val mJobDeptStatusList: ArrayList<Departments>, val context: Context) : RecyclerView.Adapter<ViewHolder2>() {

    internal lateinit var dialog_list: Dialog
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
        return ViewHolder2(LayoutInflater.from(context).inflate(R.layout.dashboard_deptlistitem, parent, false))


    }

    override fun onBindViewHolder(holder: ViewHolder2, position: Int) {


        holder?.tv_deptname.setText(mJobDeptStatusList.get(position).department_name)
        holder?.tv_deptname_grid.setText(mJobDeptStatusList.get(position).department_name)
        holder?.tv_active.setText("Active" +"\n"+ "(" + mJobDeptStatusList.get(position).active + ")")
        holder?.tv_upcoming.setText("Upcoming" +"\n"+ "(" + mJobDeptStatusList.get(position).upcoming + ")")
        holder?.tv_completed.setText("Completed" +"\n"+ "(" + mJobDeptStatusList.get(position).completed + ")")

        if (mJobDeptStatusList.get(position).department_name.equals("Prepress")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.prepress_icon)
            holder?.iv_deplistitem_grid.setImageResource(R.drawable.prepress_icon)
        } else if (mJobDeptStatusList.get(position).department_name.equals("Press")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.press_icon)
            holder?.iv_deplistitem_grid.setImageResource(R.drawable.press_icon)
        } else if (mJobDeptStatusList.get(position).department_name.equals("Bindery")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.bindery_icon)
            holder?.iv_deplistitem_grid.setImageResource(R.drawable.bindery_icon)
        } else if (mJobDeptStatusList.get(position).department_name.equals("Mailing")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.mailing_icon)
            holder?.iv_deplistitem_grid.setImageResource(R.drawable.mailing_icon)
        } else if (mJobDeptStatusList.get(position).department_name.equals("Shipping")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.shipping_icon)
            holder?.iv_deplistitem_grid.setImageResource(R.drawable.shipping_icon)
        } else if (mJobDeptStatusList.get(position).department_name.equals("Delivery")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.delivery_icon)
            holder?.iv_deplistitem_grid.setImageResource(R.drawable.delivery_icon)
        }


        if (list_status == false) {
            holder?.ll_normal_list.visibility = View.VISIBLE
            holder?.vieline_list.visibility = View.VISIBLE
            holder?.ll_gridview.visibility = View.GONE

        } else {
            holder?.ll_normal_list.visibility = View.GONE
            holder?.vieline_list.visibility = View.GONE
            holder?.ll_gridview.visibility = View.VISIBLE
        }

        /*if(position %2 == 1)
        {
            holder?.iv_deplistitem.setImageResource(R.drawable.digital_job_icon)
        }
        else
        {
            holder?.iv_deplistitem.setImageResource(R.drawable.offsetjob_icon)
        }*/



        holder?.ll_normal_list.setOnClickListener {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra("jobstatid", "")
            intent.putExtra("job_name", mJobDeptStatusList.get(position).department_name)
            intent.putExtra("dep_id", mJobDeptStatusList.get(position).id)
            context.startActivity(intent)
        }

        holder?.tv_clickhere_grid.setOnClickListener {


            dialog_list = Dialog(context)
            dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog_list!!.setContentView(R.layout.dialog_dashdeptview)
            dialog_list!!.setCancelable(false)
            dialog_list!!.setCanceledOnTouchOutside(false)
            val tv_deptname_dialog = dialog_list!!.findViewById(R.id.tv_deptname_dialog) as CustomTextViewBold
            val iv_deplistitem_dialog = dialog_list!!.findViewById(R.id.iv_deplistitem_dialog) as ImageView
            val tv_active_count_dialog = dialog_list!!.findViewById(R.id.tv_active_count_dialog) as CustomTextViewBold
            val tv_upcoming_count_dialog = dialog_list!!.findViewById(R.id.tv_upcoming_count_dialog) as CustomTextViewBold
            val tv_completed_count_dialog = dialog_list!!.findViewById(R.id.tv_completed_count_dialog) as CustomTextViewBold
            val iv_close_dialog = dialog_list!!.findViewById(R.id.iv_close_dialog) as ImageView
            val btn_dialog_dismiss = dialog_list!!.findViewById(R.id.btn_dialog_dismiss) as Button


            // notifyDataSetChanged()

            Log.d("DEPTPOSITION", mJobDeptStatusList.get(position).department_name)

            tv_deptname_dialog!!.setText(mJobDeptStatusList.get(position).department_name)
            tv_active_count_dialog!!.setText(mJobDeptStatusList.get(position).active)
            tv_upcoming_count_dialog!!.setText(mJobDeptStatusList.get(position).upcoming)
            tv_completed_count_dialog!!.setText(mJobDeptStatusList.get(position).completed)

            if (mJobDeptStatusList.get(position).department_name.equals("Prepress")) {
                iv_deplistitem_dialog.setImageResource(R.drawable.prepress_popup)
            } else if (mJobDeptStatusList.get(position).department_name.equals("Press")) {
                iv_deplistitem_dialog.setImageResource(R.drawable.press_popup)
            } else if (mJobDeptStatusList.get(position).department_name.equals("Bindery")) {
                iv_deplistitem_dialog.setImageResource(R.drawable.bindery_popup)
            } else if (mJobDeptStatusList.get(position).department_name.equals("Mailing")) {
                iv_deplistitem_dialog.setImageResource(R.drawable.mailing_popup)
            } else if (mJobDeptStatusList.get(position).department_name.equals("Shipping")) {
                iv_deplistitem_dialog.setImageResource(R.drawable.shipping_popup)
            } else if (mJobDeptStatusList.get(position).department_name.equals("Delivery")) {
                iv_deplistitem_dialog.setImageResource(R.drawable.delivery_popup)
            }

            // updateData(mJobDeptStatusList)
            dialog_list!!.show()

            iv_close_dialog.setOnClickListener {
                dialog_list!!.dismiss()
            }
            btn_dialog_dismiss.setOnClickListener {
                dialog_list!!.dismiss()
            }


        }


    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return mJobDeptStatusList.size
    }


    fun updateData(viewModels: ArrayList<Departments>) {
        mJobDeptStatusList.clear()
        mJobDeptStatusList.addAll(viewModels)
        notifyDataSetChanged()
    }


}

class ViewHolder2(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val tv_deptname = view.findViewById(R.id.tv_deptname) as CustomTextView
    val tv_active = view.findViewById(R.id.tv_active) as CustomTextView

    val tv_upcoming = view.findViewById(R.id.tv_upcoming) as CustomTextView
    val tv_completed = view.findViewById(R.id.tv_completed) as CustomTextView
    val iv_deplistitem = view.findViewById(R.id.iv_deplistitem) as ImageView


    val iv_deplistitem_grid = view.findViewById(R.id.iv_deplistitem_grid) as ImageView
    val tv_deptname_grid = view.findViewById(R.id.tv_deptname_grid) as CustomTextView
    val ll_gridview = view.findViewById(R.id.ll_gridview) as LinearLayout
    val ll_normal_list = view.findViewById(R.id.ll_normal_list) as LinearLayout
    val vieline_list = view.findViewById(R.id.vieline_list) as View
    val tv_clickhere_grid = view.findViewById(R.id.tv_clickhere_grid) as CustomTextView


}
