package indo.com.graceprintingadmin.Adapter


import android.annotation.SuppressLint
import android.app.Activity
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import indo.com.graceprintingadmin.Model.APIResponse.Comments
import indo.com.graceprintingadmin.R


data class CommentsAdapter(val mCommentLIst: ArrayList<Comments>, val activity: Activity) : BaseAdapter() {


    override fun getItem(position: Int): Any {
        return mCommentLIst.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mCommentLIst.size
    }

    @SuppressLint("ResourceAsColor")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.commentlayout, null)
       val commentname = view.findViewById(R.id.commentname) as ImageView
       val commentname_user = view.findViewById(R.id.commentname_user) as ImageView
        val commentbody = view.findViewById(R.id.commentbody) as TextView
        val commenttime = view.findViewById(R.id.commenttime) as TextView
       // val ll_msg_view = view.findViewById(R.id.ll_msg_view) as LinearLayout

       // val ll_comments = view.findViewById(R.id.ll_comments) as LinearLayout





        if(mCommentLIst.get(position).user_type.equals("admin")){

            commentname.setImageResource(R.drawable.admin_chat_icon)
            commentbody.text = mCommentLIst.get(position).comment
            commenttime.text = mCommentLIst.get(position).comment_on
            commentname_user.visibility = View.GONE
            commentname.visibility = View.VISIBLE

            val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            paramsadm.gravity = Gravity.LEFT
            //commentbody!!.setBackgroundColor(R.color.admin_chat_admin_color)
            commentbody.setBackgroundDrawable(activity.resources.getDrawable(R.drawable.chat_bg))
            commentbody!!.setLayoutParams(paramsadm)
            commentname!!.setLayoutParams(paramsadm)
            commenttime!!.setLayoutParams(paramsadm)

            //commentbody.gravity = Gravity.START

           /* val textViewadm = TextView(activity.applicationContext)
            val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            paramsadm!!.gravity = Gravity.START
            paramsadm.setMargins(5, 5, 5, 5)
            textViewadm.setPadding(2, 2, 2, 2)
            textViewadm!!.setLayoutParams(paramsadm)
            textViewadm!!.gravity = Gravity.LEFT
            textViewadm!!.setBackgroundColor(activity.resources.getColor(R.color.tab1_bg))
            textViewadm!!.setTextColor(activity.resources.getColor(R.color.tab1_bg))
            textViewadm!!.setTextSize(activity.resources.getDimension(R.dimen.comment_text))
            textViewadm!!.setBackground(activity.resources.getDrawable(R.drawable.bubble1))
            textViewadm!!.text = mCommentLIst.get(position).comment
            //ll_comments?.addView(textViewadm)
            ll_comments?.addView(textViewadm)*/




        }else if(mCommentLIst.get(position).user_type.equals("user")){

            commentname_user.setImageResource(R.drawable.user_chat_icon)
            commentname_user.visibility = View.VISIBLE
            commentname.visibility = View.GONE
            commentbody.text = mCommentLIst.get(position).comment
            commenttime.text = mCommentLIst.get(position).comment_on

            val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            paramsadm.gravity = Gravity.RIGHT
           // commentbody.setBackgroundColor(activity.resources.getColor(R.color.admin_chat_user_color))
            commentbody.setBackgroundDrawable(activity.resources.getDrawable(R.drawable.chat_bg_user))
            commentbody!!.setLayoutParams(paramsadm)
            commenttime!!.setLayoutParams(paramsadm)
            commentname!!.setLayoutParams(paramsadm)

            /*val textViewadm = TextView(activity.applicationContext)
            val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            paramsadm!!.gravity = Gravity.END
            paramsadm.setMargins(5, 5, 5, 5)
            textViewadm.setPadding(2, 2, 2, 2)
            textViewadm!!.setLayoutParams(paramsadm)
            textViewadm!!.gravity = Gravity.RIGHT
            textViewadm!!.setBackgroundColor(activity.resources.getColor(R.color.tab2_bg))
            textViewadm!!.setTextColor(activity.resources.getColor(R.color.tab2_bg))
            textViewadm!!.setTextSize(activity.resources.getDimension(R.dimen.comment_text))
            textViewadm!!.setBackground(activity.resources.getDrawable(R.drawable.bubble1))
            textViewadm!!.text = mCommentLIst.get(position).comment
            //ll_comments?.addView(textViewadm)
            ll_comments?.addView(textViewadm)*/
        }

        
        return view
    }

}
