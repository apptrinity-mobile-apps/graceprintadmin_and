package indo.com.graceprintingadmin.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import indo.com.graceprinting.Fragment.ActiveMovingFragment
import indo.com.graceprinting.Fragment.CompletedMovingFragment
import indo.com.graceprinting.Fragment.UpcomingMovingFragment


class MyPagerMovingAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {

            0 -> ActiveMovingFragment()
            1 -> UpcomingMovingFragment()
            else -> {
                return CompletedMovingFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "ACTIVE"
            1 -> "UPCOMING"
            else -> {
                return "COMPLETED"
            }
        }
    }
}