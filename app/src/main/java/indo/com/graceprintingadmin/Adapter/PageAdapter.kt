package indo.com.graceprintingadmin.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class PageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val tabNames: ArrayList<String>
    private val fragments: ArrayList<Fragment>
    private val tabTitles = arrayOf("")

    init {
        tabNames = ArrayList()
        fragments = ArrayList()
    }

    fun add(fragment: Fragment, title: String) {
        tabNames.add(title)
        fragments.add(fragment)
    }

    override fun getCount(): Int {
        return tabNames.size
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            /*0 -> return UserOrderRecentFragment()
            1 -> return UserOrderProductionFragment()
            2 -> return UserOrderCompletedFragment()
            3 -> return UserOrderAllFragment()*/
        }
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence {
        return tabNames[position]
    }

}