/*
 * Copyright (C) 2015 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package indo.com.graceprintingadmin.Adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.view.MotionEventCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Activities.JobDetailsFullList
import indo.com.graceprintingadmin.Helper.ConnectionManager
import indo.com.graceprintingadmin.Helper.CustomTextView
import indo.com.graceprintingadmin.Helper.ItemTouchHelperAdapter
import indo.com.graceprintingadmin.Helper.ItemTouchHelperViewHolder
import indo.com.graceprintingadmin.Model.APIResponse.CheckJobIdResponse
import indo.com.graceprintingadmin.Model.APIResponse.JobsListDataResponse
import indo.com.graceprintingadmin.R
import retrofit2.Call
import retrofit2.Callback
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Simple RecyclerView.Adapter that implements [ItemTouchHelperAdapter] to respond to move and
 * dismiss events from a [android.support.v7.widget.helper.ItemTouchHelper].
 *
 * @author Paul Burke (ipaulpro)
 */
class RecyclerListAdapter(val mServiceList: ArrayList<JobsListDataResponse>, val mService_ids_list: ArrayList<String>, val context: Context, var dep_stg_id: String) : RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>() {

    private val mItems = mService_ids_list

    var arrowdownup = false
    internal var prepress_text: String = ""
    internal var press_text: String = ""
    internal var bindery_text: String = ""
    internal var mailing_text: String = ""
    internal var shipping_text: String = ""
    internal var delivery_text: String = ""
    internal var toatal_count: String = ""
    internal var precount: Int = 0
    internal var presscount: Int = 0
    internal var binderycount: Int = 0
    internal var mailingcount: Int = 0
    internal var shippingcount: Int = 0
    internal var deliverycount: Int = 0

    internal var prepress_comment_view_status: Int = 0
    internal var press_comment_view_status: Int = 0
    internal var bindery_comment_view_status: Int = 0
    internal var mailing_comment_view_status: Int = 0
    internal var shipping_comment_view_status: Int = 0
    internal var delivery_comment_view_status: Int = 0

    internal var delete_job_id: String = ""


    internal lateinit var my_loader: Dialog
    lateinit var after_list: ArrayList<JobsListDataResponse>
    lateinit var mFeedsList: ArrayList<JobsListDataResponse>


    private val colors = intArrayOf(Color.parseColor("#fff9ef"), Color.parseColor("#eefbff"), Color.parseColor("#fff3eb"), Color.parseColor("#ffeffa"), Color.parseColor("#ffece1"), Color.parseColor("#ebffec"))

    private val list_images = intArrayOf(R.drawable.bg_joblist1, R.drawable.bg_joblist2, R.drawable.bg_joblist3, R.drawable.bg_joblist4, R.drawable.bg_joblist5, R.drawable.bg_joblist6)

    /*init {
        mItems.addAll(Arrays.asList(*context.resources.getStringArray(R.array.dummy_items)))
    }*/
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.listitem_listofjobs, parent, false)
        return ItemViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility", "ResourceAsColor")
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        if (selectedPosition == position) {
            holder.cv_joblistexpand.visibility = View.VISIBLE
        } else {
            holder.cv_joblistexpand.visibility = View.GONE

        }

        holder.card_view.setOnClickListener {
            if (arrowdownup == false) {
                holder.cv_joblistexpand.visibility = View.VISIBLE
                arrowdownup = true
            } else {
                holder.cv_joblistexpand.visibility = View.GONE
                arrowdownup = false
            }
            /*selectedPosition = position

            notifyDataSetChanged();*/
        }
        myloading()
        val colorPos = position % colors.size
        val imagePos = position % list_images.size
        // holder?.card_view.setBackgroundColor(colors[colorPos])
        //holder?.ll_header_joblist.setBackgroundResource(list_images[imagePos])


        /*if (mServiceList.get(position).job_part.equals("1")) {
            holder?.ll_header_joblist.setBackgroundResource(R.drawable.bg_joblist1)
        } else if (mServiceList.get(position).job_part.equals("2")) {
            holder?.ll_header_joblist.setBackgroundResource(R.drawable.bg_joblist2)
        } else if (mServiceList.get(position).job_part.equals("3")) {
            holder?.ll_header_joblist.setBackgroundResource(R.drawable.bg_joblist4)
        } else if (mServiceList.get(position).job_part.equals("4")) {
            holder?.ll_header_joblist.setBackgroundResource(R.drawable.bg_joblist6)
        }*/

        try {
            holder.tv_job_id.text = mServiceList.get(position).job_id
            holder.tv_job_name.text = mServiceList.get(position).job_name
            holder.tv_customer.text = mServiceList.get(position).custmer
            holder.tv_customer_name.text = /*"Customer : " +*/ mServiceList.get(position).custmer
            holder.tv_jobpart.text = mServiceList.get(position).job_part
            holder.tv_jobpart_header.text = "Job Part :" + mServiceList.get(position).job_part
            //holder.tv_status.text = mServiceList.get(position).created_date

            ////time date formate change
            val inputPattern = "yyyy-MM-dd HH:mm:ss"
            val outputPattern = "MM/dd/yyyy h:mm a"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var str: String? = null
            Log.e("dates_times", mServiceList.get(position).created_date.toString())
            try {
                date = inputFormat.parse(mServiceList.get(position).created_date.toString())
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            Log.e("dates_times_lll", str)
            holder.tv_status.text = str


        /*if (mServiceList.get(position).jobdonestatus.equals("0")) {
            holder.image_done.setImageResource(R.drawable.done_tick)
        } else {
            holder.image_done.setImageResource(android.R.color.transparent )
        }*/
        if (mServiceList.get(position).jobdonestatus.equals("1")) {
            holder?.ll_header_joblist.setBackgroundResource(R.drawable.yellow_bg)
            holder?.card_view.setBackgroundColor(Color.parseColor("#FFF9E3"))
        }
        if (mServiceList.get(position).jobdonestatus.equals("2")) {
            holder?.ll_header_joblist.setBackgroundResource(R.drawable.green_bg)
            holder?.card_view.setBackgroundColor(Color.parseColor("#F0FFF0"))
        }
        if (mServiceList.get(position).jobdonestatus.equals("3")) {
            holder?.ll_header_joblist.setBackgroundResource(R.drawable.red_bg)
            holder?.card_view.setBackgroundColor(Color.parseColor("#FFECEC"))
        }


        delete_job_id = mServiceList.get(position).id.toString()

        if (mServiceList.get(position).Prepress_assigned_status.equals("0")) {
            prepress_text = "N/A"
        } else if (mServiceList.get(position).Prepress_assigned_status.equals("1")) {
            prepress_text = "Not Started"
        } else if (mServiceList.get(position).Prepress_assigned_status.equals("2")) {
            prepress_text = "Processing"
        } else if (mServiceList.get(position).Prepress_assigned_status.equals("3")) {
            prepress_text = "Completed"
        }

        if (mServiceList.get(position).Press_assigned_status.equals("0")) {
            press_text = "N/A"
        } else if (mServiceList.get(position).Press_assigned_status.equals("1")) {
            press_text = "Not Started"
        } else if (mServiceList.get(position).Press_assigned_status.equals("2")) {
            press_text = "Processing"
        } else if (mServiceList.get(position).Press_assigned_status.equals("3")) {
            press_text = "Completed"
        }

        if (mServiceList.get(position).Bindery_assigned_status.equals("0")) {
            bindery_text = "N/A"
        } else if (mServiceList.get(position).Bindery_assigned_status.equals("1")) {
            bindery_text = "Not Started"
        } else if (mServiceList.get(position).Bindery_assigned_status.equals("2")) {
            bindery_text = "Processing"
        } else if (mServiceList.get(position).Bindery_assigned_status.equals("3")) {
            bindery_text = "Completed"
        }

        if (mServiceList.get(position).Mailing_assigned_status.equals("0")) {
            mailing_text = "N/A"
        } else if (mServiceList.get(position).Mailing_assigned_status.equals("1")) {
            mailing_text = "Not Started"
        } else if (mServiceList.get(position).Mailing_assigned_status.equals("2")) {
            mailing_text = "Processing"
        } else if (mServiceList.get(position).Mailing_assigned_status.equals("3")) {
            mailing_text = "Completed"
        }

        if (mServiceList.get(position).Shipping_assigned_status.equals("0")) {
            shipping_text = "N/A"
        } else if (mServiceList.get(position).Shipping_assigned_status.equals("1")) {
            shipping_text = "Not Started"
        } else if (mServiceList.get(position).Shipping_assigned_status.equals("2")) {
            shipping_text = "Processing"
        } else if (mServiceList.get(position).Shipping_assigned_status.equals("3")) {
            shipping_text = "Completed"
        }

        if (mServiceList.get(position).Delivery_assigned_status.equals("0")) {
            delivery_text = "N/A"
        } else if (mServiceList.get(position).Delivery_assigned_status.equals("1")) {
            delivery_text = "Not Started"
        } else if (mServiceList.get(position).Delivery_assigned_status.equals("2")) {
            delivery_text = "Processing"
        } else if (mServiceList.get(position).Delivery_assigned_status.equals("3")) {
            delivery_text = "Completed"
        }

        holder?.tv_prepress.text = prepress_text + " (" + mServiceList.get(position).Prepress_commentcount + ")"
        holder?.tv_press.text = press_text + " (" + mServiceList.get(position).Press_commentcount + ")"
        holder?.tv_bindery.text = bindery_text + " (" + mServiceList.get(position).Bindery_commentcount + ")"
        holder?.tv_mailing.text = mailing_text + " (" + mServiceList.get(position).Mailing_commentcount + ")"
        holder?.tv_shipping.text = shipping_text + " (" + mServiceList.get(position).Shipping_commentcount + ")"
        holder?.tv_delivery.text = delivery_text + " (" + mServiceList.get(position).Delivery_commentcount + ")"


        precount = Integer.parseInt(mServiceList.get(position).Prepress_commentcount)
        presscount = Integer.parseInt(mServiceList.get(position).Press_commentcount)
        binderycount = Integer.parseInt(mServiceList.get(position).Bindery_commentcount)
        mailingcount = Integer.parseInt(mServiceList.get(position).Mailing_commentcount)
        shippingcount = Integer.parseInt(mServiceList.get(position).Shipping_commentcount)
        deliverycount = Integer.parseInt(mServiceList.get(position).Delivery_commentcount)
        val total = precount + presscount + binderycount + mailingcount + shippingcount + deliverycount
        Log.e("TOTALCOUNT_BEFORE", total.toString())
        holder?.tv_deptcount.text = total.toString()

        prepress_comment_view_status = Integer.parseInt(mServiceList.get(position).Prepress_comment_view_status)
        press_comment_view_status = Integer.parseInt(mServiceList.get(position).Prepress_comment_view_status)
        mailing_comment_view_status = Integer.parseInt(mServiceList.get(position).Prepress_comment_view_status)
        bindery_comment_view_status = Integer.parseInt(mServiceList.get(position).Prepress_comment_view_status)
        shipping_comment_view_status = Integer.parseInt(mServiceList.get(position).Prepress_comment_view_status)
        delivery_comment_view_status = Integer.parseInt(mServiceList.get(position).Prepress_comment_view_status)

        Log.e("COMMENTVIEWSTATUS", "" + prepress_comment_view_status + "----" + press_comment_view_status + "----" + mailing_comment_view_status + "----" + bindery_comment_view_status + "----" + shipping_comment_view_status + "----" + delivery_comment_view_status)
        if (prepress_comment_view_status.equals("1") || press_comment_view_status.equals("1") || mailing_comment_view_status.equals("1") || bindery_comment_view_status.equals("1") || shipping_comment_view_status.equals("1") || delivery_comment_view_status.equals("1")) {
            holder?.tv_deptcount.setBackgroundResource(R.drawable.circle_ic_notviewed)
        } else {
            holder?.tv_deptcount.setBackgroundResource(R.drawable.circle_ic)
        }
        holder?.job_edit.setOnClickListener {
            if (ConnectionManager.checkConnection(context)) {
                val intent = Intent(this.context, JobDetailsFullList::class.java)
                intent.putExtra("job_id", mServiceList.get(position).id)
                intent.putExtra("id", mServiceList.get(position).id)
                context.startActivity(intent)
            } else {
                ConnectionManager.snackBarNetworkAlert_LinearLayout(holder.rootview, context)
            }
        }

        holder?.btn_delete.setOnClickListener {
            if (ConnectionManager.checkConnection(context)) {
                jobDelete(delete_job_id)
                mServiceList.removeAt(position)
                notifyDataSetChanged()
                my_loader.dismiss()
            } else {
                ConnectionManager.snackBarNetworkAlert_LinearLayout(holder.rootview, context)
            }
        }
        /* holder.job_edit.setOnClickListener(View.OnClickListener {

             if (ConnectionManager.checkConnection(context)) {
                 val intent = Intent(this.context, JobDetailsFullList::class.java)
                 intent.putExtra("job_id", mServiceList.get(position).id)
                 intent.putExtra("id", mServiceList.get(position).id)
                 context.startActivity(intent)
             } else {
                 ConnectionManager.snackBarNetworkAlert_LinearLayout(holder.rootview, context)
             }

         })*/
        // Start a drag whenever the handle view it touched

        if (dep_stg_id.equals("")) {
            holder.image_move_id.visibility = View.GONE
            holder.tv_active_id.visibility = View.GONE
        } else {
            holder.image_move_id.visibility = View.GONE
            holder.tv_active_id.visibility = View.VISIBLE
        }


        holder.image_move_id.setOnTouchListener { v, event ->
            if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                // mDragStartListener.onStartDrag(holder)
                Log.e("main_list_positions", v.toString() + "---" + mServiceList.get(position).id + "---" + mServiceList.get(position).toString())

                Log.e("main_positions_lll", mServiceList.get(position).id)

            }
            false
        }
        // holder.tv_active_id.text = mServiceList.get(position).dep_status



        if (mServiceList.get(position).active_status.equals("1")) {
            holder.tv_active_id.setBackgroundResource(R.drawable.green_new_bg)
            holder.tv_active_id.text = "Active"
        } else if (mServiceList.get(position).active_status.equals("0")) {
            holder.tv_active_id.setBackgroundResource(R.drawable.yellow_new_bg)
            holder.tv_active_id.text = "InActive"
        }
        holder.tv_active_id.setOnClickListener {
            if (mServiceList.get(position).dep_status.equals("0") || mServiceList.get(position).dep_status.equals("1")) {
                val apiService = ApiInterface.create()
                val call = apiService.changedepStatus(mServiceList.get(position).job_dep_id!!)
                Log.d("REQUEST", call.toString() + "")
                call.enqueue(object : Callback<CheckJobIdResponse> {
                    override fun onResponse(call: Call<CheckJobIdResponse>, response: retrofit2.Response<CheckJobIdResponse>?) {
                        if (response != null) {
                            if (response.body()!!.status.equals("1")) {
                                Log.w("Result_STATUS", response.body()!!.status.toString())
                                if (response.body()!!.status.toString().equals("1")) {
                                    if (holder.tv_active_id.text.equals("Active")) {
                                        holder.tv_active_id.setBackgroundResource(R.drawable.yellow_new_bg)
                                        holder.tv_active_id.text = "InActive"
                                    } else {
                                        holder.tv_active_id.setBackgroundResource(R.drawable.green_new_bg)
                                        holder.tv_active_id.text = "Active"
                                    }

                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<CheckJobIdResponse>, t: Throwable) {
                        Log.w("Result_Order_details", t.toString())
                    }
                })
            } else {
                Log.w("dep_status", mServiceList.get(position).dep_status.toString())
            }

        }

        } catch (e: Exception) {

        }
    }


    /*override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(mItems, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        Log.e("main_positions", mItems.toString() + "---" + toPosition.toString() + "---" + fromPosition.toString())

        *//* val itemPos = mItems.indexOf(mItems.get(fromPosition))
         mItems.removeAt(itemPos)
         mItems.add(toPosition, mItems.get(fromPosition))*//*
        System.out.println(mItems)

        var test = mItems.toString()
        test = test.replace("[\\p{Ps}\\p{Pe}]".toRegex(), "")
        Log.e("TEST", test)
        val apiService = ApiInterface.create()
        val call = apiService.changeJobOrderAPI(test, "android", dep_stg_id)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<CheckJobIdResponse> {
            override fun onResponse(call: Call<CheckJobIdResponse>, response: retrofit2.Response<CheckJobIdResponse>?) {
                if (response != null) {
                    if (response.body()!!.status.equals("1")) {
                        Log.w("Result_STATUS", response.body()!!.status.toString())

                    }
                }
            }

            override fun onFailure(call: Call<CheckJobIdResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
        return true
    }*/

    override fun getItemCount(): Int {
        return mItems.size
    }

    /**
     * Simple example of a view holder that implements [ItemTouchHelperViewHolder] and has a
     * "handle" view that initiates a drag event when touched.
     */
    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view), ItemTouchHelperViewHolder {
        lateinit var rootview: LinearLayout
        lateinit var tv_job_id: TextView
        lateinit var tv_job_name: TextView
        lateinit var tv_customer: TextView
        lateinit var tv_jobpart: TextView
        lateinit var tv_prepress: TextView
        lateinit var tv_deptcount: TextView
        lateinit var tv_press: TextView
        lateinit var tv_bindery: TextView
        lateinit var tv_mailing: TextView
        lateinit var tv_shipping: TextView
        lateinit var tv_delivery: TextView
        lateinit var tv_status: TextView
        lateinit var tv_jobpart_header: TextView
        lateinit var card_view: CardView
        lateinit var cv_joblistexpand: CardView
        lateinit var job_edit: ImageView
        lateinit var job_delete: ImageView
        lateinit var image_move_id: ImageView
        lateinit var ll_header_joblist: LinearLayout
        lateinit var tv_customer_name: CustomTextViewBold
        lateinit var btn_delete: ImageView

        lateinit var tv_active_id: CustomTextView

        init {
            rootview = view.findViewById(R.id.rootview) as LinearLayout
            tv_job_id = view.findViewById(R.id.tv_job_id) as TextView
            tv_job_name = view.findViewById(R.id.tv_job_name) as TextView
            tv_customer = view.findViewById(R.id.tv_customer) as TextView
            tv_jobpart = view.findViewById(R.id.tv_jobpart) as TextView
            tv_prepress = view.findViewById(R.id.tv_prepress) as TextView
            tv_press = view.findViewById(R.id.tv_press) as TextView
            tv_bindery = view.findViewById(R.id.tv_bindery) as TextView
            tv_mailing = view.findViewById(R.id.tv_mailing) as TextView
            tv_shipping = view.findViewById(R.id.tv_shipping) as TextView
            tv_delivery = view.findViewById(R.id.tv_delivery) as TextView
            tv_status = view.findViewById(R.id.tv_status) as TextView
            tv_jobpart_header = view.findViewById(R.id.tv_jobpart_header) as TextView
            tv_deptcount = view.findViewById(R.id.tv_deptcount) as TextView
            ll_header_joblist = view.findViewById(R.id.ll_header_joblist) as LinearLayout
            btn_delete = view.findViewById(R.id.btn_delete) as ImageView

            tv_customer_name = view.findViewById(R.id.tv_customer_name) as CustomTextViewBold

            card_view = view.findViewById(R.id.card_view) as CardView
            cv_joblistexpand = view.findViewById(R.id.cv_joblistexpand) as CardView

            job_edit = view.findViewById(R.id.btn_edit) as ImageView
            job_delete = view.findViewById(R.id.btn_delete) as ImageView
            image_move_id = view.findViewById(R.id.image_move_id) as ImageView

            tv_active_id = view.findViewById(R.id.tv_active_id) as CustomTextView
        }

        override fun onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY)
        }

        override fun onItemClear() {
            itemView.setBackgroundColor(0)
        }
    }


    private fun jobDelete(job_id: String) {

        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.jobdelete(job_id)
        Log.d("GETJOBID", job_id)
        call.enqueue(object : Callback<CheckJobIdResponse> {
            override fun onResponse(call: Call<CheckJobIdResponse>, response: retrofit2.Response<CheckJobIdResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Order_details", response.body().toString())
                    if (response.body()!!.status.equals("1")) {
                        val result = response.body()!!.result!!
                        Log.d("DELETERESULT", result)
                        //updateData(mServiceList)
                    }/*else if(response.body()!!.status.equals("2")) {
                        *//*no_service!!.text = "No Data Found"
                        no_service!!.visibility = View.VISIBLE*//*
                    }*/
                    else {

                    }

                }
            }

            override fun onFailure(call: Call<CheckJobIdResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun myloading() {
        my_loader = Dialog(context)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    fun parseDateToddMMyyyy(time: String): String? {
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val outputPattern = "dd-MMM-yyyy h:mm a"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)

        var date: Date? = null
        var str: String? = null

        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }
}
