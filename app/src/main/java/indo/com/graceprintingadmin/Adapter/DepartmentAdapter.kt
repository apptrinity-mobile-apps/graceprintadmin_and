package indo.com.graceprintingadmin.Adapter


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import indo.com.graceprintingadmin.Model.APIResponse.JobDetailViewDataDepartmentResponse
import indo.com.graceprintingadmin.R


class DepartmentAdapter(val mServiceList: ArrayList<JobDetailViewDataDepartmentResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.listitem_department, parent, false))


    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       /* holder?.tv_deptname?.text = mServiceList.get(position).department_name
        holder?.tv_username?.text = mServiceList.get(position).user_name
        //holder?.tv_status?.text = mServiceList.get(position).assigned_status
        holder?.dept_edit.setOnClickListener(View.OnClickListener {
            val intent = Intent(context,AssignjobEditActivity::class.java)
            intent.putExtra("deptname",mServiceList.get(position).department_name)
            intent.putExtra("deptjobid",mServiceList.get(position).id)
            intent.putExtra("assignedid",mServiceList.get(position).assigned_id)
            context.startActivity(intent)
        })
        holder?.dept_view.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, AssignedViewJob::class.java)
            intent.putExtra("deptname",mServiceList.get(position).department_name)
            intent.putExtra("deptjobid",mServiceList.get(position).id)
            intent.putExtra("assignedid",mServiceList.get(position).assigned_id)
            context.startActivity(intent)
        })
        if(mServiceList.get(position).assigned_status=="0") {
            holder?.tv_status?.text = "Not Yet Assigned"
            holder?.dept_edit.visibility = View.VISIBLE
            holder?.dept_view.visibility = View.INVISIBLE
        }
        if(mServiceList.get(position).assigned_status=="1") {
            holder?.tv_status?.text = "Not Started"
            holder?.dept_edit.visibility = View.VISIBLE
            holder?.dept_view.visibility = View.VISIBLE
        }
        if(mServiceList.get(position).assigned_status=="2") {
            holder?.tv_status?.text = "Processing"
            holder?.dept_edit.visibility = View.INVISIBLE
            holder?.dept_view.visibility = View.VISIBLE
        }
        if(mServiceList.get(position).assigned_status=="3") {
            holder?.tv_status?.text = "Completed"
            holder?.dept_edit.visibility = View.INVISIBLE
            holder?.dept_view.visibility = View.VISIBLE
        }*/
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return mServiceList.size
    }

    // Inflates the item views

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val tv_deptname = view.findViewById(R.id.tv_deptname) as TextView
    val tv_username = view.findViewById(R.id.tv_username) as TextView
    val tv_status = view.findViewById(R.id.tv_status) as TextView
    val dept_edit = view.findViewById(R.id.dept_edit) as ImageView
    val dept_view = view.findViewById(R.id.dept_view) as ImageView
}

