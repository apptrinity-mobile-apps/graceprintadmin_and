/*
package indo.com.graceprintingadmin.Adapter

import java.util.Collections.swap
import android.graphics.Color.LTGRAY
import sun.awt.windows.ThemeReader.getPosition
import android.widget.TextView
import indo.com.graceprintingadmin.Helper.ItemTouchHelperViewHolder
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.support.v4.view.MotionEventCompat
import android.view.View.OnTouchListener
import com.squareup.picasso.Picasso
import android.view.ViewGroup
import android.view.LayoutInflater
import indo.com.graceprintingadmin.Helper.OnStartDragListener
import indo.com.graceprintingadmin.Helper.ItemTouchHelperAdapter



class datatest {

    inner class ItemAdapter(private val mContext: Context, private var mPersonList: MutableList<ItemModel>?, private val mDragStartListener: OnStartDragListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ItemTouchHelperAdapter {
        internal var mItemClickListener: OnItemClickListener? = null
        private val mInflater: LayoutInflater

        init {
            this.mInflater = LayoutInflater.from(mContext)

        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            if (viewType == TYPE_ITEM) {
                //inflate your layout and pass it to view holder
                val v = mInflater.inflate(R.layout.person_item, viewGroup, false)
                return VHItem(v)
            }

            throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")

        }

        override fun getItemViewType(position: Int): Int {
            return TYPE_ITEM
        }


        override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

            if (viewHolder is VHItem) {

                viewHolder.title.setText(mPersonList!![i].getName())
                Picasso.with(mContext)
                        .load(mPersonList!![i].getImagePath())
                        .placeholder(R.drawable.ic_profile)
                        .into(viewHolder.imageView)

                viewHolder.image_menu.setOnTouchListener(object : View.OnTouchListener() {
                    fun onTouch(v: View, event: MotionEvent): Boolean {
                        if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                            mDragStartListener.onStartDrag(viewHolder)
                        }
                        return false
                    }
                })
            }
        }

        override fun getItemCount(): Int {
            return mPersonList!!.size
        }

        interface OnItemClickListener {
            fun onItemClick(view: View, position: Int)
        }

        fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
            this.mItemClickListener = mItemClickListener
        }

        inner class VHItem(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener, ItemTouchHelperViewHolder {
            var title: TextView
            private val imageView: ImageView
            private val image_menu: ImageView

            init {
                title = itemView.findViewById(R.id.name)
                image_menu = itemView.findViewById(R.id.image_menu) as ImageView
                imageView = itemView.findViewById(R.id.circle_imageView) as ImageView
                itemView.setOnClickListener(this)
            }

            fun onClick(v: View) {
                if (mItemClickListener != null) {
                    mItemClickListener!!.onItemClick(v, position)
                }
            }

            override fun onItemSelected() {
                itemView.setBackgroundColor(Color.LTGRAY)
            }

            override fun onItemClear() {
                itemView.setBackgroundColor(0)
            }
        }

        override fun onItemDismiss(position: Int) {
            mPersonList!!.removeAt(position)
            notifyItemRemoved(position)
        }

        override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
            //Log.v("", "Log position" + fromPosition + " " + toPosition);
            if (fromPosition < mPersonList!!.size && toPosition < mPersonList!!.size) {
                if (fromPosition < toPosition) {
                    for (i in fromPosition until toPosition) {
                        Collections.swap(mPersonList, i, i + 1)
                    }
                } else {
                    for (i in fromPosition downTo toPosition + 1) {
                        Collections.swap(mPersonList, i, i - 1)
                    }
                }
                notifyItemMoved(fromPosition, toPosition)
            }
            return true
        }

        fun updateList(list: MutableList<ItemModel>) {
            mPersonList = list
            notifyDataSetChanged()
        }

        companion object {
            private val TYPE_ITEM = 0
        }
    }

}*/
