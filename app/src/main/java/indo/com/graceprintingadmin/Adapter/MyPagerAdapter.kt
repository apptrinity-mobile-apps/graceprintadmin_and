package indo.com.graceprintingadmin.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import indo.com.graceprinting.Fragment.ActiveFragment
import indo.com.graceprinting.Fragment.CompletedFragment
import indo.com.graceprinting.Fragment.UpcomingFragment


class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {

            0 -> ActiveFragment()
            1 -> UpcomingFragment()
            else -> {
                return CompletedFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "ACTIVE"
            1 -> "UPCOMING"
            else -> {
                return "COMPLETED"
            }
        }
    }
}