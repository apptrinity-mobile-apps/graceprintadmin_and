package com.indobytes.biil.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import indo.com.graceprintingadmin.Activities.DepartMainActivity
import indo.com.graceprintingadmin.Activities.MainActivity
import indo.com.graceprintingadmin.Model.APIResponse.Jobstatuses
import indo.com.graceprintingadmin.R


class ServiceDetailsRecyclerAdapter(val mJobStatusList: ArrayList<Jobstatuses>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.dashboard_listitem, parent, false))


    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.tv_jobname.setText(mJobStatusList.get(position).job_name)
        holder?.tv_jobcount.setText("Total ("+mJobStatusList.get(position).total+")")

        holder?.tv_active_count.setText("Active (" +mJobStatusList.get(position).active+")")
        holder?.tv_upcoming_count.setText("Upcoming (" +mJobStatusList.get(position).upcoming+")")
        holder?.tv_completed_count.setText("Completed (" +mJobStatusList.get(position).completed+")")


       /* if(mJobStatusList.get(position).job_name.equals("Offset Jobs")){
            holder?.tv_jobname.setTextColor(context.resources.getColor(R.color.tab1_bg))
            holder?.tv_active_count.setBackgroundColor(context.resources.getColor(R.color.tab1_bg))

        }else if(mJobStatusList.get(position).job_name.equals("Digital Jobs")){

            holder?.tv_jobname.setTextColor(context.resources.getColor(R.color.colorpink))
            holder?.tv_active_count.setBackgroundColor(context.resources.getColor(R.color.colorpink))
        }*/


        if(position %2 == 1)
        {
            holder?.iv_listitem_dash.setImageResource(R.drawable.digital_job_icon)
            holder?.tv_jobname.setTextColor(context.resources.getColor(R.color.colorpink))
            holder?.tv_active_count!!.setBackgroundDrawable(context.resources.getDrawable(R.drawable.dash_active_digital_bg))
        }
        else
        {
            holder?.iv_listitem_dash.setImageResource(R.drawable.offsetjob_icon)
            holder?.tv_jobname.setTextColor(context.resources.getColor(R.color.tab1_bg))
            holder?.tv_active_count!!.setBackgroundDrawable(context.resources.getDrawable(R.drawable.dash_active_offset_bg))
        }



        holder?.ll_jobstatusinfo.setOnClickListener {
            val intent = Intent(context,DepartMainActivity::class.java)
            intent.putExtra("jobstatid",mJobStatusList.get(position).id)
            intent.putExtra("job_name",mJobStatusList.get(position).job_name)
            context.startActivity(intent)
        }
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return mJobStatusList.size
    }


}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val tv_jobname = view.findViewById(R.id.tv_jobname) as TextView
    val tv_jobcount = view.findViewById(R.id.tv_jobcount) as TextView

    val tv_active_count = view.findViewById(R.id.tv_active_count) as TextView
    val tv_upcoming_count = view.findViewById(R.id.tv_upcoming_count) as TextView
    val tv_completed_count = view.findViewById(R.id.tv_completed_count) as TextView
    val iv_listitem_dash = view.findViewById(R.id.iv_listitem_dash) as ImageView

    val ll_jobstatusinfo = view.findViewById(R.id.jobstatusinfo) as LinearLayout

}
