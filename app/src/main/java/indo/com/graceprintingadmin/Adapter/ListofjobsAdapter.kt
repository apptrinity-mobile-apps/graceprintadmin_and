package indo.com.graceprintingadmin.Adapter


import android.app.Activity
import android.content.Intent
import android.support.v7.widget.CardView
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import indo.com.graceprintingadmin.Activities.JobDetailsFullList
import indo.com.graceprintingadmin.Helper.ConnectionManager
import indo.com.graceprintingadmin.Model.APIResponse.JobsListDataResponse
import indo.com.graceprintingadmin.R


data class ListofjobsAdapter(val mServiceList: ArrayList<JobsListDataResponse>, val activity: Activity) : BaseAdapter() {

    internal var prepress_text: String = ""
    internal var press_text: String = ""
    internal var bindery_text: String = ""
    internal var mailing_text: String = ""
    internal var shipping_text: String = ""
    internal var delivery_text: String = ""

    var arrowdownup = false

    override fun getItem(position: Int): Any {
        return mServiceList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mServiceList.size
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View = View.inflate(activity, R.layout.listitem_listofjobs, null)


        val rootview = view.findViewById(R.id.rootview) as LinearLayout
        val tv_job_id = view.findViewById(R.id.tv_job_id) as TextView
        val tv_job_name = view.findViewById(R.id.tv_job_name) as TextView
        val tv_customer = view.findViewById(R.id.tv_customer) as TextView
        val tv_jobpart = view.findViewById(R.id.tv_jobpart) as TextView
        val tv_prepress = view.findViewById(R.id.tv_prepress) as TextView
        val tv_press = view.findViewById(R.id.tv_press) as TextView
        val tv_bindery = view.findViewById(R.id.tv_bindery) as TextView
        val tv_mailing = view.findViewById(R.id.tv_mailing) as TextView
        val tv_shipping = view.findViewById(R.id.tv_shipping) as TextView
        val tv_delivery = view.findViewById(R.id.tv_delivery) as TextView
        val tv_status = view.findViewById(R.id.tv_status) as TextView
        val tv_customer_name = view.findViewById(R.id.tv_customer_name) as CustomTextViewBold

        val card_view = view.findViewById(R.id.card_view) as CardView
        val cv_joblistexpand = view.findViewById(R.id.cv_joblistexpand) as CardView
        val image_move_id = view.findViewById(R.id.cv_joblistexpand) as ImageView


        /*image_move_id.setOnClickListener {

            if(arrowdownup == false){
                cv_joblistexpand.visibility = View.VISIBLE
                arrowdownup = true
            }else{
                cv_joblistexpand.visibility = View.GONE
                arrowdownup = false
            }

        }*/

        card_view.setOnClickListener {
            if (arrowdownup == false) {
                cv_joblistexpand.visibility = View.VISIBLE
                arrowdownup = true
            } else {
                cv_joblistexpand.visibility = View.GONE
                arrowdownup = false

            }
        }


        val job_edit = view.findViewById(R.id.btn_edit) as ImageView
        val job_delete = view.findViewById(R.id.btn_delete) as ImageView

        tv_job_id.text = mServiceList.get(position).job_id
        tv_job_name.text = mServiceList.get(position).job_name
        tv_customer.text = mServiceList.get(position).custmer
        tv_customer_name.text = mServiceList.get(position).custmer
        tv_jobpart.text = mServiceList.get(position).job_part
        tv_status.text = mServiceList.get(position).created_date




        if (mServiceList.get(position).Prepress_assigned_status.equals("0")) {
            prepress_text = "N/A"
        } else if (mServiceList.get(position).Prepress_assigned_status.equals("1")) {
            prepress_text = "Not Started"
        } else if (mServiceList.get(position).Prepress_assigned_status.equals("2")) {
            prepress_text = "Processing"
        } else if (mServiceList.get(position).Prepress_assigned_status.equals("3")) {
            prepress_text = "Completed"
        }

        if (mServiceList.get(position).Press_assigned_status.equals("0")) {
            press_text = "N/A"
        } else if (mServiceList.get(position).Press_assigned_status.equals("1")) {
            press_text = "Not Started"
        } else if (mServiceList.get(position).Press_assigned_status.equals("2")) {
            press_text = "Processing"
        } else if (mServiceList.get(position).Press_assigned_status.equals("3")) {
            press_text = "Completed"
        }

        if (mServiceList.get(position).Bindery_assigned_status.equals("0")) {
            bindery_text = "N/A"
        } else if (mServiceList.get(position).Bindery_assigned_status.equals("1")) {
            bindery_text = "Not Started"
        } else if (mServiceList.get(position).Bindery_assigned_status.equals("2")) {
            bindery_text = "Processing"
        } else if (mServiceList.get(position).Bindery_assigned_status.equals("3")) {
            bindery_text = "Completed"
        }

        if (mServiceList.get(position).Mailing_assigned_status.equals("0")) {
            mailing_text = "N/A"
        } else if (mServiceList.get(position).Mailing_assigned_status.equals("1")) {
            mailing_text = "Not Started"
        } else if (mServiceList.get(position).Mailing_assigned_status.equals("2")) {
            mailing_text = "Processing"
        } else if (mServiceList.get(position).Mailing_assigned_status.equals("3")) {
            mailing_text = "Completed"
        }

        if (mServiceList.get(position).Shipping_assigned_status.equals("0")) {
            shipping_text = "N/A"
        } else if (mServiceList.get(position).Shipping_assigned_status.equals("1")) {
            shipping_text = "Not Started"
        } else if (mServiceList.get(position).Shipping_assigned_status.equals("2")) {
            shipping_text = "Processing"
        } else if (mServiceList.get(position).Shipping_assigned_status.equals("3")) {
            shipping_text = "Completed"
        }

        if (mServiceList.get(position).Delivery_assigned_status.equals("0")) {
            delivery_text = "N/A"
        } else if (mServiceList.get(position).Delivery_assigned_status.equals("1")) {
            delivery_text = "Not Started"
        } else if (mServiceList.get(position).Delivery_assigned_status.equals("2")) {
            delivery_text = "Processing"
        } else if (mServiceList.get(position).Delivery_assigned_status.equals("3")) {
            delivery_text = "Completed"
        }

        tv_prepress.text = prepress_text + " (" + mServiceList.get(position).Prepress_commentcount + ")"
        tv_press.text = press_text + " (" + mServiceList.get(position).Press_commentcount + ")"
        tv_bindery.text = bindery_text + " (" + mServiceList.get(position).Bindery_commentcount + ")"
        tv_mailing.text = mailing_text + " (" + mServiceList.get(position).Mailing_commentcount + ")"
        tv_shipping.text = shipping_text + " (" + mServiceList.get(position).Shipping_commentcount + ")"
        tv_delivery.text = delivery_text + " (" + mServiceList.get(position).Delivery_commentcount + ")"


        //  Log.e("mServiceList1",""+mServiceList1!!.size) Log.e("mServiceList1",""+mServiceList1!!.size)g.e("mServiceList1",""+position+"--"+mServiceList1!!.get(position).department_name)

        /* if (mServiceList.get(position).service_type.equals("City")) {
             view_line.setBackgroundColor(activity.resources.getColor(R.color.dashboard_city))
         } else if (mServiceList.get(position).service_type.equals("State")) {
             view_line.setBackgroundColor(activity.resources.getColor(R.color.dashboard_state))
         } else if (mServiceList.get(position).service_type.equals("County")) {
             view_line.setBackgroundColor(activity.resources.getColor(R.color.dashboard_county))
         } else if (mServiceList.get(position).service_type.equals("Federal")) {
             view_line.setBackgroundColor(activity.resources.getColor(R.color.dashboard_federal))
         }*/

        job_edit.setOnClickListener(View.OnClickListener {

            if (ConnectionManager.checkConnection(activity)) {
                val intent = Intent(this.activity, JobDetailsFullList::class.java)
                intent.putExtra("job_id", mServiceList.get(position).id)
                intent.putExtra("id", mServiceList.get(position).id)
                activity.startActivity(intent)
            } else {
                ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview, activity)
            }

        })
        job_delete.setOnClickListener(View.OnClickListener {
            if (ConnectionManager.checkConnection(activity)) {

            } else {
                ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview, activity)
            }

        })

        return view
    }

}
