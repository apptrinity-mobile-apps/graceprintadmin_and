package indo.com.graceprintingadmin.Adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import indo.com.graceprintingadmin.Helper.CustomTextView
import indo.com.graceprintingadmin.Activities.AssignedViewJob
import indo.com.graceprintingadmin.Activities.AssignjobEditActivity
import indo.com.graceprintingadmin.Model.APIResponse.Contact
import indo.com.graceprintingadmin.R
import java.util.*


class CustomAdapter(private val mContext: Context, list: ArrayList<Contact>,val job_id:String) : BaseAdapter() {
    private val mList: ArrayList<Contact>
    var context: Context? = null
    var jobid: String = ""

    init {
        mList = list
        context = mContext
        jobid = job_id
    }

    override fun getCount(): Int {
        return mList.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getViewTypeCount(): Int {
        return ITEM_VIEW_TYPE_COUNT
    }

    override fun getItemViewType(position: Int): Int {
        val isSection = mList[position].mIsSeparator

        return if (isSection) {
            ITEM_VIEW_TYPE_SEPARATOR
        } else {
            ITEM_VIEW_TYPE_REGULAR
        }
    }

    override fun isEnabled(position: Int): Boolean {
        return getItemViewType(position) != ITEM_VIEW_TYPE_SEPARATOR
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val view: View

        val contact = mList[position]
        val itemViewType = getItemViewType(position)

        if (convertView == null) {
            val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            if (itemViewType == ITEM_VIEW_TYPE_SEPARATOR) {
                // If its a section ?
                view = inflater.inflate(R.layout.listheader, null)
            } else {
                // Regular row
                view = inflater.inflate(R.layout.listitem_department, null)
            }
        } else {
            view = convertView
        }


        if (itemViewType == ITEM_VIEW_TYPE_SEPARATOR) {
            // If separator

            val separatorView = view.findViewById(R.id.separator) as TextView
            separatorView.setText(contact.mUserName)
        } else {
            // If regular

            // Set contact name and number
            val listitemlayout = view.findViewById(R.id.ll_department) as LinearLayout
            val tv_deptname = view.findViewById(R.id.tv_deptname) as TextView
            val tv_username = view.findViewById(R.id.tv_username) as TextView
            val tv_deptcount = view.findViewById(R.id.tv_deptcount) as CustomTextView
            val tv_status = view.findViewById(R.id.tv_status) as TextView
            val dept_edit = view.findViewById(R.id.dept_edit) as ImageView
            val dept_view = view.findViewById(R.id.dept_view) as ImageView


            tv_deptcount.setText(contact.mCommentCount)
            if(contact.mCommentCountView.equals("1")){
                tv_deptcount.setBackgroundResource(R.drawable.circle_ic_notviewed)
            }

            tv_deptname.setText(contact.mDeptName)
            tv_username.setText(contact.mUserName)
            tv_status.setText(contact.mstatus)

            if (contact.assign_status == "0") {
                tv_status?.text = "Not Yet Assigned"
                dept_edit.visibility = View.VISIBLE
                dept_view.visibility = View.INVISIBLE
            }
            if (contact.assign_status == "1") {
                tv_status?.text = "Not Started"
                dept_edit.visibility = View.VISIBLE
                dept_view.visibility = View.VISIBLE
            }
            if (contact.assign_status == "2") {
                tv_status?.text = "Processing"
                dept_edit.visibility = View.INVISIBLE
                dept_view.visibility = View.VISIBLE
            }
            if (contact.assign_status == "3") {
                tv_status?.text = "Completed"
                dept_edit.visibility = View.INVISIBLE
                dept_view.visibility = View.VISIBLE
            }

            /* dept_edit.setOnClickListener { View.OnClickListener {

            } }
            dept_view.setOnClickListener { View.OnClickListener {


            } }*/

            listitemlayout.setOnClickListener {

                Log.e("listitemlayout", contact.mdeptJobid)

            }
            dept_edit.setOnClickListener {

                Log.e("dept_edit", contact.job_name+"--"+jobid)
                val intent = Intent(context, AssignjobEditActivity::class.java)
                intent.putExtra("deptname", contact.mDeptName)
                intent.putExtra("deptjobid", contact.mdeptJobid)
                intent.putExtra("assignedid", contact.mDepartmentId)
                intent.putExtra("jobname", contact.job_name)
                intent.putExtra("passingjobid", jobid)
                intent.putExtra("usertextdata", tv_username.text.toString())
                context!!.startActivity(intent)

            }
            dept_view.setOnClickListener {

                Log.e("dept_view", contact.mdeptJobid)
                Log.e("dept_edit", contact.mdeptJobid)
                val intent = Intent(context, AssignedViewJob::class.java)
                intent.putExtra("deptname", contact.mDeptName)
                intent.putExtra("deptjobid", contact.mdeptJobid)
                intent.putExtra("assignedid", contact.mDepartmentId)
                intent.putExtra("assignedid", contact.mDepartmentId)
                intent.putExtra("passingjobid", jobid)
                context!!.startActivity(intent)

            }

        }

        return view
    }

    companion object {

        // View Type for Separators
        private val ITEM_VIEW_TYPE_SEPARATOR = 0
        // View Type for Regular rows
        private val ITEM_VIEW_TYPE_REGULAR = 1
        // Types of Views that need to be handled
        // -- Separators and Regular rows --
        private val ITEM_VIEW_TYPE_COUNT = 2
    }

}