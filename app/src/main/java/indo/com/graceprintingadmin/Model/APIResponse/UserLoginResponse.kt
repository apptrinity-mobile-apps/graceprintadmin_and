package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UserLoginResponse {
     val user_info: User_info? = null

     val result: String? = null

     val status: String? = null

}
