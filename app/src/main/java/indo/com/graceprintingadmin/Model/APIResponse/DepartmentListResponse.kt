package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class DepartmentListResponse {
     val result: String? = null

     val status: String? = null

     val usersList: Array<UsersList>? = null

}
