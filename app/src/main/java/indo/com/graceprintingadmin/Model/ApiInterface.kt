package com.indobytes.a4printuser.model


import indo.com.graceprintingadmin.Model.APIResponse.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import java.util.concurrent.TimeUnit


/**
 * Created by indo67 on 4/13/2018.
 */
public interface ApiInterface {

    @FormUrlEncoded
    @POST("adminapi/login")
    abstract fun login(@Field("user_email") username: String,
                       @Field("password") password: String, @Field("device_type") device_type: String, @Field("device_token") device_token: String): Call<UserLoginResponse>

    @GET("adminapi/getJobsList")
    abstract fun getJobsList(): Call<GetJobListResponse>

    @FormUrlEncoded
    @POST("adminapi/getJobsList")
    abstract fun getJobsList2(@Field("job_type_id") job_id: String,@Field("dep_id") dep_id: String): Call<GetJobListResponse>

    @FormUrlEncoded
    @POST("adminapi/jobDetailView")
    abstract fun jobDetailView(@Field("job_id") job_id: String): Call<JobDetailViewResponse>


    @GET("adminapi/jobTypesList")
    abstract fun getjobTypeList(): Call<GetJobTypeListResponse>

    @FormUrlEncoded
    @POST("adminapi/addJob")
    fun postAddjobData(
            @Field("option_id") option_id : String,
            @Field("job_id") job_id: String,
                       @Field("job_type_id") job_type_id: String,
                       @Field("job_part") job_part: String,
            //@Field("planeed_activity") planeed_activity: String,
                       @Field("earliest_start_date") earliest_start_date: String,
                       @Field("custmer") custmer: String,
                       @Field("description") description: String,
                       @Field("promise_date") promise_date: String,
                       @Field("admin_id") admin_id: String,
                       @Field("department_ids") department_ids: String,
                       @Field("task_ids") task_ids: String,
                       @Field("type") type: String,
                       @Field("no_of_terms") no_of_terms: String,
                       @Field("terms") terms: String,
                       @Field("mailing_qty") mailing_qty: String,

                       @Field("paper_id") paper_id: String,
                       @Field("text_id") text_id: String,
                       @Field("weight_id") weight_id: String,
                       @Field("sub_text_id") sub_text_id: String,
                       @Field("size_id") size_id: String,
                       @Field("alloted_sheets") alloted_sheets: String,
                       @Field("job_title") job_title: String,
                       @Field("job_description") job_description: String,
                       @Field("number_of_shipping_terms") number_of_shipping_terms: String,
                       @Field("number_of_delivery_terms") number_of_delivery_terms: String,
                       @Field("shippingterms") shippingterms: String,
                       @Field("deliveryterms") deliveryterms: String,
                       @Field("other_paper_type") other_paper_type: String
            /*@Field("earliest_start_time") earliest_start_time: String,
            @Field("scheduled") scheduled: String,
            @Field("crit_start_date") crit_start_date: String,
            @Field("due_time") due_time: String,
            @Field("last_act_code") last_act_code: String*/): Call<UserAddJobResponce>


    @FormUrlEncoded
    @POST("adminapi/editJob")
    fun editJob(
            @Field("option_id") option_id : String,
            @Field("id") id: String,
                @Field("job_id") job_id: String,
                @Field("job_title") job_title: String,
                @Field("job_description") job_description: String,
                @Field("job_type_id") job_type_id: String,
                @Field("job_part") job_part: String,
            //  @Field("planeed_activity") planeed_activity: String,
                @Field("earliest_start_date") earliest_start_date: String,
                @Field("custmer") custmer: String,
                @Field("description") description: String,
                @Field("promise_date") promise_date: String,
                @Field("admin_id") admin_id: String,
                @Field("department_ids") department_ids: String,
                @Field("task_ids") task_ids: String,


                @Field("type") type: String,
                @Field("no_of_terms") no_of_terms: String,
                @Field("number_of_shipping_terms") number_of_shipping_terms: String,
                @Field("number_of_delivery_terms") number_of_delivery_terms: String,
                @Field("terms") terms: String,
                @Field("shippingterms") shippingterms: String,
                @Field("deliveryterms") deliveryterms: String,
                @Field("mailing_qty") mailing_qty: String,

                @Field("paper_id") paper_id: String,
                @Field("text_id") text_id: String,
                @Field("weight_id") weight_id: String,
                @Field("sub_text_id") sub_text_id: String,
                @Field("size_id") size_id: String,
                @Field("alloted_sheets") alloted_sheets: String): Call<UserLoginResponse>

    @FormUrlEncoded
    @POST("adminapi/jobEditDetailView")
    fun jobEditDetailView(@Field("job_id") job_main_id: String): Call<EditjobDetailsResponce>

    @FormUrlEncoded
    @POST("adminapi/editAssignJob")
    fun assigneditJob(@Field("job_dep_id") department_id: String,
                      @Field("user_id") user_id: String,
                      @Field("allotted_sheets") allotted_sheets: String): Call<UserLoginResponse>

    @FormUrlEncoded
    @POST("adminapi/assignJob")
    fun assignJobId(
            @Field("job_dep_id") department_id: String,
            @Field("user_id") user_id: String,
            @Field("allotted_sheets") allotted_sheets: String): Call<UserLoginResponse>

    @FormUrlEncoded
    @POST("adminapi/usersList")
    fun deptUsersList(@Field("department_id") department_id: String): Call<DeptUserListResponse>

    @FormUrlEncoded
    @POST("adminapi/editAssignJobDetailview")
    fun depteditUsersList(@Field("job_dep_id") job_dep_id: String): Call<DepteditUserListResponse>

    @FormUrlEncoded
    @POST("adminapi/departmentTaskList")
    fun departmentTaskList(@Field("job_dep_id") job_dep_id: String): Call<DeptTaskListResponse>

    @GET("adminapi/departmentWithTasksList")
    fun getDepartmentList(): Call<GetDepartmentListResponse>

    @FormUrlEncoded
    @POST("adminapi/postTaskComment")
    fun postTaskComment(@Field("assigned_task_id") assigned_task_id: String, @Field("comment") comment: String): Call<UserLoginResponse>

    @GET("adminapi/getDashboard")
    fun getDashboard(): Call<DashBoardResponse>


    @GET("adminapi/jobsheetsdropdown")
    fun jobsheetsdropdown(): Call<PaperTypeResponse>


    @GET("adminapi/optionsList")
    fun optionListdropdown(): Call<OptionListResponse>

    @FormUrlEncoded
    @POST("adminapi/texttypes")
    fun texttypes(@Field("paper_id") paper_id: String): Call<TextTypeResponse>

    @FormUrlEncoded
    @POST("adminapi/weightandsize")
    fun weightandsize(@Field("sub_text_id") sub_text_id: String): Call<WeightSizeResponse>



    @FormUrlEncoded
    @POST("adminapi/subtexttypes")
    fun subtexttypes(@Field("text_id") text_id: String): Call<SubTextTypeResponse>

    @GET("adminapi/getCustomers")
    fun getCustomers(): Call<GetCustomersListResponse>

    @FormUrlEncoded
    @POST("adminapi/getAvailableSheets")
    fun getAvailableSheets(@Field("size_id") size_id: String,@Field("weight_id") weight_id: String,@Field("sub_text_id") sub_text_id: String,@Field("paper_id") paper_id: String,@Field("text_id") text_id: String): Call<GetAvailableSheetsResponse>

    @FormUrlEncoded
    @POST("adminapi/checkJobId")
    fun checkJobId(@Field("job_id") job_id: String): Call<CheckJobIdResponse>

    @FormUrlEncoded
    @POST("adminapi/changeJobOrder")
    fun changeJobOrderAPI(@Field("job_order") job_id: String,@Field("type") type: String,@Field("dep_id") dep_stg_id: String): Call<CheckJobIdResponse>


    @FormUrlEncoded
    @POST("adminapi/jobdelete")
    fun jobdelete(@Field("job_id") job_id: String): Call<CheckJobIdResponse>


    @FormUrlEncoded
    @POST("adminapi/changedepStatus")
    fun changedepStatus(@Field("job_dep_id") job_id: String): Call<CheckJobIdResponse>

    companion object Factory {
       // val BASE_URL = "http://13.126.232.110/graceprinting/"
        val BASE_URL = "http://18.217.209.201/graceprintinternal/"
        const val IMAGE_BASE_URL = "https://www.4print.com/uploads/products/"
        const val IMAGE_BASE_ORDER_URL = "https://www.4print.com/uploads/orders/"


        val client = OkHttpClient.Builder()
                .connectTimeout(10,TimeUnit.SECONDS)
                .readTimeout(30,TimeUnit.SECONDS)

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}