package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Tasklist {
     val id: String? = null

     val assigned_status: String? = null

     val department_name: String? = null

     val taskslist: Array<Taskslist>? = null

     val assigned_id: String? = null

     var allotted_sheets: String? = null

     var used_sheets: String? = null

}
