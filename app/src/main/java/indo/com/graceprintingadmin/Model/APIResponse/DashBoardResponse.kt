package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class DashBoardResponse {
     val result: String? = null

     val status: String? = null

     val jobList: JobList? = null
}
