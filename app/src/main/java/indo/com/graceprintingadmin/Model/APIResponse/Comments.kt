package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Comments {
     val assigned_task_id: String? = null

     val comment_id: String? = null

     val comment_on: String? = null

     val user_type: String? = null

     val user_id: String? = null

     val comment: String? = null

}
