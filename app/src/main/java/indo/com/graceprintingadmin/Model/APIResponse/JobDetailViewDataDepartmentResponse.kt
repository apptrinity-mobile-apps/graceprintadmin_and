package indo.com.graceprintingadmin.Model.APIResponse

class JobDetailViewDataDepartmentResponse {

    val id: String? = null
    val job_id: String? = null
    val job_part: String? = null
    val department: Array<JobsListDepartmentsDataResponse>? = null
}
