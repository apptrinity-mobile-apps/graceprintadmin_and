package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class DeptUserListResponse {
     val result: String? = null

     val status: String? = null

     val usersList: Array<UsersList>? = null

}
