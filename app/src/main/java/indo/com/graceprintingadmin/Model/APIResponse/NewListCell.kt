package indo.com.graceprintingadmin.Model.APIResponse

class NewListCell(val name: String, val category: String,val task_id: String, val cat_id: String, val ass_status: String) : Comparable<NewListCell> {
        var isSectionHeader: Boolean = false
            private set

        init {
            isSectionHeader = false

        }

        fun setToSectionHeader() {
            isSectionHeader = true
        }

        override fun compareTo(other: NewListCell): Int {
            return this.category.compareTo(other.category)
        }
}