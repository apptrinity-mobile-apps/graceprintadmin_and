package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AssignedData {
     val users: Array<Users>? = null

     val allotted_sheets: String? = null

     val status: String? = null

     val created_on: String? = null

     val department_id: String? = null

     val job_id: String? = null

     val job_dep_id: String? = null
}
