package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class DeptTaskListResponse {
     val result: String? = null

     val status: String? = null

     val tasklist: Array<Tasklist>? = null
}
