package indo.com.graceprintingadmin.Model.APIResponse

class Contact(var mUserName: String, var mDeptName: String, var mdeptJobid: String,var mstatus: String,var mDepartmentId: String,var job_name: String,var job_id:String,var assign_id:String,var assign_status:String,  var mIsSeparator: Boolean,var mCommentCount: String,var mCommentCountView:String,var mStartdate: String,var mEnddate: String) {

    fun setUsername(name: String) {
        mUserName = name
    }

    fun setStartdate(startdate: String) {
        mStartdate = startdate
    }
    fun setEnddate(enddate: String) {
        mEnddate = enddate
    }


    fun setDeptName(dept: String) {
        mDeptName = dept
    }

    fun setCommentCount(commentcount: String) {
        mCommentCount = commentcount
    }
    fun setCommentCountView(commentcountview: String) {
        mCommentCountView = commentcountview
    }

    fun setdeptJobid(jobdepid: String) {
        mdeptJobid = jobdepid
    }
    fun setstatus(status: String) {
        mdeptJobid = status
    }
    fun setDeptid(deptid: String) {
        mDepartmentId = deptid
    }

    fun setJobname(jobname: String) {
        job_name = jobname
    }
    fun setJobid(jobid: String) {
        job_id = jobid
    }
    fun setAssignid(assignid: String) {
        assign_id = assignid
    }
    fun setAssignStatus(assignstatus: String) {
        assign_status = assignstatus
    }

    fun setIsSection(isSection: Boolean) {
        mIsSeparator = isSection
    }


}