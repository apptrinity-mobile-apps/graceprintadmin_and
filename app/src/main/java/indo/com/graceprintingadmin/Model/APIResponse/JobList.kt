package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class JobList {
     val jobstatuses: Array<Jobstatuses>? = null
     val departments: Array<Departments>? = null
}
