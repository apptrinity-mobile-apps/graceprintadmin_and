package indo.com.graceprintingadmin.Model.APIResponse

/**
 * Created by indo67 on 6/14/2018.
 */
class EditJobDetailsDataTasksResponse {
    val job_dep_task_id: String? = null
    val job_dep_id: String? = null
    val task_id: String? = null
    val created_on: String? = null
    val task_name: String? = null
}