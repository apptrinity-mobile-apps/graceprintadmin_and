package indo.com.graceprintingadmin.Model.APIResponse

class GetJobListResponse {
    val jobList: Array<JobsListDataResponse>? = null

    val result: String? = null

    val status: String? = null
}
