package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class OptiondataListResponse {

     val option_id: String? = null
     val type: String? = null
     val make_ready_time: String? = null
     val run_time: String? = null
     val created_date: String? = null

}


