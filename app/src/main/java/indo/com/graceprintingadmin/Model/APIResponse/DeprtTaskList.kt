package indo.com.graceprintingadmin.Model.APIResponse

/**
 * Created by indo67 on 6/11/2018.
 */
class DeprtTaskList{

    val id: String? = null
    val task_name: String? = null
    val department_id: String? = null
    val status: String? = null
    val created_date: String? = null
}