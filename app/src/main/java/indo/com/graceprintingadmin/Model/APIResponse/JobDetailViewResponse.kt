package indo.com.graceprintingadmin.Model.APIResponse

class JobDetailViewResponse {

    val status: String? = null
    val result: String? = null
    val data:Array<JobDetailViewDataResponse>? = null

}
