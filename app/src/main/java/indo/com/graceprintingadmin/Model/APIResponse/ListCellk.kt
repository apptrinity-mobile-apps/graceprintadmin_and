package indo.com.graceprintingadmin.Model.APIResponse

class ListCellk(val name: String, val category: String,val task_id: String, val cat_id: String) : Comparable<ListCellk> {
    var isSectionHeader: Boolean = false
        private set

    init {
        isSectionHeader = false

    }

    fun setToSectionHeader() {
        isSectionHeader = true
    }

    override fun compareTo(other: ListCellk): Int {
        return this.category.compareTo(other.category)
    }
}
