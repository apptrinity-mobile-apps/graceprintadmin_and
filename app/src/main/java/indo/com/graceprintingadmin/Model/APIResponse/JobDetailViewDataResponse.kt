package indo.com.graceprintingadmin.Model.APIResponse

class JobDetailViewDataResponse {

    val id: String? = null
    val job_id: String? = null
    val job_type_id: String? = null
    val job_part: String? = null
    val make_time: String? = null
    val run_time: String? = null
    val total_time: String? = null
    val planeed_activity: String? = null
    val earliest_start_date: String? = null
    val custmer: String? = null
    val description: String? = null
    val promise_date: String? = null
    val earliest_start_time: String? = null
    val status: String? = null
    val scheduled: String? = null
    val crit_start_date: String? = null
    val due_time: String? = null
    val last_act_code: String? = null
    val created_date: String? = null
    val job_name:String?=null



    val alloted_sheets:String?=null
    val mailing_qty:String?=null
    val no_of_terms:String?=null
    val paper_id:String?=null
    val text_id:String?=null
    val sub_text_id:String?=null
    val weight_id:String?=null
    val size_id:String?=null
    val paper_name:String?=null
    val text_name:String?=null
    val sub_text_name:String?=null
    val weight:String?=null
    val width:String?=null
    val height:String?=null
    val job_title:String?=null
    val job_description:String?=null

    val terms: Array<String>? = null
    val term_date: Array<String>? = null

    val shipping_terms: Array<String>? = null
    val shipping_term_date: Array<String>? = null

    val delivery_terms : Array < String >? = null
    val delivery_term_date: Array<String>? = null


    val department: Array<JobsListDepartmentsDataResponse>? = null

}
