package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class SheetsList {
     val sizes: Array<Sizes>? = null

     val weights: Array<Weights>? = null

     val papertypes: Array<Papertypes>? = null

}
