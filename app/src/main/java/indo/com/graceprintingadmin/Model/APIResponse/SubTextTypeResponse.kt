package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class SubTextTypeResponse {
     val result: String? = null

     val subtexttypes: Array<Subtexttypes>? = null

     val status: String? = null
}
