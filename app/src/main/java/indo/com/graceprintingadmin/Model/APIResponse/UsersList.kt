package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UsersList {

     val user_id: String? = null

     val user_name: String? = null

     //val status: String? = null
}
