package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Departments {
     val id: String? = null

     val upcoming: String? = null

     val active: String? = null

     val department_name: String? = null

     val completed: String? = null
}
