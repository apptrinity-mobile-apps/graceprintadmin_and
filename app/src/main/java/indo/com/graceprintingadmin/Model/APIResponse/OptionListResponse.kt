package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class OptionListResponse {
     val result: String? = null
     val status: String? = null
     val data: Array<OptiondataListResponse>? = null
}
