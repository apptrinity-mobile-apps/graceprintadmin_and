package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class TextTypeResponse {
     val result: String? = null

     val status: String? = null

     val texttypes: Array<Texttypes>? = null
}
