package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Taskslist {
     val assigned_task_id: String? = null

     val task_end_date: String? = null

     val task_name: String? = null

     val task_start_date: String? = null

     val job_task_id: String? = null

     val assigned_task_status: String? = null

     val assigned_id: String? = null

     val comments: Array<Comments>? = null

     var dropsarr:Array<Dropsarraylist>?=null
     var shippingdropsarr:Array<Dropsarraylist>?=null
     var deliverydropsarr:Array<Dropsarraylist>?=null

}
