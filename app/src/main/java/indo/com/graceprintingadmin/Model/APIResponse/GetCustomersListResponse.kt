package indo.com.graceprintingadmin.Model.APIResponse

class GetCustomersListResponse {

    val customers: Array<CustomersListName>? = null

    val result: String? = null

    val status: String? = null

}
