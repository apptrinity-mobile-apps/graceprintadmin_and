package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class PaperTypeResponse {
     val result: String? = null

     val status: String? = null

     val sheetsList: SheetsList? = null
}
