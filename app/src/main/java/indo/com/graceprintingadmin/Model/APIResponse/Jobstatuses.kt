package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Jobstatuses {
     val total: String? = null

     val id: String? = null

     val upcoming: String? = null

     val job_name: String? = null

     val active: String? = null

     val completed: String? = null

}
