package indo.com.graceprintingadmin.Model.APIResponse

/**
 * Created by indo67 on 5/31/2018.
 */
class GetJobTypeListResponse {
    val result: String? = null

    val status: String? = null

    val jobtypesList: Array<JobTypeResponse>? = null
}