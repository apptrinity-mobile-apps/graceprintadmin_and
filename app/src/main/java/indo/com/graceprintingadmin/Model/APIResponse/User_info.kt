package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties


@JsonIgnoreProperties(ignoreUnknown = true)
class User_info {

    val admin_id: String? = null
    val admin_name: String? = null
    val admin_username: String? = null
    val admin_email: String? = null
    val admin_password: String? = null
    val admin_image: String? = null
    val admin_permissions: String? = null
    val admin_status: String? = null
    val admin_created: String? = null






}
