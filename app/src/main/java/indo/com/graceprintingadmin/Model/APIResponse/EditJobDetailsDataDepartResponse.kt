package indo.com.graceprintingadmin.Model.APIResponse

/**
 * Created by indo67 on 6/14/2018.
 */
class EditJobDetailsDataDepartResponse {
    val job_dep_id: String? = null
    val department_id: String? = null
    val job_id: String? = null
    val created_on: String? = null
    val status: String? = null
    val department_name: String? = null
    val assigned_status: String? = null
}