package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class DepteditUserListResponse {
     val assigneddata: AssignedData? = null

     val result: String? = null

     val status: String? = null

}
