package indo.com.graceprintingadmin.Model.APIResponse

class JobsListDataResponse {

    val id: String? = null
    val job_id: String? = null
    val job_type_id: String? = null
    val department_id: String? = null
    val user_id: String? = null
    val job_part: String? = null
    val planeed_activity: String? = null
    val earliest_start_date: String? = null
    val custmer: String? = null
    val description: String? = null
    val promise_date: String? = null
    val earliest_start_time: String? = null
    val status: String? = null
    val scheduled: String? = null
    val crit_start_date: String? = null
    val due_time: String? = null
    val last_act_code: String? = null
    val created_date: String? = null
    val job_name: String? = null
    val Prepress_department: String? = null
    val Prepress_assigned_status: String? = null
    val Prepress_commentcount: String? = null
    val Prepress_comment_view_status: String? = null
    val Prepress_task: String? = null
    val Press_department: String? = null
    val Press_assigned_status: String? = null
    val Press_commentcount: String? = null
    val Press_comment_view_status: String? = null
    val Press_task: String? = null
    val Bindery_department: String? = null
    val Bindery_assigned_status	: String? = null
    val Bindery_commentcount: String? = null
    val Bindery_comment_view_status: String? = null
    val Bindery_task: String? = null
    val Mailing_department: String? = null
    val Mailing_assigned_status	: String? = null
    val Mailing_commentcount: String? = null
    val Mailing_comment_view_status: String? = null
    val Mailing_task: String? = null
    val Shipping_department: String? = null
    val Shipping_assigned_status: String? = null
    val Shipping_commentcount: String? = null
    val Shipping_comment_view_status: String? = null
    val Shipping_task: String? = null
    val Delivery_department: String? = null
    val Delivery_assigned_status: String? = null
    val Delivery_commentcount: String? = null
    val Delivery_comment_view_status: String? = null
    val Delivery_task: String? = null
    val jobdonestatus: String? = null
    val active_status: String? = null
    val dep_status: String? = null
    val job_dep_id: String? = null


}
