package indo.com.graceprintingadmin.Model.APIResponse

class GetAvailableSheetsResponse {

    val no_of_sheets: String? = null

    val result: String? = null

    val status: String? = null

}
