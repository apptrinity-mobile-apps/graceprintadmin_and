package indo.com.graceprintingadmin.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)

class Sizes {
     val id: String? = null

     val height: String? = null

     val width: String? = null

}
