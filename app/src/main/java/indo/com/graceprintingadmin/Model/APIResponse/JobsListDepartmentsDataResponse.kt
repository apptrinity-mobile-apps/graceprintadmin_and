package indo.com.graceprintingadmin.Model.APIResponse

class JobsListDepartmentsDataResponse {


    val job_dep_id: String? = null
    val department_id: String? = null
    val job_id: String? = null
    val created_on: String? = null
    val status: String? = null
    val department_name: String? = null
    val user_name: String? = null
    val user_id: String? = null
    val assigned_id: String? = null
    val assigned_status: String? = null
    val commentcount: String? = null
    val comment_view_status: String? = null
    val start_date: String? = null
    val end_date: String? = null


}