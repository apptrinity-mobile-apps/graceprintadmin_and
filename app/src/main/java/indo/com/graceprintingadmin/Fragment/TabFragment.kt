package indo.com.graceprintingadmin.Fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.model.ApiInterface
import com.squareup.picasso.Picasso
import indo.com.graceprintingadmin.Adapter.CommentsAdapter
import indo.com.graceprintingadmin.Helper.Helper
import indo.com.graceprintingadmin.Model.APIResponse.*
import indo.com.graceprintingadmin.R
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat


class TabFragment : Fragment() {
    internal var st_jobid: String = ""
    internal var st_assigned: String = ""
    lateinit var title_tab: TextView
    lateinit var post_comment: EditText
    lateinit var submit_button: CustomTextViewBold
    internal var assigned_task_id: String = ""
    internal var assigned_id: String = ""
    lateinit var commentslist: ListView
    lateinit var allotedsheet: TextView
    lateinit var usedsheet: TextView
    lateinit var remainingsheet: TextView

    lateinit var ll_allotedsheet: LinearLayout
    lateinit var ll_usedsheet: LinearLayout
    lateinit var ll_remainingsheet: LinearLayout

    public var mCommentsList: ArrayList<Comments> = ArrayList<Comments>()
    private var mCommentsAdapter: CommentsAdapter? = null
    public var mTasklist: ArrayList<Tasklist> = ArrayList<Tasklist>()

    internal var ll_delivery_view_id: LinearLayout? = null
    internal var ll_drops_main_id: LinearLayout? = null
    internal var ll_drops_id: LinearLayout? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_tab_page, container, false)
        val page = getArguments()!!.getInt(PAGE_NUM)
        val mainpage = getArguments()!!.getInt(PAGE_MAIN)

        post_comment = view.findViewById(R.id.post_comment) as EditText
        submit_button = view.findViewById(R.id.submit_button) as CustomTextViewBold
        commentslist = view.findViewById(R.id.commentslist) as ListView

        allotedsheet = view.findViewById(R.id.allotedsheet) as TextView
        usedsheet = view.findViewById(R.id.usedsheet) as TextView
        remainingsheet = view.findViewById(R.id.remainingsheet) as TextView

        ll_allotedsheet = view.findViewById(R.id.ll_allotedsheet) as LinearLayout
        ll_usedsheet = view.findViewById(R.id.ll_usedsheet) as LinearLayout
        ll_remainingsheet = view.findViewById(R.id.ll_remainingsheet) as LinearLayout

        ll_delivery_view_id = view.findViewById(R.id.ll_delivery_view_id) as LinearLayout
        ll_drops_main_id = view.findViewById(R.id.ll_drops_main_id) as LinearLayout
        ll_drops_id = view.findViewById(R.id.ll_drops_id) as LinearLayout



        submit_button.setOnClickListener {

            if (post_comment.text.toString().equals("")) {
                Toast.makeText(activity, "Please Enter Comment", Toast.LENGTH_SHORT).show()
            } else {
                postCommentAPI(inflater, container, savedInstanceState, mainpage, page)
            }


        }

        if (mTasklist2!!.size > 0 && mTasklist2.size > mainpage) {
            if (mTasklist2!!.get(mainpage).taskslist!!.size > 0 && mTasklist2!!.get(mainpage).taskslist!!.size > page) {

                val list: Array<Comments>? = mTasklist2!!.get(mainpage).taskslist!!.get(page).comments!!
                mCommentsList.clear()
                for (item: Comments in list!!.iterator()) {

                    mCommentsList.add(item)
                }
                if (mTasklist2!!.get(mainpage).taskslist!!.size > 0 && mTasklist2!!.get(mainpage).taskslist!!.get(page).assigned_task_id != null)
                    assigned_task_id = mTasklist2!!.get(mainpage).taskslist!!.get(page).assigned_task_id!!
                if (mTasklist2!!.get(mainpage).taskslist!!.size > 0 && mTasklist2!!.get(mainpage).taskslist!!.get(page).assigned_id != null)
                    assigned_id = mTasklist2!!.get(mainpage).taskslist!!.get(page).assigned_id!!
                if (mTasklist2!!.get(mainpage).department_name != null && mTasklist2!!.get(mainpage).department_name!!.equals("Press") && mTasklist2!!.get(mainpage).allotted_sheets != null
                        && mTasklist2!!.get(mainpage).used_sheets != null) {
                    allotedsheet.setText(mTasklist2!!.get(mainpage).allotted_sheets)
                    usedsheet.setText(mTasklist2!!.get(mainpage).used_sheets)
                    var allot: Int = mTasklist2!!.get(mainpage).allotted_sheets!!.toInt()
                    var used: Int = mTasklist2!!.get(mainpage).used_sheets!!.toInt()
                    var remain = allot - used
                    remainingsheet.setText(remain.toString())
                    ll_allotedsheet.visibility = View.VISIBLE
                    ll_usedsheet.visibility = View.VISIBLE
                    ll_remainingsheet.visibility = View.VISIBLE
                } else {
                    ll_allotedsheet.visibility = View.GONE
                    ll_usedsheet.visibility = View.GONE
                    ll_remainingsheet.visibility = View.GONE
                }



                if (mTasklist2!!.get(mainpage).department_name != null && mTasklist2!!.get(mainpage).department_name!!.equals("Delivery")) {

                    ll_drops_id?.removeAllViews()

                    ll_delivery_view_id!!.visibility = View.VISIBLE
                    val droparralist: Array<Dropsarraylist>? = mTasklist2!!.get(mainpage).taskslist!!.get(page).deliverydropsarr!!
                    if (droparralist!!.size > 0) {

                        for (m in 0 until droparralist!!.size) {
                            val textdrops_sno = TextView(context)
                            val textdrops = TextView(context)
                            val textdropsdate = TextView(context)
                            val textDropsstatus = TextView(context)
                            val delivery_status_stg: String = mTasklist2!!.get(mainpage).taskslist!!.get(page).deliverydropsarr!!.get(m).delivery_status!!.toString()

                            val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                            //paramsadm!!.gravity = Gravity.RIGHT
                            paramsadm.setMargins(5, 5, 5, 5)
                            textDropsstatus.setPadding(10, 5, 10, 5)
                            textdrops_sno!!.setLayoutParams(paramsadm)
                            textdrops!!.setLayoutParams(paramsadm)
                            textDropsstatus!!.setLayoutParams(paramsadm)
                            textdropsdate!!.setLayoutParams(paramsadm)

                            textdrops!!.setText(droparralist.get(m).term.toString())
                            textdrops!!.setTextColor(resources.getColor(R.color.cyan_color))
                            textdrops!!.setPadding(2, 5, 2, 5)


                            val inputFormat = SimpleDateFormat("yyyy-MM-dd")
                            val outputFormat = SimpleDateFormat("MM-dd-yyyy")
                            val inputDateStr = droparralist.get(m).date.toString()
                            val date = inputFormat.parse(inputDateStr)
                            val outputDateStr = outputFormat.format(date)
                            Log.e("DROPDATE", droparralist.get(m).date.toString() + "------" + outputDateStr)

                            textdropsdate!!.setText(outputDateStr)
                            textdropsdate!!.setTextColor(resources.getColor(R.color.cyan_color))
                            textdropsdate!!.setPadding(2, 5, 2, 5)

                            textdrops_sno!!.setText((m + 1).toString())
                            textdrops_sno.setTextColor(resources.getColor(R.color.cyan_color))
                            textdrops_sno!!.setPadding(2, 5, 2, 5)


                            ///Delivery
                            val ll_main = LinearLayout(context)
                            /* val ll_name = LinearLayout(context)
                             val ll_view = LinearLayout(context)
                             val ll_data = LinearLayout(context)
                             ll_name.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                             ll_name.setOrientation(LinearLayout.VERTICAL)

                             ll_view.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                             ll_view.setOrientation(LinearLayout.VERTICAL)

                             ll_data.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                             ll_data.setOrientation(LinearLayout.VERTICAL)

                             ll_main.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                             ll_main.setOrientation(LinearLayout.HORIZONTAL)*/

                            val ll_drop = LinearLayout(context)
                            val ll_drop_date = LinearLayout(context)
                            val ll_delivery = LinearLayout(context)
                            val ll_location = LinearLayout(context)
                            val ll_sign = LinearLayout(context)

                            ll_drop.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_drop.setOrientation(LinearLayout.HORIZONTAL)

                            ll_drop_date.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_drop_date.setOrientation(LinearLayout.HORIZONTAL)

                            ll_delivery.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_delivery.setOrientation(LinearLayout.HORIZONTAL)

                            ll_location.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_location.setOrientation(LinearLayout.HORIZONTAL)

                            ll_sign.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_sign.setOrientation(LinearLayout.HORIZONTAL)

                            ll_main.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_main.setOrientation(LinearLayout.VERTICAL)

                            val params_title = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.1f)
                            val params_text = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.6f)
                            // val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)


                            val text_drop_title = TextView(context)
                            text_drop_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_drop_title!!.setPadding(2, 5, 2, 5)
                            text_drop_title.setText("Drop")
                            text_drop_title!!.setLayoutParams(params_title)

                            val text_dates_title = TextView(context)
                            text_dates_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_dates_title!!.setPadding(2, 5, 2, 5)
                            text_dates_title.setText("Drop Date")
                            text_dates_title!!.setLayoutParams(params_title)

                            val text_deliv_date_title = TextView(context)
                            text_deliv_date_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_deliv_date_title!!.setPadding(2, 5, 2, 5)
                            text_deliv_date_title.setText("Delivery date")
                            text_deliv_date_title!!.setLayoutParams(params_title)

                            val text_location_title = TextView(context)
                            text_location_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_location_title!!.setPadding(2, 5, 2, 5)
                            text_location_title.setText("Location")
                            text_location_title!!.setLayoutParams(params_title)

                            val text_signature_title = TextView(context)
                            text_signature_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_signature_title!!.setPadding(2, 5, 2, 5)
                            text_signature_title.setText("Signature")
                            text_signature_title!!.setLayoutParams(params_title)


                            val text_view = TextView(context)
                            text_view.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view!!.setPadding(2, 5, 2, 5)
                            text_view.setText(": ")
                            text_view!!.setLayoutParams(paramsadm)

                            val text_view1 = TextView(context)
                            text_view1.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view1!!.setPadding(2, 5, 2, 5)
                            text_view1.setText(": ")
                            text_view1!!.setLayoutParams(paramsadm)

                            val text_view2 = TextView(context)
                            text_view2.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view2!!.setPadding(2, 5, 2, 5)
                            text_view2.setText(": ")
                            text_view2!!.setLayoutParams(paramsadm)

                            val text_view3 = TextView(context)
                            text_view3.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view3!!.setPadding(2, 5, 2, 5)
                            text_view3.setText(": ")
                            text_view3!!.setLayoutParams(paramsadm)

                            val text_view4 = TextView(context)
                            text_view4.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view4!!.setPadding(2, 5, 2, 5)
                            text_view4.setText(": ")
                            text_view4!!.setLayoutParams(paramsadm)


                            val textDrops_id = TextView(context)
                            textDrops_id.setTextColor(resources.getColor(R.color.cyan_color))
                            textDrops_id!!.setPadding(2, 5, 2, 5)
                            textDrops_id.setText(droparralist.get(m).term.toString())
                            textDrops_id!!.setLayoutParams(params_text)

                            val textDrops_date_id = TextView(context)
                            textDrops_date_id.setTextColor(resources.getColor(R.color.cyan_color))
                            textDrops_date_id!!.setPadding(2, 5, 2, 5)
                            textDrops_date_id.setText(droparralist.get(m).date.toString())
                            textDrops_date_id!!.setLayoutParams(params_text)

                            val textDrops_deliv_date_id = TextView(context)
                            textDrops_deliv_date_id.setTextColor(resources.getColor(R.color.cyan_color))
                            textDrops_deliv_date_id!!.setPadding(2, 5, 2, 5)
                            textDrops_deliv_date_id.setText(droparralist.get(m).delivery_date.toString())
                            textDrops_deliv_date_id!!.setLayoutParams(params_text)

                            val textDrops_deliv_location_id = TextView(context)
                            textDrops_deliv_location_id.setTextColor(resources.getColor(R.color.cyan_color))
                            textDrops_deliv_location_id!!.setPadding(2, 5, 2, 5)
                            textDrops_deliv_location_id.setText(droparralist.get(m).delivery_location.toString())
                            textDrops_deliv_location_id!!.setLayoutParams(params_text)

                            val image = ImageView(context)
                            image.layoutParams = LinearLayout.LayoutParams(0, 200, 1.6f)
                            /*  image.maxHeight = 20
                          image.maxWidth = 20*/
                            Picasso.with(context)
                                    .load(ApiInterface.BASE_URL + "uploads/delivorysignatures/" + droparralist.get(m).delivery_sign)
                                    .into(image)

                            Log.e("kkkkk", droparralist.get(m).term.toString())
                            Log.e("kkkkkkk", droparralist.get(m).date.toString())

                            ll_drop.addView(text_drop_title)
                            ll_drop.addView(text_view)
                            ll_drop.addView(textDrops_id)

                            ll_drop_date.addView(text_dates_title)
                            ll_drop_date.addView(text_view1)
                            ll_drop_date.addView(textDrops_date_id)
                            if (delivery_status_stg.equals("0")) {

                            } else {
                                ll_delivery.addView(text_deliv_date_title)
                                ll_delivery.addView(text_view2)
                                ll_delivery.addView(textDrops_deliv_date_id)

                                ll_location.addView(text_location_title)
                                ll_location.addView(text_view3)
                                ll_location.addView(textDrops_deliv_location_id)

                                ll_sign.addView(text_signature_title)
                                ll_sign.addView(text_view4)
                                ll_sign.addView(image)
                            }



                            /*ll_name.addView(text_drop_title)
                            ll_name.addView(text_dates_title)
                            ll_name.addView(text_deliv_date_title)
                            ll_name.addView(text_location_title)
                            ll_name.addView(text_signature_title)

                            ll_view.addView(text_view)
                            ll_view.addView(text_view1)
                            ll_view.addView(text_view2)
                            ll_view.addView(text_view3)

                            ll_data.addView(textDrops_id)
                            ll_data.addView(textDrops_date_id)
                            ll_data.addView(textDrops_deliv_date_id)
                            ll_data.addView(textDrops_deliv_location_id)
                            ll_data.addView(image)

                            ll_main.addView(ll_name)
                            ll_main.addView(ll_view)
                            ll_main.addView(ll_data)*/
                            ll_main.addView(ll_drop)
                            ll_main.addView(ll_drop_date)
                            ll_main.addView(ll_delivery)
                            ll_main.addView(ll_location)
                            ll_main.addView(ll_sign)
                            ll_drops_id?.addView(ll_main)

                            textDropsstatus.setAllCaps(true)
                            textDropsstatus!!.setTextColor(resources.getColor(R.color.white))

                        }
                        //childViewHolder!!.ll_drops_main_id?.addView(childViewHolder!!.ll_drops_id)
                    } else {
                    }
                }

                if (mTasklist2!!.get(mainpage).department_name != null && mTasklist2!!.get(mainpage).department_name!!.equals("Mailing")) {

                    ll_drops_id?.removeAllViews()

                    ll_delivery_view_id!!.visibility = View.VISIBLE
                    val droparralist: Array<Dropsarraylist>? = mTasklist2!!.get(mainpage).taskslist!!.get(page).dropsarr!!
                    if (droparralist!!.size > 0) {

                        for (m in 0 until droparralist!!.size) {
                            val textdrops_sno = TextView(context)
                            val textdrops = TextView(context)
                            val textdropsdate = TextView(context)
                            val textDropsstatus = TextView(context)

                            val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                            //paramsadm!!.gravity = Gravity.RIGHT
                            paramsadm.setMargins(5, 5, 5, 5)
                            textDropsstatus.setPadding(10, 5, 10, 5)
                            textdrops_sno!!.setLayoutParams(paramsadm)
                            textdrops!!.setLayoutParams(paramsadm)
                            textDropsstatus!!.setLayoutParams(paramsadm)
                            textdropsdate!!.setLayoutParams(paramsadm)

                            textdrops!!.setText(droparralist.get(m).term.toString())
                            textdrops!!.setTextColor(resources.getColor(R.color.cyan_color))
                            textdrops!!.setPadding(2, 5, 2, 5)


                            val inputFormat = SimpleDateFormat("yyyy-MM-dd")
                            val outputFormat = SimpleDateFormat("MM-dd-yyyy")
                            val inputDateStr = droparralist.get(m).date.toString()
                            val date = inputFormat.parse(inputDateStr)
                            val outputDateStr = outputFormat.format(date)
                            Log.e("DROPDATE", droparralist.get(m).date.toString() + "------" + outputDateStr)

                            textdropsdate!!.setText(outputDateStr)
                            textdropsdate!!.setTextColor(resources.getColor(R.color.cyan_color))
                            textdropsdate!!.setPadding(2, 5, 2, 5)

                            textdrops_sno!!.setText((m + 1).toString())
                            textdrops_sno.setTextColor(resources.getColor(R.color.cyan_color))
                            textdrops_sno!!.setPadding(2, 5, 2, 5)


                            ///Mailing
                            val ll_main = LinearLayout(context)


                            val ll_drop = LinearLayout(context)
                            val ll_drop_date = LinearLayout(context)


                            ll_drop.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_drop.setOrientation(LinearLayout.HORIZONTAL)

                            ll_drop_date.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_drop_date.setOrientation(LinearLayout.HORIZONTAL)



                            ll_main.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_main.setOrientation(LinearLayout.VERTICAL)

                            val params_title = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.1f)
                            val params_text = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.6f)
                            // val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)


                            val text_drop_title = TextView(context)
                            text_drop_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_drop_title!!.setPadding(2, 5, 2, 5)
                            text_drop_title.setText("Mailing Drop")
                            text_drop_title!!.setLayoutParams(params_title)

                            val text_dates_title = TextView(context)
                            text_dates_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_dates_title!!.setPadding(2, 5, 2, 5)
                            text_dates_title.setText("Mailing Drop Date")
                            text_dates_title!!.setLayoutParams(params_title)


                            val text_view = TextView(context)
                            text_view.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view!!.setPadding(2, 5, 2, 5)
                            text_view.setText(": ")
                            text_view!!.setLayoutParams(paramsadm)

                            val text_view1 = TextView(context)
                            text_view1.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view1!!.setPadding(2, 5, 2, 5)
                            text_view1.setText(": ")
                            text_view1!!.setLayoutParams(paramsadm)

                            val text_view2 = TextView(context)
                            text_view2.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view2!!.setPadding(2, 5, 2, 5)
                            text_view2.setText(": ")
                            text_view2!!.setLayoutParams(paramsadm)

                            val text_view3 = TextView(context)
                            text_view3.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view3!!.setPadding(2, 5, 2, 5)
                            text_view3.setText(": ")
                            text_view3!!.setLayoutParams(paramsadm)

                            val text_view4 = TextView(context)
                            text_view4.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view4!!.setPadding(2, 5, 2, 5)
                            text_view4.setText(": ")
                            text_view4!!.setLayoutParams(paramsadm)


                            val textDrops_id = TextView(context)
                            textDrops_id.setTextColor(resources.getColor(R.color.cyan_color))
                            textDrops_id!!.setPadding(2, 5, 2, 5)
                            textDrops_id.setText(droparralist.get(m).term.toString())
                            textDrops_id!!.setLayoutParams(params_text)

                            val textDrops_date_id = TextView(context)
                            textDrops_date_id.setTextColor(resources.getColor(R.color.cyan_color))
                            textDrops_date_id!!.setPadding(2, 5, 2, 5)
                            textDrops_date_id.setText(droparralist.get(m).date.toString() + "\n")
                            textDrops_date_id!!.setLayoutParams(params_text)



                            ll_drop.addView(text_drop_title)
                            ll_drop.addView(text_view)
                            ll_drop.addView(textDrops_id)

                            ll_drop_date.addView(text_dates_title)
                            ll_drop_date.addView(text_view1)
                            ll_drop_date.addView(textDrops_date_id)

                            ll_main.addView(ll_drop)
                            ll_main.addView(ll_drop_date)
                            ll_drops_id?.addView(ll_main)

                            textDropsstatus.setAllCaps(true)
                            textDropsstatus!!.setTextColor(resources.getColor(R.color.white))

                        }
                        //childViewHolder!!.ll_drops_main_id?.addView(childViewHolder!!.ll_drops_id)
                    } else {
                    }
                }

                if (mTasklist2!!.get(mainpage).department_name != null && mTasklist2!!.get(mainpage).department_name!!.equals("Shipping")) {

                    ll_drops_id?.removeAllViews()

                    ll_delivery_view_id!!.visibility = View.VISIBLE
                    val droparralist: Array<Dropsarraylist>? = mTasklist2!!.get(mainpage).taskslist!!.get(page).shippingdropsarr!!
                    if (droparralist!!.size > 0) {

                        for (m in 0 until droparralist!!.size) {
                            val textdrops_sno = TextView(context)
                            val textdrops = TextView(context)
                            val textdropsdate = TextView(context)
                            val textDropsstatus = TextView(context)

                            val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                            //paramsadm!!.gravity = Gravity.RIGHT
                            paramsadm.setMargins(5, 5, 5, 5)
                            textDropsstatus.setPadding(10, 5, 10, 5)
                            textdrops_sno!!.setLayoutParams(paramsadm)
                            textdrops!!.setLayoutParams(paramsadm)
                            textDropsstatus!!.setLayoutParams(paramsadm)
                            textdropsdate!!.setLayoutParams(paramsadm)

                            textdrops!!.setText(droparralist.get(m).term.toString())
                            textdrops!!.setTextColor(resources.getColor(R.color.cyan_color))
                            textdrops!!.setPadding(2, 5, 2, 5)


                            val inputFormat = SimpleDateFormat("yyyy-MM-dd")
                            val outputFormat = SimpleDateFormat("MM-dd-yyyy")
                            val inputDateStr = droparralist.get(m).date.toString()
                            val date = inputFormat.parse(inputDateStr)
                            val outputDateStr = outputFormat.format(date)
                            Log.e("DROPDATE", droparralist.get(m).date.toString() + "------" + outputDateStr)

                            textdropsdate!!.setText(outputDateStr)
                            textdropsdate!!.setTextColor(resources.getColor(R.color.cyan_color))
                            textdropsdate!!.setPadding(2, 5, 2, 5)

                            textdrops_sno!!.setText((m + 1).toString())
                            textdrops_sno.setTextColor(resources.getColor(R.color.cyan_color))
                            textdrops_sno!!.setPadding(2, 5, 2, 5)


                            ///shipping
                            val ll_main = LinearLayout(context)


                            val ll_drop = LinearLayout(context)
                            val ll_drop_date = LinearLayout(context)


                            ll_drop.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_drop.setOrientation(LinearLayout.HORIZONTAL)

                            ll_drop_date.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_drop_date.setOrientation(LinearLayout.HORIZONTAL)



                            ll_main.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            ll_main.setOrientation(LinearLayout.VERTICAL)

                            val params_title = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.1f)
                            val params_text = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.6f)
                            // val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)


                            val text_drop_title = TextView(context)
                            text_drop_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_drop_title!!.setPadding(2, 5, 2, 5)
                            text_drop_title.setText("Shipping Drop")
                            text_drop_title!!.setLayoutParams(params_title)

                            val text_dates_title = TextView(context)
                            text_dates_title.setTextColor(resources.getColor(R.color.tab2_bg))
                            text_dates_title!!.setPadding(2, 5, 2, 5)
                            text_dates_title.setText("Shipping Drop Date")
                            text_dates_title!!.setLayoutParams(params_title)


                            val text_view = TextView(context)
                            text_view.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view!!.setPadding(2, 5, 2, 5)
                            text_view.setText(": ")
                            text_view!!.setLayoutParams(paramsadm)

                            val text_view1 = TextView(context)
                            text_view1.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view1!!.setPadding(2, 5, 2, 5)
                            text_view1.setText(": ")
                            text_view1!!.setLayoutParams(paramsadm)

                            val text_view2 = TextView(context)
                            text_view2.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view2!!.setPadding(2, 5, 2, 5)
                            text_view2.setText(": ")
                            text_view2!!.setLayoutParams(paramsadm)

                            val text_view3 = TextView(context)
                            text_view3.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view3!!.setPadding(2, 5, 2, 5)
                            text_view3.setText(": ")
                            text_view3!!.setLayoutParams(paramsadm)

                            val text_view4 = TextView(context)
                            text_view4.setTextColor(resources.getColor(R.color.cyan_color))
                            text_view4!!.setPadding(2, 5, 2, 5)
                            text_view4.setText(": ")
                            text_view4!!.setLayoutParams(paramsadm)


                            val textDrops_id = TextView(context)
                            textDrops_id.setTextColor(resources.getColor(R.color.cyan_color))
                            textDrops_id!!.setPadding(2, 5, 2, 5)
                            textDrops_id.setText(droparralist.get(m).term.toString())
                            textDrops_id!!.setLayoutParams(params_text)

                            val textDrops_date_id = TextView(context)
                            textDrops_date_id.setTextColor(resources.getColor(R.color.cyan_color))
                            textDrops_date_id!!.setPadding(2, 5, 2, 5)
                            textDrops_date_id.setText(droparralist.get(m).date.toString() + "\n")
                            textDrops_date_id!!.setLayoutParams(params_text)

                            ll_drop.addView(text_drop_title)
                            ll_drop.addView(text_view)
                            ll_drop.addView(textDrops_id)

                            ll_drop_date.addView(text_dates_title)
                            ll_drop_date.addView(text_view1)
                            ll_drop_date.addView(textDrops_date_id)

                            ll_main.addView(ll_drop)
                            ll_main.addView(ll_drop_date)
                            ll_drops_id?.addView(ll_main)

                            textDropsstatus.setAllCaps(true)
                            textDropsstatus!!.setTextColor(resources.getColor(R.color.white))

                        }
                        //childViewHolder!!.ll_drops_main_id?.addView(childViewHolder!!.ll_drops_id)
                    } else {
                    }
                }

            }
            if (mCommentsList.size > 0)
                setCommentAdapter(mCommentsList)
            Helper.getListViewSize(commentslist);
        }
        return view
    }

    companion object {
        val PAGE_NUM = "PAGE_NUM"
        val PAGE_MAIN = "MAIN_NUM"
        public var mTasklist2: ArrayList<Tasklist> = ArrayList<Tasklist>()
        internal var st_jobid4: String = ""
        fun newInstance(page: Int, mTasklist: ArrayList<Tasklist>, mainpage: Int, st_jobid2: String): TabFragment {
            mTasklist2 = mTasklist
            st_jobid4 = st_jobid2
            val fragment = TabFragment()
            val args = Bundle()
            args.putInt(PAGE_NUM, page)
            args.putInt(PAGE_MAIN, mainpage)
            fragment.setArguments(args)
            return fragment
        }
    }

    private fun setCommentAdapter(mCommentsList: ArrayList<Comments>) {
        mCommentsAdapter = CommentsAdapter(mCommentsList, this.activity!!)
        commentslist!!.adapter = mCommentsAdapter
        mCommentsAdapter!!.notifyDataSetChanged()
    }


    private fun postCommentAPI(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?, mainpage: Int, page: Int) {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.postTaskComment(assigned_task_id, post_comment.text.toString())
        Log.d("LOGIN_REQUEST", call.toString() + "" + assigned_task_id + "-----" + post_comment.text.toString())
        call.enqueue(object : Callback<UserLoginResponse> {
            override fun onResponse(call: Call<UserLoginResponse>, response: retrofit2.Response<UserLoginResponse>?) {
                if (response != null && response.body() != null) {
                    // my_loader.dismiss()
//                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        //storeLoginData(response.body()!!.user_info)
                        Toast.makeText(activity, response.body()!!.result, Toast.LENGTH_SHORT).show()
                        departmentTaskList(inflater, container, savedInstanceState, mainpage, page)

                    } else {
                        // Toast.makeText(activity, "Invalid Username and Password", Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<UserLoginResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun departmentTaskList(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?, mainpage: Int, page: Int) {
        val apiService = ApiInterface.create()
        val call = apiService.departmentTaskList(st_jobid4)
        Log.d("LOGIN_REQUEST", "" + st_jobid4)
        call.enqueue(object : Callback<DeptTaskListResponse> {
            override fun onResponse(call: Call<DeptTaskListResponse>, response: retrofit2.Response<DeptTaskListResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                        val list: Array<Tasklist>? = response.body()!!.tasklist!!
                        for (item: Tasklist in list!!.iterator()) {
                            mTasklist.add(item)
                        }
                        /*post_comment.text.clear()
                        mTasklist2 = mTasklist
                        updateData(mainpage, page)*/

                        val intent = activity!!.getIntent()
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        activity!!.finish()
                        activity!!.startActivity(intent)
                    } else {
                        //  Toast.makeText(this@AssignedViewJob,""+ response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }

                //onCreateView(inflater, container, savedInstanceState)
            }

            override fun onFailure(call: Call<DeptTaskListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed" + t.message)
            }
        })
    }


}
