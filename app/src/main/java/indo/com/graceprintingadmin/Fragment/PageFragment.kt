package indo.com.graceprintingadmin.Fragment

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import indo.com.graceprintingadmin.Model.APIResponse.Tasklist
import indo.com.graceprintingadmin.R


class PageFragment : Fragment() {

    lateinit var notaskfound: TextView
    lateinit var ll_tasks: LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_page, container, false)
        val page = getArguments()!!.getInt(PAGE_NUM)

        ll_tasks = view.findViewById(R.id.ll_tasks) as LinearLayout
        val viewPager: ViewPager = view.findViewById(R.id.view_pager)
        val tabLayout: TabLayout = view.findViewById(R.id.tabs)
        notaskfound = view.findViewById(R.id.notasks) as TextView
        setTabPages(viewPager,tabLayout,page)
        return view
    }
    companion object {
        val PAGE_NUM = "PAGE_NUM"
        public var mTasklist2: ArrayList<Tasklist> = ArrayList<Tasklist>()
        internal var st_jobid2: String = ""
        fun newInstance(page: Int, mTasklist: ArrayList<Tasklist>, st_jobid: String): PageFragment {
            mTasklist2 = mTasklist
            st_jobid2 = st_jobid
            val fragment = PageFragment()
            val args = Bundle()
            args.putInt(PAGE_NUM, page)
            fragment.setArguments(args)
            return fragment
        }
    }
    private fun setTabPages(view_pager: ViewPager, tabs: TabLayout, page: Int) {
        val pageAdapter = MyAdapter(childFragmentManager)

        // create fragments from 0 to 9
        var title: String = ""
        /*var mTasklist: ArrayList<Taskslist> = ArrayList<Taskslist>()
        val list: Array<Taskslist>? = mTasklist2.get(page).taskslist!!
        for (item: Taskslist in list!!.iterator()) {
            mTasklist.add(item)
        }*/
        for (i in 0 until mTasklist2.get(page).taskslist!!.size) {
            //Log.i("task","list "+mTasklist)

            pageAdapter.addFragment(TabFragment.newInstance(i, mTasklist2,page,st_jobid2), mTasklist2.get(page).taskslist!!.get(i).task_name!!)
        }

        view_pager.adapter = pageAdapter
        tabs.setupWithViewPager(view_pager)
        pageAdapter.notifyDataSetChanged()
        view_pager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                view_pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                view_pager.currentItem = tab.position
            }

        })

        val linearLayout = tabs.getChildAt(0) as LinearLayout
        linearLayout.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
        val drawable = GradientDrawable()
        drawable.setColor(Color.WHITE)
        drawable.setSize(2, 0)
        linearLayout.dividerDrawable = drawable

        if(mTasklist2.get(page).taskslist!!.size==0) {
            view_pager.visibility = View.GONE
            tabs.visibility = View.GONE
            ll_tasks.visibility = View.GONE
            notaskfound.visibility = View.VISIBLE

        }else{
            notaskfound.visibility = View.GONE
            view_pager.visibility = View.VISIBLE
            tabs.visibility = View.VISIBLE
            ll_tasks.visibility = View.VISIBLE
        }

    }
    internal class MyAdapter(fm: android.support.v4.app.FragmentManager) : FragmentPagerAdapter(fm) {
        private val mFragments = ArrayList<Fragment>()
        private val mFragmentTitles = ArrayList<String>()

        fun addFragment(fragment: Fragment, title: String) {
            mFragments.add(fragment)
            mFragmentTitles.add(title)
        }

        override fun getItem(position: Int): Fragment {
            return mFragments.get(position)
        }

        override fun getCount(): Int {
            return mFragments.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitles.get(position)
        }
    }

}

