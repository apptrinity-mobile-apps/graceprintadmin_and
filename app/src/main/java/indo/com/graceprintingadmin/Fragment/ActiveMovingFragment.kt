package indo.com.graceprinting.Fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MotionEventCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.Helper.CustomTextViewSemiBold
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Adapter.RecyclerListAdapter
import indo.com.graceprintingadmin.Helper.ItemTouchHelperAdapter
import indo.com.graceprintingadmin.Helper.ItemTouchHelperViewHolder
import indo.com.graceprintingadmin.Model.APIResponse.GetJobListResponse
import indo.com.graceprintingadmin.Model.APIResponse.JobsListDataResponse
import indo.com.graceprintingadmin.R
import retrofit2.Call
import retrofit2.Callback
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
public class ActiveMovingFragment : Fragment() {

    private var type: String? = null
    private var param2: String? = null
    internal lateinit var my_loader: Dialog
    lateinit var rv_jobs: RecyclerView
    internal lateinit var user_name: String
    internal lateinit var dept_name: String
    internal  var jobstatid: String=""
    var dep_stg_id=""
    var job_name=""
    lateinit var mService_ids_list: ArrayList<String>
    public var mServiceInfo: ArrayList<JobsListDataResponse> = ArrayList<JobsListDataResponse>()
    var data: MutableList<JobsListDataResponse> = ArrayList<JobsListDataResponse>()
    private var nojobs: TextView? = null

    lateinit var title_id: CustomTextViewSemiBold
    lateinit var rv_jobs_dialog_id: RecyclerView
    private var mItemTouchHelper: ItemTouchHelper? = null
    var final_ids_stg = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = LayoutInflater.from(this.activity).inflate(R.layout.fragment_first, null, false)

        nojobs = rootView.findViewById(R.id.nojobs) as TextView
        rv_jobs = rootView.findViewById(R.id.rv_jobs) as RecyclerView


        readData(activity!!.intent)
        myloading()

        callgetJobsAPI()

        return rootView

    }

    private fun myloading() {
        my_loader = Dialog(this.activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun readData(intent: Intent?) {
        if(intent !=null && intent.getStringExtra("jobstatid")!=null){
            jobstatid = intent.getStringExtra("jobstatid").toString()
            job_name = intent.getStringExtra("job_name").toString()
            if(jobstatid.equals("")){
                dep_stg_id = intent.getStringExtra("dep_id").toString()
            }
        }
    }

    private fun callgetJobsAPI() {

       // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getJobsList2(jobstatid, dep_stg_id)
        Log.d("GETJOBAPI", jobstatid)
        Log.d("GETJOBAPI_MANI", "MANIBABU_MOVING")
        call.enqueue(object : Callback<GetJobListResponse> {
            override fun onResponse(call: Call<GetJobListResponse>, response: retrofit2.Response<GetJobListResponse>?) {
                if (response != null) {
                   // my_loader.dismiss()
                    mService_ids_list = ArrayList()
                    mService_ids_list.clear()
                    mServiceInfo.clear()
                    Log.w("Result_Order_details", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.jobList != null) {
                        val list: Array<JobsListDataResponse>? = response.body()!!.jobList!!
                        for (item: JobsListDataResponse in list!!.iterator()) {
                            if(item.jobdonestatus!!.contains("3")){
                                mServiceInfo.add(item)
                                data.add(item)
                                setProductAdapter(mServiceInfo, mService_ids_list)
                            }


                            ///only ids
                            mService_ids_list.add(item.id.toString())
                        }

                        if (mServiceInfo.size == 0) {
                            nojobs!!.visibility = View.VISIBLE
                        } else {
                            nojobs!!.visibility = View.GONE
                        }

                    }/*else if(response.body()!!.status.equals("2")) {
                        *//*no_service!!.text = "No Data Found"
                        no_service!!.visibility = View.VISIBLE*//*
                    }*/
                    else {
                        nojobs!!.visibility = View.VISIBLE
                    }
                }
            }

            override fun onFailure(call: Call<GetJobListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun setProductAdapter(mServiceList: ArrayList<JobsListDataResponse>, mService_ids_list: ArrayList<String>) {
        /* mServiceDetailsAdapter = ListofjobsAdapter(mServiceList, this)
         listview_listofjobs!!.adapter = mServiceDetailsAdapter
         mServiceDetailsAdapter!!.notifyDataSetChanged()*/

        val adapter = RecyclerListAdapter(mServiceList, mService_ids_list, activity!!, dep_stg_id)

        rv_jobs.setHasFixedSize(true)
        rv_jobs.adapter = adapter
        rv_jobs.layoutManager = LinearLayoutManager(activity!!)
        adapter.notifyDataSetChanged()

        /*val callback = SimpleItemTouchHelperCallback(adapter)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper!!.attachToRecyclerView(rv_jobs)*/

        /*val adapter_new = MovingRecyclerListAdapter(mServiceList, mService_ids_list, activity!!, this, dep_stg_id)
        rv_jobs_dialog_id.setHasFixedSize(true)

        rv_jobs_dialog_id.layoutManager = LinearLayoutManager(activity)

        val callback = SimpleItemTouchHelperCallback(adapter_new)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper!!.attachToRecyclerView(rv_jobs_dialog_id)
        rv_jobs_dialog_id.adapter = adapter_new*/


    }

    fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        mItemTouchHelper!!.startDrag(viewHolder)
    }

    inner class MovingRecyclerListAdapter(val mServiceList: ArrayList<JobsListDataResponse>, val mService_ids_list: ArrayList<String>, val context: Context, private val mDragStartListener: ActiveMovingFragment, var dep_stg_id: String) : RecyclerView.Adapter<MovingRecyclerListAdapter.ItemViewHolder>(), ItemTouchHelperAdapter {

        private val mItems = mService_ids_list

        var arrowdownup = false
        internal var prepress_text: String = ""
        internal var press_text: String = ""
        internal var bindery_text: String = ""
        internal var mailing_text: String = ""
        internal var shipping_text: String = ""
        internal var delivery_text: String = ""
        internal var toatal_count: String = ""
        internal var precount: Int = 0
        internal var presscount: Int = 0
        internal var binderycount: Int = 0
        internal var mailingcount: Int = 0
        internal var shippingcount: Int = 0
        internal var deliverycount: Int = 0

        internal var prepress_comment_view_status: Int = 0
        internal var press_comment_view_status: Int = 0
        internal var bindery_comment_view_status: Int = 0
        internal var mailing_comment_view_status: Int = 0
        internal var shipping_comment_view_status: Int = 0
        internal var delivery_comment_view_status: Int = 0

        internal var delete_job_id: String = ""


        internal lateinit var my_loader: Dialog
        private val colors = intArrayOf(Color.parseColor("#fff9ef"), Color.parseColor("#eefbff"), Color.parseColor("#fff3eb"), Color.parseColor("#ffeffa"), Color.parseColor("#ffece1"), Color.parseColor("#ebffec"))
        private val list_images = intArrayOf(R.drawable.bg_joblist1, R.drawable.bg_joblist2, R.drawable.bg_joblist3, R.drawable.bg_joblist4, R.drawable.bg_joblist5, R.drawable.bg_joblist6)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.dialog_list_item, parent, false)
            return ItemViewHolder(view)
        }

        @SuppressLint("ClickableViewAccessibility", "ResourceAsColor")
        override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
            myloading()
            holder.tv_job_id.text = mServiceList.get(position).job_id

            holder.tv_customer_name.text = /*"Customer : " +*/ mServiceList.get(position).custmer
            holder.tv_jobpart_header.text = "Job Part :" + mServiceList.get(position).job_part
            ////time date formate change

            if (mServiceList.get(position).jobdonestatus.equals("1")) {
                holder?.ll_header_joblist.setBackgroundResource(R.drawable.yellow_bg)
                holder?.card_view.setBackgroundColor(Color.parseColor("#FFF9E3"))
            }
            if (mServiceList.get(position).jobdonestatus.equals("2")) {
                holder?.ll_header_joblist.setBackgroundResource(R.drawable.green_bg)
                holder?.card_view.setBackgroundColor(Color.parseColor("#F0FFF0"))
            }
            if (mServiceList.get(position).jobdonestatus.equals("3")) {
                holder?.ll_header_joblist.setBackgroundResource(R.drawable.red_bg)
                holder?.card_view.setBackgroundColor(Color.parseColor("#FFECEC"))
            }


            delete_job_id = mServiceList.get(position).id.toString()

            if (mServiceList.get(position).Prepress_assigned_status.equals("0")) {
                prepress_text = "N/A"
            } else if (mServiceList.get(position).Prepress_assigned_status.equals("1")) {
                prepress_text = "Not Started"
            } else if (mServiceList.get(position).Prepress_assigned_status.equals("2")) {
                prepress_text = "Processing"
            } else if (mServiceList.get(position).Prepress_assigned_status.equals("3")) {
                prepress_text = "Completed"
            }

            if (mServiceList.get(position).Press_assigned_status.equals("0")) {
                press_text = "N/A"
            } else if (mServiceList.get(position).Press_assigned_status.equals("1")) {
                press_text = "Not Started"
            } else if (mServiceList.get(position).Press_assigned_status.equals("2")) {
                press_text = "Processing"
            } else if (mServiceList.get(position).Press_assigned_status.equals("3")) {
                press_text = "Completed"
            }

            if (mServiceList.get(position).Bindery_assigned_status.equals("0")) {
                bindery_text = "N/A"
            } else if (mServiceList.get(position).Bindery_assigned_status.equals("1")) {
                bindery_text = "Not Started"
            } else if (mServiceList.get(position).Bindery_assigned_status.equals("2")) {
                bindery_text = "Processing"
            } else if (mServiceList.get(position).Bindery_assigned_status.equals("3")) {
                bindery_text = "Completed"
            }

            if (mServiceList.get(position).Mailing_assigned_status.equals("0")) {
                mailing_text = "N/A"
            } else if (mServiceList.get(position).Mailing_assigned_status.equals("1")) {
                mailing_text = "Not Started"
            } else if (mServiceList.get(position).Mailing_assigned_status.equals("2")) {
                mailing_text = "Processing"
            } else if (mServiceList.get(position).Mailing_assigned_status.equals("3")) {
                mailing_text = "Completed"
            }

            if (mServiceList.get(position).Shipping_assigned_status.equals("0")) {
                shipping_text = "N/A"
            } else if (mServiceList.get(position).Shipping_assigned_status.equals("1")) {
                shipping_text = "Not Started"
            } else if (mServiceList.get(position).Shipping_assigned_status.equals("2")) {
                shipping_text = "Processing"
            } else if (mServiceList.get(position).Shipping_assigned_status.equals("3")) {
                shipping_text = "Completed"
            }

            if (mServiceList.get(position).Delivery_assigned_status.equals("0")) {
                delivery_text = "N/A"
            } else if (mServiceList.get(position).Delivery_assigned_status.equals("1")) {
                delivery_text = "Not Started"
            } else if (mServiceList.get(position).Delivery_assigned_status.equals("2")) {
                delivery_text = "Processing"
            } else if (mServiceList.get(position).Delivery_assigned_status.equals("3")) {
                delivery_text = "Completed"
            }

            holder?.img_moving_id.visibility = View.VISIBLE
            // Start a drag whenever the handle view it touched

            holder.img_moving_id.setOnTouchListener { v, event ->

                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder)
                    Log.e("main_list_positions",mServiceList.get(position).id )
                }else{
                    mDragStartListener.onStartDrag(holder)
                    Log.e("main_list_positions_lll",mServiceList.get(position).id )
                }
                false
            }

        }

        override fun onItemDismiss(position: Int) {
            mItems.removeAt(position)
            notifyItemRemoved(position)
        }


        override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
            //Collections.swap(mItems, fromPosition, toPosition)
            //notifyItemMoved(fromPosition, toPosition)
            if (fromPosition < mItems!!.size && toPosition < mItems!!.size) {
                if (fromPosition < toPosition) {
                    for (i in fromPosition until toPosition) {
                        Collections.swap(mItems, i, i + 1)
                    }
                } else {
                    for (i in fromPosition downTo toPosition + 1) {
                        Collections.swap(mItems, i, i - 1)
                    }
                }
                notifyItemMoved(fromPosition, toPosition)
            }

            Log.e("main_positions", mItems.toString() + "---" + toPosition.toString() + "---" + fromPosition.toString())
            var test = mItems.toString()
            test = test.replace("[\\p{Ps}\\p{Pe}]".toRegex(), "")
            Log.e("TEST", test)
            final_ids_stg = test



            return true
        }

        override fun getItemCount(): Int {
            return mItems.size
        }

        /**
         * Simple example of a view holder that implements [ItemTouchHelperViewHolder] and has a
         * "handle" view that initiates a drag event when touched.
         */
        inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view), ItemTouchHelperViewHolder {
            lateinit var rootview: LinearLayout
            lateinit var tv_job_id: TextView
            lateinit var tv_jobpart_header: TextView
            lateinit var card_view: CardView
            lateinit var ll_header_joblist: LinearLayout
            lateinit var tv_customer_name: CustomTextViewBold
            lateinit var img_moving_id: ImageView


            init {
                rootview = view.findViewById(R.id.rootview) as LinearLayout
                tv_job_id = view.findViewById(R.id.tv_job_id) as TextView

                tv_jobpart_header = view.findViewById(R.id.tv_jobpart_header) as TextView
                ll_header_joblist = view.findViewById(R.id.ll_header_joblist) as LinearLayout
                img_moving_id = view.findViewById(R.id.img_moving_id) as ImageView

                tv_customer_name = view.findViewById(R.id.tv_customer_name) as CustomTextViewBold

                card_view = view.findViewById(R.id.card_view) as CardView


            }

            override fun onItemSelected() {
                itemView.setBackgroundColor(Color.LTGRAY)
            }

            override fun onItemClear() {
                itemView.setBackgroundColor(0)
            }
        }




        private fun myloading() {
            my_loader = Dialog(context)
            my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
            my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            my_loader.setCancelable(false);
            my_loader.setContentView(R.layout.mkloader_dialog)
        }

        fun parseDateToddMMyyyy(time: String): String? {
            val inputPattern = "yyyy-MM-dd HH:mm:ss"
            val outputPattern = "dd-MMM-yyyy h:mm a"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var str: String? = null

            try {
                date = inputFormat.parse(time)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return str
        }
    }




}