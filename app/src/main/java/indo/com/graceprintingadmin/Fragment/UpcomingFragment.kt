package indo.com.graceprinting.Fragment

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Adapter.RDepartmentAdapter
import indo.com.graceprintingadmin.Model.APIResponse.GetJobListResponse
import indo.com.graceprintingadmin.Model.APIResponse.JobsListDataResponse
import indo.com.graceprintingadmin.R
import retrofit2.Call
import retrofit2.Callback


/**
 * A simple [Fragment] subclass.
 */
class UpcomingFragment : Fragment() {

    private var type: String? = null
    private var param2: String? = null
    internal lateinit var my_loader: Dialog
    lateinit var rv_jobs: RecyclerView
    internal lateinit var user_name: String
    internal lateinit var dept_name: String
    internal  var jobstatid: String=""
    var dep_stg_id=""
    var job_name=""
    lateinit var mService_ids_list: ArrayList<String>
    public var mServiceInfo: ArrayList<JobsListDataResponse> = ArrayList<JobsListDataResponse>()
    private var nojobs: TextView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val rootView = LayoutInflater.from(this.activity).inflate(R.layout.fragment_first, null, false)

        nojobs = rootView.findViewById(R.id.nojobs) as TextView
        rv_jobs = rootView.findViewById(R.id.rv_jobs) as RecyclerView

        readData(activity!!.intent)
        myloading()

        callgetJobsAPI()


        return rootView

    }

    private fun myloading() {
        my_loader = Dialog(this.activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun readData(intent: Intent?) {
        if(intent !=null && intent.getStringExtra("jobstatid")!=null){
            jobstatid = intent.getStringExtra("jobstatid").toString()
            job_name = intent.getStringExtra("job_name").toString()
            if(jobstatid.equals("")){
                dep_stg_id = intent.getStringExtra("dep_id").toString()
            }
        }
    }

    private fun callgetJobsAPI() {

        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getJobsList2(jobstatid,dep_stg_id)
        Log.d("GETJOBAPI", jobstatid)
        call.enqueue(object : Callback<GetJobListResponse> {
            override fun onResponse(call: Call<GetJobListResponse>, response: retrofit2.Response<GetJobListResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    mService_ids_list= ArrayList()
                    Log.w("Result_Order_details", response.body().toString())
                    if(response.body()!!.status.equals("1") && response.body()!!.jobList!=null) {
                        val list: Array<JobsListDataResponse>? = response.body()!!.jobList!!
                        for (item: JobsListDataResponse in list!!.iterator()) {

                            if(item.jobdonestatus!!.contains("1")){
                                mServiceInfo.add(item)
                                setProductAdapter(mServiceInfo,mService_ids_list)

                            }


                            ///only ids
                            mService_ids_list.add(item.id.toString())
                        }

                        if (mServiceInfo.size == 0) {
                            nojobs!!.visibility = View.VISIBLE
                        }else{
                            nojobs!!.visibility = View.GONE
                        }

                    }/*else if(response.body()!!.status.equals("2")) {
                        *//*no_service!!.text = "No Data Found"
                        no_service!!.visibility = View.VISIBLE*//*
                    }*/
                    else{
                        nojobs!!.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<GetJobListResponse>, t: Throwable) {
                Log.w("Result_Order_details",t.toString())
            }
        })
    }


    private fun setProductAdapter(mServiceList: ArrayList<JobsListDataResponse>, mService_ids_list:ArrayList<String>) {
        val adapter = RDepartmentAdapter(mServiceList,mService_ids_list, activity!!,/*this,*/dep_stg_id)
        rv_jobs.setHasFixedSize(true)
        rv_jobs.adapter = adapter
        rv_jobs.layoutManager = LinearLayoutManager(activity!!)

    }

}