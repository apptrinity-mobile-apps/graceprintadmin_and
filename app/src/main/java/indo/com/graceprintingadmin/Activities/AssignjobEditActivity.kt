package indo.com.graceprintingadmin.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Helper.CustomTextView
import indo.com.graceprintingadmin.Helper.MultiSelectionSpinner
import indo.com.graceprintingadmin.Model.APIResponse.*
import indo.com.graceprintingadmin.R
import mabbas007.tagsedittext.TagsEditText
import retrofit2.Call
import retrofit2.Callback
import java.util.*


class AssignjobEditActivity : AppCompatActivity() ,TagsEditText.TagsEditListener , MultiSelectionSpinner.OnMultipleItemsSelectedListener {
    override fun selectedStrings5(strings: LinkedList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedIndices6(indices: LinkedList<Int>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedStrings6(strings: LinkedList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedIndices7(indices: LinkedList<Int>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedStrings7(strings: LinkedList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedIndices8(indices: LinkedList<Int>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedStrings8(strings: LinkedList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedIndices9(indices: LinkedList<Int>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedStrings9(strings: LinkedList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedIndices10(indices: LinkedList<Int>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedStrings10(strings: LinkedList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedIndices5(indices: LinkedList<Int>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectedIndices2(indices: LinkedList<Int>) {
    }

    override fun selectedStrings2(strings: LinkedList<String>) {
    }

    override fun selectedIndices3(indices: LinkedList<Int>) {
    }

    override fun selectedStrings3(strings: LinkedList<String>) {
    }

    override fun selectedIndices4(indices: LinkedList<Int>) {
    }

    override fun selectedStrings4(strings: LinkedList<String>) {
    }

    var mSingleArray = arrayOf<String>()


    var mCollection: Collection<String> = ArrayList()
    lateinit var deptname: EditText
    lateinit var usernameTags: TagsEditText
    lateinit var username: EditText
    lateinit var jobid: EditText
    lateinit var allotedsheets: EditText
    lateinit var allotedsheets_txt: CustomTextView
    lateinit var btn_submit: Button
    public var mUserList: ArrayList<UsersList> = ArrayList<UsersList>()
    public var mUserNameList: ArrayList<String> = ArrayList<String>()

    var flag:Boolean=true

    //public var mIndexList: ArrayList<Int> = ArrayList<Int>()

    var mIndexList = mutableListOf<Int>()
    public var mEditusers: ArrayList<Users> = ArrayList<Users>()
    val mEditUserList: ArrayList<String> = ArrayList<String>()

    internal var st_deptname: String = ""
    internal var st_jobid: String = ""
    internal var st_jobname: String = ""
    internal var st_passingjobid: String = ""
    internal var st_user_textdata: String = ""
    internal var selected_id: String = ""
    internal var st_assigned: String = ""
    internal var st_alloted: String = "0"
    var mcollection=ArrayList<String>() as Collection<*>


    lateinit var admin_usernames: MultiSelectionSpinner
    lateinit var dialog_recycler_view: Dialog
    lateinit var rv_main_id: ListView
    var seletedItems = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assingjob)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        toolbar.setTitle("Assigned Job")
        readData(intent)

        userListAPI()
        val dialog_list = Dialog(this)
        dialog_list.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list.setContentView(R.layout.dialog_view)
        dialog_list.setCancelable(true)
        val list_items_list = dialog_list.findViewById(R.id.list_languages) as ListView

        dialog_recycler_view = Dialog(this)
        dialog_recycler_view.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view.setCancelable(true)
        rv_main_id = dialog_recycler_view.findViewById(R.id.rv_main_id) as ListView
        val tv_cancel_id = dialog_recycler_view.findViewById(R.id.tv_cancel_id) as TextView
        val tv_submit_id = dialog_recycler_view.findViewById(R.id.tv_submit_id) as TextView
        tv_cancel_id.setOnClickListener {
            dialog_recycler_view.dismiss()
        }
        tv_submit_id.setOnClickListener {}


        //usernameTags = findViewById(R.id.tagsEditText) as TagsEditText
        admin_usernames = findViewById(R.id.admin_usernames)
        admin_usernames.setListener(this)

        deptname = findViewById(R.id.deptname) as EditText
        username = findViewById(R.id.username) as EditText
        jobid = findViewById(R.id.jobid) as EditText
        allotedsheets = findViewById(R.id.allotedsheets) as EditText
        allotedsheets_txt = findViewById(R.id.allotedsheets_txt) as CustomTextView
        btn_submit = findViewById(R.id.btn_submit_id) as Button



        username.setOnClickListener {
            if (!dialog_list.isShowing())
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AssignjobEditActivity, R.layout.spinner_text_item, mUserNameList)
            list_items_list.setAdapter(state_array_adapter)
            list_items_list.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list.getItemAtPosition(i).toString()
                username.setText(selected_name)
                for(i in 0 until mUserList.size){
                    if(selected_name.equals(mUserList.get(i).user_name)){
                        selected_id = mUserList.get(i).user_id!!
                    }
                }

                dialog_list.dismiss()
            })
        }
        deptname.setText(st_deptname)
        jobid.setText(st_jobname)

        btn_submit.setOnClickListener {
            if(mUserNameList!= null && mUserNameList.size>0) {
                assignJobAPI()
            }else{
                assignwithAssigendJobidAPI()
            }
        }
        if(st_deptname.equals("Press")){
            allotedsheets.visibility = View.GONE
            allotedsheets_txt.visibility = View.GONE
        }else{
            allotedsheets.visibility = View.GONE
            allotedsheets_txt.visibility = View.GONE
        }
    }

    /* override fun onWindowFocusChanged(hasFocus: Boolean) {
         super.onWindowFocusChanged(hasFocus)
         if (hasFocus) {
             usernameTags.showDropDown()
         }
     }*/

    private fun readData(intent: Intent?) {
        if (intent != null && intent.getStringExtra("deptname") != null) {
            st_deptname = intent.getStringExtra("deptname")
            st_jobid = intent.getStringExtra("deptjobid")
            st_jobname = intent.getStringExtra("jobname")
            st_passingjobid= intent.getStringExtra("passingjobid")
            st_user_textdata= intent.getStringExtra("usertextdata")
            if(intent.getStringExtra("assignedid")!=null) {
                st_assigned = intent.getStringExtra("assignedid")
            }
        }
    }
    private fun getdefaultData() {
        if(st_user_textdata!=""){
            val parts = st_user_textdata.split(",")
            for (i in 0 until mUserList.size){
                for (j in 0 until parts.size){

                    if(parts.get(j).equals(mUserList.get(i).user_name)){
                        if (j == 0)
                            selected_id = mUserList.get(j).user_id!!
                        else
                            selected_id += "," + mUserList.get(j).user_id!!
                    }
                }
            }
        }
        flag=false
    }


    private fun userListAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.deptUsersList(st_assigned)
        Log.d("LOGIN_REQUEST", call.toString() + "--"+st_assigned)
        call.enqueue(object : Callback<DeptUserListResponse> {
            override fun onResponse(call: Call<DeptUserListResponse>, response: retrofit2.Response<DeptUserListResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        val list: Array<UsersList>? = response.body()!!.usersList!!
                        for (item: UsersList in list!!.iterator()) {
                            mUserList.add(item)
                        }
                        for(i in 0 until mUserList.size){
                            mUserNameList.add(mUserList.get(i).user_name!!)
                        }
                        mSingleArray = Array(mUserList.size, { i -> mUserNameList.get(i) })
                    }else{
                        Toast.makeText(this@AssignjobEditActivity,""+ response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                    }

                    admin_usernames.setItems(mSingleArray, 1)

                    selecteduserListAPI()
                    //adapter(mUserNameList)
                }
            }

            override fun onFailure(call: Call<DeptUserListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }



    private fun selecteduserListAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.depteditUsersList(st_jobid)
        //Log.d("Edit_REQUEST", call.toString() + "--"+st_jobid)
        call.enqueue(object : Callback<DepteditUserListResponse> {
            override fun onResponse(call: Call<DepteditUserListResponse>, response: retrofit2.Response<DepteditUserListResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    //Log.w("EditResult_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {



                        val list: Array<Users>? = response.body()!!.assigneddata!!.users
                        // Log.w("EditResult_Address", "Result : " + response.body()!!.assigneddata!!.users!!.size)
                        for (item: Users in list!!.iterator()) {
                            mEditusers.add(item)
                            Log.w("EditResult_Address", "Result : " + mEditusers.toString())
                        }

                        for(i in 0 until mEditusers.size){
                            mEditUserList.add(mEditusers.get(i).user_id!!)
                            Log.w("EditResult_Address", "Result : " + mEditusers.get(i).user_id!!)
                        }
                        mSingleArray = Array(mEditusers.size, { i -> mEditUserList.get(i) })
                    }else{
                        Toast.makeText(this@AssignjobEditActivity,""+ response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                    }


                    Log.w("index:::","Main"+mUserList.size)
                    Log.w("index:::","Edit"+mEditUserList.size)


                    for (i in 0 until mUserList.size){
                        for (j in 0 until mEditUserList.size){

                            if(mEditUserList.get(j).equals(mUserList.get(i).user_id)){
                                Log.w("index:::","Posi"+i)
                                mIndexList.add(i)
                            }
                        }
                    }

                    val intArrayv = IntArray(mEditUserList.size)
                    for (index in 0 until mIndexList.size) {
                        Log.w("index:::","Items:"+mIndexList.get(index))
                        intArrayv[index] = mIndexList.get(index)
                    }

                    admin_usernames.setSelection(intArrayv)

                    //adapter(mUserNameList)
                    if(flag) {
                        getdefaultData()
                    }
                }
            }
            override fun onFailure(call: Call<DepteditUserListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

/*
    private fun adapter(mUserNameList: ArrayList<String>) {
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line,mUserNameList)
        usernameTags.setAdapter(arrayAdapter)
        usernameTags.setThreshold(1)

    }
*/
    private fun assignJobAPI() {
        st_alloted=allotedsheets.text.toString()
        if(st_alloted=="" || st_alloted==null){
            st_alloted="0"
        }
        val apiService = ApiInterface.create()
        val call = apiService.assigneditJob(st_jobid,selected_id,st_alloted)
        Log.d("LOGIN_REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserLoginResponse> {
            override fun onResponse(call: Call<UserLoginResponse>, response: retrofit2.Response<UserLoginResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(this@AssignjobEditActivity,""+ response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@AssignjobEditActivity,JobDetailsFullList::class.java)
                        intent.putExtra("job_id", st_passingjobid)
                        intent.putExtra("id", st_passingjobid)
                        finish()
                        startActivity(intent)
                    }else{
                        Toast.makeText(this@AssignjobEditActivity,""+ response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                    }
                }
            }
            override fun onFailure(call: Call<UserLoginResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(deptname.text.toString()))
            deptname.setError("Enter Department Name")
        else if (TextUtils.isEmpty(username.text.toString()))
            username.setError("Please Select Username")
        else
            return true
        return false
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun assignwithAssigendJobidAPI() {
        st_alloted=allotedsheets.text.toString()
        val apiService = ApiInterface.create()
        val call = apiService.assignJobId(st_jobid,selected_id,st_alloted)
        Log.d("LOGIN_REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserLoginResponse> {
            override fun onResponse(call: Call<UserLoginResponse>, response: retrofit2.Response<UserLoginResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(this@AssignjobEditActivity,""+ response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@AssignjobEditActivity,JobDetailsFullList::class.java)
                        intent.putExtra("job_id", st_passingjobid)
                        intent.putExtra("id", st_passingjobid)
                        finish()
                        startActivity(intent)

                    }else{
                        Toast.makeText(this@AssignjobEditActivity,""+ response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<UserLoginResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    override fun onTagsChanged(collection: Collection<String>) {
        mCollection = collection
        Toast.makeText(this@AssignjobEditActivity, "Empty"+mCollection, Toast.LENGTH_SHORT).show()
    }

    override fun onEditingFinished() {
        Log.d("","OnEditing finished")
        Toast.makeText(this@AssignjobEditActivity, "Empty", Toast.LENGTH_SHORT).show()
    }



    override fun selectedIndices(indices: LinkedList<Int>) {

        val mTaskListarray=ArrayList<String>()
        for (j in 0 until indices!!.size) {
            Log.w("List: ", "Data:" + mUserList.get(indices.get(j)).user_id.toString())
            mTaskListarray.add(mUserList.get(indices.get(j)).user_id.toString())
            if (j == 0)
                selected_id = mTaskListarray.get(j)
            else
                selected_id += "," + mTaskListarray.get(j)
            Log.w("List: ", "" + mTaskListarray)
            Log.w("List:String ", "" + selected_id)
        }
    }

    override fun selectedStrings(strings: LinkedList<String>) {
        if(strings.size==0){
            Toast.makeText(this@AssignjobEditActivity, "Please Select User", Toast.LENGTH_SHORT).show()
        }

    }
}


