package indo.com.graceprintingadmin.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4printuser.Helper.CustomTextViewSemiBold
import indo.com.graceprintingadmin.Adapter.ListofjobsAdapter
import indo.com.graceprintingadmin.Adapter.MyPagerAdapter
import indo.com.graceprintingadmin.Helper.SessionManager
import indo.com.graceprintingadmin.Model.APIResponse.JobsListDataResponse
import indo.com.graceprintingadmin.R
import kotlinx.android.synthetic.main.app_bar_main.*

class DepartMainActivity : AppCompatActivity() , NavigationView.OnNavigationItemSelectedListener {

    lateinit var drawer: DrawerLayout
    internal lateinit var sessionManager: SessionManager
    internal lateinit var user_name: String
    internal lateinit var dept_name: String
    internal  var jobstatid: String=""
    internal lateinit var my_loader: Dialog
    private var listview_listofjobs: ListView? = null

    private var nojobs: TextView? = null
    public var mServiceInfo: ArrayList<JobsListDataResponse> = ArrayList<JobsListDataResponse>()
    lateinit var mService_ids_list: ArrayList<String>
    private var mServiceDetailsAdapter: ListofjobsAdapter? = null
    lateinit var textView3: CustomTextViewSemiBold
    ///moving item Recyclerview
    var dep_stg_id=""
    var job_name=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView3 = findViewById(R.id.textView3)
        Log.e("MANIBABU","MANIBABu")
        readData(intent)
        sessionManager = SessionManager(this)


        listview_listofjobs = findViewById(R.id.listview_listofjobs) as ListView

        nojobs = findViewById(R.id.nojobs) as TextView

        sessionManager.checkLogin()

        val user = sessionManager.userDetails

        user_name = user.get(SessionManager.ADMIN_USERNAME).toString()
        dept_name = user.get(SessionManager.ADMIN_NAME).toString()

        drawer = findViewById<DrawerLayout>(R.id.drawer_layout)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)


        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close)

        drawer.setDrawerListener(toggle)
        toggle.syncState()


        toggle.setDrawerIndicatorEnabled(true);
        //toggle.setHomeAsUpIndicator(R.drawable.ic_launcher_background);

        toggle.setToolbarNavigationClickListener {

            drawer.openDrawer(GravityCompat.START);
        }


        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

         val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
         viewpager_main.adapter = fragmentAdapter

         tabs_main.setupWithViewPager(viewpager_main)

       // tabs_main.getTabAt(0)!!.setCustomView(R.layout.tab1);
        tabs_main.getTabAt(0)!!.setCustomView(R.layout.tab2);
        tabs_main.getTabAt(1)!!.setCustomView(R.layout.tab3);
        tabs_main.getTabAt(2)!!.setCustomView(R.layout.tab4);


        // Log.e("DEPT", "" + user_name+"-----"+dept_name);

        val header = navigationView.getHeaderView(0)
        val username = header.findViewById(R.id.tv_username) as TextView
        val deptname = header.findViewById(R.id.tv_department_id) as TextView
        username!!.setText(dept_name)
        deptname!!.setText(user_name)

        myloading()

    }

    private fun readData(intent: Intent?) {
        if(intent !=null && intent.getStringExtra("jobstatid")!=null){
            jobstatid = intent.getStringExtra("jobstatid").toString()
            job_name = intent.getStringExtra("job_name").toString()
            if(jobstatid.equals("")){
                dep_stg_id = intent.getStringExtra("dep_id").toString()
            }
            textView3.setText(job_name)
        }
    }

    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }









    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_dashboard -> {
                val intent = Intent(this,DashboardtListActivity::class.java)
                startActivity(intent)
            }
            R.id.add_job -> {
                val intent = Intent(this,AddJobActivity::class.java)
                startActivity(intent)
            }
            /* R.id.assign_job -> {
                 val intent = Intent(this,ListofjobsActivity::class.java)
                 startActivity(intent)
             }*/

            R.id.nav_logout -> {
                sessionManager.logoutUser()
            }

            /* R.id.nav_manage -> toast("Click Manage")
             R.id.nav_share -> toast("Click Share")
             R.id.nav_send -> toast("Click Send")*/
        }

        drawer.closeDrawer(GravityCompat.START)

        return true
    }

    // Extension function to show toast message easily
    private fun Context.toast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }
}
