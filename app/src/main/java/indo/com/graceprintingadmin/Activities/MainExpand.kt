package indo.com.graceprintingadmin.Activities

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import indo.com.graceprintingadmin.R
import java.util.*

@SuppressLint("Registered")
class MainExpand : AppCompatActivity() {

    //  ExpandableListAdapter listAdapter;
    lateinit var listAdapter: ExpListViewAdapterWithCheckbox
    lateinit var expListView: ExpandableListView
    lateinit var listDataHeader: ArrayList<String>
    lateinit var listDataChild: HashMap<String, List<String>>
    lateinit var listDataChild1: HashMap<String, List<String>>
    private var lastExpandedPosition = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.expnadlayoutsample)

        // get the listview
        expListView = findViewById(R.id.lvExp) as ExpandableListView

        // preparing list data
        prepareListData()

        listAdapter = ExpListViewAdapterWithCheckbox(this, listDataHeader, listDataChild)

        // setting list adapter
        expListView.setAdapter(listAdapter)

        // Listview Group click listener
        expListView.setOnGroupClickListener { parent, v, groupPosition, id ->
            // Toast.makeText(getApplicationContext(),
            // "Group Clicked " + listDataHeader.get(groupPosition),
            // Toast.LENGTH_SHORT).show();

            false
        }

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener { groupPosition ->
            Toast.makeText(applicationContext,
                    listDataHeader[groupPosition] + " Expanded",
                    Toast.LENGTH_SHORT).show()
            if (lastExpandedPosition != -1
                    && groupPosition != lastExpandedPosition) {
                expListView.collapseGroup(lastExpandedPosition);
            }
            lastExpandedPosition = groupPosition;
        }

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener { groupPosition ->
            Toast.makeText(applicationContext,
                    listDataHeader[groupPosition] + " Collapsed",
                    Toast.LENGTH_SHORT).show()
        }

        // Listview on child click listener
        expListView.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            Toast.makeText(
                    applicationContext,
                    listDataHeader[groupPosition]
                            + " : "
                            + listDataChild[listDataHeader[groupPosition]]!!.get(
                            childPosition), Toast.LENGTH_SHORT)
                    .show()
            false
        }


        //val textView = findViewById(R.id.textView) as TextView

  /*      button.setOnClickListener {
            var count = 0
            var count1 = 0
            for (mGroupPosition in 0 until listAdapter.groupCount) {
                count = count + listAdapter.getNumberOfCheckedItemsInGroup(mGroupPosition)
            }
            for (mGroupPosition in 0 until listAdapter.groupCount) {
                count1 = count1 + listAdapter.getNumberOfGroupCheckedItems(mGroupPosition)
            }
            textView.text = count1.toString() + "" + count
        }*/

    }

    /*
     * Preparing the list data
     */
    private fun prepareListData() {
        listDataHeader = ArrayList()
        listDataChild = HashMap()
        listDataChild1 = HashMap()

        // Adding child data
        listDataHeader.add("Top 250")
        listDataHeader.add("Now Showing")
        listDataHeader.add("Coming Soon..")

        // Adding child data
        val top250 = ArrayList<String>()
        top250.add("The Shawshank Redemption")


        val nowShowing = ArrayList<String>()
        nowShowing.add("The Conjuring")


        val comingSoon = ArrayList<String>()
        comingSoon.add("2 Guns")


        listDataChild.put(listDataHeader[0],top250) // Header, Child data
        listDataChild.put(listDataHeader[1],nowShowing)
        listDataChild.put(listDataHeader[2], comingSoon)
    }

    inner class ExpListViewAdapterWithCheckbox/*  Here's the constructor we'll use to pass in our calling
         *  activity's context, group items, and child items
        */
    (// Define activity context
            private val mContext: Context, // ArrayList that is what each key in the above
            // hashmap points to
            private val mListDataGroup: ArrayList<String>, /*
         * Here we have a Hashmap containing a String key
         * (can be Integer or other type but I was testing
         * with contacts so I used contact name as the key)
        */
            private val mListDataChild: HashMap<String, List<String>>) : BaseExpandableListAdapter() {

        // Hashmap for keeping track of our checkbox check states
        private val mChildCheckStates: HashMap<Int, BooleanArray>
        private val mGroupCheckStates: HashMap<Int, BooleanArray>

        // Our getChildView & getGroupView use the viewholder patter
        // Here are the viewholders defined, the inner classes are
        // at the bottom
        private var childViewHolder: ChildViewHolder? = null
        private var groupViewHolder: GroupViewHolder? = null

        /*
              *  For the purpose of this document, I'm only using a single
         *	textview in the group (parent) and child, but you're limited only
         *	by your XML view for each group item :)
        */
        private var groupText: String? = null
        private var childText: String? = null

        init {

            // Initialize our hashmap containing our check states here
            mChildCheckStates = HashMap()
            mGroupCheckStates = HashMap()
        }

        fun getNumberOfCheckedItemsInGroup(mGroupPosition: Int): Int {
            val getChecked = mChildCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {

                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        fun getNumberOfGroupCheckedItems(mGroupPosition: Int): Int {
            val getChecked = mGroupCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {
                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        override fun getGroupCount(): Int {
            return mListDataGroup.size
        }

        /*
         * This defaults to "public object getGroup" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getGroup(groupPosition: Int): String {
            return mListDataGroup[groupPosition]
        }

        override fun getGroupId(groupPosition: Int): Long {
            return groupPosition.toLong()
        }


        override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                                  convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            //  I passed a text string into an activity holding a getter/setter
            //  which I passed in through "ExpListGroupItems".
            //  Here is where I call the getter to get that text
            groupText = getGroup(groupPosition)

            if (convertView == null) {

                val inflater = mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_group, null)

                // Initialize the GroupViewHolder defined at the bottom of this document
                groupViewHolder = GroupViewHolder()

                groupViewHolder!!.mGroupText = convertView!!.findViewById(R.id.lblListHeader) as TextView


                convertView.tag = groupViewHolder
            } else {

                groupViewHolder = convertView.tag as GroupViewHolder
            }

            groupViewHolder!!.mGroupText!!.text = groupText



            return convertView
        }

        override fun getChildrenCount(groupPosition: Int): Int {
            return mListDataChild[mListDataGroup[groupPosition]]!!.size
        }

        /*
         * This defaults to "public object getChild" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getChild(groupPosition: Int, childPosition: Int): String {
            return mListDataChild[mListDataGroup[groupPosition]]!!.get(childPosition)
        }

        override fun getChildId(groupPosition: Int, childPosition: Int): Long {
            return childPosition.toLong()
        }

        override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView


            val mGroupPosition = groupPosition

            //  I passed a text string into an activity holding a getter/setter
            //  which I passed in through "ExpListChildItems".
            //  Here is where I call the getter to get that text
            childText = getChild(groupPosition, childPosition)

            if (convertView == null) {

                val inflater = this.mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_item, null)

                childViewHolder = ChildViewHolder()

                childViewHolder!!.mChildText = convertView!!.findViewById(R.id.tv_post_comm_id)
                childViewHolder!!.rv_chat_data_id = convertView!!.findViewById(R.id.rv_chat_data_id)
                childViewHolder!!.mChildText!!.setOnClickListener { Toast.makeText(applicationContext, groupPosition.toString() + "--" + childPosition, Toast.LENGTH_SHORT).show() }



                convertView.setTag(R.layout.list_item, childViewHolder)

            } else {

                childViewHolder = convertView
                        .getTag(R.layout.list_item) as ChildViewHolder
            }

            //childViewHolder!!.mChildText!!.text = childText

            /*
		 * You have to set the onCheckChangedListener to null
		 * before restoring check states because each call to
		 * "setChecked" is accompanied by a call to the
		 * onCheckChangedListener
		*/
           // childViewHolder!!.mCheckBox!!.setOnCheckedChangeListener(null)





            return convertView
        }

        override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
            return false
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        inner class GroupViewHolder {

            internal var mGroupText: TextView? = null
            //internal var mGroupCheckbox: CheckBox? = null
        }

        inner class ChildViewHolder {

            internal var mChildText: CustomTextViewBold? = null
            internal var rv_chat_data_id: RecyclerView? = null
            //internal var mCheckBox: CheckBox? = null
        }
    }

}
