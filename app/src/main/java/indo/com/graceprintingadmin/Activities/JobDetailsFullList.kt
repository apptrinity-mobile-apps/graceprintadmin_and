package indo.com.graceprintingadmin.Activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Adapter.DepartmentAdapter
import indo.com.graceprintingadmin.Helper.CustomTextView
import indo.com.graceprintingadmin.Model.APIResponse.*
import indo.com.graceprintingadmin.R
import retrofit2.Call
import retrofit2.Callback
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class JobDetailsFullList : AppCompatActivity() {
    lateinit var rr_main_list: RecyclerView
    //var mTasklist: ArrayList<JobDetailViewDataResponse> = ArrayList<JobDetailViewDataResponse>()
    var mTasklist = ArrayList<JobDetailViewDataResponse>()
    //var mDepartments: ArrayList<JobsListDepartmentsDataResponse> = ArrayList<JobsListDepartmentsDataResponse>()

    var mDepartments: HashMap<String, Array<JobsListDepartmentsDataResponse>> = HashMap()


    internal var id: String = ""
    internal var user_name: String = ""
    internal var job_part: String = ""
    internal var job_id: String = ""
    lateinit var tv_main_id: CustomTextView


    lateinit var ll_jobdetails: LinearLayout
    lateinit var ll_date_terms_value: LinearLayout
    lateinit var layout_date_terms: LinearLayout
    private var listview_department1: ListView? = null
    lateinit var rv_dep_list_id: RecyclerView
    public var mServiceInfo: ArrayList<JobDetailViewDataDepartmentResponse> = ArrayList<JobDetailViewDataDepartmentResponse>()
    public var mServicedata: ArrayList<JobDetailViewResponse> = ArrayList<JobDetailViewResponse>()
    private var job_id_val: String = ""
    private var mServiceDetailsAdapter: DepartmentAdapter? = null



    private var tv_id: TextView? = null
    private var tv_job_id: TextView? = null
    private var tv_job_name: TextView? = null
    private var tv_jobpart: TextView? = null
    private var tv_runtime: TextView? = null
    private var tv_totaltime: TextView? = null
    private var tv_makereadytime: TextView? = null
    private var tv_plannedactivity: TextView? = null
    private var tv_earliestdate: TextView? = null
    private var tv_customer: TextView? = null
    private var tv_description: TextView? = null
    private var tv_promisedate: TextView? = null
    private var tv_cricstartdate: TextView? = null
    private var tv_duetime: TextView? = null
    private var tv_lastactcode: TextView? = null
    private var tv_scheduled: TextView? = null

    private var tv_papername: TextView? = null
    private var tv_txtname: TextView? = null
    private var tv_subtxtname: TextView? = null
    private var tv_weight: TextView? = null
    private var tv_size: TextView? = null
    private var tv_allotedsheet: TextView? = null
    private var tv_mailqty: TextView? = null
    private var tv_noterms: TextView? = null
    private var tv_job_id_detail: CustomTextViewBold? = null
    private var tv_jobpart_detail: CustomTextViewBold? = null

    private var plus: Boolean = true


    internal var job_type_id: String = ""
    internal var planeed_activity: String = ""
    internal var earliest_start_date: String = ""
    internal var custmer: String = ""
    internal var description: String = ""
    internal var promise_date: String = ""
    internal var scheduled: String = ""
    internal var crit_start_date: String = ""
    internal var due_time: String = ""
    internal var last_act_code: String = ""
    internal var earlist_starttime: String = ""

    internal var ll2: LinearLayout? = null
    val contacts = ArrayList<Contact>()

    lateinit var list_terms: HashMap<String, Array<String>>
    lateinit var list_terms_date: HashMap<String, Array<String>>
    lateinit var  shipping_new_array:HashMap<String, Array<String>>
    lateinit var  shipping_new_array_date:HashMap<String, Array<String>>
    lateinit var  delivery_new_array_date:HashMap<String, Array<String>>
    lateinit var  deliverynew_array:HashMap<String, Array<String>>

    internal var tv_noterms_shipping: TextView? = null
    internal var tv_noterms_delivery: TextView? = null
    internal var ll_delivery: LinearLayout? = null
    internal var ll_shipping: LinearLayout? = null
    internal var ll_layout_shipping_value: LinearLayout? = null
    internal var ll_layout_shipping: LinearLayout? = null
    internal var ll_layout_delivery: LinearLayout? = null
    internal var ll_layout_delivery_value: LinearLayout? = null

    internal var layout_delivery_drops: LinearLayout? = null
    internal var ll_row_delivery: LinearLayout? = null
    internal var ll_row_shipping: LinearLayout? = null
    internal var layout_shipping_drops: LinearLayout? = null

    lateinit var tv_jobtitle_id: CustomTextView
    lateinit var tv_job_desc_id: CustomTextView


    lateinit var ll_paper_type_based_id:LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_details_full_list)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        // toolbar.setTitle("Job DetailView")

        if (intent.getStringExtra("id") != null) {
            id = intent.getStringExtra("id")
            job_id = intent.getStringExtra("job_id")
        }

        rr_main_list = findViewById(R.id.rr_main_list)
        departmentTaskList()

        ///JobDetailsListLayout

        ll_jobdetails = findViewById(R.id.ll_jobdetails) as LinearLayout
        val iv_plus = findViewById(R.id.iv_plus) as ImageView
        val iv_edit = findViewById(R.id.iv_edit) as ImageView
        tv_job_id = findViewById(R.id.tv_job_id) as TextView
        tv_id = findViewById(R.id.tv_id) as TextView
        tv_job_name = findViewById(R.id.tv_jobname) as TextView
        tv_jobpart = findViewById(R.id.tv_jobpart) as TextView
        tv_runtime = findViewById(R.id.tv_runtime) as TextView
        tv_totaltime = findViewById(R.id.tv_totaltime) as TextView
        tv_makereadytime = findViewById(R.id.tv_makereadytime) as TextView
        tv_plannedactivity = findViewById(R.id.tv_plannedactivity) as TextView
        tv_earliestdate = findViewById(R.id.tv_earliestdate) as TextView
        tv_customer = findViewById(R.id.tv_customer) as TextView
        tv_description = findViewById(R.id.tv_description) as TextView
        tv_promisedate = findViewById(R.id.tv_promisedate) as TextView
        tv_scheduled = findViewById(R.id.tv_scheduled) as TextView
        tv_cricstartdate = findViewById(R.id.tv_cricstartdate) as TextView
        tv_duetime = findViewById(R.id.tv_duetime) as TextView
        tv_lastactcode = findViewById(R.id.tv_lastactcode) as TextView

        tv_papername = findViewById(R.id.tv_papername) as TextView
        ll_paper_type_based_id = findViewById(R.id.ll_paper_type_based_id)
        tv_txtname = findViewById(R.id.tv_txtname) as TextView
        tv_subtxtname = findViewById(R.id.tv_subtxtname) as TextView
        tv_weight = findViewById(R.id.tv_weight) as TextView
        tv_size = findViewById(R.id.tv_size) as TextView
        tv_allotedsheet = findViewById(R.id.tv_allotedsheet) as TextView
        tv_mailqty = findViewById(R.id.tv_mailqty) as TextView
        tv_noterms = findViewById(R.id.tv_noterms) as TextView
        tv_job_id_detail = findViewById(R.id.tv_job_id_detail) as CustomTextViewBold
        tv_jobpart_detail = findViewById(R.id.tv_jobpart_detail) as CustomTextViewBold

        ll_date_terms_value = findViewById(R.id.layout_date_terms_value) as LinearLayout
        layout_date_terms = findViewById(R.id.layout_date_terms) as LinearLayout

        layout_delivery_drops= findViewById(R.id.layout_delivery_drops) as LinearLayout
        ll_row_delivery= findViewById(R.id.ll_row_delivery) as LinearLayout
        ll_row_shipping= findViewById(R.id.ll_row_shipping) as LinearLayout
        layout_shipping_drops= findViewById(R.id.layout_shipping_drops) as LinearLayout
        
        //listview_department = findViewById(R.id.listview_department) as RecyclerView

        //listview_department!!.isNestedScrollingEnabled = false

        tv_jobtitle_id=findViewById(R.id.tv_jobtitle_id)
        tv_job_desc_id=findViewById(R.id.tv_job_desc_id)
        listview_department1 = findViewById(R.id.listview_department1) as ListView
        rv_dep_list_id = findViewById(R.id.rv_dep_list_id)
        rv_dep_list_id.isNestedScrollingEnabled=false


         tv_noterms_shipping = findViewById(R.id.tv_noterms_shipping)
         tv_noterms_delivery = findViewById(R.id.tv_noterms_delivery)
         ll_layout_shipping_value = findViewById(R.id.layout_shipping_value)
         ll_layout_shipping = findViewById(R.id.layout_shipping)
         ll_layout_delivery = findViewById(R.id.layout_delivery)
         ll_layout_delivery_value = findViewById(R.id.layout_delivery_value)

        //myloading()
        if (intent.getStringExtra("id") != null) {
            id = intent.getStringExtra("id")
            job_id = intent.getStringExtra("job_id")
        }


        // tv_job_id_detail!!.setText(job_id)


        iv_plus.setOnClickListener {
            if (plus) {
                iv_plus.setImageResource(R.drawable.minus_icon)
                plus = false;
                ll_jobdetails!!.visibility = View.VISIBLE
                iv_edit.visibility = View.VISIBLE
            } else {
                plus = true;
                iv_plus.setImageResource(R.drawable.plus)
                ll_jobdetails!!.visibility = View.GONE
                iv_edit.visibility = View.INVISIBLE
            }

        }

        iv_edit.setOnClickListener {
            Log.e("tv_job_id", "" + tv_id!!.text.toString())
            val home_intent = Intent(this@JobDetailsFullList, JobEditActivity::class.java)
            home_intent.putExtra("id", tv_id!!.text.toString())
            home_intent.putExtra("job_id", job_id)
            home_intent.putExtra("job_part", job_part)
            home_intent.putExtra("job_type_id", job_type_id)
            home_intent.putExtra("planeed_activity", planeed_activity)
            home_intent.putExtra("earliest_start_date", earliest_start_date)
            home_intent.putExtra("custmer", custmer)
            home_intent.putExtra("description", description)
            home_intent.putExtra("promise_date", promise_date)
            home_intent.putExtra("scheduled", scheduled)
            home_intent.putExtra("crit_start_date", crit_start_date)
            home_intent.putExtra("due_time", due_time)
            home_intent.putExtra("last_act_code", last_act_code)
            home_intent.putExtra("earliest_start_time", earlist_starttime)
            startActivity(home_intent)
        }
    }

    private fun departmentTaskList() {
        val apiService = ApiInterface.create()
        val call = apiService.jobDetailView(job_id)
        Log.d("DEPTTASKLIST_JOBID", call.toString() + "--------" + job_id)
        call.enqueue(object : Callback<JobDetailViewResponse> {
            override fun onResponse(call: Call<JobDetailViewResponse>, response: retrofit2.Response<JobDetailViewResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        val list: Array<JobDetailViewDataResponse>? = response.body()!!.data!!
                        for (item: JobDetailViewDataResponse in list!!.iterator()) {
                            mTasklist.add(item)
                        }

                        list_terms = HashMap()
                        list_terms_date = HashMap()

                         shipping_new_array=HashMap()
                         shipping_new_array_date=HashMap()
                         deliverynew_array=HashMap()
                         delivery_new_array_date=HashMap()

                        for (i in 0 until mTasklist!!.size) {
                            val list_ll: Array<JobsListDepartmentsDataResponse>? = mTasklist.get(i).department!!
                            //for (item: JobsListDepartmentsDataResponse in list_ll!!.iterator()) {
                            mDepartments.put(mTasklist.get(i).job_part!!, list_ll!!)
                            // }

                            list_terms.put(mTasklist.get(i).job_part!!, mTasklist.get(i).terms!!)
                            list_terms_date.put(mTasklist.get(i).job_part!!, mTasklist.get(i).term_date!!)

                            shipping_new_array.put(mTasklist.get(i).job_part!!, mTasklist.get(i).shipping_terms!!)
                            shipping_new_array_date.put(mTasklist.get(i).job_part!!, mTasklist.get(i).shipping_term_date!!)

                            deliverynew_array.put(mTasklist.get(i).job_part!!, mTasklist.get(i).delivery_terms!!)
                            delivery_new_array_date.put(mTasklist.get(i).job_part!!, mTasklist.get(i).delivery_term_date!!)

                            Log.d("job_task_status_id", mTasklist.get(i).status)
                        }
                        Log.d("term_list_size", "" + list_terms.get("1")!!.size + "----" + list_terms_date.get("1")!!.size)

                        /*for (j in 0 until mDepartments.size){
                            Log.w("list_lll", "Result : " + mDepartments.get(0).department_name)
                            val contact = Contact(mDepartments.get(j).user_name.toString(), mDepartments.get(j).department_name.toString(), mDepartments.get(j).job_dep_id.toString(),
                                    mDepartments.get(j).status.toString(), mDepartments.get(j).department_id.toString(), job_id_val, mDepartments.get(j).job_id.toString(), mDepartments.get(j).assigned_id.toString(), mDepartments.get(j).assigned_status.toString(), false)
                            contacts.add(contact)
                        }*/

                        Log.w("main_lll", "Result : " + mDepartments.size)
                        rr_main_list!!.setHasFixedSize(true)
                        val horizontalLayoutManagaer = LinearLayoutManager(this@JobDetailsFullList, LinearLayoutManager.HORIZONTAL, false)
                        //val layoutManager = LinearLayoutManager(this@JobDetailsFullList);
                        rr_main_list!!.setLayoutManager(horizontalLayoutManagaer)
                        rr_main_list!!.setItemAnimator(DefaultItemAnimator())
                        val details_adapter = MapDetailsAdapter(mTasklist, mDepartments, list_terms, list_terms_date, shipping_new_array,shipping_new_array_date,deliverynew_array,delivery_new_array_date ,this@JobDetailsFullList)
                        rr_main_list!!.setAdapter(details_adapter)
                        details_adapter.notifyDataSetChanged()

                        try {
                            //detailEditresponse(response.body()!!.data)
                        } catch (e: Exception) {
                            Log.w("Exception ", "Data Binding VIew: " + e.message)
                        }
                    } else {
                        Toast.makeText(this@JobDetailsFullList, "" + response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<JobDetailViewResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed" + t.message)
            }
        })
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_list_text_id = view.findViewById<CustomTextView>(R.id.tv_list_text_id)
        val ll_jobpartlist = view.findViewById<LinearLayout>(R.id.ll_jobpartlist)

        // var userSelected = true;
    }

    inner class MapDetailsAdapter(val title: ArrayList<JobDetailViewDataResponse>, val departments: HashMap<String, Array<JobsListDepartmentsDataResponse>>, val terms: HashMap<String, Array<String>>, val term_date: HashMap<String, Array<String>>,
                                  val shipping: HashMap<String, Array<String>>, val shipping_date: HashMap<String, Array<String>>,val delivery: HashMap<String, Array<String>>,val delivery_date: HashMap<String, Array<String>>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {


        private var temp: Boolean = false
        var arrowdownup = false
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_main, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            holder.tv_list_text_id.setText("Job Part " + title.get(position).job_part)
            tv_jobpart_detail!!.setText("Job Part :" + title.get(position).job_part)
            tv_job_id_detail!!.setText(title.get(position).job_id)

            holder.tv_list_text_id.setOnClickListener {


                contacts.clear()

                /*if(arrowdownup == false){
                    holder.tv_list_text_id.setBackgroundColor(resources.getColor(R.color.cyan_color))
                    arrowdownup = true
                }else{
                    holder.tv_list_text_id.setBackgroundColor(resources.getColor(R.color.yellow))
                    arrowdownup = false
                }*/

                val inputPattern = "yyyy-MM-dd HH:mm:ss"
                val outputPattern = "MM/dd/yyyy h:mm a"
                val inputFormat = SimpleDateFormat(inputPattern)
                val outputFormat = SimpleDateFormat(outputPattern)

                var date: Date? = null
                var datepromise: Date? = null
                var str: String? = null
                var str_promise: String? = null
                Log.e("dates_times", title.get(position).earliest_start_date.toString())
                try {
                    date = inputFormat.parse(title.get(position).earliest_start_date.toString())
                    datepromise = inputFormat.parse(title.get(position).promise_date.toString())
                    str = outputFormat.format(date)
                    str_promise = outputFormat.format(datepromise)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                Log.e("dates_times_lll", str)


                tv_id!!.setText(title.get(position).id)
                tv_job_id!!.setText(title.get(position).job_id)
                tv_job_name!!.setText(title.get(position).job_name)
                tv_jobpart!!.setText(title.get(position).job_part)

                tv_runtime!!.setText(title.get(position).run_time)
                tv_totaltime!!.setText(title.get(position).total_time)
                tv_makereadytime!!.setText(title.get(position).make_time)
                tv_jobtitle_id!!.setText(title.get(position).job_title)
                tv_job_desc_id!!.setText(title.get(position).job_description)
                tv_plannedactivity!!.setText(title.get(position).planeed_activity)
                //tv_earliestdate!!.setText(title.get(position).earliest_start_date)
                tv_earliestdate!!.setText(str)
                tv_customer!!.setText(title.get(position).custmer)
                tv_description!!.setText(title.get(position).description)
                //tv_promisedate!!.setText(title.get(position).promise_date)
                tv_promisedate!!.setText(str_promise)
                tv_cricstartdate!!.setText(title.get(position).crit_start_date)
                tv_duetime!!.setText(title.get(position).due_time)
                tv_lastactcode!!.setText(title.get(position).last_act_code)
                tv_scheduled!!.setText(title.get(position).scheduled)

                tv_papername!!.setText(title.get(position).paper_name)
                if(title.get(position).paper_name.equals("Other")){
                    ll_paper_type_based_id.visibility=View.GONE
                }else{
                    ll_paper_type_based_id.visibility=View.VISIBLE
                }
                tv_txtname!!.setText(title.get(position).text_name)
                tv_subtxtname!!.setText(title.get(position).sub_text_name)
                tv_weight!!.setText(title.get(position).weight)
                tv_size!!.setText(title.get(position).height + "*" + title.get(position).width)
                tv_allotedsheet!!.setText(title.get(position).alloted_sheets)
                tv_mailqty!!.setText(title.get(position).mailing_qty)
                tv_noterms!!.setText(title.get(position).no_of_terms)

                tv_jobpart_detail!!.setText("Job Part :" + title.get(position).job_part)
                tv_job_id_detail!!.setText(title.get(position).job_id)


                // Log.w("TERMSSIZE", "Result : " +terms.size)
                val result_terms = terms.get(title.get(position).job_part)
                val result_terms_date = term_date.get(title.get(position).job_part)

                val shipping_list = shipping.get(title.get(position).job_part)
                val shipping_date_list = shipping_date.get(title.get(position).job_part)

                val delivery_list = delivery.get(title.get(position).job_part)
                val delivery_date_list = delivery_date.get(title.get(position).job_part)

                Log.d("count",shipping_list!!.size.toString() + "====" + delivery_list!!.size.toString())

                val shipping_count:String = shipping_list!!.size.toString()
                val delivery_count:String = delivery_list!!.size.toString()


                if (result_terms!!.size > 0) {
                    ll_date_terms_value!!.removeAllViews()
                    for (k in 0 until result_terms!!.size) {
                        layout_date_terms!!.visibility=View.VISIBLE
                        val test2 = term_date.get(title.get(position).job_part)
                        //ll_date_terms_value!!.removeAllViews()
                        ll2 = LinearLayout(context)
                        ll2!!.setOrientation(LinearLayout.VERTICAL)
                        val layout2 = LayoutInflater.from(context).inflate(R.layout.layout_date_terms, ll_date_terms_value, false)

                        val tv_date_value = layout2.findViewById(R.id.tv_date_value) as TextView
                        val tv_date_term_value = layout2.findViewById(R.id.tv_date_term_value) as TextView

                        tv_date_value.setText(result_terms[k])
                        //tv_date_term_value.setText(result_terms_date!![k])

                        val inputPattern_mailing = "yyyy-MM-dd"
                        val outputPattern_mailing = "MM/dd/yyyy"
                        val inputFormat_mailing = SimpleDateFormat(inputPattern_mailing)
                        val outputFormat_mailing = SimpleDateFormat(outputPattern_mailing)
                        var date_mailing: Date? = null
                        var str_mailing: String? = null
                        Log.e("dates_times_drops", result_terms_date!![k].toString())
                        try {
                            date_mailing = inputFormat_mailing.parse(result_terms_date!![k].toString())
                            str_mailing = outputFormat_mailing.format(date_mailing)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        tv_date_term_value.setText(str_mailing)
                        Log.e("dates_times_ll_drops", str_mailing)
                        ll2!!.addView(layout2)
                        ll_date_terms_value!!.addView(ll2)
                    }
                } else {

                    ll_date_terms_value!!.removeAllViews()
                    layout_date_terms!!.visibility = View.GONE
                }

                // for shipping
                if (shipping_count.equals("") || shipping_count.isEmpty()){
                    layout_shipping_drops!!.visibility = View.GONE
                    ll_row_shipping!!.visibility = View.VISIBLE
                    tv_noterms_shipping!!.setText("0")
                }else{
                    if (shipping_list!!.size > 0) {
//                        ll_layout_shipping!!.visibility = View.VISIBLE
                        layout_shipping_drops!!.visibility = View.VISIBLE
                        tv_noterms_shipping!!.setText(shipping_count)

                        ll_layout_shipping_value!!.removeAllViews()
                        for (i in 0 until shipping_list.size) {
                            ll_shipping = LinearLayout(context)
                            ll_shipping!!.orientation = LinearLayout.VERTICAL
                            val layoutView = LayoutInflater.from(context).inflate(R.layout.layout_shipping_terms, ll_layout_shipping_value, false)

                            val tv_shipping_value: TextView = layoutView.findViewById(R.id.tv_shipping_value)
                            val tv_shipping_term_value: TextView = layoutView.findViewById(R.id.tv_shipping_term_value)

                            tv_shipping_value.setText(shipping_list.get(i))

                           // tv_shipping_term_value.setText(shipping_date_list!!.get(i))

                            val inputPattern1 = "yyyy-MM-dd"
                            val outputPattern1 = "MM/dd/yyyy"
                            val inputFormat1 = SimpleDateFormat(inputPattern1)
                            val outputFormat1 = SimpleDateFormat(outputPattern1)
                            var date: Date? = null
                            var termsdate_val: String? = null
                            try {
                                date = inputFormat1.parse(shipping_date_list!!.get(i))
                                termsdate_val = outputFormat1.format(date)
                            } catch (e: ParseException) {
                                e.printStackTrace()
                            }
                            Log.e("dates_shipp_drops_lll", shipping_date_list!!.get(i)+"---"+termsdate_val)
                            tv_shipping_term_value.setText(termsdate_val)

                            ll_shipping!!.addView(layoutView)
                            ll_layout_shipping_value!!.addView(ll_shipping)
                        }
                    } else{
                        ll_layout_shipping_value!!.removeAllViews()
                        layout_shipping_drops!!.visibility = View.GONE
                        ll_row_shipping!!.visibility = View.VISIBLE
                        tv_noterms_shipping!!.setText("0")
                    }
                }

                // for delivery
                if (delivery_count.equals("") || delivery_count.isEmpty()){
                    //ll_layout_delivery!!.visibility = View.GONE
                    layout_delivery_drops!!.visibility = View.GONE
                    ll_row_delivery!!.visibility = View.VISIBLE
                    tv_noterms_delivery!!.setText("0")
                }else{
                    if (delivery_list!!.size > 0) {
//                        ll_layout_delivery!!.visibility = View.VISIBLE
                        layout_delivery_drops!!.visibility = View.VISIBLE
                        tv_noterms_delivery!!.setText(delivery_count)
                        ll_layout_delivery_value!!.removeAllViews()
                        for (i in 0 until delivery_list.size) {
                            ll_delivery = LinearLayout(context)
                            ll_delivery!!.orientation = LinearLayout.VERTICAL
                            val lView = LayoutInflater.from(context).inflate(R.layout.layout_delivery_terms, ll_layout_delivery_value, false)

                            val tv_shipping_value: TextView = lView.findViewById(R.id.tv_delivery_value)
                            val tv_shipping_term_value: TextView = lView.findViewById(R.id.tv_delivery_term_value)

                            tv_shipping_value.setText(delivery_list.get(i))

                            //tv_shipping_term_value.setText(delivery_date_list!!.get(i))

                            val inputPattern_mailing = "yyyy-MM-dd"
                            val outputPattern_mailing = "MM/dd/yyyy"
                            val inputFormat_mailing = SimpleDateFormat(inputPattern_mailing)
                            val outputFormat_mailing = SimpleDateFormat(outputPattern_mailing)
                            var date_mailing: Date? = null
                            var str_mailing: String? = null
                            try {
                                date_mailing = inputFormat_mailing.parse(delivery_date_list!![i].toString())
                                str_mailing = outputFormat_mailing.format(date_mailing)
                            } catch (e: ParseException) {
                                e.printStackTrace()
                            }


                            tv_shipping_term_value.setText(str_mailing)

                            ll_delivery!!.addView(lView)
                            ll_layout_delivery_value!!.addView(ll_delivery)
                        }
                    } else{
                        ll_layout_delivery_value!!.removeAllViews()
                        layout_delivery_drops!!.visibility = View.GONE
                        ll_row_delivery!!.visibility = View.VISIBLE
                        tv_noterms_delivery!!.setText("0")
                    }
                }

                val result_dep = departments.get(title.get(position).job_part)
                if (result_dep!!.size > 0) {
                    for (k in 0 until result_dep!!.size) {

                        Log.w("LISTDEPT", "Result : " + result_dep.get(k).department_name)
                        val contact = Contact(result_dep.get(k).user_name.toString(), result_dep.get(k).department_name.toString(), result_dep.get(k).job_dep_id.toString(),
                                result_dep.get(k).status.toString(), result_dep.get(k).department_id.toString(), result_dep.get(k).job_id.toString(), result_dep.get(k).job_id.toString(), result_dep.get(k).assigned_id.toString(), result_dep.get(k).assigned_status.toString(), false,result_dep.get(k).commentcount.toString(),result_dep.get(k).comment_view_status.toString(),result_dep.get(k).start_date.toString(),result_dep.get(k).end_date.toString())
                        contacts.add(contact)
                    }
                   /* val adapter1 = CustomAdapter(this@JobDetailsFullList, contacts, id)
                    listview_department1!!.adapter = adapter1
                    Helper.getListViewSize(listview_department1)
                    adapter1.notifyDataSetChanged()*/


                    rv_dep_list_id!!.setHasFixedSize(true)
                    val layoutManager = LinearLayoutManager(this@JobDetailsFullList);
                    rv_dep_list_id!!.setLayoutManager(layoutManager)
                    rv_dep_list_id!!.setItemAnimator(DefaultItemAnimator())
                    val details_adapter = DataAdapter(this@JobDetailsFullList, contacts, id)
                    rv_dep_list_id!!.setAdapter(details_adapter)
                    details_adapter.notifyDataSetChanged()

                }

            }


            contacts.clear()
            /* if(arrowdownup == false){
                 holder.tv_list_text_id.setBackgroundColor(resources.getColor(R.color.cyan_color))
                 arrowdownup = true
             }else{
                 holder.tv_list_text_id.setBackgroundColor(resources.getColor(R.color.yellow))
                 arrowdownup = false

             }*/

            val inputPattern = "yyyy-MM-dd HH:mm:ss"
            val outputPattern = "MM/dd/yyyy h:mm a"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var datepromise: Date? = null
            var str: String? = null
            var str_promise: String? = null
            Log.e("dates_times", title.get(position).earliest_start_date.toString())
            try {
                date = inputFormat.parse(title.get(0).earliest_start_date.toString())
                datepromise = inputFormat.parse(title.get(0).promise_date.toString())
                str = outputFormat.format(date)
                str_promise = outputFormat.format(datepromise)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            Log.e("dates_times_lll", str)

            tv_id!!.setText(title.get(0).id)
            tv_job_id!!.setText(title.get(0).job_id)
            tv_job_name!!.setText(title.get(0).job_name)
            tv_jobpart!!.setText(title.get(0).job_part)
            tv_runtime!!.setText(title.get(0).run_time)
            tv_totaltime!!.setText(title.get(0).total_time)
            tv_makereadytime!!.setText(title.get(position).make_time)
            tv_jobtitle_id!!.setText(title.get(0).job_title)
            tv_job_desc_id!!.setText(title.get(0).job_description)
            tv_plannedactivity!!.setText(title.get(0).planeed_activity)
            tv_earliestdate!!.setText(str)
            tv_customer!!.setText(title.get(0).custmer)
            tv_description!!.setText(title.get(0).description)
            tv_promisedate!!.setText(str_promise)
            tv_cricstartdate!!.setText(title.get(0).crit_start_date)
            tv_duetime!!.setText(title.get(0).due_time)
            tv_lastactcode!!.setText(title.get(0).last_act_code)
            tv_scheduled!!.setText(title.get(0).scheduled)

            tv_papername!!.setText(title.get(0).paper_name)
            if(title.get(0).paper_name.equals("Other")){
                ll_paper_type_based_id.visibility=View.GONE
            }else{
                ll_paper_type_based_id.visibility=View.VISIBLE
            }
            tv_txtname!!.setText(title.get(0).text_name)
            tv_subtxtname!!.setText(title.get(0).sub_text_name)
            tv_weight!!.setText(title.get(0).weight)
            tv_size!!.setText(title.get(0).height + "*" + title.get(0).width)
            tv_allotedsheet!!.setText(title.get(0).alloted_sheets)
            tv_mailqty!!.setText(title.get(0).mailing_qty)
            tv_noterms!!.setText(title.get(0).no_of_terms)

            // Log.w("TERMSSIZE", "Result : " +terms.size)
            val result_terms = terms.get(title.get(0).job_part)
            val result_terms_date = term_date.get(title.get(0).job_part)

            val shipping_list = shipping.get(title.get(0).job_part)
            val shipping_date_list = shipping_date.get(title.get(0).job_part)

            val delivery_list = delivery.get(title.get(0).job_part)
            val delivery_date_list = delivery_date.get(title.get(0).job_part)

            Log.d("count",shipping_list!!.size.toString() + "====" + delivery_list!!.size.toString())

            val shipping_count:String = shipping_list!!.size.toString()
            val delivery_count:String = delivery_list!!.size.toString()

            if (result_terms!!.size > 0) {
                ll_date_terms_value!!.removeAllViews()
                for (k in 0 until result_terms!!.size) {
                    val test2 = term_date.get(title.get(0).job_part)
                    //ll_date_terms_value!!.removeAllViews()
                    ll2 = LinearLayout(context)
                    ll2!!.setOrientation(LinearLayout.VERTICAL)
                    val layout2 = LayoutInflater.from(context).inflate(R.layout.layout_date_terms, ll_date_terms_value, false)

                    val tv_date_value = layout2.findViewById(R.id.tv_date_value) as TextView
                    val tv_date_term_value = layout2.findViewById(R.id.tv_date_term_value) as TextView

                    tv_date_value.setText(result_terms[k])
                    //tv_date_term_value.setText(result_terms_date!![k])

                    val inputPattern_mailing = "yyyy-MM-dd"
                    val outputPattern_mailing = "MM/dd/yyyy"
                    val inputFormat_mailing = SimpleDateFormat(inputPattern_mailing)
                    val outputFormat_mailing = SimpleDateFormat(outputPattern_mailing)
                    var date_mailing: Date? = null
                    var str_mailing: String? = null
                    Log.e("dates_times_drops", result_terms_date!![k].toString())
                    try {
                        date_mailing = inputFormat_mailing.parse(result_terms_date!![k].toString())
                        str_mailing = outputFormat_mailing.format(date_mailing)
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                    tv_date_term_value.setText(str_mailing)
                    Log.e("dates_times_ll_drops", str_mailing)
                    ll2!!.addView(layout2)
                    ll_date_terms_value!!.addView(ll2)
                }
            } else {

                ll_date_terms_value!!.removeAllViews()
                layout_date_terms!!.visibility = View.GONE
            }

            // for shipping
            if (shipping_count.equals("") || shipping_count.isEmpty()){
                layout_shipping_drops!!.visibility = View.GONE
                ll_row_shipping!!.visibility = View.VISIBLE
                tv_noterms_shipping!!.setText("0")
            }else{
                if (shipping_list!!.size > 0) {
//                        ll_layout_shipping!!.visibility = View.VISIBLE
                    layout_shipping_drops!!.visibility = View.VISIBLE
                    tv_noterms_shipping!!.setText(shipping_count)

                    ll_layout_shipping_value!!.removeAllViews()
                    for (i in 0 until shipping_list.size) {
                        ll_shipping = LinearLayout(context)
                        ll_shipping!!.orientation = LinearLayout.VERTICAL
                        val layoutView = LayoutInflater.from(context).inflate(R.layout.layout_shipping_terms, ll_layout_shipping_value, false)

                        val tv_shipping_value: TextView = layoutView.findViewById(R.id.tv_shipping_value)
                        val tv_shipping_term_value: TextView = layoutView.findViewById(R.id.tv_shipping_term_value)

                        tv_shipping_value.setText(shipping_list.get(i))

                        //tv_shipping_term_value.setText(shipping_date_list!!.get(i))
                        Log.e("dates_shipp_drops", shipping_date_list!!.get(i))
                        val inputPattern1 = "yyyy-MM-dd"
                        val outputPattern1 = "MM/dd/yyyy"
                        val inputFormat1 = SimpleDateFormat(inputPattern1)
                        val outputFormat1 = SimpleDateFormat(outputPattern1)
                        var date: Date? = null
                        var termsdate_val: String? = null
                        try {
                            date = inputFormat1.parse(shipping_date_list!!.get(i))
                            termsdate_val = outputFormat1.format(date)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        Log.e("dates_shipp_drops_lll", shipping_date_list!!.get(i)+"---"+termsdate_val)
                        tv_shipping_term_value.setText(termsdate_val)

                        ll_shipping!!.addView(layoutView)
                        ll_layout_shipping_value!!.addView(ll_shipping)
                    }
                } else{
                    ll_layout_shipping_value!!.removeAllViews()
                    layout_shipping_drops!!.visibility = View.GONE
                    ll_row_shipping!!.visibility = View.VISIBLE
                    tv_noterms_shipping!!.setText("0")

                }
            }

            // for delivery
            if (delivery_count.equals("") || delivery_count.isEmpty()){
                //ll_layout_delivery!!.visibility = View.GONE
                layout_delivery_drops!!.visibility = View.GONE
                ll_row_delivery!!.visibility = View.VISIBLE
                tv_noterms_delivery!!.setText("0")
            }else{
                if (delivery_list!!.size > 0) {
//                        ll_layout_delivery!!.visibility = View.VISIBLE
                    layout_delivery_drops!!.visibility = View.VISIBLE
                    tv_noterms_delivery!!.setText(delivery_count)
                    ll_layout_delivery_value!!.removeAllViews()
                    for (i in 0 until delivery_list.size) {
                        ll_delivery = LinearLayout(context)
                        ll_delivery!!.orientation = LinearLayout.VERTICAL
                        val lView = LayoutInflater.from(context).inflate(R.layout.layout_delivery_terms, ll_layout_delivery_value, false)

                        val tv_delivery_value: TextView = lView.findViewById(R.id.tv_delivery_value)
                        val tv_delivery_term_value: TextView = lView.findViewById(R.id.tv_delivery_term_value)

                        tv_delivery_value.setText(delivery_list.get(i))

                        //tv_delivery_term_value.setText(delivery_date_list!!.get(i))
                        val inputPattern_mailing = "yyyy-MM-dd"
                        val outputPattern_mailing = "MM/dd/yyyy"
                        val inputFormat_mailing = SimpleDateFormat(inputPattern_mailing)
                        val outputFormat_mailing = SimpleDateFormat(outputPattern_mailing)
                        var date_mailing: Date? = null
                        var str_mailing: String? = null
                        try {
                            date_mailing = inputFormat_mailing.parse(delivery_date_list!![i].toString())
                            str_mailing = outputFormat_mailing.format(date_mailing)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        tv_delivery_term_value.setText(str_mailing)
                        ll_delivery!!.addView(lView)
                        ll_layout_delivery_value!!.addView(ll_delivery)
                    }
                } else{
                    ll_layout_delivery_value!!.removeAllViews()
                    layout_delivery_drops!!.visibility = View.GONE
                    ll_row_delivery!!.visibility = View.VISIBLE
                    tv_noterms_delivery!!.setText("0")
                }
            }

            val result_dep = departments.get(title.get(0).job_part)
            if (result_dep!!.size > 0) {
                for (k in 0 until result_dep!!.size) {
                    Log.w("LISTDEPT", "Result : " + result_dep.get(k).department_name)
                    val contact = Contact(result_dep.get(k).user_name.toString(), result_dep.get(k).department_name.toString(), result_dep.get(k).job_dep_id.toString(),
                            result_dep.get(k).status.toString(), result_dep.get(k).department_id.toString(), result_dep.get(k).job_id.toString(), result_dep.get(k).job_id.toString(), result_dep.get(k).assigned_id.toString(), result_dep.get(k).assigned_status.toString(), false,result_dep.get(k).commentcount.toString(),result_dep.get(k).comment_view_status.toString(),result_dep.get(k).start_date.toString(),result_dep.get(k).end_date.toString())
                    contacts.add(contact)
                }
                /*val adapter1 = CustomAdapter(this@JobDetailsFullList, contacts, id)
                listview_department1!!.adapter = adapter1
                Helper.getListViewSize(listview_department1)
                adapter1.notifyDataSetChanged()*/

                rv_dep_list_id!!.setHasFixedSize(true)
                val layoutManager = LinearLayoutManager(this@JobDetailsFullList);
                rv_dep_list_id!!.setLayoutManager(layoutManager)
                rv_dep_list_id!!.setItemAnimator(DefaultItemAnimator())
                val details_adapter = DataAdapter(this@JobDetailsFullList, contacts, id)
                rv_dep_list_id!!.setAdapter(details_adapter)
                details_adapter.notifyDataSetChanged()
            }
        }

        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return title.size
        }
    }

    class ViewHolder2(view: View) : RecyclerView.ViewHolder(view) {

        val listitemlayout = view.findViewById(R.id.ll_department) as LinearLayout
        val tv_deptname = view.findViewById(R.id.tv_deptname) as TextView
        val tv_username = view.findViewById(R.id.tv_username) as TextView
        val tv_deptcount = view.findViewById(R.id.tv_deptcount) as CustomTextView
        val tv_status = view.findViewById(R.id.tv_status) as TextView
        val tv_startdate = view.findViewById(R.id.tv_startdate) as TextView
        val ll_startdate = view.findViewById(R.id.ll_startdate) as LinearLayout
        val ll_enddate = view.findViewById(R.id.ll_enddate) as LinearLayout
        val tv_enddate = view.findViewById(R.id.tv_enddate) as TextView
        val dept_edit = view.findViewById(R.id.dept_edit) as ImageView
        val dept_view = view.findViewById(R.id.dept_view) as ImageView
        // var userSelected = true;
    }

    inner class DataAdapter(private val mContext: Context, list: ArrayList<Contact>,val job_id:String) : RecyclerView.Adapter<ViewHolder2>() {
        val mList: ArrayList<Contact>? = list
        var context: Context? =mContext
        var jobid: String = job_id

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
            return ViewHolder2(LayoutInflater.from(context).inflate(R.layout.listitem_department, parent, false))
        }
        override fun onBindViewHolder(holder: ViewHolder2, position: Int) {
            val contact = mList!![position]
            val itemViewType = getItemViewType(position)
            holder.tv_deptcount.setText(contact.mCommentCount)
            if(contact.mCommentCountView.equals("1")){
                holder.tv_deptcount.setBackgroundResource(R.drawable.circle_ic_notviewed)
            }

            holder.tv_deptname.setText(contact.mDeptName)
            holder.tv_username.setText(contact.mUserName)
            holder.tv_status.setText(contact.mstatus)


            if(contact.mStartdate.equals(null)||contact.mStartdate.equals("null")){
                holder.ll_startdate.visibility = View.GONE
                holder.tv_startdate.setText("")
            }else{
                val inputPattern = "yyyy-MM-dd HH:mm:ss"
                val outputPattern = "MM/dd/yyyy h:mm a"
                val inputFormat = SimpleDateFormat(inputPattern)
                val outputFormat = SimpleDateFormat(outputPattern)

                var date: Date? = null
                var datepromise: Date? = null
                var str: String? = null
                var str_promise: String? = null
                Log.e("dates_times_STARTENDDATE", contact.mStartdate.toString())
                try {
                    date = inputFormat.parse(contact.mStartdate)
                    datepromise = inputFormat.parse(contact.mEnddate)
                    str = outputFormat.format(date)
                    str_promise = outputFormat.format(datepromise)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }

                holder.tv_startdate.setText(str)
            }

            if(contact.mEnddate.equals(null)||contact.mEnddate.equals("null")){
                holder.ll_enddate.visibility = View.GONE
                holder.tv_enddate.setText("")
            }else{
                val inputPattern = "yyyy-MM-dd HH:mm:ss"
                val outputPattern = "MM/dd/yyyy h:mm a"
                val inputFormat = SimpleDateFormat(inputPattern)
                val outputFormat = SimpleDateFormat(outputPattern)

                var date: Date? = null
                var datepromise: Date? = null
                var str: String? = null
                var str_promise: String? = null
                Log.e("dates_times_STARTENDDATE", contact.mStartdate.toString())
                try {
                    date = inputFormat.parse(contact.mStartdate)
                    datepromise = inputFormat.parse(contact.mEnddate)
                    str = outputFormat.format(date)
                    str_promise = outputFormat.format(datepromise)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                holder.tv_enddate.setText(str_promise)
            }


            if (contact.assign_status == "0") {
                holder.tv_status?.text = "Not Yet Assigned"
                holder.dept_edit.visibility = View.VISIBLE
                holder.dept_view.visibility = View.INVISIBLE
            }
            if (contact.assign_status == "1") {
                holder.tv_status?.text = "Not Started"
                holder.dept_edit.visibility = View.VISIBLE
                holder.dept_view.visibility = View.VISIBLE
            }
            if (contact.assign_status == "2") {
                holder.tv_status?.text = "Processing"
                holder.dept_edit.visibility = View.INVISIBLE
                holder.dept_view.visibility = View.VISIBLE
            }
            if (contact.assign_status == "3") {
                holder.tv_status?.text = "Completed"
                holder.dept_edit.visibility = View.INVISIBLE
                holder.dept_view.visibility = View.VISIBLE
            }
            /* dept_edit.setOnClickListener { View.OnClickListener {
            } }
            dept_view.setOnClickListener { View.OnClickListener {
            } }*/

            holder.listitemlayout.setOnClickListener {
                Log.e("listitemlayout", contact.mdeptJobid)

            }
            holder.dept_edit.setOnClickListener {

                Log.e("dept_edit", contact.job_name+"--"+jobid)
                val intent = Intent(context, AssignjobEditActivity::class.java)
                intent.putExtra("deptname", contact.mDeptName)
                intent.putExtra("deptjobid", contact.mdeptJobid)
                intent.putExtra("assignedid", contact.mDepartmentId)
                intent.putExtra("jobname", contact.job_name)
                intent.putExtra("passingjobid", jobid)
                intent.putExtra("usertextdata", holder.tv_username.text.toString())
                context!!.startActivity(intent)

            }
            holder.dept_view.setOnClickListener {

                Log.e("dept_view", contact.mdeptJobid)
                Log.e("dept_edit", contact.mdeptJobid)
                val intent = Intent(context, AssignedViewJob::class.java)
                intent.putExtra("deptname", contact.mDeptName)
                intent.putExtra("deptjobid", contact.mdeptJobid)
                intent.putExtra("assignedid", contact.mDepartmentId)
                intent.putExtra("assignedid", contact.mDepartmentId)
                intent.putExtra("passingjobid", jobid)
                context!!.startActivity(intent)

            }
        }
        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return mList!!.size
        }
    }



}
