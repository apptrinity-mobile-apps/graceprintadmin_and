package indo.com.graceprintingadmin.Activities

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ListView
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Adapter.ListofjobsAdapter
import indo.com.graceprintingadmin.Model.APIResponse.GetJobListResponse
import indo.com.graceprintingadmin.Model.APIResponse.JobsListDataResponse
import indo.com.graceprintingadmin.R
import retrofit2.Call
import retrofit2.Callback

class ListofjobsActivity : AppCompatActivity(){


    internal lateinit var my_loader: Dialog
    private var listview_listofjobs: ListView? = null
    private var listview_department: RecyclerView? = null
    public var mServiceInfo: ArrayList<JobsListDataResponse> = ArrayList<JobsListDataResponse>()
    private var mServiceDetailsAdapter: ListofjobsAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listofjobs)


        val toolbar = findViewById(R.id.toolbar) as Toolbar
        listview_listofjobs = findViewById(R.id.listview_listofjobs) as ListView
        listview_department = findViewById(R.id.listview_department) as RecyclerView
        listview_department!!.visibility= View.GONE

       // var rootview = findViewById(R.id.ll_login_id) as LinearLayout

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        toolbar.setTitle("LIST OF JOBS")
        myloading()
        callgetJobsAPI()
    }


    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun callgetJobsAPI() {

        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getJobsList()
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<GetJobListResponse> {
            override fun onResponse(call: Call<GetJobListResponse>, response: retrofit2.Response<GetJobListResponse>?) {
                my_loader.dismiss()
                if (response != null) {

                    Log.w("Result_Order_details", response.body().toString())
                    if(response.body()!!.status.equals("1") && response.body()!!.jobList!=null) {
                        val list: Array<JobsListDataResponse>? = response.body()!!.jobList!!
                        for (item: JobsListDataResponse in list!!.iterator()) {
                            mServiceInfo.add(item)

                        }
                        setProductAdapter(mServiceInfo)

                        /*if (list.size == 0) {
                            no_service!!.text = "No Data Found"
                            no_service!!.visibility = View.VISIBLE
                        }*/

                    }else if(response.body()!!.status.equals("2")) {
                        /*no_service!!.text = "No Data Found"
                        no_service!!.visibility = View.VISIBLE*/
                    }

                }
            }

            override fun onFailure(call: Call<GetJobListResponse>, t: Throwable) {
                Log.w("Result_Order_details",t.toString())
            }
        })
    }



    private fun setProductAdapter(mServiceList: ArrayList<JobsListDataResponse>) {
        mServiceDetailsAdapter = ListofjobsAdapter(mServiceList, this)
        listview_listofjobs!!.adapter = mServiceDetailsAdapter
        mServiceDetailsAdapter!!.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}
