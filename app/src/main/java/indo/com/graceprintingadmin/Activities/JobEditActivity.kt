package indo.com.graceprintingadmin.Activities

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Helper.CustomTextView
import indo.com.graceprintingadmin.Helper.MultiSelectionSpinnerEdit
import indo.com.graceprintingadmin.Helper.SessionManager
import indo.com.graceprintingadmin.Model.APIResponse.*
import indo.com.graceprintingadmin.R
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class JobEditActivity : AppCompatActivity(), MultiSelectionSpinnerEdit.OnMultipleItemsSelectedListener, View.OnClickListener {
    override fun selectedIndices2(indices: LinkedList<Int>) {
    }

    override fun selectedStrings2(strings: LinkedList<String>) {
    }

    override fun selectedIndices3(indices: LinkedList<Int>) {
    }

    override fun selectedStrings3(strings: LinkedList<String>) {
    }

    override fun selectedIndices4(indices: LinkedList<Int>) {
    }

    override fun selectedStrings4(strings: LinkedList<String>) {
    }


    lateinit var admin_customer_id: AutoCompleteTextView
    lateinit var admin_job_type_id: EditText
    lateinit var job_title_one_id: EditText
    lateinit var job_desc_one_id: EditText
    lateinit var admin_job_id: EditText
    lateinit var admin_job_part_id: EditText
    lateinit var admin_Pactivity_id: EditText
    lateinit var admin_Estart_date_id: CustomTextView
    lateinit var admin_description_id: EditText
    lateinit var admin_Pdata_id: CustomTextView
    lateinit var admin_Estart_time_id: CustomTextView
    lateinit var admin_schedule_id: EditText
    lateinit var admin_Cstart_date_id: CustomTextView
    lateinit var admin_due_time_id: CustomTextView
    lateinit var admin_Lact_code_id: EditText
    lateinit var btn_submit_id: Button
    lateinit var admin_departments: MultiSelectionSpinnerEdit
    lateinit var rv_main_id: ListView
    lateinit var admin_tasks: EditText
    lateinit var list_items_list: ListView
    lateinit var list_items_list_type: ListView
    lateinit var dialog_list: Dialog
    lateinit var dialog_list_type: Dialog

    var admin_customer_stg: String? = null
    var admmin_job_title_stg: String? = null
    var job_desc_one_id_stg: String? = null
    var admin_job_type_stg: String? = null
    var admin_job_stg: String? = null
    var admin_job_part_stg: String? = null
    var admin_Pactivity_stg: String? = null
    var admin_Estart_date_stg: String? = null
    var admin_description_stg: String? = null
    var admin_Pdata_stg: String? = null
    var admin_Estart_time_stg: String? = null
    var admin_schedule_stg: String? = null
    var admin_Cstart_date_stg: String? = null
    var admin_due_time_stg: String? = null
    var admin_Lact_code_stg: String? = null
    var admin_tasks_stg: String? = null
    var mServiceInfo_Jobname = ArrayList<String>()
    var mServiceInfo_Jobname_id = ArrayList<JobTypeResponse>()
    var mDepartmentListArray = ArrayList<DepatListName>()
    var mDepartTasksArray = ArrayList<DeprtTaskList>()
    var mSingleArray = arrayOf<String>()
    //var mSingleArray = arrayOf<String>()
    var mSingleDepartArray = arrayOf<String>()
    var mSingleTasklistandCat = ArrayList<String>()
    var mSingleDepartIds = ArrayList<String>()
    var mSingleDepartDefaultIds = ArrayList<String>()
    var mDepartTasksnamesList = ArrayList<String>()

    var JobidList = ArrayList<String>()

    var job_nameList = ArrayList<String>()
    var cal = Calendar.getInstance()
    var dateofmonth: String = ""
    var monthofyear: String = ""
    var yearstg: String = ""
    var mJobType_id: String = ""
    var terms_dates: String = ""
    var terms_dates_shipping: String = ""
    var terms_dates_delivery: String = ""

    var mJob_id_main: String = ""
    var mJob_title_main: String = ""
    var mJob_description_main_part: String = ""
    var mJob_type_id_main: String = ""
    var mJob_part_main: String = ""
    var mJob_earliest_start_date_main: String = ""
    var mJob_custmer_main: String = ""
    var mJob_description_main: String = ""
    var mJob_promise_date_main: String = ""

    lateinit var dialog_recycler_view: Dialog
    lateinit var my_loader: Dialog


    lateinit var dialog_list_type2: Dialog
    lateinit var dialog_list_type3: Dialog
    lateinit var dialog_list_subtxt: Dialog
    lateinit var dialog_list_sizes: Dialog
    lateinit var dialog_list_option_type: Dialog
    lateinit var dialog_list_weights: Dialog
    lateinit var dialog_list_txt: Dialog


    lateinit var list_items_list2: ListView
    lateinit var list_items_list_type2: ListView
    lateinit var list_items_list_type3: ListView
    lateinit var list_items_list_type4: ListView
    lateinit var list_items_list_type5: ListView
    lateinit var list_items_optionlist_type1: ListView
    lateinit var list_items_list_type6: ListView
    lateinit var list_items_list_type3_txt: ListView


    var departselectedid_array = ArrayList<String>()
    var departselectedid_only = ""
    var tasksselectedid_only = ""

    var departOnlyids = ""
    var admin_id_main = ""
    var id: String = ""
    var mGetDepartmentList = ArrayList<EditJobDetailsDataDepartResponse>()
    var mGetDepartmentList_ids = ArrayList<String>()
    var mGetDepartmentList_names = ArrayList<String>()
    var departmentList_ass_status = ArrayList<String>()
    var mGetTasksList_names = ArrayList<String>()
    var mGetTasksList_ids = ArrayList<String>()
    var mGetDepartmentTasksList = ArrayList<EditJobDetailsDataTasksResponse>()
    var mSingleDepartpositionsarray = arrayOf<String>()
    internal var mChecked = SparseBooleanArray()

    var mIndexList = mutableListOf<Int>()

    var flag: Boolean = true
    var mTaskListarrayDefault = ArrayList<NewListCell>()

    var termsdate: String? = null
    var termsquantity: String? = null

    var termsdate_shipping: String? = null
    var termsquantity_shipping: String? = null

    var termsdate_delivery: String? = null
    var termsquantity_delivery: String? = null

    var mailing_qty: Int? = 0
    var shipping_qty: Int? = 0
    var delivery_qty: Int? = 0
    var mailing_qty_total: Int? = 0

    lateinit var ll_jobpart1: LinearLayout
    lateinit var ll_jobpart2: LinearLayout
    lateinit var ll_jobpart3: LinearLayout
    lateinit var ll_jobpart4: LinearLayout

    lateinit var btn_jobpart1: Button


    var sizelist: ArrayList<String> = ArrayList<String>()
    var weightlist: ArrayList<String> = ArrayList<String>()
    var papertypestringlist: ArrayList<String> = ArrayList<String>()
    var texttypelisttringlist: ArrayList<String> = ArrayList<String>()
    var subtexttypelisttringlist: ArrayList<String> = ArrayList<String>()
    var papertypelist: ArrayList<Papertypes> = ArrayList<Papertypes>()
    var texttypelist: ArrayList<Texttypes> = ArrayList<Texttypes>()
    var subtexttypelist: ArrayList<Subtexttypes> = ArrayList<Subtexttypes>()
    var sizelisttype: ArrayList<Sizes> = ArrayList<Sizes>()
    var optiontypelist: ArrayList<OptiondataListResponse> = ArrayList<OptiondataListResponse>()
    var optionliststringlist: ArrayList<String> = ArrayList<String>()
    var weightlisttype: ArrayList<Weights> = ArrayList<Weights>()


    //Mailing

    lateinit var ll_subterms1: LinearLayout
    lateinit var ll_subterms2: LinearLayout
    lateinit var ll_subterms3: LinearLayout
    lateinit var ll_subterms4: LinearLayout
    lateinit var ll_subterms5: LinearLayout
    lateinit var ll_subterms6: LinearLayout
    lateinit var ll_subterms7: LinearLayout
    lateinit var ll_subterms8: LinearLayout
    lateinit var ll_subterms9: LinearLayout
    lateinit var ll_subterms10: LinearLayout


    lateinit var ll_miling_four: LinearLayout
    lateinit var ll_shipping_four: LinearLayout
    lateinit var ll_delivery_terms_4: LinearLayout


    lateinit var admin_termssitem1: EditText
    lateinit var admin_termssitem2: EditText
    lateinit var admin_termssitem3: EditText
    lateinit var admin_termssitem4: EditText
    lateinit var admin_termssitem5: EditText
    lateinit var admin_termssitem6: EditText
    lateinit var admin_termssitem7: EditText
    lateinit var admin_termssitem8: EditText
    lateinit var admin_termssitem9: EditText
    lateinit var admin_termssitem10: EditText


    lateinit var admin_termsdate1: EditText
    lateinit var admin_termsdate2: EditText
    lateinit var admin_termsdate3: EditText
    lateinit var admin_termsdate4: EditText
    lateinit var admin_termsdate5: EditText
    lateinit var admin_termsdate6: EditText
    lateinit var admin_termsdate7: EditText
    lateinit var admin_termsdate8: EditText
    lateinit var admin_termsdate9: EditText
    lateinit var admin_termsdate10: EditText

    lateinit var admin_job_terms: EditText


    //Shipping

    lateinit var ll_ship_subterms_421: LinearLayout
    lateinit var ll_shipp_subterms422: LinearLayout
    lateinit var ll_shipp_subterms423: LinearLayout
    lateinit var ll_shipp_subterms424: LinearLayout
    lateinit var ll_shipp_subterms425: LinearLayout
    lateinit var ll_shipp_subterms426: LinearLayout
    lateinit var ll_shipp_subterms427: LinearLayout
    lateinit var ll_shipp_subterms428: LinearLayout
    lateinit var ll_shipp_subterms429: LinearLayout
    lateinit var ll_shipp_subterms4210: LinearLayout


    lateinit var admin_shipp_termssitem421: EditText
    lateinit var admin_shipp_termssitem422: EditText
    lateinit var admin_shipp_termssitem423: EditText
    lateinit var admin_shipp_termssitem424: EditText
    lateinit var admin_shipp_termssitem425: EditText
    lateinit var admin_shipp_termssitem426: EditText
    lateinit var admin_shipp_termssitem427: EditText
    lateinit var admin_shipp_termssitem428: EditText
    lateinit var admin_shipp_termssitem429: EditText
    lateinit var admin_shipp_termssitem4210: EditText


    lateinit var admin_shipp_termsdate421: EditText
    lateinit var admin_shipp_termsdate422: EditText
    lateinit var admin_shipp_termsdate423: EditText
    lateinit var admin_shipp_termsdate424: EditText
    lateinit var admin_shipp_termsdate425: EditText
    lateinit var admin_shipp_termsdate426: EditText
    lateinit var admin_shipp_termsdate427: EditText
    lateinit var admin_shipp_termsdate428: EditText
    lateinit var admin_shipp_termsdate429: EditText
    lateinit var admin_shipp_termsdate4210: EditText

    lateinit var admin_shipping_terms4: EditText


    //Delivery

    lateinit var ll_delivery_subterms_431: LinearLayout
    lateinit var ll_delivery_subterms432: LinearLayout
    lateinit var ll_delivery_subterms433: LinearLayout
    lateinit var ll_delivery_subterms434: LinearLayout
    lateinit var ll_delivery_subterms435: LinearLayout
    lateinit var ll_delivery_subterms436: LinearLayout
    lateinit var ll_delivery_subterms437: LinearLayout
    lateinit var ll_delivery_subterms438: LinearLayout
    lateinit var ll_delivery_subterms439: LinearLayout
    lateinit var ll_delivery_subterms4310: LinearLayout


    lateinit var admin_delivery_termssitem431: EditText
    lateinit var admin_delivery_termssitem432: EditText
    lateinit var admin_delivery_termssitem433: EditText
    lateinit var admin_delivery_termssitem434: EditText
    lateinit var admin_delivery_termssitem435: EditText
    lateinit var admin_delivery_termssitem436: EditText
    lateinit var admin_delivery_termssitem437: EditText
    lateinit var admin_delivery_termssitem438: EditText
    lateinit var admin_delivery_termssitem439: EditText
    lateinit var admin_delivery_termssitem4310: EditText


    lateinit var admin_delivery_termsdate431: EditText
    lateinit var admin_delivery_termsdate432: EditText
    lateinit var admin_delivery_termsdate433: EditText
    lateinit var admin_delivery_termsdate434: EditText
    lateinit var admin_delivery_termsdate435: EditText
    lateinit var admin_delivery_termsdate436: EditText
    lateinit var admin_delivery_termsdate437: EditText
    lateinit var admin_delivery_termsdate438: EditText
    lateinit var admin_delivery_termsdate439: EditText
    lateinit var admin_delivery_termsdate4310: EditText

    lateinit var admin_delivery_terms4: EditText


    lateinit var typeofpaper: EditText
    lateinit var txtname: EditText
    lateinit var subtxtname: EditText
    lateinit var weight: EditText
    lateinit var sizetxt: EditText
    lateinit var allotsheets: EditText
    lateinit var totalquantity: EditText


    var paperid: String = "0"
    var txtid: String = "0"
    var weightid: String = "0"
    var subtxtid: String = "0"
    var sizeid: String = "0"
    var option_id1: String = "0"
    var allotedsheets: String = "0"


    lateinit var customer_search_adapter: ArrayAdapter<String>
    var mCustomerNamesListArray = ArrayList<CustomersListName>()
    var mCustomerNamesArray = ArrayList<String>()
    var mCustomerIdsArray = ArrayList<String>()
    var customer_search_id: String = ""
    var aPosition: Int = 0

    lateinit var tv_availablesheets: CustomTextView
    lateinit var tv_availablesheets1: CustomTextView
    lateinit var tv_availablesheets2: CustomTextView
    lateinit var tv_availablesheets3: CustomTextView

    lateinit var tv_departments: TextView
    lateinit var tv_departments2: TextView
    lateinit var tv_departments3: TextView
    lateinit var tv_departments4: TextView

    lateinit var ll_txt_name: LinearLayout
    lateinit var ll_subtxt_name: LinearLayout
    lateinit var ll_weight: LinearLayout
    lateinit var ll_size: LinearLayout
    lateinit var ll_other_desc: LinearLayout
    lateinit var other_desc: EditText
    lateinit var admin_optionlist1: EditText


    var cat_status_array: ArrayList<String> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editjob)
        val session = SessionManager(this@JobEditActivity)
        val user: HashMap<String, String> = session.userDetails
        admin_id_main = user.get(SessionManager.ADMIN_ID)!!
        Log.e("admin_id_main_lll", admin_id_main)
        jobEditdata()
        dialog_list = Dialog(this)
        dialog_list.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list.setContentView(R.layout.dialog_view)
        dialog_list.setCancelable(true)
        list_items_list = dialog_list.findViewById(R.id.list_languages) as ListView
        list_items_list2 = dialog_list.findViewById(R.id.list_languages) as ListView

        dialog_list_type = Dialog(this)
        dialog_list_type.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_type.setContentView(R.layout.dialog_view)
        dialog_list_type.setCancelable(true)
        list_items_list_type = dialog_list_type.findViewById(R.id.list_languages) as ListView

        dialog_recycler_view = Dialog(this)
        dialog_recycler_view.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view.setCancelable(true)
        rv_main_id = dialog_recycler_view.findViewById(R.id.rv_main_id) as ListView
        val tv_cancel_id = dialog_recycler_view.findViewById(R.id.tv_cancel_id) as TextView
        val tv_submit_id = dialog_recycler_view.findViewById(R.id.tv_submit_id) as TextView

        tv_departments = findViewById(R.id.tv_departments) as TextView
        tv_departments2 = findViewById(R.id.tv_departments2) as TextView
        tv_departments3 = findViewById(R.id.tv_departments3) as TextView
        tv_departments4 = findViewById(R.id.tv_departments4) as TextView

        tv_departments.visibility = View.GONE
        tv_departments2.visibility = View.GONE
        tv_departments3.visibility = View.GONE
        tv_departments4.visibility = View.GONE


        ll_txt_name = findViewById(R.id.ll_txt_name)
        ll_subtxt_name = findViewById(R.id.ll_subtxt_name)
        ll_weight = findViewById(R.id.ll_weight)
        ll_size = findViewById(R.id.ll_size)
        ll_other_desc = findViewById(R.id.ll_other_desc)
        other_desc = findViewById(R.id.other_desc)

        tv_cancel_id.setOnClickListener {
            dialog_recycler_view.dismiss()
        }
        tv_submit_id.setOnClickListener {
            //seletedItems.add(name)
            departselectedid_only = ""
            tasksselectedid_only = ""
            Log.w("Selected", "kkkkkkkkkkkkkkkkkk" + departselectedid_only + ":" + tasksselectedid_only)
            tasksselectedid_only = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()

            val size = job_nameList.size
            Log.e("main_size", job_nameList.size.toString() + "--" + job_nameList.toString() + "---" + mSingleTasklistandCat.toString() + "--" + mSingleTasklistandCat.size)
            var appendSeparator = false
            var taslappend = false
            var jobappend = false

            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList.get(y))

                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat.get(y))
            }
            val al = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat.size) {
                hs.add(mSingleTasklistandCat.get(z).split("_").get(1))
            }
            al.addAll(hs)
            for (k in 0 until al.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al.get(k))
            }
            Log.e("hashset", hs.size.toString() + "--" + hs.toString())
            departselectedid_only = (jobid.toString())
            tasksselectedid_only = (idsb.toString())
            admin_tasks.setText(sb)
            println(sb)
            Log.w("Selected", "Afetr- kkkkkkkkkkkkkkkkkk" + departselectedid_only + ":" + tasksselectedid_only)
            dialog_recycler_view.dismiss()
        }
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        toolbar.setTitle("Edit Job")
        myloading()

        ll_jobpart1 = findViewById(R.id.ll_jobpart1)
        ll_jobpart1.visibility = View.GONE
        ll_jobpart2 = findViewById(R.id.ll_jobpart2)
        ll_jobpart3 = findViewById(R.id.ll_jobpart3)
        ll_jobpart4 = findViewById(R.id.ll_jobpart4)
        ll_jobpart4.visibility = View.VISIBLE


        admin_customer_id = findViewById(R.id.admin_customer_id) as AutoCompleteTextView
        admin_job_type_id = findViewById(R.id.admin_job_type_id) as EditText
        job_title_one_id = findViewById(R.id.job_title_four_id)
        job_desc_one_id = findViewById(R.id.job_desc_four_id) as EditText
        admin_job_id = findViewById(R.id.admin_job_id) as EditText
        admin_job_id.isFocusable = false
        admin_job_part_id = findViewById(R.id.admin_job_part_id) as EditText
        admin_Pactivity_id = findViewById(R.id.admin_Pactivity_id) as EditText
        admin_Estart_date_id = findViewById(R.id.admin_Estart_date_id) as CustomTextView
        admin_description_id = findViewById(R.id.admin_description_id) as EditText
        admin_Pdata_id = findViewById(R.id.admin_Pdata_id) as CustomTextView
        admin_Estart_time_id = findViewById(R.id.admin_Estart_time_id) as CustomTextView
        admin_schedule_id = findViewById(R.id.admin_schedule_id) as EditText
        admin_Cstart_date_id = findViewById(R.id.admin_Cstart_date_id) as CustomTextView
        admin_due_time_id = findViewById(R.id.admin_due_time_id) as CustomTextView
        admin_Lact_code_id = findViewById(R.id.admin_Lact_code_id) as EditText
        btn_submit_id = findViewById(R.id.btn_submit_id) as Button
        admin_departments = findViewById(R.id.admin_departments4)

        admin_departments.setEnabled(true)
        admin_departments.setClickable(true)


        admin_tasks = findViewById(R.id.admin_tasks4)

        tv_availablesheets = findViewById(R.id.tv_availablesheets) as CustomTextView
        tv_availablesheets1 = findViewById(R.id.tv_availablesheets1) as CustomTextView
        tv_availablesheets2 = findViewById(R.id.tv_availablesheets2) as CustomTextView
        tv_availablesheets3 = findViewById(R.id.tv_availablesheets3) as CustomTextView
        admin_tasks.setEnabled(true)
        admin_tasks.setClickable(true)
        admin_tasks.setOnClickListener {
            dialog_recycler_view.show()
        }
        val array = arrayOf("1", "2", "3", "4")


        admin_job_part_id.setOnClickListener {
            admin_job_part_id.isFocusable == false
            if (!dialog_list.isShowing())

                list_items_list.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                    val selected_name = list_items_list.getItemAtPosition(i).toString()
                    admin_job_part_id.setText(selected_name)
                    dialog_list.dismiss()

                })
        }
        admin_departments.setListener(this)

        CallDepartmentAPI()
        optionlistsdropdown()
        job_title_one_id.setText("name")

        //////////////////
        dialog_list_type2 = Dialog(this)
        dialog_list_type2.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_type2.setContentView(R.layout.dialog_view)
        dialog_list_type2.setCancelable(true)
        list_items_list_type2 = dialog_list_type2.findViewById(R.id.list_languages) as ListView


        dialog_list_type3 = Dialog(this)
        dialog_list_type3.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_type3.setContentView(R.layout.dialog_view)
        dialog_list_type3.setCancelable(true)
        list_items_list_type3_txt = dialog_list_type3.findViewById(R.id.list_languages) as ListView



        dialog_list_txt = Dialog(this)
        dialog_list_txt.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_txt.setContentView(R.layout.dialog_view)
        dialog_list_txt.setCancelable(true)
        list_items_list_type3 = dialog_list_txt.findViewById(R.id.list_languages) as ListView

        dialog_list_subtxt = Dialog(this)
        dialog_list_subtxt.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_subtxt.setContentView(R.layout.dialog_view)
        dialog_list_subtxt.setCancelable(true)
        list_items_list_type4 = dialog_list_subtxt.findViewById(R.id.list_languages) as ListView

        dialog_list_sizes = Dialog(this)
        dialog_list_sizes.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_sizes.setContentView(R.layout.dialog_view)
        dialog_list_sizes.setCancelable(true)
        list_items_list_type5 = dialog_list_sizes.findViewById(R.id.list_languages) as ListView

        dialog_list_option_type = Dialog(this)
        dialog_list_option_type.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_option_type.setContentView(R.layout.dialog_view)
        dialog_list_option_type.setCancelable(true)
        list_items_optionlist_type1 = dialog_list_option_type.findViewById(R.id.list_languages) as ListView


        dialog_list_weights = Dialog(this)
        dialog_list_weights.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_weights.setContentView(R.layout.dialog_view)
        dialog_list_weights.setCancelable(true)
        list_items_list_type6 = dialog_list_weights.findViewById(R.id.list_languages) as ListView


        //****************** Mailing ****************//////

        ll_subterms1 = findViewById(R.id.ll_subterms1) as LinearLayout
        ll_subterms2 = findViewById(R.id.ll_subterms2) as LinearLayout
        ll_subterms3 = findViewById(R.id.ll_subterms3) as LinearLayout
        ll_subterms4 = findViewById(R.id.ll_subterms4) as LinearLayout
        ll_subterms5 = findViewById(R.id.ll_subterms5) as LinearLayout
        ll_subterms6 = findViewById(R.id.ll_subterms6) as LinearLayout
        ll_subterms7 = findViewById(R.id.ll_subterms7) as LinearLayout
        ll_subterms8 = findViewById(R.id.ll_subterms8) as LinearLayout
        ll_subterms9 = findViewById(R.id.ll_subterms9) as LinearLayout
        ll_subterms10 = findViewById(R.id.ll_subterms10) as LinearLayout

        ll_miling_four = findViewById(R.id.ll_miling_four) as LinearLayout
        ll_shipping_four = findViewById(R.id.ll_shipping_four) as LinearLayout
        ll_delivery_terms_4 = findViewById(R.id.ll_delivery_terms_4) as LinearLayout

        ll_miling_four.visibility = View.GONE
        ll_shipping_four.visibility = View.GONE
        ll_delivery_terms_4.visibility = View.GONE

        admin_termssitem1 = findViewById(R.id.admin_termssitem1) as EditText
        admin_termssitem2 = findViewById(R.id.admin_termssitem2) as EditText
        admin_termssitem3 = findViewById(R.id.admin_termssitem3) as EditText
        admin_termssitem4 = findViewById(R.id.admin_termssitem4) as EditText
        admin_termssitem5 = findViewById(R.id.admin_termssitem5) as EditText
        admin_termssitem6 = findViewById(R.id.admin_termssitem6) as EditText
        admin_termssitem7 = findViewById(R.id.admin_termssitem7) as EditText
        admin_termssitem8 = findViewById(R.id.admin_termssitem8) as EditText
        admin_termssitem9 = findViewById(R.id.admin_termssitem9) as EditText
        admin_termssitem10 = findViewById(R.id.admin_termssitem10) as EditText



        admin_termsdate1 = findViewById(R.id.admin_termsdate1) as EditText
        admin_termsdate2 = findViewById(R.id.admin_termsdate2) as EditText
        admin_termsdate3 = findViewById(R.id.admin_termsdate3) as EditText
        admin_termsdate4 = findViewById(R.id.admin_termsdate4) as EditText
        admin_termsdate5 = findViewById(R.id.admin_termsdate5) as EditText
        admin_termsdate6 = findViewById(R.id.admin_termsdate6) as EditText
        admin_termsdate7 = findViewById(R.id.admin_termsdate7) as EditText
        admin_termsdate8 = findViewById(R.id.admin_termsdate8) as EditText
        admin_termsdate9 = findViewById(R.id.admin_termsdate9) as EditText
        admin_termsdate10 = findViewById(R.id.admin_termsdate10) as EditText


        admin_job_terms = findViewById(R.id.admin_job_terms) as EditText


        //****************** Shipping ****************//////

        ll_ship_subterms_421 = findViewById(R.id.ll_ship_subterms_421) as LinearLayout
        ll_shipp_subterms422 = findViewById(R.id.ll_shipp_subterms422) as LinearLayout
        ll_shipp_subterms423 = findViewById(R.id.ll_shipp_subterms423) as LinearLayout
        ll_shipp_subterms424 = findViewById(R.id.ll_shipp_subterms424) as LinearLayout
        ll_shipp_subterms425 = findViewById(R.id.ll_shipp_subterms425) as LinearLayout
        ll_shipp_subterms426 = findViewById(R.id.ll_shipp_subterms426) as LinearLayout
        ll_shipp_subterms427 = findViewById(R.id.ll_shipp_subterms427) as LinearLayout
        ll_shipp_subterms428 = findViewById(R.id.ll_shipp_subterms428) as LinearLayout
        ll_shipp_subterms429 = findViewById(R.id.ll_shipp_subterms429) as LinearLayout
        ll_shipp_subterms4210 = findViewById(R.id.ll_shipp_subterms4210) as LinearLayout



        admin_shipp_termssitem421 = findViewById(R.id.admin_shipp_termssitem421) as EditText
        admin_shipp_termssitem422 = findViewById(R.id.admin_shipp_termssitem422) as EditText
        admin_shipp_termssitem423 = findViewById(R.id.admin_shipp_termssitem423) as EditText
        admin_shipp_termssitem424 = findViewById(R.id.admin_shipp_termssitem424) as EditText
        admin_shipp_termssitem425 = findViewById(R.id.admin_shipp_termssitem425) as EditText
        admin_shipp_termssitem426 = findViewById(R.id.admin_shipp_termssitem426) as EditText
        admin_shipp_termssitem427 = findViewById(R.id.admin_shipp_termssitem427) as EditText
        admin_shipp_termssitem428 = findViewById(R.id.admin_shipp_termssitem428) as EditText
        admin_shipp_termssitem429 = findViewById(R.id.admin_shipp_termssitem429) as EditText
        admin_shipp_termssitem4210 = findViewById(R.id.admin_shipp_termssitem4210) as EditText



        admin_shipp_termsdate421 = findViewById(R.id.admin_shipp_termsdate421) as EditText
        admin_shipp_termsdate422 = findViewById(R.id.admin_shipp_termsdate422) as EditText
        admin_shipp_termsdate423 = findViewById(R.id.admin_shipp_termsdate423) as EditText
        admin_shipp_termsdate424 = findViewById(R.id.admin_shipp_termsdate424) as EditText
        admin_shipp_termsdate425 = findViewById(R.id.admin_shipp_termsdate425) as EditText
        admin_shipp_termsdate426 = findViewById(R.id.admin_shipp_termsdate426) as EditText
        admin_shipp_termsdate427 = findViewById(R.id.admin_shipp_termsdate427) as EditText
        admin_shipp_termsdate428 = findViewById(R.id.admin_shipp_termsdate428) as EditText
        admin_shipp_termsdate429 = findViewById(R.id.admin_shipp_termsdate429) as EditText
        admin_shipp_termsdate4210 = findViewById(R.id.admin_shipp_termsdate4210) as EditText


        admin_shipping_terms4 = findViewById(R.id.admin_shipping_terms4) as EditText


        //////****************** Delivery ****************//////

        ll_delivery_subterms_431 = findViewById(R.id.ll_delivery_subterms_431) as LinearLayout
        ll_delivery_subterms432 = findViewById(R.id.ll_delivery_subterms432) as LinearLayout
        ll_delivery_subterms433 = findViewById(R.id.ll_delivery_subterms433) as LinearLayout
        ll_delivery_subterms434 = findViewById(R.id.ll_delivery_subterms434) as LinearLayout
        ll_delivery_subterms435 = findViewById(R.id.ll_delivery_subterms435) as LinearLayout
        ll_delivery_subterms436 = findViewById(R.id.ll_delivery_subterms436) as LinearLayout
        ll_delivery_subterms437 = findViewById(R.id.ll_delivery_subterms437) as LinearLayout
        ll_delivery_subterms438 = findViewById(R.id.ll_delivery_subterms438) as LinearLayout
        ll_delivery_subterms439 = findViewById(R.id.ll_delivery_subterms439) as LinearLayout
        ll_delivery_subterms4310 = findViewById(R.id.ll_delivery_subterms4310) as LinearLayout



        admin_delivery_termssitem431 = findViewById(R.id.admin_delivery_termssitem431) as EditText
        admin_delivery_termssitem432 = findViewById(R.id.admin_delivery_termssitem432) as EditText
        admin_delivery_termssitem433 = findViewById(R.id.admin_delivery_termssitem433) as EditText
        admin_delivery_termssitem434 = findViewById(R.id.admin_delivery_termssitem434) as EditText
        admin_delivery_termssitem435 = findViewById(R.id.admin_delivery_termssitem435) as EditText
        admin_delivery_termssitem436 = findViewById(R.id.admin_delivery_termssitem436) as EditText
        admin_delivery_termssitem437 = findViewById(R.id.admin_delivery_termssitem437) as EditText
        admin_delivery_termssitem438 = findViewById(R.id.admin_delivery_termssitem438) as EditText
        admin_delivery_termssitem439 = findViewById(R.id.admin_delivery_termssitem439) as EditText
        admin_delivery_termssitem4310 = findViewById(R.id.admin_delivery_termssitem4310) as EditText



        admin_delivery_termsdate431 = findViewById(R.id.admin_delivery_termsdate431) as EditText
        admin_delivery_termsdate432 = findViewById(R.id.admin_delivery_termsdate432) as EditText
        admin_delivery_termsdate433 = findViewById(R.id.admin_delivery_termsdate433) as EditText
        admin_delivery_termsdate434 = findViewById(R.id.admin_delivery_termsdate434) as EditText
        admin_delivery_termsdate435 = findViewById(R.id.admin_delivery_termsdate435) as EditText
        admin_delivery_termsdate436 = findViewById(R.id.admin_delivery_termsdate436) as EditText
        admin_delivery_termsdate437 = findViewById(R.id.admin_delivery_termsdate437) as EditText
        admin_delivery_termsdate438 = findViewById(R.id.admin_delivery_termsdate438) as EditText
        admin_delivery_termsdate439 = findViewById(R.id.admin_delivery_termsdate439) as EditText
        admin_delivery_termsdate4310 = findViewById(R.id.admin_delivery_termsdate4310) as EditText


        admin_delivery_terms4 = findViewById(R.id.admin_delivery_terms4) as EditText


        typeofpaper = findViewById(R.id.admin_typeofpaper) as EditText
        txtname = findViewById(R.id.admin_txtname) as EditText
        subtxtname = findViewById(R.id.admin_subtxtname) as EditText
        weight = findViewById(R.id.admin_weight) as EditText
        sizetxt = findViewById(R.id.admin_size) as EditText
        admin_optionlist1 = findViewById(R.id.admin_optionlist1) as EditText
        allotsheets = findViewById(R.id.admin_allotedsheets) as EditText
        totalquantity = findViewById(R.id.admin_totalquantity) as EditText
        btn_jobpart1 = findViewById(R.id.btn_jobpart1) as Button
        btn_jobpart1.visibility = View.GONE


        //*********Mailing*******////
        admin_termsdate1.setOnClickListener(this)
        admin_termsdate2.setOnClickListener(this)
        admin_termsdate3.setOnClickListener(this)
        admin_termsdate4.setOnClickListener(this)
        admin_termsdate5.setOnClickListener(this)
        admin_termsdate6.setOnClickListener(this)
        admin_termsdate7.setOnClickListener(this)
        admin_termsdate8.setOnClickListener(this)
        admin_termsdate9.setOnClickListener(this)
        admin_termsdate10.setOnClickListener(this)


        //*********Shipping*******////
        admin_shipp_termsdate421.setOnClickListener {
            showDateTimePicker2(id = 1)
        }
        admin_shipp_termsdate422.setOnClickListener {
            showDateTimePicker2(id = 2)
        }
        admin_shipp_termsdate423.setOnClickListener {
            showDateTimePicker2(id = 3)
        }
        admin_shipp_termsdate424.setOnClickListener {
            showDateTimePicker2(id = 4)
        }
        admin_shipp_termsdate425.setOnClickListener {
            showDateTimePicker2(id = 5)
        }
        admin_shipp_termsdate426.setOnClickListener {
            showDateTimePicker2(id = 6)
        }
        admin_shipp_termsdate427.setOnClickListener {
            showDateTimePicker2(id = 7)
        }
        admin_shipp_termsdate428.setOnClickListener {
            showDateTimePicker2(id = 8)
        }
        admin_shipp_termsdate429.setOnClickListener {
            showDateTimePicker2(id = 9)
        }
        admin_shipp_termsdate4210.setOnClickListener {
            showDateTimePicker2(id = 10)
        }

        admin_delivery_termsdate431.setOnClickListener {
            showDateTimePicker3(id = 1)
        }
        admin_delivery_termsdate432.setOnClickListener {
            showDateTimePicker3(id = 2)
        }
        admin_delivery_termsdate433.setOnClickListener {
            showDateTimePicker3(id = 3)
        }
        admin_delivery_termsdate434.setOnClickListener {
            showDateTimePicker3(id = 4)
        }
        admin_delivery_termsdate435.setOnClickListener {
            showDateTimePicker3(id = 5)
        }
        admin_delivery_termsdate436.setOnClickListener {
            showDateTimePicker3(id = 6)
        }
        admin_delivery_termsdate437.setOnClickListener {
            showDateTimePicker3(id = 7)
        }
        admin_delivery_termsdate438.setOnClickListener {
            showDateTimePicker3(id = 8)
        }
        admin_delivery_termsdate439.setOnClickListener {
            showDateTimePicker3(id = 9)
        }
        admin_delivery_termsdate4310.setOnClickListener {
            showDateTimePicker3(id = 10)
        }


        //***********Mailing********/////

        val array2 = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        admin_job_terms.setOnClickListener {
            if (!dialog_list.isShowing())
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@JobEditActivity, android.R.layout.simple_spinner_item, array2)
            list_items_list2.setAdapter(state_array_adapter)
            // Log.e("state_aaaaa", state_value_array.size() + "");
            list_items_list2.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms.setText(selected_name)
                //displayDynamicLayout(selected_name)
                if (selected_name == "1") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.GONE
                    ll_subterms3.visibility = View.GONE
                    ll_subterms4.visibility = View.GONE
                    ll_subterms5.visibility = View.GONE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.GONE
                    ll_subterms4.visibility = View.GONE
                    ll_subterms5.visibility = View.GONE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.GONE
                    ll_subterms5.visibility = View.GONE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.GONE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.VISIBLE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.VISIBLE
                    ll_subterms8.visibility = View.VISIBLE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.VISIBLE
                    ll_subterms8.visibility = View.VISIBLE
                    ll_subterms9.visibility = View.VISIBLE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.VISIBLE
                    ll_subterms8.visibility = View.VISIBLE
                    ll_subterms9.visibility = View.VISIBLE
                    ll_subterms10.visibility = View.VISIBLE
                }
                termsquantity = ""
                termsdate = ""
                dialog_list.dismiss()

            })
        }


        //*******Shipping********//

        admin_shipping_terms4.setOnClickListener {
            if (!dialog_list.isShowing())
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@JobEditActivity, android.R.layout.simple_spinner_item, array2)
            list_items_list2.setAdapter(state_array_adapter)
            // Log.e("state_aaaaa", state_value_array.size() + "");
            list_items_list2.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms4.setText(selected_name)
                //displayDynamicLayout(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.GONE
                    ll_shipp_subterms423.visibility = View.GONE
                    ll_shipp_subterms424.visibility = View.GONE
                    ll_shipp_subterms425.visibility = View.GONE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.GONE
                    ll_shipp_subterms424.visibility = View.GONE
                    ll_shipp_subterms425.visibility = View.GONE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.GONE
                    ll_shipp_subterms425.visibility = View.GONE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.GONE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.VISIBLE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.VISIBLE
                    ll_shipp_subterms428.visibility = View.VISIBLE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.VISIBLE
                    ll_shipp_subterms428.visibility = View.VISIBLE
                    ll_shipp_subterms429.visibility = View.VISIBLE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.VISIBLE
                    ll_shipp_subterms428.visibility = View.VISIBLE
                    ll_shipp_subterms429.visibility = View.VISIBLE
                    ll_shipp_subterms4210.visibility = View.VISIBLE
                }
                termsquantity_shipping = ""
                termsdate_shipping = ""
                dialog_list.dismiss()

            })
        }


        //*******Delivery********//

        admin_delivery_terms4.setOnClickListener {
            if (!dialog_list.isShowing())
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@JobEditActivity, android.R.layout.simple_spinner_item, array2)
            list_items_list2.setAdapter(state_array_adapter)
            // Log.e("state_aaaaa", state_value_array.size() + "");
            list_items_list2.setOnItemClickListener({ adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms4.setText(selected_name)
                //displayDynamicLayout(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.GONE
                    ll_delivery_subterms433.visibility = View.GONE
                    ll_delivery_subterms434.visibility = View.GONE
                    ll_delivery_subterms435.visibility = View.GONE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.GONE
                    ll_delivery_subterms434.visibility = View.GONE
                    ll_delivery_subterms435.visibility = View.GONE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.GONE
                    ll_delivery_subterms435.visibility = View.GONE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.GONE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.VISIBLE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.VISIBLE
                    ll_delivery_subterms438.visibility = View.VISIBLE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.VISIBLE
                    ll_delivery_subterms438.visibility = View.VISIBLE
                    ll_delivery_subterms439.visibility = View.VISIBLE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.VISIBLE
                    ll_delivery_subterms438.visibility = View.VISIBLE
                    ll_delivery_subterms439.visibility = View.VISIBLE
                    ll_delivery_subterms4310.visibility = View.VISIBLE
                }
                termsquantity_delivery = ""
                termsdate_delivery = ""
                dialog_list.dismiss()

            })
        }
        typeofpaper.setOnClickListener {
            if (!dialog_list_type2.isShowing())
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@JobEditActivity, android.R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.setAdapter(state_array_adapter)
            list_items_list_type2.setOnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper.setText(selected_name)
                if (selected_name.equals("Other")) {
                    ll_txt_name.visibility = View.GONE
                    ll_subtxt_name.visibility = View.GONE
                    ll_weight.visibility = View.GONE
                    ll_size.visibility = View.GONE
                    ll_other_desc.visibility = View.VISIBLE
                } else {
                    ll_txt_name.visibility = View.VISIBLE
                    ll_subtxt_name.visibility = View.VISIBLE
                    ll_weight.visibility = View.VISIBLE
                    ll_size.visibility = View.VISIBLE
                    ll_other_desc.visibility = View.GONE
                }
                dialog_list_type2.dismiss()
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid = papertypelist.get(i).id!!

                        sizetxt.setText("")
                        weight.setText("")
                        subtxtname.setText("")
                        txtname.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }


            }
        }
        txtname.setOnClickListener {
            if (!dialog_list_type3.isShowing())
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@JobEditActivity, android.R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.setAdapter(state_array_adapter)
            list_items_list_type3_txt.setOnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid = texttypelist.get(i).id!!
                        sizetxt.setText("")
                        weight.setText("")
                        subtxtname.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }


            }
        }
        subtxtname.setOnClickListener {
            if (!dialog_list_subtxt.isShowing())
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@JobEditActivity, android.R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.setAdapter(state_array_adapter)
            list_items_list_type4.setOnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid = subtexttypelist.get(i).id!!

                        Log.w("AVAILABLESHEETS_ID_BEF", "*** " + sizeid + "----" + weightid + "---" + subtxtid + "---" + paperid + "---" + txtid)

                        sizetxt.setText("")
                        weight.setText("")
                        weightsizeAPi(subtxtid)
                        CallAvailableSheetsAPI(sizeid, weightid, subtxtid, paperid, txtid, "0")
                    }
                }

            }
        }

        sizetxt.setOnClickListener {
            if (!dialog_list_sizes.isShowing())
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@JobEditActivity, android.R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.setAdapter(state_array_adapter)
            list_items_list_type5.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt.setText(selected_name)
                Log.e("size id", sizelisttype.get(i).id)
                sizeid = sizelisttype.get(i).id!!
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).width!!.toString() + "*" + sizelisttype.get(i).height!!.toString()).equals(selected_name)) {
                        sizeid = sizelisttype.get(i).id!!

                        Log.w("AVAILABLESHEETS_ID_BEFORECALL", "*** " + sizeid + "----" + weightid + "---" + subtxtid + "---" + paperid + "---" + txtid)
                        CallAvailableSheetsAPI(sizeid, weightid, subtxtid, paperid, txtid, "0")
                    }
                }

            })
        }
        admin_optionlist1.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist1.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id1 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }

        weight.setOnClickListener {
            if (!dialog_list_weights.isShowing())
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@JobEditActivity, android.R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.setAdapter(state_array_adapter)
            list_items_list_type6.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid = weightlisttype.get(i).id!!

                        Log.w("AVAILABLESHEETS_ID_BEFORECALL", "*** " + sizeid + "----" + weightid + "---" + subtxtid + "---" + paperid + "---" + txtid)

                        sizetxt.setText("")
                        CallAvailableSheetsAPI(sizeid, weightid, subtxtid, paperid, txtid, "0")
                    }
                }

            })
        }

        ///////////

        btn_submit_id.setOnClickListener {


            admin_customer_stg = admin_customer_id.getText().toString()
            admin_job_type_stg = admin_job_type_id.getText().toString()
            admmin_job_title_stg = job_title_one_id.text.toString()
            job_desc_one_id_stg = job_desc_one_id.text.toString()
            admin_job_stg = admin_job_id.getText().toString()
            admin_job_part_stg = admin_job_part_id.getText().toString()
            admin_Pactivity_stg = admin_Pactivity_id.getText().toString()
            admin_Estart_date_stg = admin_Estart_date_id.getText().toString()
            admin_description_stg = admin_description_id.getText().toString()
            admin_Pdata_stg = admin_Pdata_id.getText().toString()
            admin_Estart_time_stg = admin_Estart_time_id.getText().toString()
            admin_schedule_stg = admin_schedule_id.getText().toString()
            admin_Cstart_date_stg = admin_Cstart_date_id.getText().toString()
            admin_due_time_stg = admin_due_time_id.getText().toString()
            admin_Lact_code_stg = admin_Lact_code_id.getText().toString()
            admin_tasks_stg = admin_tasks.getText().toString()


            //************Mailing*************////

            Log.e("admin_termsdate1", admin_termsdate1.text.toString() + "title  " + admmin_job_title_stg)
            termsdate = admin_termsdate1.text.toString() + "," + admin_termsdate2.text.toString() + "," + admin_termsdate3.text.toString() + "," +
                    "" + admin_termsdate4.text.toString() + "," + admin_termsdate5.text.toString() + "," + admin_termsdate6.text.toString() + "," + admin_termsdate7.text.toString() + "," +
                    "" + admin_termsdate8.text.toString() + "," + admin_termsdate9.text.toString() + "," + "" + admin_termsdate10.text.toString() + ","


            termsquantity = admin_termssitem1.text.toString() + "," + admin_termssitem2.text.toString() + "," + admin_termssitem3.text.toString() + "," +
                    "" + admin_termssitem4.text.toString() + "," + admin_termssitem5.text.toString() + "," + admin_termssitem6.text.toString() + "," + admin_termssitem7.text.toString() + "," +
                    "" + admin_termssitem8.text.toString() + "," + admin_termssitem9.text.toString() + "," + "" + admin_termssitem10.text.toString() + ","


//************* Shipping***/////////////////////

            Log.e("admin_shipp_termsdate421", admin_shipp_termsdate421.text.toString())
            termsdate_shipping = admin_shipp_termsdate421.text.toString() + "," + admin_shipp_termsdate422.text.toString() + "," + admin_shipp_termsdate423.text.toString() + "," +
                    "" + admin_shipp_termsdate424.text.toString() + "," + admin_shipp_termsdate425.text.toString() + "," + admin_shipp_termsdate426.text.toString() + "," + admin_shipp_termsdate427.text.toString() + "," +
                    "" + admin_shipp_termsdate428.text.toString() + "," + admin_shipp_termsdate429.text.toString() + "," + "" + admin_shipp_termsdate4210.text.toString() + ","


            termsquantity_shipping = admin_shipp_termssitem421.text.toString() + "," + admin_shipp_termssitem422.text.toString() + "," + admin_shipp_termssitem423.text.toString() + "," +
                    "" + admin_shipp_termssitem424.text.toString() + "," + admin_shipp_termssitem425.text.toString() + "," + admin_shipp_termssitem426.text.toString() + "," + admin_shipp_termssitem427.text.toString() + "," +
                    "" + admin_shipp_termssitem428.text.toString() + "," + admin_shipp_termssitem429.text.toString() + "," + "" + admin_shipp_termssitem4210.text.toString() + ","


            //************* Delivery***/////////////////////

            Log.e("admin_delivery_termsdate431", admin_delivery_termsdate431.text.toString())
            termsdate_delivery = admin_delivery_termsdate431.text.toString() + "," + admin_delivery_termsdate432.text.toString() + "," + admin_delivery_termsdate433.text.toString() + "," +
                    "" + admin_delivery_termsdate434.text.toString() + "," + admin_delivery_termsdate435.text.toString() + "," + admin_delivery_termsdate436.text.toString() + "," + admin_delivery_termsdate437.text.toString() + "," +
                    "" + admin_delivery_termsdate438.text.toString() + "," + admin_delivery_termsdate439.text.toString() + "," + "" + admin_delivery_termsdate4310.text.toString() + ","


            termsquantity_delivery = admin_delivery_termssitem431.text.toString() + "," + admin_delivery_termssitem432.text.toString() + "," + admin_delivery_termssitem433.text.toString() + "," +
                    "" + admin_delivery_termssitem434.text.toString() + "," + admin_delivery_termssitem435.text.toString() + "," + admin_delivery_termssitem436.text.toString() + "," + admin_delivery_termssitem437.text.toString() + "," +
                    "" + admin_delivery_termssitem438.text.toString() + "," + admin_delivery_termssitem439.text.toString() + "," + "" + admin_delivery_termssitem4310.text.toString() + ","


            ////////////*************Mailing************///////////////

            val termsdate = termsdate!!.split(",")
            val termsquantity = termsquantity!!.split(",")

            var termslist = ArrayList<String>()
            var termsdatelist = ArrayList<String>()

            for (i in 0 until termsdate.size) {
                if (termsdate.get(i) != "") {
                    termslist.add("\"" + termsdate.get(i) + "\"")
                }
            }
            for (i in 0 until termsquantity.size) {
                if (termsquantity.get(i) != "") {
                    termsdatelist.add("\"" + termsquantity.get(i) + "\"")
                }
            }
            for (i in 0 until termsquantity.size) {
                if (termsquantity.get(i) != "") {
                    mailing_qty = mailing_qty!!.toInt() + termsquantity.get(i).toInt()
                }
            }
            Log.w("terms ", ":::Qyab " + mailing_qty)
            terms_dates = "[" + termsdatelist.toString() + "," + termslist.toString() + "]"

            Log.w("terms ", ":::" + terms_dates)


            ////////////*************Shipping************///////////////

            val termsdate_shipping = termsdate_shipping!!.split(",")
            val termsquantity_shipping = termsquantity_shipping!!.split(",")

            var termslist_shippping = ArrayList<String>()
            var termsdatelist_shipping = ArrayList<String>()

            for (i in 0 until termsdate_shipping.size) {
                if (termsdate_shipping.get(i) != "") {
                    termslist_shippping.add("\"" + termsdate_shipping.get(i) + "\"")
                }
            }
            for (i in 0 until termsquantity_shipping.size) {
                if (termsquantity_shipping.get(i) != "") {
                    termsdatelist_shipping.add("\"" + termsquantity_shipping.get(i) + "\"")
                }
            }
            for (i in 0 until termsquantity_shipping.size) {
                if (termsquantity_shipping.get(i) != "") {
                    shipping_qty = shipping_qty!!.toInt() + termsquantity_shipping.get(i).toInt()
                }
            }
            Log.w("terms_shipping ", ":::Qyab " + shipping_qty)
            terms_dates_shipping = "[" + termsdatelist_shipping.toString() + "," + termslist_shippping.toString() + "]"

            Log.w("terms_shipping", ":::" + terms_dates_shipping)


            ////////////*************Delivery************///////////////

            val termsdate_delivery = termsdate_delivery!!.split(",")
            val termsquantity_delivery = termsquantity_delivery!!.split(",")

            var termslist_delivery = ArrayList<String>()
            var termsdatelist_delivery = ArrayList<String>()

            for (i in 0 until termsdate_delivery.size) {
                if (termsdate_delivery.get(i) != "") {
                    termslist_delivery.add("\"" + termsdate_delivery.get(i) + "\"")
                }
            }
            for (i in 0 until termsquantity_delivery.size) {
                if (termsquantity_delivery.get(i) != "") {
                    termsdatelist_delivery.add("\"" + termsquantity_delivery.get(i) + "\"")
                }
            }
            for (i in 0 until termsquantity_delivery.size) {
                if (termsquantity_delivery.get(i) != "") {
                    delivery_qty = delivery_qty!!.toInt() + termsquantity_delivery.get(i).toInt()
                }
            }
            Log.w("terms_delivery ", ":::Qyab " + delivery_qty)
            terms_dates_delivery = "[" + termsdatelist_delivery.toString() + "," + termslist_delivery.toString() + "]"

            Log.w("terms_delivery", ":::" + terms_dates_delivery)


            mailing_qty_total = mailing_qty!! + delivery_qty!! + shipping_qty!!

            Log.w("mailing_qty_total", ":::" + mailing_qty_total)



            if (allotsheets.text.toString() != "") {
                allotedsheets = allotsheets.text.toString()
            }


            if (hasValidCredentials()) {
                AddJobCallAPI()
            }
            Log.d("LOGIN_REQUEST_data", "" + departselectedid_only + "-----" + tasksselectedid_only)
        }


        val cStartdate = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            dateofmonth = dayOfMonth.toString()
            monthofyear = monthOfYear.toString()
            yearstg = year.toString()
            val myFormat = "MM/dd/yyyy hh:mm a" // mention the format you need

            val sdf = SimpleDateFormat(myFormat, Locale.UK)

            val date = sdf.format(cal.time)
            admin_Cstart_date_id.text = date

        }
        admin_Estart_date_id.setOnClickListener {
            showDateTimePicker(admin_Estart_date_id.id)
        }
        admin_Pdata_id.setOnClickListener {
            showDateTimePicker(admin_Pdata_id.id)
        }
        admin_Cstart_date_id.setOnClickListener {

            val datePickerDialog = DatePickerDialog(this@JobEditActivity, cStartdate, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
            datePickerDialog.datePicker.minDate = cal.getTimeInMillis()
            datePickerDialog.show()
        }



        admin_Estart_time_id.setOnClickListener {

            val mcurrentTime = Calendar.getInstance()
            val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
            val minute = mcurrentTime.get(Calendar.MINUTE)

            val mTimePicker = TimePickerDialog(this@JobEditActivity, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                admin_Estart_time_id.setText(selectedHour.toString() + ":" + selectedMinute)
            }, hour, minute, true)//Yes 24 hour time
            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }


        admin_job_type_id.setFocusable(false);
        admin_job_type_id.setFocusableInTouchMode(false);
        admin_job_type_id.setOnClickListener {
            if (!dialog_list_type.isShowing())
                dialog_list_type.show()


        }


        admin_due_time_id.setOnClickListener {
            val mcurrentTime = Calendar.getInstance()
            val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
            val minute = mcurrentTime.get(Calendar.MINUTE)

            val mTimePicker = TimePickerDialog(this@JobEditActivity, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                admin_due_time_id.setText(selectedHour.toString() + ":" + selectedMinute)
            }, hour, minute, true)//Yes 24 hour time
            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }
        mCustomerNamesArray = ArrayList()
        mCustomerIdsArray = ArrayList()

        CallgetCustomersAPI()


        customer_search_adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, mCustomerNamesArray)
        admin_customer_id.setOnItemClickListener { parent, view, position, id ->

            aPosition = mCustomerNamesArray.indexOf(customer_search_adapter.getItem(position).toString())
            Log.e("names_ids", aPosition.toString() + "--" + mCustomerIdsArray.get(aPosition))
            customer_search_id = mCustomerIdsArray.get(aPosition)

            Log.e("search_names_ids", customer_search_id)

            admin_customer_stg = customer_search_id
        }


    }

    private fun defaultSelectedData(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<NewListCell>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        //departselectedid_only = ""
        tasksselectedid_only = ""

        val size = mGetDepartmentList_ids.size
        Log.e("fffffffffff", "Dept-size" + mGetDepartmentList_ids.size)
        Log.e("fffffffffff", "Task-size" + mGetTasksList_ids.size)
        // Log.e("main_size", job_nameList.size.toString() + "--" + job_nameList.toString() + "---" + mSingleTasklistandCat.toString() + "--" + mSingleTasklistandCat.size)
        var appendSeparator = false
        var taslappend = false
        var jobappend = false

        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked.put(i, true)
                    job_nameList.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList.get(y))

            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat.get(y))


        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat.size) {
            hs.add(mSingleTasklistandCat.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        Log.e("hashset", hs.size.toString() + "--" + hs.toString())
        //departselectedid_only = (sb.toString())
        tasksselectedid_only = (idsb.toString())
        Log.e("fffffffffff", "Dept" + this.departselectedid_only + "|" + jobid)
        Log.e("fffffffffff", "task" + tasksselectedid_only + "|" + idsb)
        flag = false

    }


    private fun defaultSelectedDataEdit(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<NewListCell>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        //departselectedid_only = ""
        tasksselectedid_only = ""

        val size = mGetDepartmentList_ids.size
        Log.e("fffffffffff", "Dept-size" + mGetDepartmentList_ids.size)
        Log.e("fffffffffff", "Task-size" + mGetTasksList_ids.size)
        // Log.e("main_size", job_nameList.size.toString() + "--" + job_nameList.toString() + "---" + mSingleTasklistandCat.toString() + "--" + mSingleTasklistandCat.size)
        var appendSeparator = false
        var taslappend = false
        var jobappend = false

        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked.put(i, true)
                    job_nameList.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList.get(y))

            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat.get(y))



        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat.size) {
            hs.add(mSingleTasklistandCat.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        Log.e("hashset", hs.size.toString() + "--" + hs.toString())
        //departselectedid_only = (sb.toString())
        tasksselectedid_only = (idsb.toString())
        Log.e("fffffffffff", "Dept" + this.departselectedid_only + "|" + jobid)
        Log.e("fffffffffff", "task" + tasksselectedid_only + "|" + idsb)
        flag = false

    }

    override fun selectedIndices(indices: LinkedList<Int>) {
        val taskmain = ""
        mSingleTasklistandCat.clear()
        job_nameList.clear()

        admin_tasks.setText("")
        mChecked.clear()
        val mTaskListarray = ArrayList<NewListCell>()

        departselectedid_array.clear()
        var departlappend = false
        val departidssb = StringBuilder()

        // try {
        val mTaskslistids = ArrayList<String>()
        for (j in 0 until indices!!.size) {


            val mTaskslistfinal = ArrayList<String>()

            departselectedid_array.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            //cat_status_array!!.add(mDepartmentListArray.get(indices.get(j)).assigned_status.toString())
            print("---cat_status_array----$cat_status_array")
            Log.e("main_depart_lll", indices!!.size.toString() + "---" + mDepartmentListArray.get(j).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                mTaskslistids.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(NewListCell(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString(), cat_status_array.get(indices.get(j))))

                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(NewListCell(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString(), cat_status_array.get(indices.get(j))))

                }


            }

        }
        //  }catch (e:Exception){
        //    Log.e("catch_exp",e.message)
        //  }
        var selecteddepart = ""
        for (m in 0 until departselectedid_array.size) {
            Log.e("main_get_dep_ids", departselectedid_array!!.toString())

            if (m == 0)
                selecteddepart = departselectedid_array.get(m)
            else
                selecteddepart += "," + departselectedid_array.get(m)
        }
        departselectedid_only = selecteddepart.toString()
        Log.e("departselectedid__list", departselectedid_array.toString() + "---" + departselectedid_only)
        mTaskListarrayDefault = mTaskListarray
        val adapter = ListAdapterk(this@JobEditActivity, mTaskListarray, cat_status_array)
        rv_main_id.adapter = adapter

        defaultSelectedDataEdit(departselectedid_array, mTaskslistids, mTaskListarray)


    }

    override fun selectedStrings(strings: LinkedList<String>) {
        Toast.makeText(this, strings.toString(), Toast.LENGTH_LONG).show();

        if (strings.toString().contains("Mailing")) {
            ll_miling_four.visibility = View.VISIBLE
        } else {
            ll_miling_four.visibility = View.GONE
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_four.visibility = View.VISIBLE

        } else {
            ll_shipping_four.visibility = View.GONE
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_4.visibility = View.VISIBLE

        } else {
            ll_delivery_terms_4.visibility = View.GONE
        }

    }

    private fun hasValidCredentials(): Boolean {

        if (TextUtils.isEmpty(admin_customer_stg))
            admin_customer_id.setError("Customer Required")
        else if (TextUtils.isEmpty(admin_job_stg))
            admin_job_id.setError("Jobid Required")
        else if (TextUtils.isEmpty(admin_job_part_stg))
            admin_job_part_id.setError("Job Part Required")
        else if (TextUtils.isEmpty(admin_Estart_date_stg))
            admin_Estart_date_id.setError("StartDate Required")
        else if (TextUtils.isEmpty(admin_description_stg))
            admin_description_id.setError("Description Required")
        else if (TextUtils.isEmpty(admin_Pdata_stg))
            admin_Pdata_id.setError("Promisedata Required")
        else if (TextUtils.isEmpty(admin_tasks_stg))
            admin_tasks.setError("Please Select Departments and Tasks")
        else
            return true
        return false
    }

    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun GetJobDetailsAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.jobEditDetailView(job_main_id = id!!)
        Log.d("JOBMAINID", id)
        call.enqueue(object : Callback<EditjobDetailsResponce> {
            override fun onResponse(call: Call<EditjobDetailsResponce>, response: retrofit2.Response<EditjobDetailsResponce>?) {
                my_loader.dismiss()
                if (response != null) {

                    Log.w("Result_Address", "Result : " + response.body()!!.data)
                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {
                        val jobDATA: EditJobDetailsDataResponse? = response.body()!!.data!!
                        mJob_id_main = jobDATA!!.job_id!!
                        mJob_type_id_main = jobDATA!!.job_type_id!!
                        mJob_part_main = jobDATA!!.job_part!!
                        mJob_earliest_start_date_main = jobDATA!!.earliest_start_date!!
                        mJob_custmer_main = jobDATA!!.custmer!!
                        mJob_description_main = jobDATA!!.description!!
                        mJob_promise_date_main = jobDATA!!.promise_date!!

                        mJob_title_main = jobDATA!!.job_title!!
                        mJob_description_main_part = jobDATA!!.job_description!!

                        admin_customer_id.setText(mJob_custmer_main)
                        // admin_job_type_id.setText(mJob_type_id_main)
                        admin_job_id.setText(mJob_id_main)
                        admin_job_part_id.setText(mJob_part_main)
                        admin_Estart_date_id.setText(mJob_earliest_start_date_main)
                        admin_description_id.setText(mJob_description_main)
                        admin_Pdata_id.setText(mJob_promise_date_main)


                        job_title_one_id.setText(mJob_title_main)

                        job_desc_one_id.setText(mJob_description_main_part)
                        Log.e("job_title_name", mJob_description_main_part + "---" + mJob_title_main)

                        val paper_name = response.body()!!.data!!.paper_name


                        val text_name = response.body()!!.data!!.text_name
                        val sub_text_name = response.body()!!.data!!.sub_text_name
                        val weight2 = response.body()!!.data!!.weight
                        val size_id = response.body()!!.data!!.size_id
                        val alloted_sheets = response.body()!!.data!!.alloted_sheets
                        val mailing_qty = response.body()!!.data!!.mailing_qty
                        val no_of_terms = response.body()!!.data!!.no_of_terms
                        val terms = (response.body()!!.data!!.terms)
                        val no_of_shipping_terms = (response.body()!!.data!!.number_of_shipping_terms)
                        val no_of_delivery_terms = (response.body()!!.data!!.number_of_delivery_terms)
                        val shippingterms = (response.body()!!.data!!.shipping_terms)
                        val deliveryterms = (response.body()!!.data!!.delivery_terms)
                        val term_date = (response.body()!!.data!!.term_date)
                        val shipping_term_date = (response.body()!!.data!!.shipping_term_date)
                        val delivery_term_date = (response.body()!!.data!!.delivery_term_date)
                        val width = response.body()!!.data!!.width
                        val option_type = response.body()!!.data!!.type
                        val height = response.body()!!.data!!.height


                        // val job_title = response.body()!!.data!!.job_title
                        // val job_desc = response.body()!!.data!!.job_description


                        //*******Mailing Drops*****////////

                        admin_job_terms.setText(no_of_terms.toString())

                        if (no_of_terms == "1") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.GONE
                            ll_subterms3.visibility = View.GONE
                            ll_subterms4.visibility = View.GONE
                            ll_subterms5.visibility = View.GONE
                            ll_subterms6.visibility = View.GONE
                            ll_subterms7.visibility = View.GONE
                            ll_subterms8.visibility = View.GONE
                            ll_subterms9.visibility = View.GONE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "2") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.GONE
                            ll_subterms4.visibility = View.GONE
                            ll_subterms5.visibility = View.GONE
                            ll_subterms6.visibility = View.GONE
                            ll_subterms7.visibility = View.GONE
                            ll_subterms8.visibility = View.GONE
                            ll_subterms9.visibility = View.GONE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "3") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.VISIBLE
                            ll_subterms4.visibility = View.GONE
                            ll_subterms5.visibility = View.GONE
                            ll_subterms6.visibility = View.GONE
                            ll_subterms7.visibility = View.GONE
                            ll_subterms8.visibility = View.GONE
                            ll_subterms9.visibility = View.GONE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "4") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.VISIBLE
                            ll_subterms4.visibility = View.VISIBLE
                            ll_subterms5.visibility = View.GONE
                            ll_subterms6.visibility = View.GONE
                            ll_subterms7.visibility = View.GONE
                            ll_subterms8.visibility = View.GONE
                            ll_subterms9.visibility = View.GONE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "5") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.VISIBLE
                            ll_subterms4.visibility = View.VISIBLE
                            ll_subterms5.visibility = View.VISIBLE
                            ll_subterms6.visibility = View.GONE
                            ll_subterms7.visibility = View.GONE
                            ll_subterms8.visibility = View.GONE
                            ll_subterms9.visibility = View.GONE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "6") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.VISIBLE
                            ll_subterms4.visibility = View.VISIBLE
                            ll_subterms5.visibility = View.VISIBLE
                            ll_subterms6.visibility = View.VISIBLE
                            ll_subterms7.visibility = View.GONE
                            ll_subterms8.visibility = View.GONE
                            ll_subterms9.visibility = View.GONE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "7") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.VISIBLE
                            ll_subterms4.visibility = View.VISIBLE
                            ll_subterms5.visibility = View.VISIBLE
                            ll_subterms6.visibility = View.VISIBLE
                            ll_subterms7.visibility = View.VISIBLE
                            ll_subterms8.visibility = View.GONE
                            ll_subterms9.visibility = View.GONE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "8") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.VISIBLE
                            ll_subterms4.visibility = View.VISIBLE
                            ll_subterms5.visibility = View.VISIBLE
                            ll_subterms6.visibility = View.VISIBLE
                            ll_subterms7.visibility = View.VISIBLE
                            ll_subterms8.visibility = View.VISIBLE
                            ll_subterms9.visibility = View.GONE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "9") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.VISIBLE
                            ll_subterms4.visibility = View.VISIBLE
                            ll_subterms5.visibility = View.VISIBLE
                            ll_subterms6.visibility = View.VISIBLE
                            ll_subterms7.visibility = View.VISIBLE
                            ll_subterms8.visibility = View.VISIBLE
                            ll_subterms9.visibility = View.VISIBLE
                            ll_subterms10.visibility = View.GONE
                        } else if (no_of_terms == "10") {
                            ll_subterms1.visibility = View.VISIBLE
                            ll_subterms2.visibility = View.VISIBLE
                            ll_subterms3.visibility = View.VISIBLE
                            ll_subterms4.visibility = View.VISIBLE
                            ll_subterms5.visibility = View.VISIBLE
                            ll_subterms6.visibility = View.VISIBLE
                            ll_subterms7.visibility = View.VISIBLE
                            ll_subterms8.visibility = View.VISIBLE
                            ll_subterms9.visibility = View.VISIBLE
                            ll_subterms10.visibility = View.VISIBLE
                        }


                        for (k in 0 until terms!!.size) {
                            if (no_of_terms == "1") {
                                admin_termssitem1.setText(terms.get(0))
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                            } else if (no_of_terms == "2") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                            } else if (no_of_terms == "3") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                                admin_termssitem3.setText(terms.get(2).toString())
                                admin_termsdate3.setText(term_date.get(2).toString())
                            } else if (no_of_terms == "4") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                                admin_termssitem3.setText(terms.get(2).toString())
                                admin_termsdate3.setText(term_date.get(2).toString())
                                admin_termssitem4.setText(terms.get(3).toString())
                                admin_termsdate4.setText(term_date.get(3).toString())
                            } else if (no_of_terms == "5") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                                admin_termssitem3.setText(terms.get(2).toString())
                                admin_termsdate3.setText(term_date.get(2).toString())
                                admin_termssitem4.setText(terms.get(3).toString())
                                admin_termsdate4.setText(term_date.get(3).toString())
                                admin_termssitem5.setText(terms.get(4).toString())
                                admin_termsdate5.setText(term_date.get(4).toString())
                            } else if (no_of_terms == "6") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                                admin_termssitem3.setText(terms.get(2).toString())
                                admin_termsdate3.setText(term_date.get(2).toString())
                                admin_termssitem4.setText(terms.get(3).toString())
                                admin_termsdate4.setText(term_date.get(3).toString())
                                admin_termssitem5.setText(terms.get(4).toString())
                                admin_termsdate5.setText(term_date.get(4).toString())
                                admin_termssitem6.setText(terms.get(5).toString())
                                admin_termsdate6.setText(term_date.get(5).toString())
                            } else if (no_of_terms == "7") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                                admin_termssitem3.setText(terms.get(2).toString())
                                admin_termsdate3.setText(term_date.get(2).toString())
                                admin_termssitem4.setText(terms.get(3).toString())
                                admin_termsdate4.setText(term_date.get(3).toString())
                                admin_termssitem5.setText(terms.get(4).toString())
                                admin_termsdate5.setText(term_date.get(4).toString())
                                admin_termssitem6.setText(terms.get(5).toString())
                                admin_termsdate6.setText(term_date.get(5).toString())
                                admin_termssitem7.setText(terms.get(6).toString())
                                admin_termsdate7.setText(term_date.get(6).toString())
                            } else if (no_of_terms == "8") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                                admin_termssitem3.setText(terms.get(2).toString())
                                admin_termsdate3.setText(term_date.get(2).toString())
                                admin_termssitem4.setText(terms.get(3).toString())
                                admin_termsdate4.setText(term_date.get(3).toString())
                                admin_termssitem5.setText(terms.get(4).toString())
                                admin_termsdate5.setText(term_date.get(4).toString())
                                admin_termssitem6.setText(terms.get(5).toString())
                                admin_termsdate6.setText(term_date.get(5).toString())
                                admin_termssitem7.setText(terms.get(6).toString())
                                admin_termsdate7.setText(term_date.get(6).toString())
                                admin_termssitem8.setText(terms.get(7).toString())
                                admin_termsdate8.setText(term_date.get(7).toString())
                            } else if (no_of_terms == "9") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                                admin_termssitem3.setText(terms.get(2).toString())
                                admin_termsdate3.setText(term_date.get(2).toString())
                                admin_termssitem4.setText(terms.get(3).toString())
                                admin_termsdate4.setText(term_date.get(3).toString())
                                admin_termssitem5.setText(terms.get(4).toString())
                                admin_termsdate5.setText(term_date.get(4).toString())
                                admin_termssitem6.setText(terms.get(5).toString())
                                admin_termsdate6.setText(term_date.get(5).toString())
                                admin_termssitem7.setText(terms.get(6).toString())
                                admin_termsdate7.setText(term_date.get(6).toString())
                                admin_termssitem8.setText(terms.get(7).toString())
                                admin_termsdate8.setText(term_date.get(7).toString())
                                admin_termssitem9.setText(terms.get(8).toString())
                                admin_termsdate9.setText(term_date.get(8).toString())
                            } else if (no_of_terms == "10") {
                                admin_termssitem1.setText(terms.get(0).toString())
                                admin_termsdate1.setText(term_date!!.get(0).toString())
                                admin_termssitem2.setText(terms.get(1).toString())
                                admin_termsdate2.setText(term_date.get(1).toString())
                                admin_termssitem3.setText(terms.get(2).toString())
                                admin_termsdate3.setText(term_date.get(2).toString())
                                admin_termssitem4.setText(terms.get(3).toString())
                                admin_termsdate4.setText(term_date.get(3).toString())
                                admin_termssitem5.setText(terms.get(4).toString())
                                admin_termsdate5.setText(term_date.get(4).toString())
                                admin_termssitem6.setText(terms.get(5).toString())
                                admin_termsdate6.setText(term_date.get(5).toString())
                                admin_termssitem7.setText(terms.get(6).toString())
                                admin_termsdate7.setText(term_date.get(6).toString())
                                admin_termssitem8.setText(terms.get(7).toString())
                                admin_termsdate8.setText(term_date.get(7).toString())
                                admin_termssitem9.setText(terms.get(8).toString())
                                admin_termsdate9.setText(term_date.get(8).toString())
                                admin_termssitem10.setText(terms.get(9).toString())
                                admin_termsdate10.setText(term_date.get(9).toString())

                            }
                        }

                        //************Shipping Drops *****/////////////
                        admin_shipping_terms4.setText(no_of_shipping_terms.toString())

                        if (no_of_shipping_terms == "1") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.GONE
                            ll_shipp_subterms423.visibility = View.GONE
                            ll_shipp_subterms424.visibility = View.GONE
                            ll_shipp_subterms425.visibility = View.GONE
                            ll_shipp_subterms426.visibility = View.GONE
                            ll_shipp_subterms427.visibility = View.GONE
                            ll_shipp_subterms428.visibility = View.GONE
                            ll_shipp_subterms429.visibility = View.GONE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "2") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.GONE
                            ll_shipp_subterms424.visibility = View.GONE
                            ll_shipp_subterms425.visibility = View.GONE
                            ll_shipp_subterms426.visibility = View.GONE
                            ll_shipp_subterms427.visibility = View.GONE
                            ll_shipp_subterms428.visibility = View.GONE
                            ll_shipp_subterms429.visibility = View.GONE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "3") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.VISIBLE
                            ll_shipp_subterms424.visibility = View.GONE
                            ll_shipp_subterms425.visibility = View.GONE
                            ll_shipp_subterms426.visibility = View.GONE
                            ll_shipp_subterms427.visibility = View.GONE
                            ll_shipp_subterms428.visibility = View.GONE
                            ll_shipp_subterms429.visibility = View.GONE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "4") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.VISIBLE
                            ll_shipp_subterms424.visibility = View.VISIBLE
                            ll_shipp_subterms425.visibility = View.GONE
                            ll_shipp_subterms426.visibility = View.GONE
                            ll_shipp_subterms427.visibility = View.GONE
                            ll_shipp_subterms428.visibility = View.GONE
                            ll_shipp_subterms429.visibility = View.GONE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "5") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.VISIBLE
                            ll_shipp_subterms424.visibility = View.VISIBLE
                            ll_shipp_subterms425.visibility = View.VISIBLE
                            ll_shipp_subterms426.visibility = View.GONE
                            ll_shipp_subterms427.visibility = View.GONE
                            ll_shipp_subterms428.visibility = View.GONE
                            ll_shipp_subterms429.visibility = View.GONE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "6") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.VISIBLE
                            ll_shipp_subterms424.visibility = View.VISIBLE
                            ll_shipp_subterms425.visibility = View.VISIBLE
                            ll_shipp_subterms426.visibility = View.VISIBLE
                            ll_shipp_subterms427.visibility = View.GONE
                            ll_shipp_subterms428.visibility = View.GONE
                            ll_shipp_subterms429.visibility = View.GONE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "7") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.VISIBLE
                            ll_shipp_subterms424.visibility = View.VISIBLE
                            ll_shipp_subterms425.visibility = View.VISIBLE
                            ll_shipp_subterms426.visibility = View.VISIBLE
                            ll_shipp_subterms427.visibility = View.VISIBLE
                            ll_shipp_subterms428.visibility = View.GONE
                            ll_shipp_subterms429.visibility = View.GONE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "8") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.VISIBLE
                            ll_shipp_subterms424.visibility = View.VISIBLE
                            ll_shipp_subterms425.visibility = View.VISIBLE
                            ll_shipp_subterms426.visibility = View.VISIBLE
                            ll_shipp_subterms427.visibility = View.VISIBLE
                            ll_shipp_subterms428.visibility = View.VISIBLE
                            ll_shipp_subterms429.visibility = View.GONE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "9") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.VISIBLE
                            ll_shipp_subterms424.visibility = View.VISIBLE
                            ll_shipp_subterms425.visibility = View.VISIBLE
                            ll_shipp_subterms426.visibility = View.VISIBLE
                            ll_shipp_subterms427.visibility = View.VISIBLE
                            ll_shipp_subterms428.visibility = View.VISIBLE
                            ll_shipp_subterms429.visibility = View.VISIBLE
                            ll_shipp_subterms4210.visibility = View.GONE
                        } else if (no_of_shipping_terms == "10") {
                            ll_ship_subterms_421.visibility = View.VISIBLE
                            ll_shipp_subterms422.visibility = View.VISIBLE
                            ll_shipp_subterms423.visibility = View.VISIBLE
                            ll_shipp_subterms424.visibility = View.VISIBLE
                            ll_shipp_subterms425.visibility = View.VISIBLE
                            ll_shipp_subterms426.visibility = View.VISIBLE
                            ll_shipp_subterms427.visibility = View.VISIBLE
                            ll_shipp_subterms428.visibility = View.VISIBLE
                            ll_shipp_subterms429.visibility = View.VISIBLE
                            ll_shipp_subterms4210.visibility = View.VISIBLE
                        }



                        if (shippingterms != null) {
                            for (s in 0 until shippingterms!!.size) {
                                if (no_of_shipping_terms == "1") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0))
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                } else if (no_of_shipping_terms == "2") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                } else if (no_of_shipping_terms == "3") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                    admin_shipp_termssitem423.setText(shippingterms.get(2).toString())
                                    admin_shipp_termsdate423.setText(shipping_term_date.get(2).toString())
                                } else if (no_of_shipping_terms == "4") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                    admin_shipp_termssitem423.setText(shippingterms.get(2).toString())
                                    admin_shipp_termsdate423.setText(shipping_term_date.get(2).toString())
                                    admin_shipp_termssitem424.setText(shippingterms.get(3).toString())
                                    admin_shipp_termsdate424.setText(shipping_term_date.get(3).toString())
                                } else if (no_of_shipping_terms == "5") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                    admin_shipp_termssitem423.setText(shippingterms.get(2).toString())
                                    admin_shipp_termsdate423.setText(shipping_term_date.get(2).toString())
                                    admin_shipp_termssitem424.setText(shippingterms.get(3).toString())
                                    admin_shipp_termsdate424.setText(shipping_term_date.get(3).toString())
                                    admin_shipp_termssitem425.setText(shippingterms.get(4).toString())
                                    admin_shipp_termsdate425.setText(shipping_term_date.get(4).toString())
                                } else if (no_of_shipping_terms == "6") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                    admin_shipp_termssitem423.setText(shippingterms.get(2).toString())
                                    admin_shipp_termsdate423.setText(shipping_term_date.get(2).toString())
                                    admin_shipp_termssitem424.setText(shippingterms.get(3).toString())
                                    admin_shipp_termsdate424.setText(shipping_term_date.get(3).toString())
                                    admin_shipp_termssitem425.setText(shippingterms.get(4).toString())
                                    admin_shipp_termsdate425.setText(shipping_term_date.get(4).toString())
                                    admin_shipp_termssitem426.setText(shippingterms.get(5).toString())
                                    admin_shipp_termsdate426.setText(shipping_term_date.get(5).toString())
                                } else if (no_of_shipping_terms == "7") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                    admin_shipp_termssitem423.setText(shippingterms.get(2).toString())
                                    admin_shipp_termsdate423.setText(shipping_term_date.get(2).toString())
                                    admin_shipp_termssitem424.setText(shippingterms.get(3).toString())
                                    admin_shipp_termsdate424.setText(shipping_term_date.get(3).toString())
                                    admin_shipp_termssitem425.setText(shippingterms.get(4).toString())
                                    admin_shipp_termsdate425.setText(shipping_term_date.get(4).toString())
                                    admin_shipp_termssitem426.setText(shippingterms.get(5).toString())
                                    admin_shipp_termsdate426.setText(shipping_term_date.get(5).toString())
                                    admin_shipp_termssitem427.setText(shippingterms.get(6).toString())
                                    admin_shipp_termsdate427.setText(shipping_term_date.get(6).toString())
                                } else if (no_of_shipping_terms == "8") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                    admin_shipp_termssitem423.setText(shippingterms.get(2).toString())
                                    admin_shipp_termsdate423.setText(shipping_term_date.get(2).toString())
                                    admin_shipp_termssitem424.setText(shippingterms.get(3).toString())
                                    admin_shipp_termsdate424.setText(shipping_term_date.get(3).toString())
                                    admin_shipp_termssitem425.setText(shippingterms.get(4).toString())
                                    admin_shipp_termsdate425.setText(shipping_term_date.get(4).toString())
                                    admin_shipp_termssitem426.setText(shippingterms.get(5).toString())
                                    admin_shipp_termsdate426.setText(shipping_term_date.get(5).toString())
                                    admin_shipp_termssitem427.setText(shippingterms.get(6).toString())
                                    admin_shipp_termsdate427.setText(shipping_term_date.get(6).toString())
                                    admin_shipp_termssitem428.setText(shippingterms.get(7).toString())
                                    admin_shipp_termsdate428.setText(shipping_term_date.get(7).toString())
                                } else if (no_of_shipping_terms == "9") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                    admin_shipp_termssitem423.setText(shippingterms.get(2).toString())
                                    admin_shipp_termsdate423.setText(shipping_term_date.get(2).toString())
                                    admin_shipp_termssitem424.setText(shippingterms.get(3).toString())
                                    admin_shipp_termsdate424.setText(shipping_term_date.get(3).toString())
                                    admin_shipp_termssitem425.setText(shippingterms.get(4).toString())
                                    admin_shipp_termsdate425.setText(shipping_term_date.get(4).toString())
                                    admin_shipp_termssitem426.setText(shippingterms.get(5).toString())
                                    admin_shipp_termsdate426.setText(shipping_term_date.get(5).toString())
                                    admin_shipp_termssitem427.setText(shippingterms.get(6).toString())
                                    admin_shipp_termsdate427.setText(shipping_term_date.get(6).toString())
                                    admin_shipp_termssitem428.setText(shippingterms.get(7).toString())
                                    admin_shipp_termsdate428.setText(shipping_term_date.get(7).toString())
                                    admin_shipp_termssitem429.setText(shippingterms.get(8).toString())
                                    admin_shipp_termsdate429.setText(shipping_term_date.get(8).toString())
                                } else if (no_of_shipping_terms == "10") {
                                    admin_shipp_termssitem421.setText(shippingterms.get(0).toString())
                                    admin_shipp_termsdate421.setText(shipping_term_date!!.get(0).toString())
                                    admin_shipp_termssitem422.setText(shippingterms.get(1).toString())
                                    admin_shipp_termsdate422.setText(shipping_term_date.get(1).toString())
                                    admin_shipp_termssitem423.setText(shippingterms.get(2).toString())
                                    admin_shipp_termsdate423.setText(shipping_term_date.get(2).toString())
                                    admin_shipp_termssitem424.setText(shippingterms.get(3).toString())
                                    admin_shipp_termsdate424.setText(shipping_term_date.get(3).toString())
                                    admin_shipp_termssitem425.setText(shippingterms.get(4).toString())
                                    admin_shipp_termsdate425.setText(shipping_term_date.get(4).toString())
                                    admin_shipp_termssitem426.setText(shippingterms.get(5).toString())
                                    admin_shipp_termsdate426.setText(shipping_term_date.get(5).toString())
                                    admin_shipp_termssitem427.setText(shippingterms.get(6).toString())
                                    admin_shipp_termsdate427.setText(shipping_term_date.get(6).toString())
                                    admin_shipp_termssitem428.setText(shippingterms.get(7).toString())
                                    admin_shipp_termsdate428.setText(shipping_term_date.get(7).toString())
                                    admin_shipp_termssitem429.setText(shippingterms.get(8).toString())
                                    admin_shipp_termsdate429.setText(shipping_term_date.get(8).toString())
                                    admin_shipp_termssitem4210.setText(shippingterms.get(9).toString())
                                    admin_shipp_termsdate4210.setText(shipping_term_date.get(9).toString())

                                }
                            }
                        }


                        //************Delivery Drops *****/////////////
                        admin_delivery_terms4.setText(no_of_delivery_terms.toString())


                        if (no_of_delivery_terms == "1") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.GONE
                            ll_delivery_subterms433.visibility = View.GONE
                            ll_delivery_subterms434.visibility = View.GONE
                            ll_delivery_subterms435.visibility = View.GONE
                            ll_delivery_subterms436.visibility = View.GONE
                            ll_delivery_subterms437.visibility = View.GONE
                            ll_delivery_subterms438.visibility = View.GONE
                            ll_delivery_subterms439.visibility = View.GONE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "2") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.GONE
                            ll_delivery_subterms434.visibility = View.GONE
                            ll_delivery_subterms435.visibility = View.GONE
                            ll_delivery_subterms436.visibility = View.GONE
                            ll_delivery_subterms437.visibility = View.GONE
                            ll_delivery_subterms438.visibility = View.GONE
                            ll_delivery_subterms439.visibility = View.GONE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "3") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.VISIBLE
                            ll_delivery_subterms434.visibility = View.GONE
                            ll_delivery_subterms435.visibility = View.GONE
                            ll_delivery_subterms436.visibility = View.GONE
                            ll_delivery_subterms437.visibility = View.GONE
                            ll_delivery_subterms438.visibility = View.GONE
                            ll_delivery_subterms439.visibility = View.GONE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "4") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.VISIBLE
                            ll_delivery_subterms434.visibility = View.VISIBLE
                            ll_delivery_subterms435.visibility = View.GONE
                            ll_delivery_subterms436.visibility = View.GONE
                            ll_delivery_subterms437.visibility = View.GONE
                            ll_delivery_subterms438.visibility = View.GONE
                            ll_delivery_subterms439.visibility = View.GONE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "5") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.VISIBLE
                            ll_delivery_subterms434.visibility = View.VISIBLE
                            ll_delivery_subterms435.visibility = View.VISIBLE
                            ll_delivery_subterms436.visibility = View.GONE
                            ll_delivery_subterms437.visibility = View.GONE
                            ll_delivery_subterms438.visibility = View.GONE
                            ll_delivery_subterms439.visibility = View.GONE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "6") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.VISIBLE
                            ll_delivery_subterms434.visibility = View.VISIBLE
                            ll_delivery_subterms435.visibility = View.VISIBLE
                            ll_delivery_subterms436.visibility = View.VISIBLE
                            ll_delivery_subterms437.visibility = View.GONE
                            ll_delivery_subterms438.visibility = View.GONE
                            ll_delivery_subterms439.visibility = View.GONE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "7") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.VISIBLE
                            ll_delivery_subterms434.visibility = View.VISIBLE
                            ll_delivery_subterms435.visibility = View.VISIBLE
                            ll_delivery_subterms436.visibility = View.VISIBLE
                            ll_delivery_subterms437.visibility = View.VISIBLE
                            ll_delivery_subterms438.visibility = View.GONE
                            ll_delivery_subterms439.visibility = View.GONE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "8") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.VISIBLE
                            ll_delivery_subterms434.visibility = View.VISIBLE
                            ll_delivery_subterms435.visibility = View.VISIBLE
                            ll_delivery_subterms436.visibility = View.VISIBLE
                            ll_delivery_subterms437.visibility = View.VISIBLE
                            ll_delivery_subterms438.visibility = View.VISIBLE
                            ll_delivery_subterms439.visibility = View.GONE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "9") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.VISIBLE
                            ll_delivery_subterms434.visibility = View.VISIBLE
                            ll_delivery_subterms435.visibility = View.VISIBLE
                            ll_delivery_subterms436.visibility = View.VISIBLE
                            ll_delivery_subterms437.visibility = View.VISIBLE
                            ll_delivery_subterms438.visibility = View.VISIBLE
                            ll_delivery_subterms439.visibility = View.VISIBLE
                            ll_delivery_subterms4310.visibility = View.GONE
                        } else if (no_of_delivery_terms == "10") {
                            ll_delivery_subterms_431.visibility = View.VISIBLE
                            ll_delivery_subterms432.visibility = View.VISIBLE
                            ll_delivery_subterms433.visibility = View.VISIBLE
                            ll_delivery_subterms434.visibility = View.VISIBLE
                            ll_delivery_subterms435.visibility = View.VISIBLE
                            ll_delivery_subterms436.visibility = View.VISIBLE
                            ll_delivery_subterms437.visibility = View.VISIBLE
                            ll_delivery_subterms438.visibility = View.VISIBLE
                            ll_delivery_subterms439.visibility = View.VISIBLE
                            ll_delivery_subterms4310.visibility = View.VISIBLE
                        }


                        if (deliveryterms != null) {
                            for (d in 0 until deliveryterms!!.size) {
                                if (no_of_delivery_terms == "1") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0))
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                } else if (no_of_delivery_terms == "2") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(delivery_term_date.get(1).toString())
                                } else if (no_of_delivery_terms == "3") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(delivery_term_date.get(1).toString())
                                    admin_delivery_termssitem433.setText(deliveryterms.get(2).toString())
                                    admin_delivery_termsdate433.setText(delivery_term_date.get(2).toString())
                                } else if (no_of_delivery_terms == "4") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(shipping_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(shipping_term_date.get(1).toString())
                                    admin_delivery_termssitem433.setText(deliveryterms.get(2).toString())
                                    admin_delivery_termsdate433.setText(shipping_term_date.get(2).toString())
                                    admin_delivery_termssitem434.setText(deliveryterms.get(3).toString())
                                    admin_delivery_termsdate434.setText(shipping_term_date.get(3).toString())
                                } else if (no_of_delivery_terms == "5") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(delivery_term_date.get(1).toString())
                                    admin_delivery_termssitem433.setText(deliveryterms.get(2).toString())
                                    admin_delivery_termsdate433.setText(delivery_term_date.get(2).toString())
                                    admin_delivery_termssitem434.setText(deliveryterms.get(3).toString())
                                    admin_delivery_termsdate434.setText(delivery_term_date.get(3).toString())
                                    admin_delivery_termssitem435.setText(deliveryterms.get(4).toString())
                                    admin_delivery_termsdate435.setText(delivery_term_date.get(4).toString())
                                } else if (no_of_delivery_terms == "6") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(delivery_term_date.get(1).toString())
                                    admin_delivery_termssitem433.setText(deliveryterms.get(2).toString())
                                    admin_delivery_termsdate433.setText(delivery_term_date.get(2).toString())
                                    admin_delivery_termssitem434.setText(deliveryterms.get(3).toString())
                                    admin_delivery_termsdate434.setText(delivery_term_date.get(3).toString())
                                    admin_delivery_termssitem435.setText(deliveryterms.get(4).toString())
                                    admin_delivery_termsdate435.setText(delivery_term_date.get(4).toString())
                                    admin_delivery_termssitem436.setText(deliveryterms.get(5).toString())
                                    admin_delivery_termsdate436.setText(delivery_term_date.get(5).toString())
                                } else if (no_of_delivery_terms == "7") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(delivery_term_date.get(1).toString())
                                    admin_delivery_termssitem433.setText(deliveryterms.get(2).toString())
                                    admin_delivery_termsdate433.setText(delivery_term_date.get(2).toString())
                                    admin_delivery_termssitem434.setText(deliveryterms.get(3).toString())
                                    admin_delivery_termsdate434.setText(delivery_term_date.get(3).toString())
                                    admin_delivery_termssitem435.setText(deliveryterms.get(4).toString())
                                    admin_delivery_termsdate435.setText(delivery_term_date.get(4).toString())
                                    admin_delivery_termssitem436.setText(deliveryterms.get(5).toString())
                                    admin_delivery_termsdate436.setText(delivery_term_date.get(5).toString())
                                    admin_delivery_termssitem437.setText(deliveryterms.get(6).toString())
                                    admin_delivery_termsdate437.setText(delivery_term_date.get(6).toString())
                                } else if (no_of_delivery_terms == "8") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(delivery_term_date.get(1).toString())
                                    admin_delivery_termssitem433.setText(deliveryterms.get(2).toString())
                                    admin_delivery_termsdate433.setText(delivery_term_date.get(2).toString())
                                    admin_delivery_termssitem434.setText(deliveryterms.get(3).toString())
                                    admin_delivery_termsdate434.setText(delivery_term_date.get(3).toString())
                                    admin_delivery_termssitem435.setText(deliveryterms.get(4).toString())
                                    admin_delivery_termsdate435.setText(delivery_term_date.get(4).toString())
                                    admin_delivery_termssitem436.setText(deliveryterms.get(5).toString())
                                    admin_delivery_termsdate436.setText(delivery_term_date.get(5).toString())
                                    admin_delivery_termssitem437.setText(deliveryterms.get(6).toString())
                                    admin_delivery_termsdate437.setText(delivery_term_date.get(6).toString())
                                    admin_delivery_termssitem438.setText(deliveryterms.get(7).toString())
                                    admin_delivery_termsdate438.setText(delivery_term_date.get(7).toString())
                                } else if (no_of_delivery_terms == "9") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(delivery_term_date.get(1).toString())
                                    admin_delivery_termssitem433.setText(deliveryterms.get(2).toString())
                                    admin_delivery_termsdate433.setText(delivery_term_date.get(2).toString())
                                    admin_delivery_termssitem434.setText(deliveryterms.get(3).toString())
                                    admin_delivery_termsdate434.setText(delivery_term_date.get(3).toString())
                                    admin_delivery_termssitem435.setText(deliveryterms.get(4).toString())
                                    admin_delivery_termsdate435.setText(delivery_term_date.get(4).toString())
                                    admin_delivery_termssitem436.setText(deliveryterms.get(5).toString())
                                    admin_delivery_termsdate436.setText(delivery_term_date.get(5).toString())
                                    admin_delivery_termssitem437.setText(deliveryterms.get(6).toString())
                                    admin_delivery_termsdate437.setText(delivery_term_date.get(6).toString())
                                    admin_delivery_termssitem438.setText(deliveryterms.get(7).toString())
                                    admin_delivery_termsdate438.setText(delivery_term_date.get(7).toString())
                                    admin_delivery_termssitem439.setText(deliveryterms.get(8).toString())
                                    admin_delivery_termsdate439.setText(delivery_term_date.get(8).toString())
                                } else if (no_of_delivery_terms == "10") {
                                    admin_delivery_termssitem431.setText(deliveryterms.get(0).toString())
                                    admin_delivery_termsdate431.setText(delivery_term_date!!.get(0).toString())
                                    admin_delivery_termssitem432.setText(deliveryterms.get(1).toString())
                                    admin_delivery_termsdate432.setText(delivery_term_date.get(1).toString())
                                    admin_delivery_termssitem433.setText(deliveryterms.get(2).toString())
                                    admin_delivery_termsdate433.setText(delivery_term_date.get(2).toString())
                                    admin_delivery_termssitem434.setText(deliveryterms.get(3).toString())
                                    admin_delivery_termsdate434.setText(delivery_term_date.get(3).toString())
                                    admin_delivery_termssitem435.setText(deliveryterms.get(4).toString())
                                    admin_delivery_termsdate435.setText(delivery_term_date.get(4).toString())
                                    admin_delivery_termssitem436.setText(deliveryterms.get(5).toString())
                                    admin_delivery_termsdate436.setText(delivery_term_date.get(5).toString())
                                    admin_delivery_termssitem437.setText(deliveryterms.get(6).toString())
                                    admin_delivery_termsdate437.setText(delivery_term_date.get(6).toString())
                                    admin_delivery_termssitem438.setText(deliveryterms.get(7).toString())
                                    admin_delivery_termsdate438.setText(delivery_term_date.get(7).toString())
                                    admin_delivery_termssitem439.setText(deliveryterms.get(8).toString())
                                    admin_delivery_termsdate439.setText(delivery_term_date.get(8).toString())
                                    admin_delivery_termssitem4310.setText(deliveryterms.get(9).toString())
                                    admin_delivery_termsdate4310.setText(delivery_term_date.get(9).toString())

                                }

                            }
                        }

                        Log.d("paper_name", paper_name + "" + papertypestringlist.contains(paper_name));
                        typeofpaper.setText(paper_name)
                        if (response.body()!!.data!!.paper_name.equals("Other")) {

                            ll_txt_name.visibility = View.GONE
                            ll_subtxt_name.visibility = View.GONE
                            ll_weight.visibility = View.GONE
                            ll_size.visibility = View.GONE
                            ll_other_desc.visibility = View.VISIBLE
                        } else {
                            ll_txt_name.visibility = View.VISIBLE
                            ll_subtxt_name.visibility = View.VISIBLE
                            ll_weight.visibility = View.VISIBLE
                            ll_size.visibility = View.VISIBLE
                            ll_other_desc.visibility = View.GONE
                        }
                        txtname.setText(text_name)
                        subtxtname.setText(sub_text_name)
                        weight.setText(weight2)
                        sizetxt.setText(width + "*" + height)
                        admin_optionlist1.setText(option_type)
                        allotsheets.setText(alloted_sheets)
                        totalquantity.setText(mailing_qty)


                        val jobDepartArray: Array<EditJobDetailsDataDepartResponse>? = response.body()!!.data!!.departments!!
                        for (item: EditJobDetailsDataDepartResponse in jobDepartArray!!.iterator()) {
                            mGetDepartmentList.add(item)
                            //admin_departments.setSelection(intArrayOf(2, 6))

                        }
                        for (i in 0 until mGetDepartmentList!!.size) {
                            mGetDepartmentList_ids.add(mGetDepartmentList.get(i).department_id.toString())
                            mGetDepartmentList_names.add(mGetDepartmentList.get(i).department_name.toString())
                            departmentList_ass_status.add(mGetDepartmentList.get(i).assigned_status.toString())

                            if (mGetDepartmentList.get(i).department_name.toString().equals("Mailing")) {
                                ll_miling_four.visibility = View.VISIBLE
                            }
                            if (mGetDepartmentList.get(i).department_name.toString().equals("Shipping")) {
                                ll_shipping_four.visibility = View.VISIBLE
                            }
                            if (mGetDepartmentList.get(i).department_name.toString().equals("Delivery")) {
                                ll_delivery_terms_4.visibility = View.VISIBLE
                            }

                        }
                        Log.w("array_dep_ids", "Result : " + mGetDepartmentList_ids.size + "---" + departmentList_ass_status.size + "---" + departmentList_ass_status.toString())
                        val jobDepartTasksArray: Array<EditJobDetailsDataTasksResponse>? = response.body()!!.data!!.tasks!!
                        for (item: EditJobDetailsDataTasksResponse in jobDepartTasksArray!!.iterator()) {
                            mGetDepartmentTasksList.add(item)

                        }
                        for (i in 0 until mGetDepartmentTasksList!!.size) {
                            mGetTasksList_names.add(mGetDepartmentTasksList.get(i).task_name.toString())
                            mGetTasksList_ids.add(mGetDepartmentTasksList.get(i).task_id.toString())
                        }
                        var appendSeparator = false

                        val sb = StringBuilder()

                        for (y in 0 until mGetTasksList_names.size) {
                            if (appendSeparator)
                                sb.append(',') // a comma
                            appendSeparator = true
                            sb.append(mGetTasksList_names.get(y))

                        }
                        admin_tasks.setText(sb)

                        Log.w("index:::", "Main" + mGetDepartmentList_names.size)
                        Log.w("index:::", "Edit" + mDepartmentListArray.size)
                        val int_ass_status = IntArray(mDepartmentListArray.size)

                        for (i in 0 until mDepartmentListArray.size) {

                            int_ass_status[i] = 0
                            Log.e("aaaaaaa ", "" + int_ass_status.size)
                            for (j in 0 until mGetDepartmentList_names.size) {

                                if (mGetDepartmentList_names.get(j).equals(mDepartmentListArray.get(i).department_name.toString())) {

                                    mIndexList.add(i)
                                    int_ass_status[i] = departmentList_ass_status.get(j).toInt()
                                    Log.e("aaaaaaa ", i.toString()+"---"+cat_status_array.size)
                                    mSingleDepartIds.add(mDepartmentListArray.get(i).id.toString())
                                }
                            }
                        }
                        val intArrayv = IntArray(mGetDepartmentList_names.size)
                        val int_ass_status_new:ArrayList<String> = ArrayList()
                        for (index in 0 until mIndexList.size) {
                            Log.w("index:::", "Items:" + mIndexList.get(index) + "---" + mIndexList.size)
                            intArrayv[index] = mIndexList.get(index)
                            int_ass_status_new.add(departmentList_ass_status.get(index))

                        }

                        /*   for (i in 0 until mDepartmentListArray.size){

                                   Log.e("aaaaaaa "+i, intArrayv[i].toString())
                               if(i==intArrayv[i]){
                                   int_ass_status[i] = departmentList_ass_status.get(i).toInt()
                               }else{
                                   int_ass_status[i] = 0
                               }
                           }*/

                        admin_departments.setSelection(intArrayv)
                        admin_departments.setSelectionStatus(int_ass_status)


                        //admin_departments.setSelection(mSingleDepartArray)
                        Log.e("mSingleDepartArray_ll", mSingleDepartArray.toString() + "---" + int_ass_status.size)

                        for (k in 0 until int_ass_status.size) {
                            Log.e("int_ass_status_full", int_ass_status.get(k).toString())

                            cat_status_array!!.add(int_ass_status.get(k).toString())
                            Log.e("int_ass_status_", ""+cat_status_array.size)
                        }
                        Log.e("int_ass_status_lll", ""+cat_status_array.size)
                        /*if (strings.toString().contains("Mailing")) {
            ll_miling_four.visibility = View.VISIBLE
        } else {
            ll_miling_four.visibility = View.GONE
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_four.visibility = View.VISIBLE

        } else {
            ll_shipping_four.visibility = View.GONE
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_4.visibility = View.VISIBLE

        } else {
            ll_delivery_terms_4.visibility = View.GONE
        }*/

                    }
                }
                mSingleTasklistandCat.clear()
                job_nameList.clear()
                val mTaskListarray = ArrayList<NewListCell>()

                departselectedid_array.clear()

                // var indices_postion: LinkedList<Int>
                for (j in 0 until mDepartmentListArray.size) {


                    val mTaskslistfinal = ArrayList<String>()
                    departselectedid_array.add(mDepartmentListArray.get(j).id.toString())
                    Log.e("main_depart_lll", mSingleDepartIds!!.size.toString() + "---" + mDepartmentListArray.get(j).id.toString())
                    /*for (m in 0 until mGetDepartmentList_ids.size){
                        Log.e("main_get_dep_ids", mGetDepartmentList_ids!!.toString())
                    }*/
                    var mString = ""
                    for (k in 0 until mDepartmentListArray.get(j).tasks!!.size) {
                        mTaskslistfinal.add(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString())
                        if (mString == mDepartmentListArray.get(j).department_name.toString()) {
                            mTaskListarray.add(NewListCell(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString(), cat_status_array.get(j)))
                            mDepartTasksnamesList.add(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString())

                            Log.e("main_if",/*departmentList_ass_status.get(j)+"---"+departmentList_ass_status.size+*/"---"+cat_status_array.size+"---"+cat_status_array.get(j)+"-ids-"+mDepartmentListArray.get(j).id.toString()+"---"+mDepartmentListArray.size)
                        } else {
                            mString = mDepartmentListArray.get(j).department_name.toString()
                            mTaskListarray.add(NewListCell(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString(), cat_status_array.get(j)))
                            Log.e("main_if_else",/*departmentList_ass_status.get(j)+"---"+departmentList_ass_status.size+*/"---"+cat_status_array.size+"---"+cat_status_array.get(j)+"-ids-"+mDepartmentListArray.get(j).id.toString()+"---"+mDepartmentListArray.size)

                        }
                    }
                }
                var departlappend = false
                val departidssb = StringBuilder()
                var departids = ""
                for (m in 0 until mGetDepartmentList_ids.size) {
                    Log.e("main_get_dep_ids", mGetDepartmentList_ids!!.toString())
                    /*if (departlappend) {
                        departidssb.append(',') // a comma
                    } else {
                        departlappend = true
                        departidssb.append(mGetDepartmentList_ids.get(m))
                    }*/
                    if (m == 0)
                        departids = mGetDepartmentList_ids.get(m)
                    else
                        departids += "," + mGetDepartmentList_ids.get(m)
                }
                departselectedid_only = departids.toString()

                val adapter = ListAdapterk(this@JobEditActivity, mTaskListarray, cat_status_array)
                rv_main_id.adapter = adapter

                //if(flag) {
                Log.w("fffffffffff", "Dept1" + mGetDepartmentList_ids + "--" + departselectedid_array.toString() + "---" + departselectedid_only)
                Log.w("fffffffffff", "task1" + mGetTasksList_ids)
                if (flag) {
                    defaultSelectedData(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray)
                }
                //}
                Log.w("fffffffffff", "Dept2" + mGetDepartmentList_ids)
                Log.w("fffffffffff", "task2" + mGetTasksList_ids)
                callgetJobsAPI()
                jobsheetsdropdown()
            }

            override fun onFailure(call: Call<EditjobDetailsResponce>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })


    }


    private fun CallDepartmentAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.getDepartmentList()
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<GetDepartmentListResponse> {
            override fun onResponse(call: Call<GetDepartmentListResponse>, response: retrofit2.Response<GetDepartmentListResponse>?) {
                if (response != null) {

                    //my_loader.dismiss()
                    Log.w("Result_Order_details", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.departmentsList != null) {
                        val list: Array<DepatListName>? = response.body()!!.departmentsList!!

                        for (item: DepatListName in list!!.iterator()) {
                            mDepartmentListArray.add(item)

                        }
                        Log.e("mDepartmentListArray", "" + mDepartmentListArray.size)
                        for (i in 0 until mDepartmentListArray!!.size) {
                            Log.e("mDepartmentListArray", mDepartmentListArray.get(i).department_name.toString() + "--" + mDepartmentListArray.size)
                            mSingleArray = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })

                            /*  if (mDepartmentListArray.get(i).id.toString().equals(mGetDepartmentList_ids)) {
                                  Log.e("ids_same", "" + mDepartmentListArray.get(i).id.toString())

                              }*/


                            val list: Array<DeprtTaskList>? = mDepartmentListArray.get(i).tasks!!
                            for (item: DeprtTaskList in list!!.iterator()) {
                                mDepartTasksArray.add(item)

                            }
                            for (j in 0 until mDepartTasksArray!!.size) {
                                Log.e("mDepartTasksArray_lll", "" + mDepartTasksArray.size + "---" + mDepartTasksArray.get(j).task_name)
                                // departselectedsublist.add(ListCellk(mDepartTasksArray.get(j).task_name.toString(),mDepartmentListArray.get(i).department_name.toString()))
                            }

                        }
                        Log.e("mDepartmentListArray", "" + mSingleArray.size)
                        admin_departments.setItems(mSingleArray, 1)

                        GetJobDetailsAPI()

                    } else if (response.body()!!.status.equals("2")) {
                        //no_service!!.text = "No Data Found"
                        // no_service!!.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<GetDepartmentListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun callgetJobsAPI() {

        //my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getjobTypeList()
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<GetJobTypeListResponse> {
            override fun onResponse(call: Call<GetJobTypeListResponse>, response: retrofit2.Response<GetJobTypeListResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    mServiceInfo_Jobname_id.clear()
                    Log.w("Result_Order_details", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.jobtypesList != null) {
                        val list: Array<JobTypeResponse>? = response.body()!!.jobtypesList!!
                        for (item in 0 until list!!.size) {
                            mServiceInfo_Jobname.add(list!!.get(item)!!.job_name!!)
                            mServiceInfo_Jobname_id.add(list!!.get(item))

                            if (mJob_type_id_main.equals(list!!.get(item).id)) {
                                admin_job_type_id.setText(list!!.get(item).job_name)
                                mJobType_id = list!!.get(item).id!!
                            }

                        }

                        if (list.size == 0) {
                            //no_service!!.text = "No Data Found"
                            // no_service!!.visibility = View.VISIBLE
                        }

                    } else if (response.body()!!.status.equals("2")) {
                        //no_service!!.text = "No Data Found"
                        // no_service!!.visibility = View.VISIBLE
                    }
                    val adapter = Dataadapter(this@JobEditActivity, mServiceInfo_Jobname_id)
                    list_items_list_type.adapter = adapter
                }
            }

            override fun onFailure(call: Call<GetJobTypeListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun AddJobCallAPI() {
        my_loader.show()
        Log.d("submit_sozeifd", sizeid)
        val apiService = ApiInterface.create()
        val call = apiService.editJob(option_id = option_id1,id = id, job_id = admin_job_stg!!, job_title = admmin_job_title_stg.toString(), job_description = job_desc_one_id_stg.toString(), job_type_id = mJobType_id!!, job_part = admin_job_part_stg!!, /*planeed_activity = admin_Pactivity_stg!!,*/ earliest_start_date = admin_Estart_date_stg!!, custmer = admin_customer_stg!!, /*crit_start_date = admin_Cstart_date_stg!!,*/
                description = admin_description_stg!!, /*due_time = admin_due_time_stg!!, earliest_start_time = admin_Estart_time_stg!!, job_type_id = admin_job_type_stg!!, last_act_code = admin_Lact_code_stg!!,*/ promise_date = admin_Pdata_stg!!/*, scheduled = admin_schedule_stg!!*/, department_ids = "[" + departselectedid_only + "]",
                task_ids = tasksselectedid_only.toString(), admin_id = admin_id_main, type = "android", alloted_sheets = allotedsheets, mailing_qty = mailing_qty_total.toString(),
                no_of_terms = admin_job_terms.text.toString(), number_of_shipping_terms = admin_shipping_terms4.text.toString(), number_of_delivery_terms = admin_delivery_terms4.text.toString(), paper_id = paperid, size_id = sizeid, sub_text_id = subtxtid, terms = terms_dates, shippingterms = terms_dates_shipping, deliveryterms = terms_dates_delivery, text_id = txtid, weight_id = weightid)
        Log.d("Data_sent_ids", call.toString() + "" + departselectedid_only + "-----" + tasksselectedid_only + "---" + mJobType_id + "---" + tasksselectedid_only.toString())
        call.enqueue(object : Callback<UserLoginResponse> {
            override fun onResponse(call: Call<UserLoginResponse>, response: retrofit2.Response<UserLoginResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(applicationContext, response.body()!!.result, Toast.LENGTH_SHORT).show()
                        val home_intent = Intent(this@JobEditActivity, DashboardtListActivity::class.java)
                        home_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(home_intent)
                    }
                }
            }

            override fun onFailure(call: Call<UserLoginResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    inner class ListAdapterk(context: Context, items: java.util.ArrayList<NewListCell>, cat_status_array: ArrayList<String>?) : ArrayAdapter<NewListCell>(context, 0, items) {

        internal var inflater: LayoutInflater
        var mtaskslist = ""
        var index = 0

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            index = 1
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            //If the cell is a section header we inflate the header layout
            if (cell!!.isSectionHeader) {
                v = inflater.inflate(R.layout.section_header, null)

                v!!.isClickable = false

                val header = v.findViewById(R.id.name) as TextView
                header.setText(cell.name)
            } else {
                v = inflater.inflate(R.layout.select_task_items, null)
                val name = v!!.findViewById(R.id.tv_task_name_id) as TextView
                val category = v.findViewById(R.id.tv_category_id) as TextView
                val chbContent = v.findViewById(R.id.chbContent) as CheckBox
                val chbContent2 = v.findViewById(R.id.chbContent2) as CheckBox
                val chbContent3 = v.findViewById(R.id.chbContent3) as CheckBox
                val chbContent4 = v.findViewById(R.id.chbContent4) as CheckBox


                chbContent.setText(cell.name)
                category.setText(cell.category)


                if (mtaskslist.equals(cell.category)) {
                    category.visibility == View.GONE
                } else {
                    mtaskslist = cell.category
                    category.setText(cell.category)
                }

                /*chbContent.setOnClickListener {
                    job_nameList.add(cell.name)
                    mSingleTasklistandCat.add(cell.task_id + "_" + cell.cat_id)
                }*/


                if (index == 1) {
                    chbContent.setChecked(if (mChecked.get(position) == true) true else false)
                    chbContent.visibility = View.VISIBLE
                    chbContent.setText(cell.name)
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                }
                /* else if(index==2){
                     chbContent2.setChecked(if (mChecked2.get(position) == true) true else false)
                     chbContent.visibility = View.GONE
                     chbContent2.visibility = View.VISIBLE
                     chbContent2.setText(cell.name)
                     chbContent3.visibility = View.GONE
                     chbContent4.visibility = View.GONE
                 }
                 else if(index==3){
                     chbContent3.setChecked(if (mChecked3.get(position) == true) true else false)
                     chbContent.visibility = View.GONE
                     chbContent2.visibility = View.GONE
                     chbContent3.visibility = View.VISIBLE
                     chbContent3.setText(cell.name)
                     chbContent4.visibility = View.GONE
                 }
                 else if(index==4){
                     chbContent4.setChecked(if (mChecked4.get(position) == true) true else false)
                     chbContent.visibility = View.GONE
                     chbContent2.visibility = View.GONE
                     chbContent3.visibility = View.GONE
                     chbContent4.visibility = View.VISIBLE
                     chbContent4.setText(cell.name)
                 }*/

                /* for (index in 0 until mIndexList.size) {
                     Log.w("index:::", "Items:" + mIndexList.get(index) + "---" + mIndexList.size)
                     intArrayv[index] = mIndexList.get(index)
                     if (cat_status_array.get(position).toString().equals("0")||cat_status_array.get(position).toString().equals("1")){
                         chbContent.isEnabled = true
                     }else{
                         chbContent.isEnabled = false
                     }
                 }*/
                if (cell.ass_status.equals("0") || cell.ass_status.equals("1")) {
                    chbContent.isEnabled = true
                } else {
                    chbContent.isEnabled = false
                }
                Log.e("ass_status", cell.ass_status)
                chbContent.setOnCheckedChangeListener { buttonView, isChecked ->


                    if (isChecked) {

                        mChecked.put(position, isChecked)
                        job_nameList.add(cell.name)
                        mSingleTasklistandCat.add(cell.task_id + "_" + cell.cat_id)
                        JobidList.add(cell.cat_id)
                        Log.e("mChecked_ll", "" + position + "" + mChecked.size() + "--" + job_nameList.size + "--" + mSingleTasklistandCat.size)

                    } else {

                        mChecked.delete(position)
                        job_nameList.remove(cell.name)
                        mSingleTasklistandCat.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList.remove(cell.cat_id)
                        Log.e("mChecked", "" + position + "" + mChecked.size() + "--" + job_nameList.size + "--" + mSingleTasklistandCat.size)

                    }
                }


                for (i in 0 until mDepartTasksnamesList.size) {
                    for (j in 0 until mGetTasksList_names.size) {
                        if (mDepartTasksnamesList.get(i).equals(mGetTasksList_names.get(j))) {
                            Log.e("main_tasks_list", mDepartTasksnamesList.get(i))
                        }

                    }
                }


            }
            return v
        }
    }

    inner class Dataadapter(context: Context, items: java.util.ArrayList<JobTypeResponse>) : ArrayAdapter<JobTypeResponse>(context, 0, items) {

        internal var inflater: LayoutInflater
        var mtaskslist = ""

        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)

            v = inflater.inflate(R.layout.section_header, null)
            val name = v!!.findViewById(R.id.name) as TextView

            name.setText(cell.job_name)
            mJobType_id = cell.id.toString()
            name.setOnClickListener {
                mJobType_id = cell.id.toString()
                dialog_list_type.dismiss()
                admin_job_type_id.setText(cell.job_name)
                Log.e("mJobType_id_lll", mJobType_id.toString())
            }


            return v
        }

    }

    private fun jobEditdata() {

        id = intent.getStringExtra("id")
        Log.e("main_job_id", id)

    }


    var date: Calendar = Calendar.getInstance()
    fun showDateTimePicker(id: Int) {
        val currentDate = Calendar.getInstance();
        val date = Calendar.getInstance();
        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
            date.set(year, monthofyear, dayofmonth)
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourofday, minute ->

                date.set(Calendar.HOUR_OF_DAY, hourofday);
                date.set(Calendar.MINUTE, minute);
                Log.v("dateshowDatepicker", "The choosen one " + date.getTime())

                val myFormat = "MM/dd/yyyy hh:mm aa" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.UK)
                val date2 = sdf.format(date.getTime())

                if (id == R.id.admin_Estart_date_id) {
                    admin_Estart_date_id.text = date2
                } else if (id == R.id.admin_Pdata_id) {
                    admin_Pdata_id.text = date2
                }
                admin_Estart_date_id.text = date2

            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }


    //////////////*******Mailing*******//////////////
    fun showDateTimePicker1(id: Int) {
        val currentDate = Calendar.getInstance();
        val date = Calendar.getInstance();
        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
            date.set(year, monthofyear, dayofmonth)

            val myFormat = "MM/dd/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.UK)
            val date2 = sdf.format(date.getTime())
            if (id == R.id.admin_termsdate1) {
                admin_termsdate1.setText(date2)
            } else if (id == R.id.admin_termsdate2) {
                admin_termsdate2.setText(date2)
            } else if (id == R.id.admin_termsdate3) {
                admin_termsdate3.setText(date2)
            } else if (id == R.id.admin_termsdate4) {
                admin_termsdate4.setText(date2)
            } else if (id == R.id.admin_termsdate5) {
                admin_termsdate5.setText(date2)
            } else if (id == R.id.admin_termsdate6) {
                admin_termsdate6.setText(date2)
            } else if (id == R.id.admin_termsdate7) {
                admin_termsdate7.setText(date2)
            } else if (id == R.id.admin_termsdate8) {
                admin_termsdate8.setText(date2)
            } else if (id == R.id.admin_termsdate9) {
                admin_termsdate9.setText(date2)
            } else if (id == R.id.admin_termsdate10) {
                admin_termsdate10.setText(date2)
            }

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }


//////////////*******Shipping*******//////////////

    fun showDateTimePicker2(id: Int) {
        val currentDate = Calendar.getInstance();
        val date = Calendar.getInstance();
        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
            date.set(year, monthofyear, dayofmonth)

            val myFormat = "MM/dd/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.UK)
            val date2 = sdf.format(date.getTime())
            if (id == 1) {
                admin_shipp_termsdate421.setText(date2)
            } else if (id == 2) {
                admin_shipp_termsdate422.setText(date2)
            } else if (id == 3) {
                admin_shipp_termsdate423.setText(date2)
            } else if (id == 4) {
                admin_shipp_termsdate424.setText(date2)
            } else if (id == 5) {
                admin_shipp_termsdate425.setText(date2)
            } else if (id == 6) {
                admin_shipp_termsdate426.setText(date2)
            } else if (id == 7) {
                admin_shipp_termsdate427.setText(date2)
            } else if (id == 8) {
                admin_shipp_termsdate428.setText(date2)
            } else if (id == 9) {
                admin_shipp_termsdate429.setText(date2)
            } else if (id == 10) {
                admin_shipp_termsdate4210.setText(date2)
            }

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }


    //////////////*******Delivery*******//////////////

    fun showDateTimePicker3(id: Int) {
        val currentDate = Calendar.getInstance();
        val date = Calendar.getInstance();
        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
            date.set(year, monthofyear, dayofmonth)

            val myFormat = "MM/dd/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.UK)
            val date2 = sdf.format(date.getTime())
            if (id == 1) {
                admin_delivery_termsdate431.setText(date2)
            } else if (id == 2) {
                admin_delivery_termsdate432.setText(date2)
            } else if (id == 3) {
                admin_delivery_termsdate433.setText(date2)
            } else if (id == 4) {
                admin_delivery_termsdate434.setText(date2)
            } else if (id == 5) {
                admin_delivery_termsdate435.setText(date2)
            } else if (id == 6) {
                admin_delivery_termsdate436.setText(date2)
            } else if (id == 7) {
                admin_delivery_termsdate437.setText(date2)
            } else if (id == 8) {
                admin_delivery_termsdate438.setText(date2)
            } else if (id == 9) {
                admin_delivery_termsdate439.setText(date2)
            } else if (id == 10) {
                admin_delivery_termsdate4310.setText(date2)
            }

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }


    override fun onClick(v: View?) {
        showDateTimePicker1(v!!.id)

    }


    private fun jobsheetsdropdown() {
        sizelisttype.clear()
        sizelist.clear()
        weightlisttype.clear()
        weightlist.clear()
        papertypelist.clear()
        papertypestringlist.clear()

        val apiService = ApiInterface.create()
        val call = apiService.jobsheetsdropdown()
        call.enqueue(object : Callback<PaperTypeResponse> {
            override fun onResponse(call: Call<PaperTypeResponse>, response: retrofit2.Response<PaperTypeResponse>?) {
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.sheetsList != null) {

                        if (response.body()!!.sheetsList!!.papertypes != null) {
                            val list: Array<Papertypes>? = response.body()!!.sheetsList!!.papertypes
                            for (item: Papertypes in list!!.iterator()) {
                                papertypelist.add(item)
                            }

                            for (i in 0 until papertypelist.size) {
                                if (papertypelist.get(i).paper_name!!.toString().equals(typeofpaper.text.toString())) {
                                    paperid = papertypelist.get(i).id!!
                                    texttypes(papertypelist.get(i).id!!)
                                }
                            }

                            for (i in 0 until papertypelist.size) {
                                papertypestringlist.add(papertypelist.get(i).paper_name!!)
                            }

                        }
                    }

                } else {
                    Toast.makeText(applicationContext, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<PaperTypeResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun weightsizeAPi(sub_text_id: String) {
        sizelisttype.clear()
        sizelist.clear()
        weightlisttype.clear()
        weightlist.clear()
        Log.w("WEIGHTSIZESUBTEXTID", "*** " + sub_text_id)
        val apiService = ApiInterface.create()
        val call = apiService.weightandsize(sub_text_id)
        call.enqueue(object : Callback<WeightSizeResponse> {
            override fun onResponse(call: Call<WeightSizeResponse>, response: retrofit2.Response<WeightSizeResponse>?) {
                Log.w("WEIGHTSIZESTATUS", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.weightandsizelist != null) {
                        if (response.body()!!.weightandsizelist!!.sizes != null) {
                            val list: Array<Sizes>? = response.body()!!.weightandsizelist!!.sizes
                            for (item: Sizes in list!!.iterator()) {
                                sizelisttype.add(item)
                            }
                            for (i in 0 until response.body()!!.weightandsizelist!!.sizes!!.size) {
                                sizelist.add(response.body()!!.weightandsizelist!!.sizes!!.get(i).height + "*" + response.body()!!.weightandsizelist!!.sizes!!.get(i).width)
                            }

                        }
                        if (response.body()!!.weightandsizelist!!.weights != null) {

                            val list: Array<Weights>? = response.body()!!.weightandsizelist!!.weights
                            for (item: Weights in list!!.iterator()) {
                                weightlisttype.add(item)
                            }

                            for (i in 0 until response.body()!!.weightandsizelist!!.weights!!.size) {
                                weightlist.add(response.body()!!.weightandsizelist!!.weights!!.get(i).weight!!)
                            }
                        }

                    }

                } else {
                    Toast.makeText(applicationContext, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<WeightSizeResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun texttypes(id: String) {
        texttypelist.clear()
        texttypelisttringlist.clear()
        val apiService = ApiInterface.create()
        val call = apiService.texttypes(id)
        call.enqueue(object : Callback<TextTypeResponse> {
            override fun onResponse(call: Call<TextTypeResponse>, response: retrofit2.Response<TextTypeResponse>?) {
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.texttypes != null) {
                        val list: Array<Texttypes>? = response.body()!!.texttypes!!
                        for (item: Texttypes in list!!.iterator()) {
                            texttypelist.add(item)
                        }
                        for (i in 0 until texttypelist.size) {
                            texttypelisttringlist.add(texttypelist.get(i).text_name!!)
                        }

                        for (i in 0 until texttypelist.size) {
                            if (texttypelist.get(i).text_name!!.toString().equals(txtname.text.toString())) {
                                txtid = texttypelist.get(i).id!!
                                SubTextTypeResponse(texttypelist.get(i).id!!)
                            }
                        }
                    }
                }
            }
            override fun onFailure(call: Call<TextTypeResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun SubTextTypeResponse(id: String) {
        subtexttypelist.clear()
        subtexttypelisttringlist.clear()

        val apiService = ApiInterface.create()
        val call = apiService.subtexttypes(id)
        call.enqueue(object : Callback<SubTextTypeResponse> {
            override fun onResponse(call: Call<SubTextTypeResponse>, response: retrofit2.Response<SubTextTypeResponse>?) {
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.subtexttypes != null) {
                        val list: Array<Subtexttypes>? = response.body()!!.subtexttypes!!
                        for (item: Subtexttypes in list!!.iterator()) {
                            subtexttypelist.add(item)
                        }
                        for (i in 0 until subtexttypelist.size) {
                            subtexttypelisttringlist.add(subtexttypelist.get(i).sub_text_name!!)
                        }

                        for (i in 0 until subtexttypelist.size) {
                            if (subtexttypelist.get(i).sub_text_name!!.toString().equals(subtxtname.text.toString())) {
                                subtxtid = subtexttypelist.get(i).id!!
                            }
                        }

                    }
                }

            }

            override fun onFailure(call: Call<SubTextTypeResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun CallgetCustomersAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.getCustomers()
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<GetCustomersListResponse> {
            override fun onResponse(call: Call<GetCustomersListResponse>, response: retrofit2.Response<GetCustomersListResponse>?) {
                if (response != null) {
                    Log.w("CUSTOMERRESPONSE", response.body().toString())


                    if (response.body()!!.status.equals("1") && response.body()!!.customers != null) {
                        Log.w("CUSTOMERRESPONSE_STATUS", response.body()!!.status.toString())
                        val list: Array<CustomersListName>? = response.body()!!.customers!!

                        for (item: CustomersListName in list!!.iterator()) {
                            mCustomerNamesListArray.add(item)
                            mCustomerNamesArray.add(item.c_name!!)
                            mCustomerIdsArray.add(item.id!!)
                            Log.d("CUSTOMERRESPONSE_NAMES", item.c_name!!)

                        }

                        admin_customer_id.setAdapter(customer_search_adapter)

                    }
                }
            }

            override fun onFailure(call: Call<GetCustomersListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun CallAvailableSheetsAPI(size_id: String, weight_id: String, sub_text_id: String, paper_id: String, text_id: String, job_part: String) {
        val apiService = ApiInterface.create()
        Log.w("AVAILABLESHEETS_ID", "*** " + size_id + "----" + weight_id + "---" + sub_text_id + "---" + paper_id + "---" + text_id)
        val call = apiService.getAvailableSheets(size_id, weight_id, sub_text_id, paper_id, text_id)
        call.enqueue(object : Callback<GetAvailableSheetsResponse> {
            override fun onResponse(call: Call<GetAvailableSheetsResponse>, response: retrofit2.Response<GetAvailableSheetsResponse>?) {
                Log.w("AVAILABLESHEETS_STATUS", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.no_of_sheets != null) {
                        val no_f_sheets = response.body()!!.no_of_sheets!!
                        Log.w("no_f_sheets", no_f_sheets)

                        if (job_part.equals("0")) {
                            tv_availablesheets.setText(no_f_sheets)
                        } else if (job_part.equals("1")) {
                            tv_availablesheets1.setText(no_f_sheets)
                        } else if (job_part.equals("2")) {
                            tv_availablesheets2.setText(no_f_sheets)
                        } else if (job_part.equals("3")) {
                            tv_availablesheets3.setText(no_f_sheets)
                        }


                    }
                } else {
                    tv_availablesheets.setText(response.body()!!.no_of_sheets!!)
                }

            }

            override fun onFailure(call: Call<GetAvailableSheetsResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun optionlistsdropdown() {
        optionliststringlist.clear()
        papertypelist.clear()
        papertypestringlist.clear()
        val apiService = ApiInterface.create()
        val call = apiService.optionListdropdown()
        call.enqueue(object : Callback<OptionListResponse> {
            override fun onResponse(call: Call<OptionListResponse>, response: retrofit2.Response<OptionListResponse>?) {
                if (response != null) {
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        val list: Array<OptiondataListResponse>? = response.body()!!.data!!
                        for (item: OptiondataListResponse in list!!.iterator()) {
                            optiontypelist.add(item)
                        }
                        for (i in 0 until optiontypelist.size) {
                            optionliststringlist.add(optiontypelist.get(i).type!!)
                        }
                    }else{
                    }
                }
            }
            override fun onFailure(call: Call<OptionListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed"+t.message)
            }
        })
    }

}
