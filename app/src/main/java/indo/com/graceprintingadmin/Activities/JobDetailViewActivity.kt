package indo.com.graceprintingadmin.Activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.TextView
import indo.com.graceprintingadmin.Adapter.DepartmentAdapter
import indo.com.graceprintingadmin.Model.APIResponse.JobDetailViewDataDepartmentResponse
import indo.com.graceprintingadmin.Model.APIResponse.JobDetailViewDataResponse
import indo.com.graceprintingadmin.Model.APIResponse.JobDetailViewResponse
import indo.com.graceprintingadmin.R





class JobDetailViewActivity : AppCompatActivity() {


    private var listview_department: RecyclerView? = null
    private var listview_department1: ListView? = null
    public var mServiceInfo: ArrayList<JobDetailViewDataDepartmentResponse> = ArrayList<JobDetailViewDataDepartmentResponse>()
    public var mServicedata: ArrayList<JobDetailViewResponse> = ArrayList<JobDetailViewResponse>()
    internal var job_id: String = ""
    private var job_id_val: String = ""
    private var mServiceDetailsAdapter: DepartmentAdapter? = null

    private var tv_job_id: TextView? = null
    private var tv_job_name: TextView? = null
    private var tv_jobpart: TextView? = null
    private var tv_plannedactivity: TextView? = null
    private var tv_earliestdate: TextView? = null
    private var tv_customer: TextView? = null
    private var tv_description: TextView? = null
    private var tv_promisedate: TextView? = null
    private var tv_cricstartdate: TextView? = null
    private var tv_duetime: TextView? = null
    private var tv_lastactcode: TextView? = null
    private var tv_scheduled: TextView? = null

    private var tv_papername: TextView? = null
    private var tv_txtname: TextView? = null
    private var tv_subtxtname: TextView? = null
    private var tv_weight: TextView? = null
    private var tv_size: TextView? = null
    private var tv_allotedsheet: TextView? = null
    private var tv_mailqty: TextView? = null
    private var tv_noterms: TextView? = null

    private var plus: Boolean = true


    internal  var id: String = ""
    internal  var user_name: String = ""
    internal  var job_part: String = ""
    internal  var job_type_id: String = ""
    internal  var planeed_activity: String = ""
    internal  var earliest_start_date: String = ""
    internal  var custmer: String = ""
    internal  var description: String = ""
    internal  var promise_date: String = ""
    internal  var scheduled: String = ""
    internal  var crit_start_date: String = ""
    internal  var due_time: String = ""
    internal  var last_act_code: String = ""
    internal  var earlist_starttime: String = ""
    internal  var ll_jobdetails:LinearLayout? = null
    internal var  ll2:LinearLayout? = null
    internal var  ll_date_terms_value:LinearLayout? = null
    internal var  layout_date_terms:LinearLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_detailview)


        val toolbar = findViewById(R.id.toolbar) as Toolbar
         ll_jobdetails = findViewById(R.id.ll_jobdetails) as LinearLayout
        val iv_plus = findViewById(R.id.iv_plus) as ImageView
        val iv_edit = findViewById(R.id.iv_edit) as ImageView
        tv_job_id = findViewById(R.id.tv_job_id) as TextView
        tv_job_name = findViewById(R.id.tv_jobname) as TextView
        tv_jobpart = findViewById(R.id.tv_jobpart) as TextView
        tv_plannedactivity = findViewById(R.id.tv_plannedactivity) as TextView
        tv_earliestdate = findViewById(R.id.tv_earliestdate) as TextView
        tv_customer = findViewById(R.id.tv_customer) as TextView
        tv_description = findViewById(R.id.tv_description) as TextView
        tv_promisedate = findViewById(R.id.tv_promisedate) as TextView
        tv_scheduled = findViewById(R.id.tv_scheduled) as TextView
        tv_cricstartdate = findViewById(R.id.tv_cricstartdate) as TextView
        tv_duetime = findViewById(R.id.tv_duetime) as TextView
        tv_lastactcode = findViewById(R.id.tv_lastactcode) as TextView

        tv_papername = findViewById(R.id.tv_papername) as TextView
        tv_txtname = findViewById(R.id.tv_txtname) as TextView
        tv_subtxtname = findViewById(R.id.tv_subtxtname) as TextView
        tv_weight = findViewById(R.id.tv_weight) as TextView
        tv_size = findViewById(R.id.tv_size) as TextView
        tv_allotedsheet = findViewById(R.id.tv_allotedsheet) as TextView
        tv_mailqty = findViewById(R.id.tv_mailqty) as TextView
        tv_noterms = findViewById(R.id.tv_noterms) as TextView

         ll_date_terms_value = findViewById(R.id.layout_date_terms_value) as LinearLayout
        layout_date_terms = findViewById(R.id.layout_date_terms) as LinearLayout
        //val lm = findViewById(R.id.linearMain) as LinearLayout



/*
        listview_department = findViewById(R.id.listview_department) as RecyclerView

        listview_department!!.isNestedScrollingEnabled = false*/

        listview_department1 = findViewById(R.id.listview_department1) as ListView



        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        toolbar.setTitle("Assigned To User")
        //myloading()
        if(intent.getStringExtra("id")!=null) {
            id = intent.getStringExtra("id")
            job_id = intent.getStringExtra("job_id")
        }
        //callJobDetailViewAPI()


        iv_plus.setOnClickListener(View.OnClickListener {
            if (plus) {
                iv_plus.setImageResource(R.drawable.minus_icon)
                plus = false;
                ll_jobdetails!!.visibility = View.VISIBLE
                iv_edit.visibility = View.VISIBLE
            } else {
                plus = true;
                iv_plus.setImageResource(R.drawable.plus)
                ll_jobdetails!!.visibility = View.GONE
                iv_edit.visibility = View.INVISIBLE
            }

        })

        iv_edit.setOnClickListener(View.OnClickListener {
            val home_intent = Intent(this@JobDetailViewActivity, JobEditActivity::class.java)
            home_intent.putExtra("id",id)
            home_intent.putExtra("job_id",job_id)
            home_intent.putExtra("job_part",job_part)
            home_intent.putExtra("job_type_id",job_type_id)
            home_intent.putExtra("planeed_activity",planeed_activity)
            home_intent.putExtra("earliest_start_date",earliest_start_date)
            home_intent.putExtra("custmer",custmer)
            home_intent.putExtra("description",description)
            home_intent.putExtra("promise_date",promise_date)
            home_intent.putExtra("scheduled",scheduled)
            home_intent.putExtra("crit_start_date",crit_start_date)
            home_intent.putExtra("due_time",due_time)
            home_intent.putExtra("last_act_code",last_act_code)
            home_intent.putExtra("earliest_start_time",earlist_starttime)
            startActivity(home_intent)

        })


    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

   /* private fun callJobDetailViewAPI() {
        // my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.jobDetailView(job_id)
        Log.d("JOBDETAILVIEW", job_id)
        call.enqueue(object : Callback<JobDetailViewResponse> {
            override fun onResponse(call: Call<JobDetailViewResponse>, response: retrofit2.Response<JobDetailViewResponse>?) {
                if (response != null) {
                    // my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)

                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {
                        job_id_val = response.body()!!.data!!.job_id.toString()
                        job_id = response.body()!!.data!!.id.toString()
                        val job_part = response.body()!!.data!!.job_part
                        val job_name = response.body()!!.data!!.job_name
                        val job_type_id = response.body()!!.data!!.job_type_id
                        val planeed_activity = response.body()!!.data!!.planeed_activity
                        val earliest_start_date = response.body()!!.data!!.earliest_start_date
                        val custmer = response.body()!!.data!!.custmer
                        val description = response.body()!!.data!!.description
                        val promise_date = response.body()!!.data!!.promise_date
                        val scheduled = response.body()!!.data!!.scheduled
                        val crit_start_date = response.body()!!.data!!.crit_start_date
                        val due_time = response.body()!!.data!!.due_time
                        val last_act_code = response.body()!!.data!!.last_act_code
                        val paper_name = response.body()!!.data!!.paper_name
                        val text_name = response.body()!!.data!!.text_name
                        val sub_text_name = response.body()!!.data!!.sub_text_name
                        val weight = response.body()!!.data!!.weight
                        val size_id = response.body()!!.data!!.size_id
                        val alloted_sheets = response.body()!!.data!!.alloted_sheets
                        val mailing_qty = response.body()!!.data!!.mailing_qty
                        val no_of_terms = response.body()!!.data!!.no_of_terms
                        val width = response.body()!!.data!!.width
                        val height = response.body()!!.data!!.height
                        if(no_of_terms.equals("0")){
                            layout_date_terms!!.visibility =  View.GONE
                        }


                        val term_list = response.body()!!.data!!.terms
                        val termdate_list = response.body()!!.data!!.term_date
                        Log.d("term_list", ""+term_list!!.size+"---"+term_list.toString())
                        for (k in 0 until term_list!!.size){
                            ll2 = LinearLayout(applicationContext)
                            ll2!!.setOrientation(LinearLayout.VERTICAL)
                            val layout2 = LayoutInflater.from(applicationContext).inflate(R.layout.layout_date_terms, ll_jobdetails, false)

                            val tv_date_value = layout2.findViewById(R.id.tv_date_value) as TextView
                            val tv_date_term_value = layout2.findViewById(R.id.tv_date_term_value) as TextView

                            tv_date_value.setText(term_list.get(k))
                            tv_date_term_value.setText(termdate_list!!.get(k))

                            ll2!!.addView(layout2)
                            ll_date_terms_value!!.addView(ll2)
                        }



                        try {
                            detailEditresponse(response.body()!!.data)
                        }catch (e:Exception){Log.w("Exception ","Data Binding VIew: "+e.message)}

                        tv_job_id!!.setText(job_id_val)
                        tv_job_name!!.setText(job_name)
                        tv_jobpart!!.setText(job_part)
                        tv_plannedactivity!!.setText(planeed_activity)
                        tv_earliestdate!!.setText(earliest_start_date)
                        tv_customer!!.setText(custmer)
                        tv_description!!.setText(description)
                        tv_promisedate!!.setText(promise_date)
                        tv_cricstartdate!!.setText(crit_start_date)
                        tv_duetime!!.setText(due_time)
                        tv_lastactcode!!.setText(last_act_code)
                        tv_scheduled!!.setText(scheduled)


                        tv_papername!!.setText(paper_name)
                        tv_txtname !!.setText(text_name)
                        tv_subtxtname!!.setText(sub_text_name)
                        tv_weight!!.setText(weight)
                        tv_size !!.setText(width+"*"+height)
                        tv_allotedsheet !!.setText(alloted_sheets)
                        tv_mailqty!!.setText(mailing_qty)
                        tv_noterms !!.setText(no_of_terms)

                        if(response.body()!!.data!!.jobpart_departments !=null && response.body()!!.data!!.jobpart_departments!!.size>0) {
                            val list: Array<JobDetailViewDataDepartmentResponse>? = response.body()!!.data!!.jobpart_departments
                            val contacts = ArrayList<Contact>()
                            //  var position = 0
                            // var isSeparator = false
                            if(list!!.isNotEmpty()) {
                                for (item: JobDetailViewDataDepartmentResponse in list!!.iterator()) {
                                    // isSeparator = false
                                    // if (isSeparator) {
                                    val contact = Contact("Job Part: " + item.job_part.toString(), "", "", "", "", "", "", "", "", true)
                                    contacts.add(contact)
                                    // }
                                    if (item.department!=null && item.department!!.size > 0) {
                                        val list1: Array<JobsListDepartmentsDataResponse>? = item.department
                                        for (item1: JobsListDepartmentsDataResponse in list1!!.iterator()) {
                                            Log.e("Response_Product", item1.user_name.toString() + "---" + item1.job_dep_id.toString())
                                            val contact = Contact(item1.user_name.toString(), item1.department_name.toString(), item1.job_dep_id.toString(),
                                                    item1.status.toString(), item1.department_id.toString(), job_id_val, item1.job_id.toString(), item1.assigned_id.toString(), item1.assigned_status.toString(), false)
                                            contacts.add(contact)
                                        }
                                    }

                                    // position++

                                    *//*mServiceInfo.add(item)
                            //setdataDepartmentAdapter(mServiceInfo)
                            listview_department!!.setHasFixedSize(true)
                            val layoutManager = LinearLayoutManager(this@JobDetailViewActivity);
                            listview_department!!.setLayoutManager(layoutManager)
                            listview_department!!.setItemAnimator(DefaultItemAnimator())
                            val details_adapter = DepartmentAdapter(mServiceInfo, this@JobDetailViewActivity)
                            listview_department!!.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()*//*
                                }
                            }

                            // Creating our custom adapter
                            val adapter1 = CustomAdapter(this@JobDetailViewActivity, contacts, id)

                            // Create the list view and bind the adapter
                            // val listView = findViewById(R.id.listview) as ListView
                            listview_department1!!.adapter = adapter1
                            Helper.getListViewSize(listview_department1)
                        }

                    } else {
                        Toast.makeText(applicationContext, "Invalid Username and Password", Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<JobDetailViewResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed" + t.message)
            }
        })
    }*/

    private fun detailEditresponse(data: JobDetailViewDataResponse?) {

         id = data!!.id!!
         job_id = data!!.job_id!!
         job_part = data!!.job_part!!
         job_type_id = data!!.job_type_id!!
         planeed_activity = data!!.planeed_activity!!
         earliest_start_date = data!!.earliest_start_date!!
         custmer = data!!.custmer!!
         description = data!!.description!!
         promise_date = data!!.promise_date!!
         scheduled = data!!.scheduled!!
         crit_start_date = data!!.crit_start_date!!
         due_time = data!!.due_time!!
         last_act_code = data!!.last_act_code!!
         earlist_starttime = data!!.earliest_start_time!!

    }


   /* private fun setdataDepartmentAdapter(mServiceInfo: ArrayList<JobDetailViewDataDepartmentResponse>) {
        mServiceDetailsAdapter = DepartmentAdapter(mServiceInfo, this)
        listview_department!!.adapter = mServiceDetailsAdapter
        mServiceDetailsAdapter!!.notifyDataSetChanged()
    }*/


}
