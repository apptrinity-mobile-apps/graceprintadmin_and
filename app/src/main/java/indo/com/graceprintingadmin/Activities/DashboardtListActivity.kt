package indo.com.graceprintingadmin.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.*
import android.util.Log
import android.view.MenuItem
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4printuser.model.ApiInterface
import com.indobytes.biil.adapter.ServiceDetailsRecyclerAdapter
import indo.com.graceprintingadmin.Adapter.DeptDetailsRecyclerAdapter
import indo.com.graceprintingadmin.Helper.SessionManager
import indo.com.graceprintingadmin.Model.APIResponse.DashBoardResponse
import indo.com.graceprintingadmin.Model.APIResponse.Departments
import indo.com.graceprintingadmin.Model.APIResponse.Jobstatuses
import indo.com.graceprintingadmin.R
import retrofit2.Call
import retrofit2.Callback


class DashboardtListActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    lateinit var drawer: DrawerLayout
    internal lateinit var user_name: String
    internal lateinit var dept_name: String
    internal var list_status: Boolean = false

    private var rv_jobstatusview: RecyclerView? = null
    private var rv_jobdeptstatusview: RecyclerView? = null
    private var iv_dept_view: ImageView? = null
    internal lateinit var my_loader: Dialog
    private var mSearchListAdapter: ServiceDetailsRecyclerAdapter? = null
    private var mDeptListAdapter: DeptDetailsRecyclerAdapter? = null
    internal lateinit var sessionManager: SessionManager

    var jobstatuslist: ArrayList<Jobstatuses> = ArrayList<Jobstatuses>()
    var jobdeptstatuslist: ArrayList<Departments> = ArrayList<Departments>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboardnav)

        // val toolbar = findViewById(R.id.toolbar) as Toolbar
        /*setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(false)*/
        /*  toolbar.setNavigationOnClickListener {
              onBackPressed()
          }*/
        sessionManager = SessionManager(this)
        Log.w("CHECKLOGIN", "*** " + sessionManager.checkLogin())
        sessionManager.checkLogin()

        val usersize = sessionManager.userDetails.size
        Log.w("SESSINSIZE", "*** " + usersize)
        val user = sessionManager.userDetails
        sessionManager.tabstatus("-1")

        user_name = user.get(SessionManager.ADMIN_USERNAME).toString()
        dept_name = user.get(SessionManager.ADMIN_NAME).toString()

        drawer = findViewById<DrawerLayout>(R.id.drawer_layout)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)


        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close)

        drawer.setDrawerListener(toggle)
        toggle.syncState()


        toggle.setDrawerIndicatorEnabled(true);
        //toggle.setHomeAsUpIndicator(R.drawable.ic_launcher_background);

        toggle.setToolbarNavigationClickListener {

            drawer.openDrawer(GravityCompat.START);
        }

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        /* val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
         viewpager_main.adapter = fragmentAdapter

         tabs_main.setupWithViewPager(viewpager_main)*/

        // tabs_main.getTabAt(0)!!.setCustomView(R.layout.tab1);
        //tabs_main.getTabAt(1)!!.setCustomView(R.layout.tab2);
        //tabs_main.getTabAt(2)!!.setCustomView(R.layout.tab3);
        //tabs_main.getTabAt(3)!!.setCustomView(R.layout.tab4);


        // Log.e("DEPT", "" + user_name+"-----"+dept_name);

        val header = navigationView.getHeaderView(0)
        val username = header.findViewById(R.id.tv_username) as TextView
        val deptname = header.findViewById(R.id.tv_department_id) as TextView
        username!!.setText(dept_name)
        deptname!!.setText(user_name)

        toolbar.setTitle("DEPARTMENTS")


        // user_id = user.get(SessionManager.PROFILE_ID).toString()
        myloading()


        rv_jobstatusview = findViewById(R.id.rv_depmentlist) as RecyclerView
        iv_dept_view = findViewById(R.id.iv_dept_view) as ImageView
        rv_jobdeptstatusview = findViewById(R.id.lv_depmentlist2) as RecyclerView
        callSearchList()

        iv_dept_view!!.setOnClickListener {

            if (list_status == false) {
                iv_dept_view!!.setImageResource(R.drawable.dept_eye_close)
                callSearchList()
                list_status = true
            } else {
                iv_dept_view!!.setImageResource(R.drawable.dept_view_open)
                callSearchList()
                list_status = false

            }
        }

    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_dashboard -> {
                val intent = Intent(this, DashboardtListActivity::class.java)
                startActivity(intent)
            }
            R.id.add_job -> {
                val intent = Intent(this, AddJobActivity::class.java)
                startActivity(intent)
            }
            /* R.id.assign_job -> {
                 val intent = Intent(this,ListofjobsActivity::class.java)
                 startActivity(intent)
             }*/

            R.id.nav_logout -> {
                sessionManager.logoutUser()
            }

            /* R.id.nav_manage -> toast("Click Manage")
             R.id.nav_share -> toast("Click Share")
             R.id.nav_send -> toast("Click Send")*/
        }

        drawer.closeDrawer(GravityCompat.START)

        return true
    }

    private fun callSearchList() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getDashboard()
        jobstatuslist.clear()
        jobdeptstatuslist.clear()
        call.enqueue(object : Callback<DashBoardResponse> {
            override fun onResponse(call: Call<DashBoardResponse>, response: retrofit2.Response<DashBoardResponse>?) {
                my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.jobList != null) {
                        if (response.body()!!.jobList!!.jobstatuses != null && response.body()!!.jobList!!.jobstatuses!!.size > 0) {
                            val list: Array<Jobstatuses> = response.body()!!.jobList!!.jobstatuses!!

                            for (item: Jobstatuses in list.iterator()) {
                                jobstatuslist.add(item)
                            }
                            setProductAdapter(jobstatuslist)
                        }
                        if (response.body()!!.jobList!!.jobstatuses != null && response.body()!!.jobList!!.departments!!.size > 0) {
                            val list: Array<Departments> = response.body()!!.jobList!!.departments!!

                            for (item: Departments in list.iterator()) {
                                jobdeptstatuslist.add(item)
                            }
                            setDeptAdapter(jobdeptstatuslist)
                        }
                    }

                } else {
                    Toast.makeText(applicationContext, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<DashBoardResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun setProductAdapter(deptlistdata: ArrayList<Jobstatuses>) {
        rv_jobstatusview!!.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this);
        rv_jobstatusview!!.setLayoutManager(layoutManager)
        rv_jobstatusview!!.setItemAnimator(DefaultItemAnimator())
        mSearchListAdapter = ServiceDetailsRecyclerAdapter(deptlistdata, this)
        rv_jobstatusview!!.adapter = mSearchListAdapter
        mSearchListAdapter!!.notifyDataSetChanged()
    }

    private fun setDeptAdapter(deptlistdata: ArrayList<Departments>) {
        if (list_status == true) {
            rv_jobdeptstatusview!!.setHasFixedSize(true)
            val gridLayoutManager = GridLayoutManager(this@DashboardtListActivity, 2)
            rv_jobdeptstatusview!!.setLayoutManager(gridLayoutManager)
            rv_jobdeptstatusview!!.setItemAnimator(DefaultItemAnimator())
            mDeptListAdapter = DeptDetailsRecyclerAdapter(list_status, deptlistdata, this)
            rv_jobdeptstatusview!!.setAdapter(mDeptListAdapter)
            mDeptListAdapter!!.notifyDataSetChanged()

        } else {
            rv_jobdeptstatusview!!.setHasFixedSize(true)
            val layoutManager = LinearLayoutManager(this);
            rv_jobdeptstatusview!!.setLayoutManager(layoutManager)
            rv_jobdeptstatusview!!.setItemAnimator(DefaultItemAnimator())
            mDeptListAdapter = DeptDetailsRecyclerAdapter(list_status, deptlistdata, this)
            rv_jobdeptstatusview!!.adapter = mDeptListAdapter
            mDeptListAdapter!!.notifyDataSetChanged()
        }
    }

    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}
