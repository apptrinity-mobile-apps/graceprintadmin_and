package indo.com.graceprintingadmin.Activities

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Helper.CustomTextView
import indo.com.graceprintingadmin.Helper.MultiSelectionSpinner
import indo.com.graceprintingadmin.Helper.SessionManager
import indo.com.graceprintingadmin.Model.APIResponse.*
import indo.com.graceprintingadmin.R
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AddJobActivity : AppCompatActivity(), MultiSelectionSpinner.OnMultipleItemsSelectedListener, View.OnClickListener {


    var seletedItems = ArrayList<String>()
    lateinit var admin_customer_id: AutoCompleteTextView
    lateinit var admin_job_type_id: EditText
    lateinit var admin_job_id: EditText
    lateinit var admin_job_part_id: EditText
    lateinit var admin_job_terms: EditText
    lateinit var admin_job_terms1: EditText
    lateinit var admin_job_terms2: EditText
    lateinit var admin_job_terms3: EditText
    lateinit var admin_job_terms5: EditText
    lateinit var admin_job_terms6: EditText
    lateinit var admin_job_terms7: EditText
    lateinit var admin_job_terms8: EditText
    lateinit var admin_job_terms9: EditText
    lateinit var admin_job_terms10: EditText
    lateinit var admin_Pactivity_id: EditText
    lateinit var admin_Estart_date_id: CustomTextView
    lateinit var admin_description_id: EditText
    lateinit var admin_Pdata_id: CustomTextView
    lateinit var tv_departments: TextView
    lateinit var tv_departments2: TextView
    lateinit var tv_departments3: TextView
    lateinit var tv_departments4: TextView
    lateinit var tv_departments5: TextView
    lateinit var tv_departments6: TextView
    lateinit var tv_departments7: TextView
    lateinit var tv_departments8: TextView
    lateinit var tv_departments9: TextView
    lateinit var tv_departments10: TextView
    lateinit var admin_Estart_time_id: CustomTextView
    lateinit var admin_schedule_id: EditText
    lateinit var admin_Cstart_date_id: CustomTextView
    lateinit var admin_due_time_id: CustomTextView
    lateinit var tv_availablesheets: CustomTextView
    lateinit var tv_availablesheets1: CustomTextView
    lateinit var tv_error_jobID: CustomTextView
    lateinit var tv_availablesheets2: CustomTextView
    lateinit var tv_availablesheets3: CustomTextView
    lateinit var tv_availablesheets5: CustomTextView
    lateinit var tv_availablesheets6: CustomTextView
    lateinit var tv_availablesheets7: CustomTextView
    lateinit var tv_availablesheets8: CustomTextView
    lateinit var tv_availablesheets9: CustomTextView
    lateinit var tv_availablesheets10: CustomTextView
    lateinit var admin_Lact_code_id: EditText
    lateinit var btn_submit_id: Button
    lateinit var admin_departments: MultiSelectionSpinner
    lateinit var admin_departments2: MultiSelectionSpinner
    lateinit var admin_departments3: MultiSelectionSpinner
    lateinit var admin_departments4: MultiSelectionSpinner
    lateinit var admin_departments5: MultiSelectionSpinner
    lateinit var admin_departments6: MultiSelectionSpinner
    lateinit var admin_departments7: MultiSelectionSpinner
    lateinit var admin_departments8: MultiSelectionSpinner
    lateinit var admin_departments9: MultiSelectionSpinner
    lateinit var admin_departments10: MultiSelectionSpinner
    lateinit var rv_main_id: ListView
    lateinit var rv_main_id2: ListView
    lateinit var rv_main_id3: ListView
    lateinit var rv_main_id4: ListView
    lateinit var rv_main_id5: ListView
    lateinit var rv_main_id6: ListView
    lateinit var rv_main_id7: ListView
    lateinit var rv_main_id8: ListView
    lateinit var rv_main_id9: ListView
    lateinit var rv_main_id10: ListView
    lateinit var admin_tasks: EditText
    lateinit var admin_tasks2: EditText
    lateinit var admin_tasks3: EditText
    lateinit var admin_tasks4: EditText
    lateinit var admin_tasks5: EditText
    lateinit var admin_tasks6: EditText
    lateinit var admin_tasks7: EditText
    lateinit var admin_tasks8: EditText
    lateinit var admin_tasks9: EditText
    lateinit var admin_tasks10: EditText
    lateinit var ll_jobpart1: LinearLayout
    lateinit var ll_jobpart2: LinearLayout
    lateinit var ll_jobpart3: LinearLayout
    lateinit var ll_jobpart4: LinearLayout
    lateinit var ll_jobpart5: LinearLayout
    lateinit var ll_jobpart6: LinearLayout
    lateinit var ll_jobpart7: LinearLayout
    lateinit var ll_jobpart8: LinearLayout
    lateinit var ll_jobpart9: LinearLayout
    lateinit var ll_jobpart10: LinearLayout
    lateinit var btn_jobpart1: Button
    lateinit var btn_jobpart2: Button
    lateinit var btn_jobpart3: Button
    lateinit var btn_jobpart4: Button
    lateinit var btn_jobpart5: Button
    lateinit var btn_jobpart6: Button
    lateinit var btn_jobpart7: Button
    lateinit var btn_jobpart8: Button
    lateinit var btn_jobpart9: Button
    lateinit var btn_jobpart10: Button
    var sizelist: ArrayList<String> = ArrayList<String>()
    var weightlist: ArrayList<String> = ArrayList<String>()
    var papertypestringlist: ArrayList<String> = ArrayList<String>()
    var optionliststringlist: ArrayList<String> = ArrayList<String>()
    var texttypelisttringlist: ArrayList<String> = ArrayList<String>()
    var subtexttypelisttringlist: ArrayList<String> = ArrayList<String>()
    var papertypelist: ArrayList<Papertypes> = ArrayList<Papertypes>()
    var optiontypelist: ArrayList<OptiondataListResponse> = ArrayList<OptiondataListResponse>()
    var texttypelist: ArrayList<Texttypes> = ArrayList<Texttypes>()
    var subtexttypelist: ArrayList<Subtexttypes> = ArrayList<Subtexttypes>()
    var sizelisttype: ArrayList<Sizes> = ArrayList<Sizes>()
    var weightlisttype: ArrayList<Weights> = ArrayList<Weights>()
    lateinit var ll_subterms1: LinearLayout
    lateinit var ll_subterms11: LinearLayout
    lateinit var ll_subterms21: LinearLayout
    lateinit var ll_subterms31: LinearLayout
    lateinit var ll_subterms51: LinearLayout
    lateinit var ll_subterms61: LinearLayout
    lateinit var ll_subterms71: LinearLayout
    lateinit var ll_subterms81: LinearLayout
    lateinit var ll_subterms91: LinearLayout
    lateinit var ll_subterms101: LinearLayout
    lateinit var ll_subterms2: LinearLayout
    lateinit var ll_subterms12: LinearLayout
    lateinit var ll_subterms22: LinearLayout
    lateinit var ll_subterms32: LinearLayout
    lateinit var ll_subterms52: LinearLayout
    lateinit var ll_subterms62: LinearLayout
    lateinit var ll_subterms72: LinearLayout
    lateinit var ll_subterms82: LinearLayout
    lateinit var ll_subterms92: LinearLayout
    lateinit var ll_subterms102: LinearLayout
    lateinit var ll_subterms3: LinearLayout
    lateinit var ll_subterms13: LinearLayout
    lateinit var ll_subterms23: LinearLayout
    lateinit var ll_subterms33: LinearLayout
    lateinit var ll_subterms53: LinearLayout
    lateinit var ll_subterms63: LinearLayout
    lateinit var ll_subterms73: LinearLayout
    lateinit var ll_subterms83: LinearLayout
    lateinit var ll_subterms93: LinearLayout
    lateinit var ll_subterms103: LinearLayout
    lateinit var ll_subterms4: LinearLayout
    lateinit var ll_subterms14: LinearLayout
    lateinit var ll_subterms24: LinearLayout
    lateinit var ll_subterms34: LinearLayout
    lateinit var ll_subterms54: LinearLayout
    lateinit var ll_subterms64: LinearLayout
    lateinit var ll_subterms74: LinearLayout
    lateinit var ll_subterms84: LinearLayout
    lateinit var ll_subterms94: LinearLayout
    lateinit var ll_subterms104: LinearLayout
    lateinit var ll_subterms5: LinearLayout
    lateinit var ll_subterms15: LinearLayout
    lateinit var ll_subterms25: LinearLayout
    lateinit var ll_subterms35: LinearLayout
    lateinit var ll_subterms55: LinearLayout
    lateinit var ll_subterms65: LinearLayout
    lateinit var ll_subterms75: LinearLayout
    lateinit var ll_subterms85: LinearLayout
    lateinit var ll_subterms95: LinearLayout
    lateinit var ll_subterms105: LinearLayout
    lateinit var ll_subterms6: LinearLayout
    lateinit var ll_subterms16: LinearLayout
    lateinit var ll_subterms26: LinearLayout
    lateinit var ll_subterms36: LinearLayout
    lateinit var ll_subterms56: LinearLayout
    lateinit var ll_subterms66: LinearLayout
    lateinit var ll_subterms76: LinearLayout
    lateinit var ll_subterms86: LinearLayout
    lateinit var ll_subterms96: LinearLayout
    lateinit var ll_subterms106: LinearLayout
    lateinit var ll_subterms7: LinearLayout
    lateinit var ll_subterms17: LinearLayout
    lateinit var ll_subterms27: LinearLayout
    lateinit var ll_subterms37: LinearLayout
    lateinit var ll_subterms57: LinearLayout
    lateinit var ll_subterms67: LinearLayout
    lateinit var ll_subterms77: LinearLayout
    lateinit var ll_subterms87: LinearLayout
    lateinit var ll_subterms97: LinearLayout
    lateinit var ll_subterms107: LinearLayout
    lateinit var ll_subterms8: LinearLayout
    lateinit var ll_subterms18: LinearLayout
    lateinit var ll_subterms28: LinearLayout
    lateinit var ll_subterms38: LinearLayout
    lateinit var ll_subterms58: LinearLayout
    lateinit var ll_subterms68: LinearLayout
    lateinit var ll_subterms78: LinearLayout
    lateinit var ll_subterms88: LinearLayout
    lateinit var ll_subterms98: LinearLayout
    lateinit var ll_subterms108: LinearLayout
    lateinit var ll_subterms9: LinearLayout
    lateinit var ll_subterms19: LinearLayout
    lateinit var ll_subterms29: LinearLayout
    lateinit var ll_subterms39: LinearLayout
    lateinit var ll_subterms59: LinearLayout
    lateinit var ll_subterms69: LinearLayout
    lateinit var ll_subterms79: LinearLayout
    lateinit var ll_subterms89: LinearLayout
    lateinit var ll_subterms99: LinearLayout
    lateinit var ll_subterms109: LinearLayout
    lateinit var ll_subterms10: LinearLayout
    lateinit var ll_subterms110: LinearLayout
    lateinit var ll_subterms210: LinearLayout
    lateinit var ll_subterms310: LinearLayout
    lateinit var ll_subterms510: LinearLayout
    lateinit var ll_subterms610: LinearLayout
    lateinit var ll_subterms710: LinearLayout
    lateinit var ll_subterms810: LinearLayout
    lateinit var ll_subterms910: LinearLayout
    lateinit var ll_subterms1010: LinearLayout
    lateinit var admin_termssitem1: EditText
    lateinit var admin_termssitem11: EditText
    lateinit var admin_termssitem21: EditText
    lateinit var admin_termssitem31: EditText
    lateinit var admin_termssitem51: EditText
    lateinit var admin_termssitem61: EditText
    lateinit var admin_termssitem71: EditText
    lateinit var admin_termssitem81: EditText
    lateinit var admin_termssitem91: EditText
    lateinit var admin_termssitem101: EditText
    lateinit var admin_termssitem52: EditText
    lateinit var admin_termssitem62: EditText
    lateinit var admin_termssitem72: EditText
    lateinit var admin_termssitem82: EditText
    lateinit var admin_termssitem92: EditText
    lateinit var admin_termssitem102: EditText
    lateinit var admin_termssitem53: EditText
    lateinit var admin_termssitem63: EditText
    lateinit var admin_termssitem73: EditText
    lateinit var admin_termssitem83: EditText
    lateinit var admin_termssitem93: EditText
    lateinit var admin_termssitem103: EditText
    lateinit var admin_termssitem54: EditText
    lateinit var admin_termssitem64: EditText
    lateinit var admin_termssitem74: EditText
    lateinit var admin_termssitem84: EditText
    lateinit var admin_termssitem94: EditText
    lateinit var admin_termssitem104: EditText
    lateinit var admin_termssitem55: EditText
    lateinit var admin_termssitem65: EditText
    lateinit var admin_termssitem75: EditText
    lateinit var admin_termssitem85: EditText
    lateinit var admin_termssitem95: EditText
    lateinit var admin_termssitem105: EditText
    lateinit var admin_termssitem56: EditText
    lateinit var admin_termssitem66: EditText
    lateinit var admin_termssitem76: EditText
    lateinit var admin_termssitem86: EditText
    lateinit var admin_termssitem96: EditText
    lateinit var admin_termssitem106: EditText
    lateinit var admin_termssitem57: EditText
    lateinit var admin_termssitem67: EditText
    lateinit var admin_termssitem77: EditText
    lateinit var admin_termssitem87: EditText
    lateinit var admin_termssitem97: EditText
    lateinit var admin_termssitem107: EditText
    lateinit var admin_termssitem58: EditText
    lateinit var admin_termssitem68: EditText
    lateinit var admin_termssitem78: EditText
    lateinit var admin_termssitem88: EditText
    lateinit var admin_termssitem98: EditText
    lateinit var admin_termssitem108: EditText
    lateinit var admin_termssitem59: EditText
    lateinit var admin_termssitem69: EditText
    lateinit var admin_termssitem79: EditText
    lateinit var admin_termssitem89: EditText
    lateinit var admin_termssitem99: EditText
    lateinit var admin_termssitem109: EditText
    lateinit var admin_termssitem510: EditText
    lateinit var admin_termssitem610: EditText
    lateinit var admin_termssitem710: EditText
    lateinit var admin_termssitem810: EditText
    lateinit var admin_termssitem910: EditText
    lateinit var admin_termssitem1010: EditText
    lateinit var admin_termssitem2: EditText
    lateinit var admin_termssitem12: EditText
    lateinit var admin_termssitem22: EditText
    lateinit var admin_termssitem32: EditText
    lateinit var admin_termssitem3: EditText
    lateinit var admin_termssitem13: EditText
    lateinit var admin_termssitem23: EditText
    lateinit var admin_termssitem33: EditText
    lateinit var admin_termssitem4: EditText
    lateinit var admin_termssitem14: EditText
    lateinit var admin_termssitem24: EditText
    lateinit var admin_termssitem34: EditText
    lateinit var admin_termssitem5: EditText
    lateinit var admin_termssitem15: EditText
    lateinit var admin_termssitem25: EditText
    lateinit var admin_termssitem35: EditText
    lateinit var admin_termssitem6: EditText
    lateinit var admin_termssitem16: EditText
    lateinit var admin_termssitem26: EditText
    lateinit var admin_termssitem36: EditText
    lateinit var admin_termssitem7: EditText
    lateinit var admin_termssitem17: EditText
    lateinit var admin_termssitem27: EditText
    lateinit var admin_termssitem37: EditText
    lateinit var admin_termssitem8: EditText
    lateinit var admin_termssitem18: EditText
    lateinit var admin_termssitem28: EditText
    lateinit var admin_termssitem38: EditText
    lateinit var admin_termssitem9: EditText
    lateinit var admin_termssitem19: EditText
    lateinit var admin_termssitem29: EditText
    lateinit var admin_termssitem39: EditText
    lateinit var admin_termssitem10: EditText
    lateinit var admin_termssitem110: EditText
    lateinit var admin_termssitem210: EditText
    lateinit var admin_termssitem310: EditText
    lateinit var admin_termsdate1: EditText
    lateinit var admin_termsdate11: EditText
    lateinit var admin_termsdate21: EditText
    lateinit var admin_termsdate31: EditText
    lateinit var admin_termsdate51: EditText
    lateinit var admin_termsdate61: EditText
    lateinit var admin_termsdate71: EditText
    lateinit var admin_termsdate81: EditText
    lateinit var admin_termsdate91: EditText
    lateinit var admin_termsdate101: EditText
    lateinit var admin_termsdate52: EditText
    lateinit var admin_termsdate62: EditText
    lateinit var admin_termsdate72: EditText
    lateinit var admin_termsdate82: EditText
    lateinit var admin_termsdate92: EditText
    lateinit var admin_termsdate102: EditText
    lateinit var admin_termsdate53: EditText
    lateinit var admin_termsdate63: EditText
    lateinit var admin_termsdate73: EditText
    lateinit var admin_termsdate83: EditText
    lateinit var admin_termsdate93: EditText
    lateinit var admin_termsdate103: EditText
    lateinit var admin_termsdate54: EditText
    lateinit var admin_termsdate64: EditText
    lateinit var admin_termsdate74: EditText
    lateinit var admin_termsdate84: EditText
    lateinit var admin_termsdate94: EditText
    lateinit var admin_termsdate104: EditText
    lateinit var admin_termsdate55: EditText
    lateinit var admin_termsdate65: EditText
    lateinit var admin_termsdate75: EditText
    lateinit var admin_termsdate85: EditText
    lateinit var admin_termsdate95: EditText
    lateinit var admin_termsdate105: EditText
    lateinit var admin_termsdate56: EditText
    lateinit var admin_termsdate66: EditText
    lateinit var admin_termsdate76: EditText
    lateinit var admin_termsdate86: EditText
    lateinit var admin_termsdate96: EditText
    lateinit var admin_termsdate106: EditText
    lateinit var admin_termsdate57: EditText
    lateinit var admin_termsdate67: EditText
    lateinit var admin_termsdate77: EditText
    lateinit var admin_termsdate87: EditText
    lateinit var admin_termsdate97: EditText
    lateinit var admin_termsdate107: EditText
    lateinit var admin_termsdate58: EditText
    lateinit var admin_termsdate68: EditText
    lateinit var admin_termsdate78: EditText
    lateinit var admin_termsdate88: EditText
    lateinit var admin_termsdate98: EditText
    lateinit var admin_termsdate108: EditText
    lateinit var admin_termsdate59: EditText
    lateinit var admin_termsdate69: EditText
    lateinit var admin_termsdate79: EditText
    lateinit var admin_termsdate89: EditText
    lateinit var admin_termsdate99: EditText
    lateinit var admin_termsdate109: EditText
    lateinit var admin_termsdate510: EditText
    lateinit var admin_termsdate610: EditText
    lateinit var admin_termsdate710: EditText
    lateinit var admin_termsdate810: EditText
    lateinit var admin_termsdate910: EditText
    lateinit var admin_termsdate1010: EditText
    lateinit var admin_termsdate2: EditText
    lateinit var admin_termsdate12: EditText
    lateinit var admin_termsdate22: EditText
    lateinit var admin_termsdate32: EditText
    lateinit var admin_termsdate3: EditText
    lateinit var admin_termsdate13: EditText
    lateinit var admin_termsdate23: EditText
    lateinit var admin_termsdate33: EditText
    lateinit var admin_termsdate4: EditText
    lateinit var admin_termsdate14: EditText
    lateinit var admin_termsdate24: EditText
    lateinit var admin_termsdate34: EditText
    lateinit var admin_termsdate5: EditText
    lateinit var admin_termsdate15: EditText
    lateinit var admin_termsdate25: EditText
    lateinit var admin_termsdate35: EditText
    lateinit var admin_termsdate6: EditText
    lateinit var admin_termsdate16: EditText
    lateinit var admin_termsdate26: EditText
    lateinit var admin_termsdate36: EditText
    lateinit var admin_termsdate7: EditText
    lateinit var admin_termsdate17: EditText
    lateinit var admin_termsdate27: EditText
    lateinit var admin_termsdate37: EditText
    lateinit var admin_termsdate8: EditText
    lateinit var admin_termsdate18: EditText
    lateinit var admin_termsdate28: EditText
    lateinit var admin_termsdate38: EditText
    lateinit var admin_termsdate9: EditText
    lateinit var admin_termsdate19: EditText
    lateinit var admin_termsdate29: EditText
    lateinit var admin_termsdate39: EditText
    lateinit var admin_termsdate10: EditText
    lateinit var admin_termsdate110: EditText
    lateinit var admin_termsdate210: EditText
    lateinit var admin_termsdate310: EditText
    lateinit var typeofpaper: EditText
    lateinit var typeofpaper1: EditText
    lateinit var typeofpaper2: EditText
    lateinit var typeofpaper3: EditText
    lateinit var typeofpaper5: EditText
    lateinit var typeofpaper6: EditText
    lateinit var typeofpaper7: EditText
    lateinit var typeofpaper8: EditText
    lateinit var typeofpaper9: EditText
    lateinit var typeofpaper10: EditText
    lateinit var admin_optionlist: EditText
    lateinit var admin_optionlist1: EditText
    lateinit var admin_optionlist2: EditText
    lateinit var admin_optionlist3: EditText
    lateinit var admin_optionlist4: EditText
    lateinit var admin_optionlist5: EditText
    lateinit var admin_optionlist6: EditText
    lateinit var admin_optionlist7: EditText
    lateinit var admin_optionlist8: EditText
    lateinit var admin_optionlist9: EditText
    lateinit var admin_optionlist10: EditText
    lateinit var txtname: EditText
    lateinit var txtname1: EditText
    lateinit var txtname2: EditText
    lateinit var txtname3: EditText
    lateinit var txtname5: EditText
    lateinit var txtname6: EditText
    lateinit var txtname7: EditText
    lateinit var txtname8: EditText
    lateinit var txtname9: EditText
    lateinit var txtname10: EditText
    lateinit var subtxtname: EditText
    lateinit var subtxtname1: EditText
    lateinit var subtxtname2: EditText
    lateinit var subtxtname3: EditText
    lateinit var subtxtname5: EditText
    lateinit var subtxtname6: EditText
    lateinit var subtxtname7: EditText
    lateinit var subtxtname8: EditText
    lateinit var subtxtname9: EditText
    lateinit var subtxtname10: EditText
    lateinit var weight: EditText
    lateinit var weight1: EditText
    lateinit var weight2: EditText
    lateinit var weight3: EditText
    lateinit var weight5: EditText
    lateinit var weight6: EditText
    lateinit var weight7: EditText
    lateinit var weight8: EditText
    lateinit var weight9: EditText
    lateinit var weight10: EditText
    lateinit var sizetxt: EditText
    lateinit var sizetxt1: EditText
    lateinit var sizetxt2: EditText
    lateinit var sizetxt3: EditText
    lateinit var sizetxt5: EditText
    lateinit var sizetxt6: EditText
    lateinit var sizetxt7: EditText
    lateinit var sizetxt8: EditText
    lateinit var sizetxt9: EditText
    lateinit var sizetxt10: EditText
    lateinit var allotsheets: EditText
    lateinit var allotsheets1: EditText
    lateinit var allotsheets2: EditText
    lateinit var allotsheets3: EditText
    lateinit var allotsheets5: EditText
    lateinit var allotsheets6: EditText
    lateinit var allotsheets7: EditText
    lateinit var allotsheets8: EditText
    lateinit var allotsheets9: EditText
    lateinit var allotsheets10: EditText
    lateinit var totalquantity: EditText
    lateinit var totalquantity1: EditText
    lateinit var totalquantity2: EditText
    lateinit var totalquantity3: EditText
    lateinit var totalquantity5: EditText
    lateinit var totalquantity6: EditText
    lateinit var totalquantity7: EditText
    lateinit var totalquantity8: EditText
    lateinit var totalquantity9: EditText
    lateinit var totalquantity10: EditText
    lateinit var ll_txt_name: LinearLayout
    lateinit var ll_subtxt_name: LinearLayout
    lateinit var ll_weight: LinearLayout
    lateinit var ll_size: LinearLayout
    lateinit var ll_txt_name_one: LinearLayout
    lateinit var ll_subtxt_name_one: LinearLayout
    lateinit var ll_weight_one: LinearLayout
    lateinit var ll_size_one: LinearLayout
    lateinit var ll_optionlist_one: LinearLayout
    lateinit var ll_txt_name_two: LinearLayout
    lateinit var ll_subtxt_name_two: LinearLayout
    lateinit var ll_weight_two: LinearLayout
    lateinit var ll_size_two: LinearLayout
    lateinit var ll_txt_name_three: LinearLayout
    lateinit var ll_subtxt_name_three: LinearLayout
    lateinit var ll_weight_three: LinearLayout
    lateinit var ll_size_three: LinearLayout
    lateinit var ll_txt_name_five: LinearLayout
    lateinit var ll_subtxt_name_five: LinearLayout
    lateinit var ll_weight_five: LinearLayout
    lateinit var ll_size_five: LinearLayout
    lateinit var ll_txt_name_six: LinearLayout
    lateinit var ll_subtxt_name_six: LinearLayout
    lateinit var ll_weight_six: LinearLayout
    lateinit var ll_size_six: LinearLayout
    lateinit var ll_txt_name_seven: LinearLayout
    lateinit var ll_subtxt_name_seven: LinearLayout
    lateinit var ll_weight_seven: LinearLayout
    lateinit var ll_size_seven: LinearLayout
    lateinit var ll_txt_name_eight: LinearLayout
    lateinit var ll_subtxt_name_eight: LinearLayout
    lateinit var ll_weight_eight: LinearLayout
    lateinit var ll_size_eight: LinearLayout
    lateinit var ll_txt_name_nine: LinearLayout
    lateinit var ll_subtxt_name_nine: LinearLayout
    lateinit var ll_weight_nine: LinearLayout
    lateinit var ll_size_nine: LinearLayout
    lateinit var ll_txt_name_ten: LinearLayout
    lateinit var ll_subtxt_name_ten: LinearLayout
    lateinit var ll_weight_ten: LinearLayout
    lateinit var ll_size_ten: LinearLayout
    lateinit var ll_other_desc: LinearLayout
    lateinit var ll_other_desc_one: LinearLayout
    lateinit var ll_other_desc_two: LinearLayout
    lateinit var ll_other_desc_three: LinearLayout
    lateinit var ll_other_desc_five: LinearLayout
    lateinit var ll_other_desc_six: LinearLayout
    lateinit var ll_other_desc_seven: LinearLayout
    lateinit var ll_other_desc_eight: LinearLayout
    lateinit var ll_other_desc_nine: LinearLayout
    lateinit var ll_other_desc_ten: LinearLayout
    lateinit var other_desc: EditText
    lateinit var other_desc_one: EditText
    lateinit var other_desc_two: EditText
    lateinit var other_desc_three: EditText
    lateinit var other_desc_five: EditText
    lateinit var other_desc_six: EditText
    lateinit var other_desc_seven: EditText
    lateinit var other_desc_eight: EditText
    lateinit var other_desc_nine: EditText
    lateinit var other_desc_ten: EditText
    var paperid: String = "0"
    var paperid1: String = "0"
    var paperid2: String = "0"
    var paperid3: String = "0"
    var paperid5: String = "0"
    var paperid6: String = "0"
    var paperid7: String = "0"
    var paperid8: String = "0"
    var paperid9: String = "0"
    var paperid10: String = "0"
    var option_id: String = "0"
    var option_id1: String = "0"
    var option_id2: String = "0"
    var option_id3: String = "0"
    var option_id5: String = "0"
    var option_id6: String = "0"
    var option_id7: String = "0"
    var option_id8: String = "0"
    var option_id9: String = "0"
    var option_id10: String = "0"
    var txtid: String = "0"
    var txtid1: String = "0"
    var txtid2: String = "0"
    var txtid3: String = "0"
    var txtid5: String = "0"
    var txtid6: String = "0"
    var txtid7: String = "0"
    var txtid8: String = "0"
    var txtid9: String = "0"
    var txtid10: String = "0"
    var weightid: String = "0"
    var weightid1: String = "0"
    var weightid2: String = "0"
    var weightid3: String = "0"
    var weightid5: String = "0"
    var weightid6: String = "0"
    var weightid7: String = "0"
    var weightid8: String = "0"
    var weightid9: String = "0"
    var weightid10: String = "0"
    var subtxtid: String = "0"
    var subtxtid1: String = "0"
    var subtxtid2: String = "0"
    var subtxtid3: String = "0"
    var subtxtid5: String = "0"
    var subtxtid6: String = "0"
    var subtxtid7: String = "0"
    var subtxtid8: String = "0"
    var subtxtid9: String = "0"
    var subtxtid10: String = "0"
    var sizeid: String = "0"
    var sizeid1: String = "0"
    var sizeid2: String = "0"
    var sizeid3: String = "0"
    var sizeid5: String = "0"
    var sizeid6: String = "0"
    var sizeid7: String = "0"
    var sizeid8: String = "0"
    var sizeid9: String = "0"
    var sizeid10: String = "0"
    var allotedsheets: String = "0"
    var admin_terms: String = "0"
    var admin_terms1: String = "0"
    var admin_terms2: String = "0"
    var admin_terms3: String = "0"
    var admin_terms5: String = "0"
    var admin_terms6: String = "0"
    var admin_terms7: String = "0"
    var admin_terms8: String = "0"
    var admin_terms9: String = "0"
    var admin_terms10: String = "0"
    var aloted_sheets: String = "0"
    var aloted_sheets1: String = "0"
    var aloted_sheets2: String = "0"
    var aloted_sheets3: String = "0"
    var aloted_sheets5: String = "0"
    var aloted_sheets6: String = "0"
    var aloted_sheets7: String = "0"
    var aloted_sheets8: String = "0"
    var aloted_sheets9: String = "0"
    var aloted_sheets10: String = "0"
    var tot_qty: String = "0"
    var tot_qty1: String = "0"
    var tot_qty2: String = "0"
    var tot_qty3: String = "0"
    var tot_qty5: String = "0"
    var tot_qty6: String = "0"
    var tot_qty7: String = "0"
    var tot_qty8: String = "0"
    var tot_qty9: String = "0"
    var tot_qty10: String = "0"
    var chkJobIderrorStg: String? = null
    lateinit var list_items_list: ListView
    lateinit var list_items_list2: ListView
    lateinit var list_items_list_type: ListView
    lateinit var list_items_list_type2: ListView
    lateinit var list_items_optionlist_type1: ListView
    lateinit var list_items_list_type3: ListView
    lateinit var list_items_list_type4: ListView
    lateinit var list_items_list_type5: ListView
    lateinit var list_items_list_type6: ListView
    lateinit var list_items_list_type3_txt: ListView
    lateinit var dialog_list: Dialog
    lateinit var dialog_listpaper: Dialog
    lateinit var dialog_list_type: Dialog
    lateinit var dialog_list_type2: Dialog
    lateinit var dialog_list_option_type: Dialog
    lateinit var dialog_list_type3: Dialog
    lateinit var dialog_list_subtxt: Dialog
    lateinit var dialog_list_sizes: Dialog
    lateinit var dialog_list_weights: Dialog
    lateinit var dialog_list_txt: Dialog
    var admin_customer_stg: String? = null
    var admin_job_type_stg: String? = null
    var admin_job_stg: String? = null
    var admin_job_part_stg: String? = null
    var admin_Pactivity_stg: String? = null
    var admin_Estart_date_stg: String? = null
    var admin_description_stg: String? = null
    var admin_Pdata_stg: String? = null
    var admin_Estart_time_stg: String? = null
    var admin_schedule_stg: String? = null
    var admin_Cstart_date_stg: String? = null
    var admin_due_time_stg: String? = null
    var admin_Lact_code_stg: String? = null
    var admin_tasks_stg: String? = null
    var mServiceInfo_Jobname = ArrayList<String>()
    var mServiceInfo_Jobname_id = ArrayList<JobTypeResponse>()
    var mDepartmentListArray = ArrayList<DepatListName>()
    var mDepartTasksArray = ArrayList<DeprtTaskList>()
    var mSingleArray = arrayOf<String>()
    var mSingleArray2 = arrayOf<String>()
    var mSingleArray3 = arrayOf<String>()
    var mSingleArray4 = arrayOf<String>()
    var mSingleArray5 = arrayOf<String>()
    var mSingleArray6 = arrayOf<String>()
    var mSingleArray7 = arrayOf<String>()
    var mSingleArray8 = arrayOf<String>()
    var mSingleArray9 = arrayOf<String>()
    var mSingleArray10 = arrayOf<String>()
    var mSingleTasklistandCat = ArrayList<String>()
    var mSingleTasklistandCat2 = ArrayList<String>()
    var mSingleTasklistandCat3 = ArrayList<String>()
    var mSingleTasklistandCat4 = ArrayList<String>()
    var mSingleTasklistandCat5 = ArrayList<String>()
    var mSingleTasklistandCat6 = ArrayList<String>()
    var mSingleTasklistandCat7 = ArrayList<String>()
    var mSingleTasklistandCat8 = ArrayList<String>()
    var mSingleTasklistandCat9 = ArrayList<String>()
    var mSingleTasklistandCat10 = ArrayList<String>()
    var JobidList = ArrayList<String>()
    var JobidList2 = ArrayList<String>()
    var JobidList3 = ArrayList<String>()
    var JobidList4 = ArrayList<String>()
    var JobidList5 = ArrayList<String>()
    var JobidList6 = ArrayList<String>()
    var JobidList7 = ArrayList<String>()
    var JobidList8 = ArrayList<String>()
    var JobidList9 = ArrayList<String>()
    var JobidList10 = ArrayList<String>()
    var termsdate: String? = null
    var termsdate1: String? = null
    var termsdate2: String? = null
    var termsdate3: String? = null
    var termsdate5: String? = null
    var termsdate6: String? = null
    var termsdate7: String? = null
    var termsdate8: String? = null
    var termsdate9: String? = null
    var termsdate10: String? = null
    var termsquantity: String? = null
    var termsquantity1: String? = null
    var termsquantity2: String? = null
    var termsquantity3: String? = null
    var termsquantity5: String? = null
    var termsquantity6: String? = null
    var termsquantity7: String? = null
    var termsquantity8: String? = null
    var termsquantity9: String? = null
    var termsquantity10: String? = null
    var mailing_qty: Int? = 0
    var job_nameList = ArrayList<String>()
    var job_nameList2 = ArrayList<String>()
    var job_nameList3 = ArrayList<String>()
    var job_nameList4 = ArrayList<String>()
    var job_nameList5 = ArrayList<String>()
    var job_nameList6 = ArrayList<String>()
    var job_nameList7 = ArrayList<String>()
    var job_nameList8 = ArrayList<String>()
    var job_nameList9 = ArrayList<String>()
    var job_nameList10 = ArrayList<String>()
    var cal = Calendar.getInstance()
    var dateofmonth: String = ""
    var monthofyear: String = ""
    var yearstg: String = ""
    var mJobType_id: String = ""
    lateinit var dialog_recycler_view: Dialog
    lateinit var dialog_recycler_view2: Dialog
    lateinit var dialog_recycler_view3: Dialog
    lateinit var dialog_recycler_view4: Dialog
    lateinit var dialog_recycler_view5: Dialog
    lateinit var dialog_recycler_view6: Dialog
    lateinit var dialog_recycler_view7: Dialog
    lateinit var dialog_recycler_view8: Dialog
    lateinit var dialog_recycler_view9: Dialog
    lateinit var dialog_recycler_view10: Dialog
    var departselectedid_array = ArrayList<String>()
    var departselectedid_array2 = ArrayList<String>()
    var departselectedid_array3 = ArrayList<String>()
    var departselectedid_array4 = ArrayList<String>()
    var departselectedid_array5 = ArrayList<String>()
    var departselectedid_array6 = ArrayList<String>()
    var departselectedid_array7 = ArrayList<String>()
    var departselectedid_array8 = ArrayList<String>()
    var departselectedid_array9 = ArrayList<String>()
    var departselectedid_array10 = ArrayList<String>()
    var departselectedid_only = ""
    var departselectedid_only2 = ""
    var departselectedid_only3 = ""
    var departselectedid_only4 = ""
    var departselectedid_only5 = ""
    var departselectedid_only6 = ""
    var departselectedid_only7 = ""
    var departselectedid_only8 = ""
    var departselectedid_only9 = ""
    var departselectedid_only10 = ""
    var tasksselectedid_only = ""
    var tasksselectedid_only2 = ""
    var tasksselectedid_only3 = ""
    var tasksselectedid_only4 = ""
    var tasksselectedid_only5 = ""
    var tasksselectedid_only6 = ""
    var tasksselectedid_only7 = ""
    var tasksselectedid_only8 = ""
    var tasksselectedid_only9 = ""
    var tasksselectedid_only10 = ""
    var admin_id_main = ""
    internal var mChecked = SparseBooleanArray()
    internal var mChecked2 = SparseBooleanArray()
    internal var mChecked3 = SparseBooleanArray()
    internal var mChecked4 = SparseBooleanArray()
    internal var mChecked5 = SparseBooleanArray()
    internal var mChecked6 = SparseBooleanArray()
    internal var mChecked7 = SparseBooleanArray()
    internal var mChecked8 = SparseBooleanArray()
    internal var mChecked9 = SparseBooleanArray()
    internal var mChecked10 = SparseBooleanArray()
    var al1 = ArrayList<String>()
    var al2 = ArrayList<String>()
    var al3 = ArrayList<String>()
    var al4 = ArrayList<String>()
    var al5 = ArrayList<String>()
    var al6 = ArrayList<String>()
    var al7 = ArrayList<String>()
    var al8 = ArrayList<String>()
    var al9 = ArrayList<String>()
    var al10 = ArrayList<String>()
    lateinit var customer_search_adapter: ArrayAdapter<String>
    var mCustomerNamesListArray = ArrayList<CustomersListName>()
    var mCustomerNamesArray = ArrayList<String>()
    var mCustomerIdsArray = ArrayList<String>()
    var customer_search_id: String = ""
    var aPosition: Int = 0
    lateinit var ll_shipping_terms_1: LinearLayout
    lateinit var admin_shipping_terms1: EditText
    lateinit var ll_shipp_120: LinearLayout
    lateinit var ll_ship_subterms_121: LinearLayout
    lateinit var ll_shipp_subterms122: LinearLayout
    lateinit var ll_shipp_subterms123: LinearLayout
    lateinit var ll_shipp_subterms124: LinearLayout
    lateinit var ll_shipp_subterms125: LinearLayout
    lateinit var ll_shipp_subterms126: LinearLayout
    lateinit var ll_shipp_subterms127: LinearLayout
    lateinit var ll_shipp_subterms128: LinearLayout
    lateinit var ll_shipp_subterms129: LinearLayout
    lateinit var ll_shipp_subterms1210: LinearLayout
    lateinit var admin_shipp_termssitem121: EditText
    lateinit var admin_shipp_termsdate121: EditText
    lateinit var admin_shipp_termssitem122: EditText
    lateinit var admin_shipp_termsdate122: EditText
    lateinit var admin_shipp_termssitem123: EditText
    lateinit var admin_shipp_termsdate123: EditText
    lateinit var admin_shipp_termssitem124: EditText
    lateinit var admin_shipp_termsdate124: EditText
    lateinit var admin_shipp_termssitem125: EditText
    lateinit var admin_shipp_termsdate125: EditText
    lateinit var admin_shipp_termssitem126: EditText
    lateinit var admin_shipp_termsdate126: EditText
    lateinit var admin_shipp_termssitem127: EditText
    lateinit var admin_shipp_termsdate127: EditText
    lateinit var admin_shipp_termssitem128: EditText
    lateinit var admin_shipp_termsdate128: EditText
    lateinit var admin_shipp_termssitem129: EditText
    lateinit var admin_shipp_termsdate129: EditText
    lateinit var admin_shipp_termssitem1210: EditText
    lateinit var admin_shipp_termsdate1210: EditText
    lateinit var admin_delivery_terms1: EditText
    lateinit var ll_delivery_130: LinearLayout
    lateinit var ll_delivery_terms_1: LinearLayout
    lateinit var ll_delivery_subterms_131: LinearLayout
    lateinit var ll_delivery_subterms132: LinearLayout
    lateinit var ll_delivery_subterms133: LinearLayout
    lateinit var ll_delivery_subterms134: LinearLayout
    lateinit var ll_delivery_subterms135: LinearLayout
    lateinit var ll_delivery_subterms136: LinearLayout
    lateinit var ll_delivery_subterms137: LinearLayout
    lateinit var ll_delivery_subterms138: LinearLayout
    lateinit var ll_delivery_subterms139: LinearLayout
    lateinit var ll_delivery_subterms1310: LinearLayout
    lateinit var admin_delivery_termssitem131: EditText
    lateinit var admin_delivery_termsdate131: EditText
    lateinit var admin_delivery_termssitem132: EditText
    lateinit var admin_delivery_termsdate132: EditText
    lateinit var admin_delivery_termssitem133: EditText
    lateinit var admin_delivery_termsdate133: EditText
    lateinit var admin_delivery_termssitem134: EditText
    lateinit var admin_delivery_termsdate134: EditText
    lateinit var admin_delivery_termssitem135: EditText
    lateinit var admin_delivery_termsdate135: EditText
    lateinit var admin_delivery_termssitem136: EditText
    lateinit var admin_delivery_termsdate136: EditText
    lateinit var admin_delivery_termssitem137: EditText
    lateinit var admin_delivery_termsdate137: EditText
    lateinit var admin_delivery_termssitem138: EditText
    lateinit var admin_delivery_termsdate138: EditText
    lateinit var admin_delivery_termssitem139: EditText
    lateinit var admin_delivery_termsdate139: EditText
    lateinit var admin_delivery_termssitem1310: EditText
    lateinit var admin_delivery_termsdate1310: EditText
    lateinit var admin_shipping_terms2: EditText
    lateinit var ll_shipp_220: LinearLayout
    lateinit var ll_shipping_two: LinearLayout
    lateinit var ll_ship_subterms_221: LinearLayout
    lateinit var ll_shipp_subterms222: LinearLayout
    lateinit var ll_shipp_subterms223: LinearLayout
    lateinit var ll_shipp_subterms224: LinearLayout
    lateinit var ll_shipp_subterms225: LinearLayout
    lateinit var ll_shipp_subterms226: LinearLayout
    lateinit var ll_shipp_subterms227: LinearLayout
    lateinit var ll_shipp_subterms228: LinearLayout
    lateinit var ll_shipp_subterms229: LinearLayout
    lateinit var ll_shipp_subterms2210: LinearLayout
    lateinit var admin_shipp_termssitem221: EditText
    lateinit var admin_shipp_termsdate221: EditText
    lateinit var admin_shipp_termssitem222: EditText
    lateinit var admin_shipp_termsdate222: EditText
    lateinit var admin_shipp_termssitem223: EditText
    lateinit var admin_shipp_termsdate223: EditText
    lateinit var admin_shipp_termssitem224: EditText
    lateinit var admin_shipp_termsdate224: EditText
    lateinit var admin_shipp_termssitem225: EditText
    lateinit var admin_shipp_termsdate225: EditText
    lateinit var admin_shipp_termssitem226: EditText
    lateinit var admin_shipp_termsdate226: EditText
    lateinit var admin_shipp_termssitem227: EditText
    lateinit var admin_shipp_termsdate227: EditText
    lateinit var admin_shipp_termssitem228: EditText
    lateinit var admin_shipp_termsdate228: EditText
    lateinit var admin_shipp_termssitem229: EditText
    lateinit var admin_shipp_termsdate229: EditText
    lateinit var admin_shipp_termssitem2210: EditText
    lateinit var admin_shipp_termsdate2210: EditText
    lateinit var admin_delivery_terms2: EditText
    lateinit var ll_delivery_230: LinearLayout
    lateinit var ll_delivery_terms_2: LinearLayout
    lateinit var ll_delivery_subterms_231: LinearLayout
    lateinit var ll_delivery_subterms232: LinearLayout
    lateinit var ll_delivery_subterms233: LinearLayout
    lateinit var ll_delivery_subterms234: LinearLayout
    lateinit var ll_delivery_subterms235: LinearLayout
    lateinit var ll_delivery_subterms236: LinearLayout
    lateinit var ll_delivery_subterms237: LinearLayout
    lateinit var ll_delivery_subterms238: LinearLayout
    lateinit var ll_delivery_subterms239: LinearLayout
    lateinit var ll_delivery_subterms2310: LinearLayout
    lateinit var admin_delivery_termssitem231: EditText
    lateinit var admin_delivery_termsdate231: EditText
    lateinit var admin_delivery_termssitem232: EditText
    lateinit var admin_delivery_termsdate232: EditText
    lateinit var admin_delivery_termssitem233: EditText
    lateinit var admin_delivery_termsdate233: EditText
    lateinit var admin_delivery_termssitem234: EditText
    lateinit var admin_delivery_termsdate234: EditText
    lateinit var admin_delivery_termssitem235: EditText
    lateinit var admin_delivery_termsdate235: EditText
    lateinit var admin_delivery_termssitem236: EditText
    lateinit var admin_delivery_termsdate236: EditText
    lateinit var admin_delivery_termssitem237: EditText
    lateinit var admin_delivery_termsdate237: EditText
    lateinit var admin_delivery_termssitem238: EditText
    lateinit var admin_delivery_termsdate238: EditText
    lateinit var admin_delivery_termssitem239: EditText
    lateinit var admin_delivery_termsdate239: EditText
    lateinit var admin_delivery_termssitem2310: EditText
    lateinit var admin_delivery_termsdate2310: EditText
    lateinit var admin_shipping_terms3: EditText
    lateinit var ll_shipp_320: LinearLayout
    lateinit var ll_shipping_three: LinearLayout
    lateinit var ll_ship_subterms_321: LinearLayout
    lateinit var ll_shipp_subterms322: LinearLayout
    lateinit var ll_shipp_subterms323: LinearLayout
    lateinit var ll_shipp_subterms324: LinearLayout
    lateinit var ll_shipp_subterms325: LinearLayout
    lateinit var ll_shipp_subterms326: LinearLayout
    lateinit var ll_shipp_subterms327: LinearLayout
    lateinit var ll_shipp_subterms328: LinearLayout
    lateinit var ll_shipp_subterms329: LinearLayout
    lateinit var ll_shipp_subterms3210: LinearLayout
    lateinit var admin_shipp_termssitem321: EditText
    lateinit var admin_shipp_termsdate321: EditText
    lateinit var admin_shipp_termssitem322: EditText
    lateinit var admin_shipp_termsdate322: EditText
    lateinit var admin_shipp_termssitem323: EditText
    lateinit var admin_shipp_termsdate323: EditText
    lateinit var admin_shipp_termssitem324: EditText
    lateinit var admin_shipp_termsdate324: EditText
    lateinit var admin_shipp_termssitem325: EditText
    lateinit var admin_shipp_termsdate325: EditText
    lateinit var admin_shipp_termssitem326: EditText
    lateinit var admin_shipp_termsdate326: EditText
    lateinit var admin_shipp_termssitem327: EditText
    lateinit var admin_shipp_termsdate327: EditText
    lateinit var admin_shipp_termssitem328: EditText
    lateinit var admin_shipp_termsdate328: EditText
    lateinit var admin_shipp_termssitem329: EditText
    lateinit var admin_shipp_termsdate329: EditText
    lateinit var admin_shipp_termssitem3210: EditText
    lateinit var admin_shipp_termsdate3210: EditText
    lateinit var admin_delivery_terms3: EditText
    lateinit var ll_delivery_terms_3: LinearLayout
    lateinit var ll_delivery_330: LinearLayout
    lateinit var ll_delivery_subterms_331: LinearLayout
    lateinit var ll_delivery_subterms332: LinearLayout
    lateinit var ll_delivery_subterms333: LinearLayout
    lateinit var ll_delivery_subterms334: LinearLayout
    lateinit var ll_delivery_subterms335: LinearLayout
    lateinit var ll_delivery_subterms336: LinearLayout
    lateinit var ll_delivery_subterms337: LinearLayout
    lateinit var ll_delivery_subterms338: LinearLayout
    lateinit var ll_delivery_subterms339: LinearLayout
    lateinit var ll_delivery_subterms3310: LinearLayout
    lateinit var admin_delivery_termssitem331: EditText
    lateinit var admin_delivery_termsdate331: EditText
    lateinit var admin_delivery_termssitem332: EditText
    lateinit var admin_delivery_termsdate332: EditText
    lateinit var admin_delivery_termssitem333: EditText
    lateinit var admin_delivery_termsdate333: EditText
    lateinit var admin_delivery_termssitem334: EditText
    lateinit var admin_delivery_termsdate334: EditText
    lateinit var admin_delivery_termssitem335: EditText
    lateinit var admin_delivery_termsdate335: EditText
    lateinit var admin_delivery_termssitem336: EditText
    lateinit var admin_delivery_termsdate336: EditText
    lateinit var admin_delivery_termssitem337: EditText
    lateinit var admin_delivery_termsdate337: EditText
    lateinit var admin_delivery_termssitem338: EditText
    lateinit var admin_delivery_termsdate338: EditText
    lateinit var admin_delivery_termssitem339: EditText
    lateinit var admin_delivery_termsdate339: EditText
    lateinit var admin_delivery_termssitem3310: EditText
    lateinit var admin_delivery_termsdate3310: EditText
    lateinit var admin_shipping_terms4: EditText
    lateinit var ll_shipp_420: LinearLayout
    lateinit var ll_shipping_four: LinearLayout
    lateinit var ll_ship_subterms_421: LinearLayout
    lateinit var ll_shipp_subterms422: LinearLayout
    lateinit var ll_shipp_subterms423: LinearLayout
    lateinit var ll_shipp_subterms424: LinearLayout
    lateinit var ll_shipp_subterms425: LinearLayout
    lateinit var ll_shipp_subterms426: LinearLayout
    lateinit var ll_shipp_subterms427: LinearLayout
    lateinit var ll_shipp_subterms428: LinearLayout
    lateinit var ll_shipp_subterms429: LinearLayout
    lateinit var ll_shipp_subterms4210: LinearLayout
    lateinit var admin_shipp_termssitem421: EditText
    lateinit var admin_shipp_termsdate421: EditText
    lateinit var admin_shipp_termssitem422: EditText
    lateinit var admin_shipp_termsdate422: EditText
    lateinit var admin_shipp_termssitem423: EditText
    lateinit var admin_shipp_termsdate423: EditText
    lateinit var admin_shipp_termssitem424: EditText
    lateinit var admin_shipp_termsdate424: EditText
    lateinit var admin_shipp_termssitem425: EditText
    lateinit var admin_shipp_termsdate425: EditText
    lateinit var admin_shipp_termssitem426: EditText
    lateinit var admin_shipp_termsdate426: EditText
    lateinit var admin_shipp_termssitem427: EditText
    lateinit var admin_shipp_termsdate427: EditText
    lateinit var admin_shipp_termssitem428: EditText
    lateinit var admin_shipp_termsdate428: EditText
    lateinit var admin_shipp_termssitem429: EditText
    lateinit var admin_shipp_termsdate429: EditText
    lateinit var admin_shipp_termssitem4210: EditText
    lateinit var admin_shipp_termsdate4210: EditText
    lateinit var admin_delivery_terms4: EditText
    lateinit var ll_delivery_430: LinearLayout
    lateinit var ll_delivery_terms_4: LinearLayout
    lateinit var ll_delivery_subterms_431: LinearLayout
    lateinit var ll_delivery_subterms432: LinearLayout
    lateinit var ll_delivery_subterms433: LinearLayout
    lateinit var ll_delivery_subterms434: LinearLayout
    lateinit var ll_delivery_subterms435: LinearLayout
    lateinit var ll_delivery_subterms436: LinearLayout
    lateinit var ll_delivery_subterms437: LinearLayout
    lateinit var ll_delivery_subterms438: LinearLayout
    lateinit var ll_delivery_subterms439: LinearLayout
    lateinit var ll_delivery_subterms4310: LinearLayout
    lateinit var admin_delivery_termssitem431: EditText
    lateinit var admin_delivery_termsdate431: EditText
    lateinit var admin_delivery_termssitem432: EditText
    lateinit var admin_delivery_termsdate432: EditText
    lateinit var admin_delivery_termssitem433: EditText
    lateinit var admin_delivery_termsdate433: EditText
    lateinit var admin_delivery_termssitem434: EditText
    lateinit var admin_delivery_termsdate434: EditText
    lateinit var admin_delivery_termssitem435: EditText
    lateinit var admin_delivery_termsdate435: EditText
    lateinit var admin_delivery_termssitem436: EditText
    lateinit var admin_delivery_termsdate436: EditText
    lateinit var admin_delivery_termssitem437: EditText
    lateinit var admin_delivery_termsdate437: EditText
    lateinit var admin_delivery_termssitem438: EditText
    lateinit var admin_delivery_termsdate438: EditText
    lateinit var admin_delivery_termssitem439: EditText
    lateinit var admin_delivery_termsdate439: EditText
    lateinit var admin_delivery_termssitem4310: EditText
    lateinit var admin_delivery_termsdate4310: EditText
    lateinit var admin_shipping_terms5: EditText
    lateinit var ll_shipp_520: LinearLayout
    lateinit var ll_shipping_five: LinearLayout
    lateinit var ll_ship_subterms_521: LinearLayout
    lateinit var ll_shipp_subterms522: LinearLayout
    lateinit var ll_shipp_subterms523: LinearLayout
    lateinit var ll_shipp_subterms524: LinearLayout
    lateinit var ll_shipp_subterms525: LinearLayout
    lateinit var ll_shipp_subterms526: LinearLayout
    lateinit var ll_shipp_subterms527: LinearLayout
    lateinit var ll_shipp_subterms528: LinearLayout
    lateinit var ll_shipp_subterms529: LinearLayout
    lateinit var ll_shipp_subterms5210: LinearLayout
    lateinit var admin_shipp_termssitem521: EditText
    lateinit var admin_shipp_termsdate521: EditText
    lateinit var admin_shipp_termssitem522: EditText
    lateinit var admin_shipp_termsdate522: EditText
    lateinit var admin_shipp_termssitem523: EditText
    lateinit var admin_shipp_termsdate523: EditText
    lateinit var admin_shipp_termssitem524: EditText
    lateinit var admin_shipp_termsdate524: EditText
    lateinit var admin_shipp_termssitem525: EditText
    lateinit var admin_shipp_termsdate525: EditText
    lateinit var admin_shipp_termssitem526: EditText
    lateinit var admin_shipp_termsdate526: EditText
    lateinit var admin_shipp_termssitem527: EditText
    lateinit var admin_shipp_termsdate527: EditText
    lateinit var admin_shipp_termssitem528: EditText
    lateinit var admin_shipp_termsdate528: EditText
    lateinit var admin_shipp_termssitem529: EditText
    lateinit var admin_shipp_termsdate529: EditText
    lateinit var admin_shipp_termssitem5210: EditText
    lateinit var admin_shipp_termsdate5210: EditText
    lateinit var admin_delivery_terms5: EditText
    lateinit var ll_delivery_530: LinearLayout
    lateinit var ll_delivery_terms_5: LinearLayout
    lateinit var ll_delivery_subterms_531: LinearLayout
    lateinit var ll_delivery_subterms532: LinearLayout
    lateinit var ll_delivery_subterms533: LinearLayout
    lateinit var ll_delivery_subterms534: LinearLayout
    lateinit var ll_delivery_subterms535: LinearLayout
    lateinit var ll_delivery_subterms536: LinearLayout
    lateinit var ll_delivery_subterms537: LinearLayout
    lateinit var ll_delivery_subterms538: LinearLayout
    lateinit var ll_delivery_subterms539: LinearLayout
    lateinit var ll_delivery_subterms5310: LinearLayout
    lateinit var admin_delivery_termssitem531: EditText
    lateinit var admin_delivery_termsdate531: EditText
    lateinit var admin_delivery_termssitem532: EditText
    lateinit var admin_delivery_termsdate532: EditText
    lateinit var admin_delivery_termssitem533: EditText
    lateinit var admin_delivery_termsdate533: EditText
    lateinit var admin_delivery_termssitem534: EditText
    lateinit var admin_delivery_termsdate534: EditText
    lateinit var admin_delivery_termssitem535: EditText
    lateinit var admin_delivery_termsdate535: EditText
    lateinit var admin_delivery_termssitem536: EditText
    lateinit var admin_delivery_termsdate536: EditText
    lateinit var admin_delivery_termssitem537: EditText
    lateinit var admin_delivery_termsdate537: EditText
    lateinit var admin_delivery_termssitem538: EditText
    lateinit var admin_delivery_termsdate538: EditText
    lateinit var admin_delivery_termssitem539: EditText
    lateinit var admin_delivery_termsdate539: EditText
    lateinit var admin_delivery_termssitem5310: EditText
    lateinit var admin_delivery_termsdate5310: EditText
    lateinit var admin_shipping_terms6: EditText
    lateinit var ll_shipp_620: LinearLayout
    lateinit var ll_shipping_six: LinearLayout
    lateinit var ll_ship_subterms_621: LinearLayout
    lateinit var ll_shipp_subterms622: LinearLayout
    lateinit var ll_shipp_subterms623: LinearLayout
    lateinit var ll_shipp_subterms624: LinearLayout
    lateinit var ll_shipp_subterms625: LinearLayout
    lateinit var ll_shipp_subterms626: LinearLayout
    lateinit var ll_shipp_subterms627: LinearLayout
    lateinit var ll_shipp_subterms628: LinearLayout
    lateinit var ll_shipp_subterms629: LinearLayout
    lateinit var ll_shipp_subterms6210: LinearLayout
    lateinit var admin_shipp_termssitem621: EditText
    lateinit var admin_shipp_termsdate621: EditText
    lateinit var admin_shipp_termssitem622: EditText
    lateinit var admin_shipp_termsdate622: EditText
    lateinit var admin_shipp_termssitem623: EditText
    lateinit var admin_shipp_termsdate623: EditText
    lateinit var admin_shipp_termssitem624: EditText
    lateinit var admin_shipp_termsdate624: EditText
    lateinit var admin_shipp_termssitem625: EditText
    lateinit var admin_shipp_termsdate625: EditText
    lateinit var admin_shipp_termssitem626: EditText
    lateinit var admin_shipp_termsdate626: EditText
    lateinit var admin_shipp_termssitem627: EditText
    lateinit var admin_shipp_termsdate627: EditText
    lateinit var admin_shipp_termssitem628: EditText
    lateinit var admin_shipp_termsdate628: EditText
    lateinit var admin_shipp_termssitem629: EditText
    lateinit var admin_shipp_termsdate629: EditText
    lateinit var admin_shipp_termssitem6210: EditText
    lateinit var admin_shipp_termsdate6210: EditText
    lateinit var admin_delivery_terms6: EditText
    lateinit var ll_delivery_630: LinearLayout
    lateinit var ll_delivery_terms_6: LinearLayout
    lateinit var ll_delivery_subterms_631: LinearLayout
    lateinit var ll_delivery_subterms632: LinearLayout
    lateinit var ll_delivery_subterms633: LinearLayout
    lateinit var ll_delivery_subterms634: LinearLayout
    lateinit var ll_delivery_subterms635: LinearLayout
    lateinit var ll_delivery_subterms636: LinearLayout
    lateinit var ll_delivery_subterms637: LinearLayout
    lateinit var ll_delivery_subterms638: LinearLayout
    lateinit var ll_delivery_subterms639: LinearLayout
    lateinit var ll_delivery_subterms6310: LinearLayout
    lateinit var admin_delivery_termssitem631: EditText
    lateinit var admin_delivery_termsdate631: EditText
    lateinit var admin_delivery_termssitem632: EditText
    lateinit var admin_delivery_termsdate632: EditText
    lateinit var admin_delivery_termssitem633: EditText
    lateinit var admin_delivery_termsdate633: EditText
    lateinit var admin_delivery_termssitem634: EditText
    lateinit var admin_delivery_termsdate634: EditText
    lateinit var admin_delivery_termssitem635: EditText
    lateinit var admin_delivery_termsdate635: EditText
    lateinit var admin_delivery_termssitem636: EditText
    lateinit var admin_delivery_termsdate636: EditText
    lateinit var admin_delivery_termssitem637: EditText
    lateinit var admin_delivery_termsdate637: EditText
    lateinit var admin_delivery_termssitem638: EditText
    lateinit var admin_delivery_termsdate638: EditText
    lateinit var admin_delivery_termssitem639: EditText
    lateinit var admin_delivery_termsdate639: EditText
    lateinit var admin_delivery_termssitem6310: EditText
    lateinit var admin_delivery_termsdate6310: EditText
    lateinit var admin_shipping_terms7: EditText
    lateinit var ll_shipp_720: LinearLayout
    lateinit var ll_shipping_seven: LinearLayout
    lateinit var ll_ship_subterms_721: LinearLayout
    lateinit var ll_shipp_subterms722: LinearLayout
    lateinit var ll_shipp_subterms723: LinearLayout
    lateinit var ll_shipp_subterms724: LinearLayout
    lateinit var ll_shipp_subterms725: LinearLayout
    lateinit var ll_shipp_subterms726: LinearLayout
    lateinit var ll_shipp_subterms727: LinearLayout
    lateinit var ll_shipp_subterms728: LinearLayout
    lateinit var ll_shipp_subterms729: LinearLayout
    lateinit var ll_shipp_subterms7210: LinearLayout
    lateinit var admin_shipp_termssitem721: EditText
    lateinit var admin_shipp_termsdate721: EditText
    lateinit var admin_shipp_termssitem722: EditText
    lateinit var admin_shipp_termsdate722: EditText
    lateinit var admin_shipp_termssitem723: EditText
    lateinit var admin_shipp_termsdate723: EditText
    lateinit var admin_shipp_termssitem724: EditText
    lateinit var admin_shipp_termsdate724: EditText
    lateinit var admin_shipp_termssitem725: EditText
    lateinit var admin_shipp_termsdate725: EditText
    lateinit var admin_shipp_termssitem726: EditText
    lateinit var admin_shipp_termsdate726: EditText
    lateinit var admin_shipp_termssitem727: EditText
    lateinit var admin_shipp_termsdate727: EditText
    lateinit var admin_shipp_termssitem728: EditText
    lateinit var admin_shipp_termsdate728: EditText
    lateinit var admin_shipp_termssitem729: EditText
    lateinit var admin_shipp_termsdate729: EditText
    lateinit var admin_shipp_termssitem7210: EditText
    lateinit var admin_shipp_termsdate7210: EditText
    lateinit var admin_delivery_terms7: EditText
    lateinit var ll_delivery_730: LinearLayout
    lateinit var ll_delivery_terms_7: LinearLayout
    lateinit var ll_delivery_subterms_731: LinearLayout
    lateinit var ll_delivery_subterms732: LinearLayout
    lateinit var ll_delivery_subterms733: LinearLayout
    lateinit var ll_delivery_subterms734: LinearLayout
    lateinit var ll_delivery_subterms735: LinearLayout
    lateinit var ll_delivery_subterms736: LinearLayout
    lateinit var ll_delivery_subterms737: LinearLayout
    lateinit var ll_delivery_subterms738: LinearLayout
    lateinit var ll_delivery_subterms739: LinearLayout
    lateinit var ll_delivery_subterms7310: LinearLayout
    lateinit var admin_delivery_termssitem731: EditText
    lateinit var admin_delivery_termsdate731: EditText
    lateinit var admin_delivery_termssitem732: EditText
    lateinit var admin_delivery_termsdate732: EditText
    lateinit var admin_delivery_termssitem733: EditText
    lateinit var admin_delivery_termsdate733: EditText
    lateinit var admin_delivery_termssitem734: EditText
    lateinit var admin_delivery_termsdate734: EditText
    lateinit var admin_delivery_termssitem735: EditText
    lateinit var admin_delivery_termsdate735: EditText
    lateinit var admin_delivery_termssitem736: EditText
    lateinit var admin_delivery_termsdate736: EditText
    lateinit var admin_delivery_termssitem737: EditText
    lateinit var admin_delivery_termsdate737: EditText
    lateinit var admin_delivery_termssitem738: EditText
    lateinit var admin_delivery_termsdate738: EditText
    lateinit var admin_delivery_termssitem739: EditText
    lateinit var admin_delivery_termsdate739: EditText
    lateinit var admin_delivery_termssitem7310: EditText
    lateinit var admin_delivery_termsdate7310: EditText
    lateinit var admin_shipping_terms8: EditText
    lateinit var ll_shipp_820: LinearLayout
    lateinit var ll_shipping_eight: LinearLayout
    lateinit var ll_ship_subterms_821: LinearLayout
    lateinit var ll_shipp_subterms822: LinearLayout
    lateinit var ll_shipp_subterms823: LinearLayout
    lateinit var ll_shipp_subterms824: LinearLayout
    lateinit var ll_shipp_subterms825: LinearLayout
    lateinit var ll_shipp_subterms826: LinearLayout
    lateinit var ll_shipp_subterms827: LinearLayout
    lateinit var ll_shipp_subterms828: LinearLayout
    lateinit var ll_shipp_subterms829: LinearLayout
    lateinit var ll_shipp_subterms8210: LinearLayout
    lateinit var admin_shipp_termssitem821: EditText
    lateinit var admin_shipp_termsdate821: EditText
    lateinit var admin_shipp_termssitem822: EditText
    lateinit var admin_shipp_termsdate822: EditText
    lateinit var admin_shipp_termssitem823: EditText
    lateinit var admin_shipp_termsdate823: EditText
    lateinit var admin_shipp_termssitem824: EditText
    lateinit var admin_shipp_termsdate824: EditText
    lateinit var admin_shipp_termssitem825: EditText
    lateinit var admin_shipp_termsdate825: EditText
    lateinit var admin_shipp_termssitem826: EditText
    lateinit var admin_shipp_termsdate826: EditText
    lateinit var admin_shipp_termssitem827: EditText
    lateinit var admin_shipp_termsdate827: EditText
    lateinit var admin_shipp_termssitem828: EditText
    lateinit var admin_shipp_termsdate828: EditText
    lateinit var admin_shipp_termssitem829: EditText
    lateinit var admin_shipp_termsdate829: EditText
    lateinit var admin_shipp_termssitem8210: EditText
    lateinit var admin_shipp_termsdate8210: EditText
    lateinit var admin_delivery_terms8: EditText
    lateinit var ll_delivery_830: LinearLayout
    lateinit var ll_delivery_terms_8: LinearLayout
    lateinit var ll_delivery_subterms_831: LinearLayout
    lateinit var ll_delivery_subterms832: LinearLayout
    lateinit var ll_delivery_subterms833: LinearLayout
    lateinit var ll_delivery_subterms834: LinearLayout
    lateinit var ll_delivery_subterms835: LinearLayout
    lateinit var ll_delivery_subterms836: LinearLayout
    lateinit var ll_delivery_subterms837: LinearLayout
    lateinit var ll_delivery_subterms838: LinearLayout
    lateinit var ll_delivery_subterms839: LinearLayout
    lateinit var ll_delivery_subterms8310: LinearLayout
    lateinit var admin_delivery_termssitem831: EditText
    lateinit var admin_delivery_termsdate831: EditText
    lateinit var admin_delivery_termssitem832: EditText
    lateinit var admin_delivery_termsdate832: EditText
    lateinit var admin_delivery_termssitem833: EditText
    lateinit var admin_delivery_termsdate833: EditText
    lateinit var admin_delivery_termssitem834: EditText
    lateinit var admin_delivery_termsdate834: EditText
    lateinit var admin_delivery_termssitem835: EditText
    lateinit var admin_delivery_termsdate835: EditText
    lateinit var admin_delivery_termssitem836: EditText
    lateinit var admin_delivery_termsdate836: EditText
    lateinit var admin_delivery_termssitem837: EditText
    lateinit var admin_delivery_termsdate837: EditText
    lateinit var admin_delivery_termssitem838: EditText
    lateinit var admin_delivery_termsdate838: EditText
    lateinit var admin_delivery_termssitem839: EditText
    lateinit var admin_delivery_termsdate839: EditText
    lateinit var admin_delivery_termssitem8310: EditText
    lateinit var admin_delivery_termsdate8310: EditText
    lateinit var admin_shipping_terms9: EditText
    lateinit var ll_shipp_920: LinearLayout
    lateinit var ll_shipping_nine: LinearLayout
    lateinit var ll_ship_subterms_921: LinearLayout
    lateinit var ll_shipp_subterms922: LinearLayout
    lateinit var ll_shipp_subterms923: LinearLayout
    lateinit var ll_shipp_subterms924: LinearLayout
    lateinit var ll_shipp_subterms925: LinearLayout
    lateinit var ll_shipp_subterms926: LinearLayout
    lateinit var ll_shipp_subterms927: LinearLayout
    lateinit var ll_shipp_subterms928: LinearLayout
    lateinit var ll_shipp_subterms929: LinearLayout
    lateinit var ll_shipp_subterms9210: LinearLayout
    lateinit var admin_shipp_termssitem921: EditText
    lateinit var admin_shipp_termsdate921: EditText
    lateinit var admin_shipp_termssitem922: EditText
    lateinit var admin_shipp_termsdate922: EditText
    lateinit var admin_shipp_termssitem923: EditText
    lateinit var admin_shipp_termsdate923: EditText
    lateinit var admin_shipp_termssitem924: EditText
    lateinit var admin_shipp_termsdate924: EditText
    lateinit var admin_shipp_termssitem925: EditText
    lateinit var admin_shipp_termsdate925: EditText
    lateinit var admin_shipp_termssitem926: EditText
    lateinit var admin_shipp_termsdate926: EditText
    lateinit var admin_shipp_termssitem927: EditText
    lateinit var admin_shipp_termsdate927: EditText
    lateinit var admin_shipp_termssitem928: EditText
    lateinit var admin_shipp_termsdate928: EditText
    lateinit var admin_shipp_termssitem929: EditText
    lateinit var admin_shipp_termsdate929: EditText
    lateinit var admin_shipp_termssitem9210: EditText
    lateinit var admin_shipp_termsdate9210: EditText
    lateinit var admin_delivery_terms9: EditText
    lateinit var ll_delivery_930: LinearLayout
    lateinit var ll_delivery_terms_9: LinearLayout
    lateinit var ll_delivery_subterms_931: LinearLayout
    lateinit var ll_delivery_subterms932: LinearLayout
    lateinit var ll_delivery_subterms933: LinearLayout
    lateinit var ll_delivery_subterms934: LinearLayout
    lateinit var ll_delivery_subterms935: LinearLayout
    lateinit var ll_delivery_subterms936: LinearLayout
    lateinit var ll_delivery_subterms937: LinearLayout
    lateinit var ll_delivery_subterms938: LinearLayout
    lateinit var ll_delivery_subterms939: LinearLayout
    lateinit var ll_delivery_subterms9310: LinearLayout
    lateinit var admin_delivery_termssitem931: EditText
    lateinit var admin_delivery_termsdate931: EditText
    lateinit var admin_delivery_termssitem932: EditText
    lateinit var admin_delivery_termsdate932: EditText
    lateinit var admin_delivery_termssitem933: EditText
    lateinit var admin_delivery_termsdate933: EditText
    lateinit var admin_delivery_termssitem934: EditText
    lateinit var admin_delivery_termsdate934: EditText
    lateinit var admin_delivery_termssitem935: EditText
    lateinit var admin_delivery_termsdate935: EditText
    lateinit var admin_delivery_termssitem936: EditText
    lateinit var admin_delivery_termsdate936: EditText
    lateinit var admin_delivery_termssitem937: EditText
    lateinit var admin_delivery_termsdate937: EditText
    lateinit var admin_delivery_termssitem938: EditText
    lateinit var admin_delivery_termsdate938: EditText
    lateinit var admin_delivery_termssitem939: EditText
    lateinit var admin_delivery_termsdate939: EditText
    lateinit var admin_delivery_termssitem9310: EditText
    lateinit var admin_delivery_termsdate9310: EditText
    lateinit var admin_shipping_terms10: EditText
    lateinit var ll_shipp_1020: LinearLayout
    lateinit var ll_shipping_ten: LinearLayout
    lateinit var ll_ship_subterms_1021: LinearLayout
    lateinit var ll_shipp_subterms1022: LinearLayout
    lateinit var ll_shipp_subterms1023: LinearLayout
    lateinit var ll_shipp_subterms1024: LinearLayout
    lateinit var ll_shipp_subterms1025: LinearLayout
    lateinit var ll_shipp_subterms1026: LinearLayout
    lateinit var ll_shipp_subterms1027: LinearLayout
    lateinit var ll_shipp_subterms1028: LinearLayout
    lateinit var ll_shipp_subterms1029: LinearLayout
    lateinit var ll_shipp_subterms10210: LinearLayout
    lateinit var admin_shipp_termssitem1021: EditText
    lateinit var admin_shipp_termsdate1021: EditText
    lateinit var admin_shipp_termssitem1022: EditText
    lateinit var admin_shipp_termsdate1022: EditText
    lateinit var admin_shipp_termssitem1023: EditText
    lateinit var admin_shipp_termsdate1023: EditText
    lateinit var admin_shipp_termssitem1024: EditText
    lateinit var admin_shipp_termsdate1024: EditText
    lateinit var admin_shipp_termssitem1025: EditText
    lateinit var admin_shipp_termsdate1025: EditText
    lateinit var admin_shipp_termssitem1026: EditText
    lateinit var admin_shipp_termsdate1026: EditText
    lateinit var admin_shipp_termssitem1027: EditText
    lateinit var admin_shipp_termsdate1027: EditText
    lateinit var admin_shipp_termssitem1028: EditText
    lateinit var admin_shipp_termsdate1028: EditText
    lateinit var admin_shipp_termssitem1029: EditText
    lateinit var admin_shipp_termsdate1029: EditText
    lateinit var admin_shipp_termssitem10210: EditText
    lateinit var admin_shipp_termsdate10210: EditText
    lateinit var admin_delivery_terms10: EditText
    lateinit var ll_delivery_1030: LinearLayout
    lateinit var ll_delivery_terms_10: LinearLayout
    lateinit var ll_delivery_subterms_1031: LinearLayout
    lateinit var ll_delivery_subterms1032: LinearLayout
    lateinit var ll_delivery_subterms1033: LinearLayout
    lateinit var ll_delivery_subterms1034: LinearLayout
    lateinit var ll_delivery_subterms1035: LinearLayout
    lateinit var ll_delivery_subterms1036: LinearLayout
    lateinit var ll_delivery_subterms1037: LinearLayout
    lateinit var ll_delivery_subterms1038: LinearLayout
    lateinit var ll_delivery_subterms1039: LinearLayout
    lateinit var ll_delivery_subterms10310: LinearLayout
    lateinit var admin_delivery_termssitem1031: EditText
    lateinit var admin_delivery_termsdate1031: EditText
    lateinit var admin_delivery_termssitem1032: EditText
    lateinit var admin_delivery_termsdate1032: EditText
    lateinit var admin_delivery_termssitem1033: EditText
    lateinit var admin_delivery_termsdate1033: EditText
    lateinit var admin_delivery_termssitem1034: EditText
    lateinit var admin_delivery_termsdate1034: EditText
    lateinit var admin_delivery_termssitem1035: EditText
    lateinit var admin_delivery_termsdate1035: EditText
    lateinit var admin_delivery_termssitem1036: EditText
    lateinit var admin_delivery_termsdate1036: EditText
    lateinit var admin_delivery_termssitem1037: EditText
    lateinit var admin_delivery_termsdate1037: EditText
    lateinit var admin_delivery_termssitem1038: EditText
    lateinit var admin_delivery_termsdate1038: EditText
    lateinit var admin_delivery_termssitem1039: EditText
    lateinit var admin_delivery_termsdate1039: EditText
    lateinit var admin_delivery_termssitem10310: EditText
    lateinit var admin_delivery_termsdate10310: EditText
    var shipping_termsdate1: String? = null
    var shipping_termsdate2: String? = null
    var shipping_termsdate3: String? = null
    var shipping_termsdate4: String? = null
    var shipping_termsdate5: String? = null
    var shipping_termsdate6: String? = null
    var shipping_termsdate7: String? = null
    var shipping_termsdate8: String? = null
    var shipping_termsdate9: String? = null
    var shipping_termsdate10: String? = null
    var delivery_termsdate1: String? = null
    var delivery_termsdate2: String? = null
    var delivery_termsdate3: String? = null
    var delivery_termsdate4: String? = null
    var delivery_termsdate5: String? = null
    var delivery_termsdate6: String? = null
    var delivery_termsdate7: String? = null
    var delivery_termsdate8: String? = null
    var delivery_termsdate9: String? = null
    var delivery_termsdate10: String? = null
    var shipping_qty1: String = ""
    var shipping_qty2: String = ""
    var shipping_qty3: String = ""
    var shipping_qty4: String = ""
    var shipping_qty5: String = ""
    var shipping_qty6: String = ""
    var shipping_qty7: String = ""
    var shipping_qty8: String = ""
    var shipping_qty9: String = ""
    var shipping_qty10: String = ""
    var delivery_qty1: String = ""
    var delivery_qty2: String = ""
    var delivery_qty3: String = ""
    var delivery_qty4: String = ""
    var delivery_qty5: String = ""
    var delivery_qty6: String = ""
    var delivery_qty7: String = ""
    var delivery_qty8: String = ""
    var delivery_qty9: String = ""
    var delivery_qty10: String = ""
    lateinit var job_title_one_id: EditText
    lateinit var job_title_two_id: EditText
    lateinit var job_title_three_id: EditText
    lateinit var job_title_four_id: EditText
    lateinit var job_desc_one_id: EditText
    lateinit var job_desc_two_id: EditText
    lateinit var job_desc_three_id: EditText
    lateinit var job_desc_four_id: EditText
    lateinit var job_desc_five_id: EditText
    lateinit var job_desc_six_id: EditText
    lateinit var job_desc_seven_id: EditText
    lateinit var job_desc_eight_id: EditText
    lateinit var job_desc_nine_id: EditText
    lateinit var job_desc_ten_id: EditText
    var no_of_shipping_1_stg: String = "0"
    var no_of_shipping_2_stg: String = "0"
    var no_of_shipping_3_stg: String = "0"
    var no_of_shipping_4_stg: String = "0"
    var no_of_shipping_5_stg: String = "0"
    var no_of_shipping_6_stg: String = "0"
    var no_of_shipping_7_stg: String = "0"
    var no_of_shipping_8_stg: String = "0"
    var no_of_shipping_9_stg: String = "0"
    var no_of_shipping_10_stg: String = "0"
    var no_of_delivery_1_stg: String = "0"
    var no_of_delivery_2_stg: String = "0"
    var no_of_delivery_3_stg: String = "0"
    var no_of_delivery_4_stg: String = "0"
    var no_of_delivery_5_stg: String = "0"
    var no_of_delivery_6_stg: String = "0"
    var no_of_delivery_7_stg: String = "0"
    var no_of_delivery_8_stg: String = "0"
    var no_of_delivery_9_stg: String = "0"
    var no_of_delivery_10_stg: String = "0"
    var job_title_1_stg: String = ""
    var job_title_2_stg: String = ""
    var job_title_3_stg: String = ""
    var job_title_4_stg: String = ""
    var job_desc_1_stg: String = ""
    var job_desc_2_stg: String = ""
    var job_desc_3_stg: String = ""
    var job_desc_4_stg: String = ""
    var job_desc_5_stg: String = ""
    var job_desc_6_stg: String = ""
    var job_desc_7_stg: String = ""
    var job_desc_8_stg: String = ""
    var job_desc_9_stg: String = ""
    var job_desc_10_stg: String = ""
    val array2 = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    var final_job_parts = ""
    var job_mailing_1 = "Mailing"
    var job_mailing_2 = "Mailing"
    var job_mailing_3 = "Mailing"
    var job_mailing_4 = "Mailing"
    var job_mailing_5 = "Mailing"
    var job_mailing_6 = "Mailing"
    var job_mailing_7 = "Mailing"
    var job_mailing_8 = "Mailing"
    var job_mailing_9 = "Mailing"
    var job_mailing_10 = "Mailing"
    var job_shipping_1 = "Shipping"
    var job_shipping_2 = "Shipping"
    var job_shipping_3 = "Shipping"
    var job_shipping_4 = "Shipping"
    var job_shipping_5 = "Shipping"
    var job_shipping_6 = "Shipping"
    var job_shipping_7 = "Shipping"
    var job_shipping_8 = "Shipping"
    var job_shipping_9 = "Shipping"
    var job_shipping_10 = "Shipping"
    var job_delivery_1 = "Delivery"
    var job_delivery_2 = "Delivery"
    var job_delivery_3 = "Delivery"
    var job_delivery_4 = "Delivery"
    var job_delivery_5 = "Delivery"
    var job_delivery_6 = "Delivery"
    var job_delivery_7 = "Delivery"
    var job_delivery_8 = "Delivery"
    var job_delivery_9 = "Delivery"
    var job_delivery_10 = "Delivery"
    lateinit var ll_mailing_terms_1: LinearLayout
    lateinit var ll_miling_two: LinearLayout
    lateinit var ll_miling_three: LinearLayout
    lateinit var ll_miling_four: LinearLayout
    lateinit var ll_miling_five: LinearLayout
    lateinit var ll_miling_six: LinearLayout
    lateinit var ll_miling_seven: LinearLayout
    lateinit var ll_miling_eight: LinearLayout
    lateinit var ll_miling_nine: LinearLayout
    lateinit var ll_miling_ten: LinearLayout
    var mGetDepartmentList_names = ArrayList<String>()
    var mGetDepartmentList_ids = ArrayList<String>()
    var mGetTasksList_names = ArrayList<String>()
    var mGetTasksList_ids = ArrayList<String>()
    var mDepartTasksnamesList = ArrayList<String>()
    var flag: Boolean = true
    var flag2: Boolean = true
    var flag3: Boolean = true
    var flag4: Boolean = true
    var flag5: Boolean = true
    var flag6: Boolean = true
    var flag7: Boolean = true
    var flag8: Boolean = true
    var flag9: Boolean = true
    var flag10: Boolean = true
    var mIndexList = mutableListOf<Int>()
    var dep_def_ids_flag: Boolean = true
    var dep_def_ids_flag2: Boolean = true
    var dep_def_ids_flag3: Boolean = true
    var dep_def_ids_flag4: Boolean = true
    var dep_def_ids_flag5: Boolean = true
    var dep_def_ids_flag6: Boolean = true
    var dep_def_ids_flag7: Boolean = true
    var dep_def_ids_flag8: Boolean = true
    var dep_def_ids_flag9: Boolean = true
    var dep_def_ids_flag10: Boolean = true
    var other_val_job = ""
    var other_val_job_1 = ""
    var other_val_job_2 = ""
    var other_val_job_3 = ""
    var other_val_job_5 = ""
    var other_val_job_6 = ""
    var other_val_job_7 = ""
    var other_val_job_8 = ""
    var other_val_job_9 = ""
    var other_val_job_10 = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addjob)
        val session = SessionManager(this@AddJobActivity)
        val user: HashMap<String, String> = session.userDetails
        admin_id_main = user.get(SessionManager.ADMIN_ID)!!
        Log.e("admin_id_main_lll", admin_id_main)
        ll_mailing_terms_1 = findViewById(R.id.ll_mailing_terms_1)
        ll_miling_two = findViewById(R.id.ll_miling_two)
        ll_miling_three = findViewById(R.id.ll_miling_three)
        ll_miling_four = findViewById(R.id.ll_miling_four)
        ll_miling_five = findViewById(R.id.ll_miling_five)
        ll_miling_six = findViewById(R.id.ll_miling_six)
        ll_miling_seven = findViewById(R.id.ll_miling_seven)
        ll_miling_eight = findViewById(R.id.ll_miling_eight)
        ll_miling_nine = findViewById(R.id.ll_miling_nine)
        ll_miling_ten = findViewById(R.id.ll_miling_ten)
        dialog_list = Dialog(this)
        dialog_list.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list.setContentView(R.layout.dialog_view)
        dialog_list.setCancelable(true)
        list_items_list = dialog_list.findViewById(R.id.list_languages) as ListView
        list_items_list2 = dialog_list.findViewById(R.id.list_languages) as ListView
        dialog_recycler_view = Dialog(this)
        dialog_recycler_view.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view.setCancelable(true)
        dialog_recycler_view2 = Dialog(this)
        dialog_recycler_view2.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view2.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view2.setCancelable(true)
        dialog_recycler_view3 = Dialog(this)
        dialog_recycler_view3.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view3.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view3.setCancelable(true)
        dialog_recycler_view4 = Dialog(this)
        dialog_recycler_view4.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view4.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view4.setCancelable(true)
        dialog_recycler_view5 = Dialog(this)
        dialog_recycler_view5.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view5.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view5.setCancelable(true)
        dialog_recycler_view6 = Dialog(this)
        dialog_recycler_view6.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view6.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view6.setCancelable(true)
        dialog_recycler_view7 = Dialog(this)
        dialog_recycler_view7.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view7.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view7.setCancelable(true)
        dialog_recycler_view8 = Dialog(this)
        dialog_recycler_view8.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view8.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view8.setCancelable(true)
        dialog_recycler_view9 = Dialog(this)
        dialog_recycler_view9.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view9.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view9.setCancelable(true)
        dialog_recycler_view10 = Dialog(this)
        dialog_recycler_view10.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_recycler_view10.setContentView(R.layout.dialog_dep_view)
        dialog_recycler_view10.setCancelable(true)
        dialog_list_type2 = Dialog(this)
        dialog_list_type2.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_type2.setContentView(R.layout.dialog_view)
        dialog_list_type2.setCancelable(true)
        list_items_list_type2 = dialog_list_type2.findViewById(R.id.list_languages) as ListView
        dialog_list_option_type = Dialog(this)
        dialog_list_option_type.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_option_type.setContentView(R.layout.dialog_view)
        dialog_list_option_type.setCancelable(true)
        list_items_optionlist_type1 = dialog_list_option_type.findViewById(R.id.list_languages) as ListView
        dialog_list_type3 = Dialog(this)
        dialog_list_type3.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_type3.setContentView(R.layout.dialog_view)
        dialog_list_type3.setCancelable(true)
        list_items_list_type3_txt = dialog_list_type3.findViewById(R.id.list_languages) as ListView

        dialog_list_type = Dialog(this)
        dialog_list_type.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_type.setContentView(R.layout.dialog_view)
        dialog_list_type.setCancelable(true)
        list_items_list_type = dialog_list_type.findViewById(R.id.list_languages) as ListView
        dialog_list_txt = Dialog(this)
        dialog_list_txt.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_txt.setContentView(R.layout.dialog_view)
        dialog_list_txt.setCancelable(true)
        list_items_list_type3 = dialog_list_txt.findViewById(R.id.list_languages) as ListView
        dialog_list_subtxt = Dialog(this)
        dialog_list_subtxt.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_subtxt.setContentView(R.layout.dialog_view)
        dialog_list_subtxt.setCancelable(true)
        list_items_list_type4 = dialog_list_subtxt.findViewById(R.id.list_languages) as ListView
        dialog_list_sizes = Dialog(this)
        dialog_list_sizes.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_sizes.setContentView(R.layout.dialog_view)
        dialog_list_sizes.setCancelable(true)
        list_items_list_type5 = dialog_list_sizes.findViewById(R.id.list_languages) as ListView
        dialog_list_weights = Dialog(this)
        dialog_list_weights.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list_weights.setContentView(R.layout.dialog_view)
        dialog_list_weights.setCancelable(true)
        list_items_list_type6 = dialog_list_weights.findViewById(R.id.list_languages) as ListView
        dialog_listpaper = Dialog(this)
        dialog_listpaper.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_listpaper.setContentView(R.layout.dialog_view)
        dialog_listpaper.setCancelable(true)
        JobOneDropswidgets()
        JobTwoDropswidgets()
        JobThreeDropswidgets()
        JobFourDropswidgets()
        JobFiveDropswidgets()
        JobSixDropswidgets()
        JobSevenDropswidgets()
        JobEightDropswidgets()
        JobNineDropswidgets()
        JobTenDropswidgets()

        rv_main_id = dialog_recycler_view.findViewById(R.id.rv_main_id) as ListView
        rv_main_id2 = dialog_recycler_view2.findViewById(R.id.rv_main_id) as ListView
        rv_main_id3 = dialog_recycler_view3.findViewById(R.id.rv_main_id) as ListView
        rv_main_id4 = dialog_recycler_view4.findViewById(R.id.rv_main_id) as ListView
        rv_main_id5 = dialog_recycler_view5.findViewById(R.id.rv_main_id) as ListView
        rv_main_id6 = dialog_recycler_view6.findViewById(R.id.rv_main_id) as ListView
        rv_main_id7 = dialog_recycler_view7.findViewById(R.id.rv_main_id) as ListView
        rv_main_id8 = dialog_recycler_view8.findViewById(R.id.rv_main_id) as ListView
        rv_main_id9 = dialog_recycler_view9.findViewById(R.id.rv_main_id) as ListView
        rv_main_id10 = dialog_recycler_view10.findViewById(R.id.rv_main_id) as ListView

        val tv_cancel_id = dialog_recycler_view.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id2 = dialog_recycler_view2.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id3 = dialog_recycler_view3.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id4 = dialog_recycler_view4.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id5 = dialog_recycler_view5.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id6 = dialog_recycler_view6.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id7 = dialog_recycler_view7.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id8 = dialog_recycler_view8.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id9 = dialog_recycler_view9.findViewById(R.id.tv_cancel_id) as TextView
        val tv_cancel_id10 = dialog_recycler_view10.findViewById(R.id.tv_cancel_id) as TextView

        val tv_submit_id = dialog_recycler_view.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id2 = dialog_recycler_view2.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id3 = dialog_recycler_view3.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id4 = dialog_recycler_view4.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id5 = dialog_recycler_view5.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id6 = dialog_recycler_view6.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id7 = dialog_recycler_view7.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id8 = dialog_recycler_view8.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id9 = dialog_recycler_view9.findViewById(R.id.tv_submit_id) as TextView
        val tv_submit_id10 = dialog_recycler_view10.findViewById(R.id.tv_submit_id) as TextView

        tv_cancel_id.setOnClickListener {
            dialog_recycler_view.dismiss()
        }
        tv_cancel_id2.setOnClickListener {
            dialog_recycler_view2.dismiss()
        }
        tv_cancel_id3.setOnClickListener {
            dialog_recycler_view3.dismiss()
        }
        tv_cancel_id4.setOnClickListener {
            dialog_recycler_view4.dismiss()
        }
        tv_cancel_id5.setOnClickListener {
            dialog_recycler_view5.dismiss()
        }
        tv_cancel_id6.setOnClickListener {
            dialog_recycler_view6.dismiss()
        }
        tv_cancel_id7.setOnClickListener {
            dialog_recycler_view7.dismiss()
        }
        tv_cancel_id8.setOnClickListener {
            dialog_recycler_view8.dismiss()
        }
        tv_cancel_id9.setOnClickListener {
            dialog_recycler_view9.dismiss()
        }
        tv_cancel_id10.setOnClickListener {
            dialog_recycler_view10.dismiss()
        }
        tv_submit_id.setOnClickListener {
            tasksselectedid_only = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()
            val size = job_nameList.size
            Log.e("main_size", job_nameList.size.toString() + "--" + job_nameList.toString() + "---" + mSingleTasklistandCat.toString() + "--" + mSingleTasklistandCat.size)
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList.get(y))

                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat.get(y))
            }
            al1 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat.size) {
                hs.add(mSingleTasklistandCat.get(z).split("_").get(1))
            }
            al1.addAll(hs)
            for (k in 0 until al1.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al1.get(k))
            }
            Log.e("hashset", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only = (idsb.toString())
            departselectedid_only = (jobid.toString())
            admin_tasks.setText(sb)
            println(sb)

            dialog_recycler_view.dismiss()
        }
        tv_submit_id2.setOnClickListener {
            //seletedItems.add(name)
            tasksselectedid_only2 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()
            val size = job_nameList2.size
            Log.e("main_size2", job_nameList2.size.toString() + "--" + job_nameList2.toString() + "---" + mSingleTasklistandCat2.toString() + "--" + mSingleTasklistandCat2.size)
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList2.get(y))

                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat2.get(y))
            }
            al2 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat2.size) {
                hs.add(mSingleTasklistandCat2.get(z).split("_").get(1))
            }
            al2.addAll(hs)
            for (k in 0 until al2.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al2.get(k))
            }
            Log.e("hashset2", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only2 = (idsb.toString())
            departselectedid_only2 = (jobid.toString())
            admin_tasks2.setText(sb)
            println(sb)
            dialog_recycler_view2.dismiss()
        }
        tv_submit_id3.setOnClickListener {
            tasksselectedid_only3 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()
            val size = job_nameList3.size
            Log.e("main_size3", job_nameList3.size.toString() + "--" + job_nameList3.toString() + "---" + mSingleTasklistandCat3.toString() + "--" + mSingleTasklistandCat3.size)
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList3.get(y))
                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat3.get(y))
            }
            al3 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat3.size) {
                hs.add(mSingleTasklistandCat3.get(z).split("_").get(1))
            }
            al3.addAll(hs)
            for (k in 0 until al3.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al3.get(k))
            }
            Log.e("hashset3", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only3 = (idsb.toString())
            departselectedid_only3 = (jobid.toString())
            admin_tasks3.setText(sb)
            println(sb)

            dialog_recycler_view3.dismiss()
        }

        tv_submit_id4.setOnClickListener {
            tasksselectedid_only4 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()

            val size = job_nameList4.size
            Log.e("main_size4", job_nameList4.size.toString() + "--" + job_nameList4.toString() + "---" + mSingleTasklistandCat4.toString() + "--" + mSingleTasklistandCat4.size)
            var appendSeparator = false
            var taslappend = false
            var jobappend = false

            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList4.get(y))

                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat4.get(y))
            }
            al4 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat4.size) {
                hs.add(mSingleTasklistandCat4.get(z).split("_").get(1))
            }
            al4.addAll(hs)
            for (k in 0 until al4.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al4.get(k))
            }
            Log.e("hashset4", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only4 = (idsb.toString())
            departselectedid_only4 = (jobid.toString())
            admin_tasks4.setText(sb)
            println(sb)

            dialog_recycler_view4.dismiss()
        }


        tv_submit_id5.setOnClickListener {
            tasksselectedid_only5 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()
            val size = job_nameList5.size
            Log.e("main_size4", job_nameList5.size.toString() + "--" + job_nameList5.toString() + "---" + mSingleTasklistandCat5.toString() + "--" + mSingleTasklistandCat5.size)
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList5.get(y))

                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat5.get(y))
            }
            al5 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat5.size) {
                hs.add(mSingleTasklistandCat5.get(z).split("_").get(1))
            }
            al5.addAll(hs)
            for (k in 0 until al5.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al5.get(k))
            }
            Log.e("hashset4", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only5 = (idsb.toString())
            departselectedid_only5 = (jobid.toString())
            admin_tasks5.setText(sb)
            println(sb)
            dialog_recycler_view5.dismiss()
        }


        tv_submit_id6.setOnClickListener {
            //seletedItems.add(name)
            tasksselectedid_only6 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()

            val size = job_nameList6.size
            Log.e("main_size4", job_nameList6.size.toString() + "--" + job_nameList6.toString() + "---" + mSingleTasklistandCat6.toString() + "--" + mSingleTasklistandCat6.size)
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList6.get(y))
                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat6.get(y))
            }
            al6 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat6.size) {
                hs.add(mSingleTasklistandCat6.get(z).split("_").get(1))
            }
            al6.addAll(hs)
            for (k in 0 until al6.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al6.get(k))
            }
            Log.e("hashset6", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only6 = (idsb.toString())
            departselectedid_only6 = (jobid.toString())
            admin_tasks6.setText(sb)
            println(sb)

            dialog_recycler_view6.dismiss()
        }
        tv_submit_id7.setOnClickListener {
            tasksselectedid_only7 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()
            val size = job_nameList7.size
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList7.get(y))
                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat7.get(y))

            }
            al7 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat7.size) {
                hs.add(mSingleTasklistandCat7.get(z).split("_").get(1))
            }
            al7.addAll(hs)
            for (k in 0 until al7.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al7.get(k))
            }
            Log.e("hashset7", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only7 = (idsb.toString())
            departselectedid_only7 = (jobid.toString())
            admin_tasks7.setText(sb)
            println(sb)
            dialog_recycler_view7.dismiss()
        }
        tv_submit_id8.setOnClickListener {
            tasksselectedid_only8 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()
            val size = job_nameList8.size
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList8.get(y))

                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat8.get(y))
            }
            al8 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat8.size) {
                hs.add(mSingleTasklistandCat8.get(z).split("_").get(1))
            }
            al8.addAll(hs)
            for (k in 0 until al8.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al8.get(k))
            }
            Log.e("hashset8", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only8 = (idsb.toString())
            departselectedid_only8 = (jobid.toString())
            admin_tasks8.setText(sb)
            println(sb)

            dialog_recycler_view8.dismiss()
        }

        tv_submit_id9.setOnClickListener {
            tasksselectedid_only9 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()
            val size = job_nameList9.size
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList9.get(y))

                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat9.get(y))
            }
            al9 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat9.size) {
                hs.add(mSingleTasklistandCat9.get(z).split("_").get(1))
            }
            al9.addAll(hs)
            for (k in 0 until al9.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al9.get(k))
            }
            Log.e("hashset9", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only9 = (idsb.toString())
            departselectedid_only9 = (jobid.toString())
            admin_tasks9.setText(sb)
            println(sb)

            dialog_recycler_view9.dismiss()
        }


        tv_submit_id10.setOnClickListener {
            tasksselectedid_only10 = ""
            val sb = StringBuilder()
            val idsb = StringBuilder()
            val jobid = StringBuilder()
            val size = job_nameList10.size
            var appendSeparator = false
            var taslappend = false
            var jobappend = false
            for (y in 0 until size) {
                if (appendSeparator)
                    sb.append(',') // a comma
                appendSeparator = true
                sb.append(job_nameList10.get(y))

                if (taslappend)
                    idsb.append(',') // a comma
                taslappend = true
                idsb.append(mSingleTasklistandCat10.get(y))
            }
            al10 = ArrayList<String>()
            val hs = HashSet<String>()
            for (z in 0 until mSingleTasklistandCat10.size) {
                hs.add(mSingleTasklistandCat10.get(z).split("_").get(1))
            }
            al10.addAll(hs)
            for (k in 0 until al10.size) {
                if (jobappend)
                    jobid.append(',') // a comma
                jobappend = true
                jobid.append(al10.get(k))
            }
            Log.e("hashset10", hs.size.toString() + "--" + hs.toString())
            tasksselectedid_only10 = (idsb.toString())
            departselectedid_only10 = (jobid.toString())
            admin_tasks10.setText(sb)
            println(sb)

            dialog_recycler_view10.dismiss()
        }

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        toolbar.title = "Add Job"
        admin_Pdata_id = findViewById<CustomTextView>(R.id.admin_Pdata_id)
        admin_Estart_date_id = findViewById<CustomTextView>(R.id.admin_Estart_date_id)
        admin_Cstart_date_id = findViewById<CustomTextView>(R.id.admin_Cstart_date_id)
        val myFormat = "MM/dd/yyyy HH:mm" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        val date = sdf.format(cal.time)
        admin_Pdata_id.text = date.toString()
        admin_Estart_date_id.text = date.toString()
        admin_Cstart_date_id.text = date.toString()
        callgetJobsAPI()

        ll_subterms1 = findViewById<LinearLayout>(R.id.ll_subterms1)
        ll_subterms11 = findViewById<LinearLayout>(R.id.ll_subterms11)
        ll_subterms21 = findViewById<LinearLayout>(R.id.ll_subterms21)
        ll_subterms31 = findViewById<LinearLayout>(R.id.ll_subterms31)
        ll_subterms51 = findViewById<LinearLayout>(R.id.ll_subterms51)
        ll_subterms61 = findViewById<LinearLayout>(R.id.ll_subterms61)
        ll_subterms71 = findViewById<LinearLayout>(R.id.ll_subterms71)
        ll_subterms81 = findViewById<LinearLayout>(R.id.ll_subterms81)
        ll_subterms91 = findViewById<LinearLayout>(R.id.ll_subterms91)
        ll_subterms101 = findViewById<LinearLayout>(R.id.ll_subterms101)

        ll_subterms52 = findViewById<LinearLayout>(R.id.ll_subterms52)
        ll_subterms62 = findViewById<LinearLayout>(R.id.ll_subterms62)
        ll_subterms72 = findViewById<LinearLayout>(R.id.ll_subterms72)
        ll_subterms82 = findViewById<LinearLayout>(R.id.ll_subterms82)
        ll_subterms92 = findViewById<LinearLayout>(R.id.ll_subterms92)
        ll_subterms102 = findViewById<LinearLayout>(R.id.ll_subterms102)

        ll_subterms53 = findViewById<LinearLayout>(R.id.ll_subterms53)
        ll_subterms63 = findViewById<LinearLayout>(R.id.ll_subterms63)
        ll_subterms73 = findViewById<LinearLayout>(R.id.ll_subterms73)
        ll_subterms83 = findViewById<LinearLayout>(R.id.ll_subterms83)
        ll_subterms93 = findViewById<LinearLayout>(R.id.ll_subterms93)
        ll_subterms103 = findViewById<LinearLayout>(R.id.ll_subterms103)

        ll_subterms54 = findViewById<LinearLayout>(R.id.ll_subterms54)
        ll_subterms64 = findViewById<LinearLayout>(R.id.ll_subterms64)
        ll_subterms74 = findViewById<LinearLayout>(R.id.ll_subterms74)
        ll_subterms84 = findViewById<LinearLayout>(R.id.ll_subterms84)
        ll_subterms94 = findViewById<LinearLayout>(R.id.ll_subterms94)
        ll_subterms104 = findViewById<LinearLayout>(R.id.ll_subterms104)

        ll_subterms55 = findViewById<LinearLayout>(R.id.ll_subterms55)
        ll_subterms65 = findViewById<LinearLayout>(R.id.ll_subterms65)
        ll_subterms75 = findViewById<LinearLayout>(R.id.ll_subterms75)
        ll_subterms85 = findViewById<LinearLayout>(R.id.ll_subterms85)
        ll_subterms95 = findViewById<LinearLayout>(R.id.ll_subterms95)
        ll_subterms105 = findViewById<LinearLayout>(R.id.ll_subterms105)

        ll_subterms56 = findViewById<LinearLayout>(R.id.ll_subterms56)
        ll_subterms66 = findViewById<LinearLayout>(R.id.ll_subterms66)
        ll_subterms76 = findViewById<LinearLayout>(R.id.ll_subterms76)
        ll_subterms86 = findViewById<LinearLayout>(R.id.ll_subterms86)
        ll_subterms96 = findViewById<LinearLayout>(R.id.ll_subterms96)
        ll_subterms106 = findViewById<LinearLayout>(R.id.ll_subterms106)

        ll_subterms57 = findViewById<LinearLayout>(R.id.ll_subterms57)
        ll_subterms67 = findViewById<LinearLayout>(R.id.ll_subterms67)
        ll_subterms77 = findViewById<LinearLayout>(R.id.ll_subterms77)
        ll_subterms87 = findViewById<LinearLayout>(R.id.ll_subterms87)
        ll_subterms97 = findViewById<LinearLayout>(R.id.ll_subterms97)
        ll_subterms107 = findViewById<LinearLayout>(R.id.ll_subterms107)

        ll_subterms58 = findViewById<LinearLayout>(R.id.ll_subterms58)
        ll_subterms68 = findViewById<LinearLayout>(R.id.ll_subterms68)
        ll_subterms78 = findViewById<LinearLayout>(R.id.ll_subterms78)
        ll_subterms88 = findViewById<LinearLayout>(R.id.ll_subterms88)
        ll_subterms98 = findViewById<LinearLayout>(R.id.ll_subterms98)
        ll_subterms108 = findViewById<LinearLayout>(R.id.ll_subterms108)

        ll_subterms59 = findViewById<LinearLayout>(R.id.ll_subterms59)
        ll_subterms69 = findViewById<LinearLayout>(R.id.ll_subterms69)
        ll_subterms79 = findViewById<LinearLayout>(R.id.ll_subterms79)
        ll_subterms89 = findViewById<LinearLayout>(R.id.ll_subterms89)
        ll_subterms99 = findViewById<LinearLayout>(R.id.ll_subterms99)
        ll_subterms109 = findViewById<LinearLayout>(R.id.ll_subterms109)

        ll_subterms510 = findViewById<LinearLayout>(R.id.ll_subterms510)
        ll_subterms610 = findViewById<LinearLayout>(R.id.ll_subterms610)
        ll_subterms710 = findViewById<LinearLayout>(R.id.ll_subterms710)
        ll_subterms810 = findViewById<LinearLayout>(R.id.ll_subterms810)
        ll_subterms910 = findViewById<LinearLayout>(R.id.ll_subterms910)
        ll_subterms1010 = findViewById<LinearLayout>(R.id.ll_subterms1010)

        ll_subterms2 = findViewById<LinearLayout>(R.id.ll_subterms2)
        ll_subterms12 = findViewById<LinearLayout>(R.id.ll_subterms12)
        ll_subterms22 = findViewById<LinearLayout>(R.id.ll_subterms22)
        ll_subterms32 = findViewById<LinearLayout>(R.id.ll_subterms32)
        ll_subterms3 = findViewById<LinearLayout>(R.id.ll_subterms3)
        ll_subterms13 = findViewById<LinearLayout>(R.id.ll_subterms13)
        ll_subterms23 = findViewById<LinearLayout>(R.id.ll_subterms23)
        ll_subterms33 = findViewById<LinearLayout>(R.id.ll_subterms33)
        ll_subterms4 = findViewById<LinearLayout>(R.id.ll_subterms4)
        ll_subterms14 = findViewById<LinearLayout>(R.id.ll_subterms14)
        ll_subterms24 = findViewById<LinearLayout>(R.id.ll_subterms24)
        ll_subterms34 = findViewById<LinearLayout>(R.id.ll_subterms34)
        ll_subterms5 = findViewById<LinearLayout>(R.id.ll_subterms5)
        ll_subterms15 = findViewById<LinearLayout>(R.id.ll_subterms15)
        ll_subterms25 = findViewById<LinearLayout>(R.id.ll_subterms25)
        ll_subterms35 = findViewById<LinearLayout>(R.id.ll_subterms35)

        ll_subterms6 = findViewById<LinearLayout>(R.id.ll_subterms6)
        ll_subterms16 = findViewById<LinearLayout>(R.id.ll_subterms16)
        ll_subterms26 = findViewById<LinearLayout>(R.id.ll_subterms26)
        ll_subterms36 = findViewById<LinearLayout>(R.id.ll_subterms36)
        ll_subterms7 = findViewById<LinearLayout>(R.id.ll_subterms7)
        ll_subterms17 = findViewById<LinearLayout>(R.id.ll_subterms17)
        ll_subterms27 = findViewById<LinearLayout>(R.id.ll_subterms27)
        ll_subterms37 = findViewById<LinearLayout>(R.id.ll_subterms37)
        ll_subterms8 = findViewById<LinearLayout>(R.id.ll_subterms8)
        ll_subterms18 = findViewById<LinearLayout>(R.id.ll_subterms18)
        ll_subterms28 = findViewById<LinearLayout>(R.id.ll_subterms28)
        ll_subterms38 = findViewById<LinearLayout>(R.id.ll_subterms38)
        ll_subterms9 = findViewById<LinearLayout>(R.id.ll_subterms9)
        ll_subterms19 = findViewById<LinearLayout>(R.id.ll_subterms19)
        ll_subterms29 = findViewById<LinearLayout>(R.id.ll_subterms29)
        ll_subterms39 = findViewById<LinearLayout>(R.id.ll_subterms39)
        ll_subterms10 = findViewById<LinearLayout>(R.id.ll_subterms10)
        ll_subterms110 = findViewById<LinearLayout>(R.id.ll_subterms110)
        ll_subterms210 = findViewById<LinearLayout>(R.id.ll_subterms210)
        ll_subterms310 = findViewById<LinearLayout>(R.id.ll_subterms310)

        admin_termssitem1 = findViewById<EditText>(R.id.admin_termssitem1)
        admin_termssitem11 = findViewById<EditText>(R.id.admin_termssitem11)
        admin_termssitem21 = findViewById<EditText>(R.id.admin_termssitem21)
        admin_termssitem31 = findViewById<EditText>(R.id.admin_termssitem31)
        admin_termssitem51 = findViewById<EditText>(R.id.admin_termssitem51)
        admin_termssitem61 = findViewById<EditText>(R.id.admin_termssitem61)
        admin_termssitem71 = findViewById<EditText>(R.id.admin_termssitem71)
        admin_termssitem81 = findViewById<EditText>(R.id.admin_termssitem81)
        admin_termssitem91 = findViewById<EditText>(R.id.admin_termssitem91)
        admin_termssitem101 = findViewById<EditText>(R.id.admin_termssitem101)

        admin_termssitem52 = findViewById<EditText>(R.id.admin_termssitem52)
        admin_termssitem62 = findViewById<EditText>(R.id.admin_termssitem62)
        admin_termssitem72 = findViewById<EditText>(R.id.admin_termssitem72)
        admin_termssitem82 = findViewById<EditText>(R.id.admin_termssitem82)
        admin_termssitem92 = findViewById<EditText>(R.id.admin_termssitem92)
        admin_termssitem102 = findViewById<EditText>(R.id.admin_termssitem102)

        admin_termssitem53 = findViewById<EditText>(R.id.admin_termssitem53)
        admin_termssitem63 = findViewById<EditText>(R.id.admin_termssitem63)
        admin_termssitem73 = findViewById<EditText>(R.id.admin_termssitem73)
        admin_termssitem83 = findViewById<EditText>(R.id.admin_termssitem83)
        admin_termssitem93 = findViewById<EditText>(R.id.admin_termssitem93)
        admin_termssitem103 = findViewById<EditText>(R.id.admin_termssitem103)

        admin_termssitem54 = findViewById<EditText>(R.id.admin_termssitem54)
        admin_termssitem64 = findViewById<EditText>(R.id.admin_termssitem64)
        admin_termssitem74 = findViewById<EditText>(R.id.admin_termssitem74)
        admin_termssitem84 = findViewById<EditText>(R.id.admin_termssitem84)
        admin_termssitem94 = findViewById<EditText>(R.id.admin_termssitem94)
        admin_termssitem104 = findViewById<EditText>(R.id.admin_termssitem104)

        admin_termssitem55 = findViewById<EditText>(R.id.admin_termssitem55)
        admin_termssitem65 = findViewById<EditText>(R.id.admin_termssitem65)
        admin_termssitem75 = findViewById<EditText>(R.id.admin_termssitem75)
        admin_termssitem85 = findViewById<EditText>(R.id.admin_termssitem85)
        admin_termssitem95 = findViewById<EditText>(R.id.admin_termssitem95)
        admin_termssitem105 = findViewById<EditText>(R.id.admin_termssitem105)

        admin_termssitem56 = findViewById<EditText>(R.id.admin_termssitem56)
        admin_termssitem66 = findViewById<EditText>(R.id.admin_termssitem66)
        admin_termssitem76 = findViewById<EditText>(R.id.admin_termssitem76)
        admin_termssitem86 = findViewById<EditText>(R.id.admin_termssitem86)
        admin_termssitem96 = findViewById<EditText>(R.id.admin_termssitem96)
        admin_termssitem106 = findViewById<EditText>(R.id.admin_termssitem106)

        admin_termssitem57 = findViewById<EditText>(R.id.admin_termssitem57)
        admin_termssitem67 = findViewById<EditText>(R.id.admin_termssitem67)
        admin_termssitem77 = findViewById<EditText>(R.id.admin_termssitem77)
        admin_termssitem87 = findViewById<EditText>(R.id.admin_termssitem87)
        admin_termssitem97 = findViewById<EditText>(R.id.admin_termssitem97)
        admin_termssitem107 = findViewById<EditText>(R.id.admin_termssitem107)

        admin_termssitem58 = findViewById<EditText>(R.id.admin_termssitem58)
        admin_termssitem68 = findViewById<EditText>(R.id.admin_termssitem68)
        admin_termssitem78 = findViewById<EditText>(R.id.admin_termssitem78)
        admin_termssitem88 = findViewById<EditText>(R.id.admin_termssitem88)
        admin_termssitem98 = findViewById<EditText>(R.id.admin_termssitem98)
        admin_termssitem108 = findViewById<EditText>(R.id.admin_termssitem108)

        admin_termssitem59 = findViewById<EditText>(R.id.admin_termssitem59)
        admin_termssitem69 = findViewById<EditText>(R.id.admin_termssitem69)
        admin_termssitem79 = findViewById<EditText>(R.id.admin_termssitem79)
        admin_termssitem89 = findViewById<EditText>(R.id.admin_termssitem89)
        admin_termssitem99 = findViewById<EditText>(R.id.admin_termssitem99)
        admin_termssitem109 = findViewById<EditText>(R.id.admin_termssitem109)

        admin_termssitem510 = findViewById<EditText>(R.id.admin_termssitem510)
        admin_termssitem610 = findViewById<EditText>(R.id.admin_termssitem610)
        admin_termssitem710 = findViewById<EditText>(R.id.admin_termssitem710)
        admin_termssitem810 = findViewById<EditText>(R.id.admin_termssitem810)
        admin_termssitem910 = findViewById<EditText>(R.id.admin_termssitem910)
        admin_termssitem1010 = findViewById<EditText>(R.id.admin_termssitem1010)

        admin_termssitem2 = findViewById<EditText>(R.id.admin_termssitem2)
        admin_termssitem12 = findViewById<EditText>(R.id.admin_termssitem12)
        admin_termssitem22 = findViewById<EditText>(R.id.admin_termssitem22)
        admin_termssitem32 = findViewById<EditText>(R.id.admin_termssitem32)

        admin_termssitem3 = findViewById<EditText>(R.id.admin_termssitem3)
        admin_termssitem13 = findViewById<EditText>(R.id.admin_termssitem13)
        admin_termssitem23 = findViewById<EditText>(R.id.admin_termssitem23)
        admin_termssitem33 = findViewById<EditText>(R.id.admin_termssitem33)

        admin_termssitem4 = findViewById<EditText>(R.id.admin_termssitem4)
        admin_termssitem14 = findViewById<EditText>(R.id.admin_termssitem14)
        admin_termssitem24 = findViewById<EditText>(R.id.admin_termssitem24)
        admin_termssitem34 = findViewById<EditText>(R.id.admin_termssitem34)

        admin_termssitem5 = findViewById<EditText>(R.id.admin_termssitem5)
        admin_termssitem15 = findViewById<EditText>(R.id.admin_termssitem15)
        admin_termssitem25 = findViewById<EditText>(R.id.admin_termssitem25)
        admin_termssitem35 = findViewById<EditText>(R.id.admin_termssitem35)

        admin_termssitem6 = findViewById<EditText>(R.id.admin_termssitem6)
        admin_termssitem16 = findViewById<EditText>(R.id.admin_termssitem16)
        admin_termssitem26 = findViewById<EditText>(R.id.admin_termssitem26)
        admin_termssitem36 = findViewById<EditText>(R.id.admin_termssitem36)

        admin_termssitem7 = findViewById<EditText>(R.id.admin_termssitem7)
        admin_termssitem17 = findViewById<EditText>(R.id.admin_termssitem17)
        admin_termssitem27 = findViewById<EditText>(R.id.admin_termssitem27)
        admin_termssitem37 = findViewById<EditText>(R.id.admin_termssitem37)

        admin_termssitem8 = findViewById<EditText>(R.id.admin_termssitem8)
        admin_termssitem18 = findViewById<EditText>(R.id.admin_termssitem18)
        admin_termssitem28 = findViewById<EditText>(R.id.admin_termssitem28)
        admin_termssitem38 = findViewById<EditText>(R.id.admin_termssitem38)

        admin_termssitem9 = findViewById<EditText>(R.id.admin_termssitem9)
        admin_termssitem19 = findViewById<EditText>(R.id.admin_termssitem19)
        admin_termssitem29 = findViewById<EditText>(R.id.admin_termssitem29)
        admin_termssitem39 = findViewById<EditText>(R.id.admin_termssitem39)

        admin_termssitem10 = findViewById<EditText>(R.id.admin_termssitem10)
        admin_termssitem110 = findViewById<EditText>(R.id.admin_termssitem110)
        admin_termssitem210 = findViewById<EditText>(R.id.admin_termssitem210)
        admin_termssitem310 = findViewById<EditText>(R.id.admin_termssitem310)

        admin_termsdate1 = findViewById<EditText>(R.id.admin_termsdate1)
        admin_termsdate11 = findViewById<EditText>(R.id.admin_termsdate11)
        admin_termsdate21 = findViewById<EditText>(R.id.admin_termsdate21)
        admin_termsdate31 = findViewById<EditText>(R.id.admin_termsdate31)
        admin_termsdate51 = findViewById<EditText>(R.id.admin_termsdate51)
        admin_termsdate61 = findViewById<EditText>(R.id.admin_termsdate61)
        admin_termsdate71 = findViewById<EditText>(R.id.admin_termsdate71)
        admin_termsdate81 = findViewById<EditText>(R.id.admin_termsdate81)
        admin_termsdate91 = findViewById<EditText>(R.id.admin_termsdate91)
        admin_termsdate101 = findViewById<EditText>(R.id.admin_termsdate101)

        admin_termsdate52 = findViewById<EditText>(R.id.admin_termsdate52)
        admin_termsdate62 = findViewById<EditText>(R.id.admin_termsdate62)
        admin_termsdate72 = findViewById<EditText>(R.id.admin_termsdate72)
        admin_termsdate82 = findViewById<EditText>(R.id.admin_termsdate82)
        admin_termsdate92 = findViewById<EditText>(R.id.admin_termsdate92)
        admin_termsdate102 = findViewById<EditText>(R.id.admin_termsdate102)

        admin_termsdate53 = findViewById<EditText>(R.id.admin_termsdate53)
        admin_termsdate63 = findViewById<EditText>(R.id.admin_termsdate63)
        admin_termsdate73 = findViewById<EditText>(R.id.admin_termsdate73)
        admin_termsdate83 = findViewById<EditText>(R.id.admin_termsdate83)
        admin_termsdate93 = findViewById<EditText>(R.id.admin_termsdate93)
        admin_termsdate103 = findViewById<EditText>(R.id.admin_termsdate103)

        admin_termsdate54 = findViewById<EditText>(R.id.admin_termsdate54)
        admin_termsdate64 = findViewById<EditText>(R.id.admin_termsdate64)
        admin_termsdate74 = findViewById<EditText>(R.id.admin_termsdate74)
        admin_termsdate84 = findViewById<EditText>(R.id.admin_termsdate84)
        admin_termsdate94 = findViewById<EditText>(R.id.admin_termsdate94)
        admin_termsdate104 = findViewById<EditText>(R.id.admin_termsdate104)

        admin_termsdate55 = findViewById<EditText>(R.id.admin_termsdate55)
        admin_termsdate65 = findViewById<EditText>(R.id.admin_termsdate65)
        admin_termsdate75 = findViewById<EditText>(R.id.admin_termsdate75)
        admin_termsdate85 = findViewById<EditText>(R.id.admin_termsdate85)
        admin_termsdate95 = findViewById<EditText>(R.id.admin_termsdate95)
        admin_termsdate105 = findViewById<EditText>(R.id.admin_termsdate105)

        admin_termsdate56 = findViewById<EditText>(R.id.admin_termsdate56)
        admin_termsdate66 = findViewById<EditText>(R.id.admin_termsdate66)
        admin_termsdate76 = findViewById<EditText>(R.id.admin_termsdate76)
        admin_termsdate86 = findViewById<EditText>(R.id.admin_termsdate86)
        admin_termsdate96 = findViewById<EditText>(R.id.admin_termsdate96)
        admin_termsdate106 = findViewById<EditText>(R.id.admin_termsdate106)

        admin_termsdate57 = findViewById<EditText>(R.id.admin_termsdate57)
        admin_termsdate67 = findViewById<EditText>(R.id.admin_termsdate67)
        admin_termsdate77 = findViewById<EditText>(R.id.admin_termsdate77)
        admin_termsdate87 = findViewById<EditText>(R.id.admin_termsdate87)
        admin_termsdate97 = findViewById<EditText>(R.id.admin_termsdate97)
        admin_termsdate107 = findViewById<EditText>(R.id.admin_termsdate107)

        admin_termsdate58 = findViewById<EditText>(R.id.admin_termsdate58)
        admin_termsdate68 = findViewById<EditText>(R.id.admin_termsdate68)
        admin_termsdate78 = findViewById<EditText>(R.id.admin_termsdate78)
        admin_termsdate88 = findViewById<EditText>(R.id.admin_termsdate88)
        admin_termsdate98 = findViewById<EditText>(R.id.admin_termsdate98)
        admin_termsdate108 = findViewById<EditText>(R.id.admin_termsdate108)


        admin_termsdate59 = findViewById<EditText>(R.id.admin_termsdate59)
        admin_termsdate69 = findViewById<EditText>(R.id.admin_termsdate69)
        admin_termsdate79 = findViewById<EditText>(R.id.admin_termsdate79)
        admin_termsdate89 = findViewById<EditText>(R.id.admin_termsdate89)
        admin_termsdate99 = findViewById<EditText>(R.id.admin_termsdate99)
        admin_termsdate109 = findViewById<EditText>(R.id.admin_termsdate109)

        admin_termsdate510 = findViewById<EditText>(R.id.admin_termsdate510)
        admin_termsdate610 = findViewById<EditText>(R.id.admin_termsdate610)
        admin_termsdate710 = findViewById<EditText>(R.id.admin_termsdate710)
        admin_termsdate810 = findViewById<EditText>(R.id.admin_termsdate810)
        admin_termsdate910 = findViewById<EditText>(R.id.admin_termsdate910)
        admin_termsdate1010 = findViewById<EditText>(R.id.admin_termsdate1010)

        admin_termsdate2 = findViewById<EditText>(R.id.admin_termsdate2)
        admin_termsdate12 = findViewById<EditText>(R.id.admin_termsdate12)
        admin_termsdate22 = findViewById<EditText>(R.id.admin_termsdate22)
        admin_termsdate32 = findViewById<EditText>(R.id.admin_termsdate32)

        admin_termsdate3 = findViewById<EditText>(R.id.admin_termsdate3)
        admin_termsdate13 = findViewById<EditText>(R.id.admin_termsdate13)
        admin_termsdate23 = findViewById<EditText>(R.id.admin_termsdate23)
        admin_termsdate33 = findViewById<EditText>(R.id.admin_termsdate33)

        admin_termsdate4 = findViewById<EditText>(R.id.admin_termsdate4)
        admin_termsdate14 = findViewById<EditText>(R.id.admin_termsdate14)
        admin_termsdate24 = findViewById<EditText>(R.id.admin_termsdate24)
        admin_termsdate34 = findViewById<EditText>(R.id.admin_termsdate34)

        admin_termsdate5 = findViewById<EditText>(R.id.admin_termsdate5)
        admin_termsdate15 = findViewById<EditText>(R.id.admin_termsdate15)
        admin_termsdate25 = findViewById<EditText>(R.id.admin_termsdate25)
        admin_termsdate35 = findViewById<EditText>(R.id.admin_termsdate35)

        admin_termsdate6 = findViewById<EditText>(R.id.admin_termsdate6)
        admin_termsdate16 = findViewById<EditText>(R.id.admin_termsdate16)
        admin_termsdate26 = findViewById<EditText>(R.id.admin_termsdate26)
        admin_termsdate36 = findViewById<EditText>(R.id.admin_termsdate36)

        admin_termsdate7 = findViewById<EditText>(R.id.admin_termsdate7)
        admin_termsdate17 = findViewById<EditText>(R.id.admin_termsdate17)
        admin_termsdate27 = findViewById<EditText>(R.id.admin_termsdate27)
        admin_termsdate37 = findViewById<EditText>(R.id.admin_termsdate37)

        admin_termsdate8 = findViewById<EditText>(R.id.admin_termsdate8)
        admin_termsdate18 = findViewById<EditText>(R.id.admin_termsdate18)
        admin_termsdate28 = findViewById<EditText>(R.id.admin_termsdate28)
        admin_termsdate38 = findViewById<EditText>(R.id.admin_termsdate38)

        admin_termsdate9 = findViewById<EditText>(R.id.admin_termsdate9)
        admin_termsdate19 = findViewById<EditText>(R.id.admin_termsdate19)
        admin_termsdate29 = findViewById<EditText>(R.id.admin_termsdate29)
        admin_termsdate39 = findViewById<EditText>(R.id.admin_termsdate39)

        admin_termsdate10 = findViewById<EditText>(R.id.admin_termsdate10)
        admin_termsdate110 = findViewById<EditText>(R.id.admin_termsdate110)
        admin_termsdate210 = findViewById<EditText>(R.id.admin_termsdate210)
        admin_termsdate310 = findViewById<EditText>(R.id.admin_termsdate310)

        typeofpaper = findViewById<EditText>(R.id.admin_typeofpaper)
        typeofpaper1 = findViewById<EditText>(R.id.admin_typeofpaper1)
        typeofpaper2 = findViewById<EditText>(R.id.admin_typeofpaper2)
        typeofpaper3 = findViewById<EditText>(R.id.admin_typeofpaper3)
        typeofpaper5 = findViewById<EditText>(R.id.admin_typeofpaper5)
        typeofpaper6 = findViewById<EditText>(R.id.admin_typeofpaper6)
        typeofpaper7 = findViewById<EditText>(R.id.admin_typeofpaper7)
        typeofpaper8 = findViewById<EditText>(R.id.admin_typeofpaper8)
        typeofpaper9 = findViewById<EditText>(R.id.admin_typeofpaper9)
        typeofpaper10 = findViewById<EditText>(R.id.admin_typeofpaper10)

        admin_optionlist = findViewById<EditText>(R.id.admin_optionlist)
        admin_optionlist1 = findViewById<EditText>(R.id.admin_optionlist1)
        admin_optionlist2 = findViewById<EditText>(R.id.admin_optionlist2)
        admin_optionlist3 = findViewById<EditText>(R.id.admin_optionlist3)
        admin_optionlist5 = findViewById<EditText>(R.id.admin_optionlist5)
        admin_optionlist6 = findViewById<EditText>(R.id.admin_optionlist6)
        admin_optionlist7 = findViewById<EditText>(R.id.admin_optionlist7)
        admin_optionlist8 = findViewById<EditText>(R.id.admin_optionlist8)
        admin_optionlist9 = findViewById<EditText>(R.id.admin_optionlist9)
        admin_optionlist10 = findViewById<EditText>(R.id.admin_optionlist10)

        txtname = findViewById<EditText>(R.id.admin_txtname)
        txtname1 = findViewById<EditText>(R.id.admin_txtname1)
        txtname2 = findViewById<EditText>(R.id.admin_txtname2)
        txtname3 = findViewById<EditText>(R.id.admin_txtname3)
        txtname5 = findViewById<EditText>(R.id.admin_txtname5)
        txtname6 = findViewById<EditText>(R.id.admin_txtname6)
        txtname7 = findViewById<EditText>(R.id.admin_txtname7)
        txtname8 = findViewById<EditText>(R.id.admin_txtname8)
        txtname9 = findViewById<EditText>(R.id.admin_txtname9)
        txtname10 = findViewById<EditText>(R.id.admin_txtname10)

        subtxtname = findViewById<EditText>(R.id.admin_subtxtname)
        subtxtname1 = findViewById<EditText>(R.id.admin_subtxtname1)
        subtxtname2 = findViewById<EditText>(R.id.admin_subtxtname2)
        subtxtname3 = findViewById<EditText>(R.id.admin_subtxtname3)
        subtxtname5 = findViewById<EditText>(R.id.admin_subtxtname5)
        subtxtname6 = findViewById<EditText>(R.id.admin_subtxtname6)
        subtxtname7 = findViewById<EditText>(R.id.admin_subtxtname7)
        subtxtname8 = findViewById<EditText>(R.id.admin_subtxtname8)
        subtxtname9 = findViewById<EditText>(R.id.admin_subtxtname9)
        subtxtname10 = findViewById<EditText>(R.id.admin_subtxtname10)

        weight = findViewById<EditText>(R.id.admin_weight)
        weight1 = findViewById<EditText>(R.id.admin_weight1)
        weight2 = findViewById<EditText>(R.id.admin_weight2)
        weight3 = findViewById<EditText>(R.id.admin_weight3)
        weight5 = findViewById<EditText>(R.id.admin_weight5)
        weight6 = findViewById<EditText>(R.id.admin_weight6)
        weight7 = findViewById<EditText>(R.id.admin_weight7)
        weight8 = findViewById<EditText>(R.id.admin_weight8)
        weight9 = findViewById<EditText>(R.id.admin_weight9)
        weight10 = findViewById<EditText>(R.id.admin_weight10)

        sizetxt = findViewById<EditText>(R.id.admin_size)
        sizetxt1 = findViewById<EditText>(R.id.admin_size1)
        sizetxt2 = findViewById<EditText>(R.id.admin_size2)
        sizetxt3 = findViewById<EditText>(R.id.admin_size3)
        sizetxt5 = findViewById<EditText>(R.id.admin_size5)
        sizetxt6 = findViewById<EditText>(R.id.admin_size6)
        sizetxt7 = findViewById<EditText>(R.id.admin_size7)
        sizetxt8 = findViewById<EditText>(R.id.admin_size8)
        sizetxt9 = findViewById<EditText>(R.id.admin_size9)
        sizetxt10 = findViewById<EditText>(R.id.admin_size10)

        allotsheets = findViewById<EditText>(R.id.admin_allotedsheets)
        allotsheets1 = findViewById<EditText>(R.id.admin_allotedsheets1)
        allotsheets2 = findViewById<EditText>(R.id.admin_allotedsheets2)
        allotsheets3 = findViewById<EditText>(R.id.admin_allotedsheets3)
        allotsheets5 = findViewById<EditText>(R.id.admin_allotedsheets5)
        allotsheets6 = findViewById<EditText>(R.id.admin_allotedsheets6)
        allotsheets7 = findViewById<EditText>(R.id.admin_allotedsheets7)
        allotsheets8 = findViewById<EditText>(R.id.admin_allotedsheets8)
        allotsheets9 = findViewById<EditText>(R.id.admin_allotedsheets9)
        allotsheets10 = findViewById<EditText>(R.id.admin_allotedsheets10)

        totalquantity = findViewById<EditText>(R.id.admin_totalquantity)
        totalquantity1 = findViewById<EditText>(R.id.admin_totalquantity1)
        totalquantity2 = findViewById<EditText>(R.id.admin_totalquantity2)
        totalquantity3 = findViewById<EditText>(R.id.admin_totalquantity3)
        totalquantity5 = findViewById<EditText>(R.id.admin_totalquantity5)
        totalquantity6 = findViewById<EditText>(R.id.admin_totalquantity6)
        totalquantity7 = findViewById<EditText>(R.id.admin_totalquantity7)
        totalquantity8 = findViewById<EditText>(R.id.admin_totalquantity8)
        totalquantity9 = findViewById<EditText>(R.id.admin_totalquantity9)
        totalquantity10 = findViewById<EditText>(R.id.admin_totalquantity10)

        admin_customer_id = findViewById<AutoCompleteTextView>(R.id.admin_customer_id)
        admin_job_type_id = findViewById<EditText>(R.id.admin_job_type_id)
        admin_job_id = findViewById<EditText>(R.id.admin_job_id)
        admin_job_part_id = findViewById<EditText>(R.id.admin_job_part_id)
        admin_job_terms = findViewById<EditText>(R.id.admin_job_terms)
        admin_job_terms1 = findViewById<EditText>(R.id.admin_job_terms1)
        admin_job_terms2 = findViewById<EditText>(R.id.admin_job_terms2)
        admin_job_terms3 = findViewById<EditText>(R.id.admin_job_terms3)

        admin_job_terms5 = findViewById<EditText>(R.id.admin_job_terms5)
        admin_job_terms6 = findViewById<EditText>(R.id.admin_job_terms6)
        admin_job_terms7 = findViewById<EditText>(R.id.admin_job_terms7)
        admin_job_terms8 = findViewById<EditText>(R.id.admin_job_terms8)
        admin_job_terms9 = findViewById<EditText>(R.id.admin_job_terms9)
        admin_job_terms10 = findViewById<EditText>(R.id.admin_job_terms10)
        admin_Pactivity_id = findViewById<EditText>(R.id.admin_Pactivity_id)
        admin_description_id = findViewById<EditText>(R.id.admin_description_id)
        admin_Estart_time_id = findViewById<CustomTextView>(R.id.admin_Estart_time_id)
        admin_schedule_id = findViewById<EditText>(R.id.admin_schedule_id)
        admin_due_time_id = findViewById<CustomTextView>(R.id.admin_due_time_id)
        tv_availablesheets = findViewById<CustomTextView>(R.id.tv_availablesheets)
        tv_availablesheets1 = findViewById<CustomTextView>(R.id.tv_availablesheets1)
        tv_availablesheets2 = findViewById<CustomTextView>(R.id.tv_availablesheets2)
        tv_availablesheets3 = findViewById<CustomTextView>(R.id.tv_availablesheets3)
        tv_availablesheets5 = findViewById<CustomTextView>(R.id.tv_availablesheets5)
        tv_availablesheets6 = findViewById<CustomTextView>(R.id.tv_availablesheets6)
        tv_availablesheets7 = findViewById<CustomTextView>(R.id.tv_availablesheets7)
        tv_availablesheets8 = findViewById<CustomTextView>(R.id.tv_availablesheets8)
        tv_availablesheets9 = findViewById<CustomTextView>(R.id.tv_availablesheets9)
        tv_availablesheets10 = findViewById<CustomTextView>(R.id.tv_availablesheets10)

        tv_error_jobID = findViewById<CustomTextView>(R.id.tv_error_jobID)
        tv_departments = findViewById<TextView>(R.id.tv_departments)
        tv_departments2 = findViewById<TextView>(R.id.tv_departments2)
        tv_departments3 = findViewById<TextView>(R.id.tv_departments3)
        tv_departments4 = findViewById<TextView>(R.id.tv_departments4)

        tv_departments5 = findViewById<TextView>(R.id.tv_departments5)
        tv_departments6 = findViewById<TextView>(R.id.tv_departments6)
        tv_departments7 = findViewById<TextView>(R.id.tv_departments7)
        tv_departments8 = findViewById<TextView>(R.id.tv_departments8)
        tv_departments9 = findViewById<TextView>(R.id.tv_departments9)
        tv_departments10 = findViewById<TextView>(R.id.tv_departments10)

        admin_Lact_code_id = findViewById<EditText>(R.id.admin_Lact_code_id)
        btn_submit_id = findViewById<Button>(R.id.btn_submit_id)

        admin_departments = findViewById(R.id.admin_departments)
        admin_departments2 = findViewById(R.id.admin_departments2)
        admin_departments3 = findViewById(R.id.admin_departments3)
        admin_departments4 = findViewById(R.id.admin_departments4)
        admin_departments6 = findViewById(R.id.admin_departments6)
        admin_departments7 = findViewById(R.id.admin_departments7)
        admin_departments8 = findViewById(R.id.admin_departments8)
        admin_departments9 = findViewById(R.id.admin_departments9)
        admin_departments10 = findViewById(R.id.admin_departments10)
        admin_departments5 = findViewById(R.id.admin_departments5)

        admin_tasks = findViewById(R.id.admin_tasks)
        admin_tasks2 = findViewById(R.id.admin_tasks2)
        admin_tasks3 = findViewById(R.id.admin_tasks3)
        admin_tasks4 = findViewById(R.id.admin_tasks4)
        admin_tasks5 = findViewById(R.id.admin_tasks5)
        admin_tasks6 = findViewById(R.id.admin_tasks6)
        admin_tasks7 = findViewById(R.id.admin_tasks7)
        admin_tasks8 = findViewById(R.id.admin_tasks8)
        admin_tasks9 = findViewById(R.id.admin_tasks9)
        admin_tasks10 = findViewById(R.id.admin_tasks10)

        ll_jobpart1 = findViewById(R.id.ll_jobpart1)
        ll_jobpart2 = findViewById(R.id.ll_jobpart2)
        ll_jobpart3 = findViewById(R.id.ll_jobpart3)
        ll_jobpart4 = findViewById(R.id.ll_jobpart4)
        ll_jobpart5 = findViewById(R.id.ll_jobpart5)
        ll_jobpart6 = findViewById(R.id.ll_jobpart6)
        ll_jobpart7 = findViewById(R.id.ll_jobpart7)
        ll_jobpart8 = findViewById(R.id.ll_jobpart8)
        ll_jobpart9 = findViewById(R.id.ll_jobpart9)
        ll_jobpart10 = findViewById(R.id.ll_jobpart10)

        btn_jobpart1 = findViewById(R.id.btn_jobpart1)
        btn_jobpart2 = findViewById(R.id.btn_jobpart2)
        btn_jobpart3 = findViewById(R.id.btn_jobpart3)
        btn_jobpart4 = findViewById(R.id.btn_jobpart4)
        btn_jobpart5 = findViewById(R.id.btn_jobpart5)
        btn_jobpart6 = findViewById(R.id.btn_jobpart6)
        btn_jobpart7 = findViewById(R.id.btn_jobpart7)
        btn_jobpart8 = findViewById(R.id.btn_jobpart8)
        btn_jobpart9 = findViewById(R.id.btn_jobpart9)
        btn_jobpart10 = findViewById(R.id.btn_jobpart10)

        ll_txt_name = findViewById(R.id.ll_txt_name)
        ll_subtxt_name = findViewById(R.id.ll_subtxt_name)
        ll_weight = findViewById(R.id.ll_weight)
        ll_size = findViewById(R.id.ll_size)

        ll_txt_name_one = findViewById(R.id.ll_txt_name_one)
        ll_subtxt_name_one = findViewById(R.id.ll_subtxt_name_one)
        ll_weight_one = findViewById(R.id.ll_weight_one)
        ll_size_one = findViewById(R.id.ll_size_one)
        ll_optionlist_one = findViewById(R.id.ll_optionlist_one)

        ll_txt_name_two = findViewById(R.id.ll_txt_name_two)
        ll_subtxt_name_two = findViewById(R.id.ll_subtxt_name_two)
        ll_weight_two = findViewById(R.id.ll_weight_two)
        ll_size_two = findViewById(R.id.ll_size_two)

        ll_txt_name_three = findViewById(R.id.ll_txt_name_three)
        ll_subtxt_name_three = findViewById(R.id.ll_subtxt_name_three)
        ll_weight_three = findViewById(R.id.ll_weight_three)
        ll_size_three = findViewById(R.id.ll_size_three)

        ll_txt_name_five = findViewById(R.id.ll_txt_name5)
        ll_subtxt_name_five = findViewById(R.id.ll_subtxt_name5)
        ll_weight_five = findViewById(R.id.ll_weight5)
        ll_size_five = findViewById(R.id.ll_size5)

        ll_txt_name_six = findViewById(R.id.ll_txt_name6)
        ll_subtxt_name_six = findViewById(R.id.ll_subtxt_name6)
        ll_weight_six = findViewById(R.id.ll_weight6)
        ll_size_six = findViewById(R.id.ll_size6)

        ll_txt_name_seven = findViewById(R.id.ll_txt_name7)
        ll_subtxt_name_seven = findViewById(R.id.ll_subtxt_name7)
        ll_weight_seven = findViewById(R.id.ll_weight7)
        ll_size_seven = findViewById(R.id.ll_size7)

        ll_txt_name_eight = findViewById(R.id.ll_txt_name8)
        ll_subtxt_name_eight = findViewById(R.id.ll_subtxt_name8)
        ll_weight_eight = findViewById(R.id.ll_weight8)
        ll_size_eight = findViewById(R.id.ll_size8)

        ll_txt_name_nine = findViewById(R.id.ll_txt_name9)
        ll_subtxt_name_nine = findViewById(R.id.ll_subtxt_name9)
        ll_weight_nine = findViewById(R.id.ll_weight9)
        ll_size_nine = findViewById(R.id.ll_size9)

        ll_txt_name_ten = findViewById(R.id.ll_txt_name10)
        ll_subtxt_name_ten = findViewById(R.id.ll_subtxt_name10)
        ll_weight_ten = findViewById(R.id.ll_weight10)
        ll_size_ten = findViewById(R.id.ll_size10)

        ll_other_desc = findViewById(R.id.ll_other_desc)
        ll_other_desc_one = findViewById(R.id.ll_other_desc_one)
        ll_other_desc_two = findViewById(R.id.ll_other_desc_two)
        ll_other_desc_three = findViewById(R.id.ll_other_desc_three)
        ll_other_desc_five = findViewById(R.id.ll_other_desc5)
        ll_other_desc_six = findViewById(R.id.ll_other_desc6)
        ll_other_desc_seven = findViewById(R.id.ll_other_desc7)
        ll_other_desc_eight = findViewById(R.id.ll_other_desc8)
        ll_other_desc_nine = findViewById(R.id.ll_other_desc9)
        ll_other_desc_ten = findViewById(R.id.ll_other_desc10)

        other_desc = findViewById(R.id.other_desc)
        other_desc_one = findViewById(R.id.other_desc_one)
        other_desc_two = findViewById(R.id.other_desc_two)
        other_desc_three = findViewById(R.id.other_desc_three)
        other_desc_five = findViewById(R.id.other_desc5)
        other_desc_six = findViewById(R.id.other_desc6)
        other_desc_seven = findViewById(R.id.other_desc7)
        other_desc_eight = findViewById(R.id.other_desc8)
        other_desc_nine = findViewById(R.id.other_desc9)
        other_desc_ten = findViewById(R.id.other_desc10)
        ll_jobpart1.visibility = View.GONE
        btn_jobpart1.setOnClickListener {
            ll_jobpart1.visibility = View.VISIBLE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.GONE
        }
        btn_jobpart2.setOnClickListener {
            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.VISIBLE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.GONE
        }
        btn_jobpart3.setOnClickListener {
            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.VISIBLE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.GONE
        }
        btn_jobpart4.setOnClickListener {
            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.VISIBLE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.GONE
        }
        btn_jobpart5.setOnClickListener {
            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.VISIBLE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.GONE
        }

        btn_jobpart6.setOnClickListener {
            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.VISIBLE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.GONE
        }

        btn_jobpart7.setOnClickListener {

            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.VISIBLE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.GONE
        }
        btn_jobpart8.setOnClickListener {
            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.VISIBLE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.GONE
        }

        btn_jobpart9.setOnClickListener {
            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.VISIBLE
            ll_jobpart10.visibility = View.GONE
        }
        btn_jobpart10.setOnClickListener {

            ll_jobpart1.visibility = View.GONE
            ll_jobpart2.visibility = View.GONE
            ll_jobpart3.visibility = View.GONE
            ll_jobpart4.visibility = View.GONE
            ll_jobpart5.visibility = View.GONE
            ll_jobpart6.visibility = View.GONE
            ll_jobpart7.visibility = View.GONE
            ll_jobpart8.visibility = View.GONE
            ll_jobpart9.visibility = View.GONE
            ll_jobpart10.visibility = View.VISIBLE
        }
        admin_tasks.setOnClickListener {
            if (!admin_departments.selectedItemsAsString.toString().equals("")) {
                dialog_recycler_view.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }
        admin_tasks2.setOnClickListener {
            if (!admin_departments2.selectedItemsAsString2.toString().equals("")) {
                dialog_recycler_view2.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }
        admin_tasks3.setOnClickListener {
            if (!admin_departments3.selectedItemsAsString3.toString().equals("")) {
                dialog_recycler_view3.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }
        admin_tasks4.setOnClickListener {
            if (!admin_departments4.selectedItemsAsString4.toString().equals("")) {
                dialog_recycler_view4.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }


        admin_tasks5.setOnClickListener {
            if (!admin_departments5.selectedItemsAsString5.toString().equals("")) {
                dialog_recycler_view5.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }


        admin_tasks6.setOnClickListener {
            if (!admin_departments6.selectedItemsAsString6.toString().equals("")) {
                dialog_recycler_view6.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }


        admin_tasks7.setOnClickListener {
            if (!admin_departments7.selectedItemsAsString7.toString().equals("")) {
                dialog_recycler_view7.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }

        admin_tasks8.setOnClickListener {
            if (!admin_departments8.selectedItemsAsString8.toString().equals("")) {
                dialog_recycler_view8.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }

        admin_tasks9.setOnClickListener {
            if (!admin_departments9.selectedItemsAsString9.toString().equals("")) {
                dialog_recycler_view9.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }

        admin_tasks10.setOnClickListener {
            if (!admin_departments10.selectedItemsAsString10.toString().equals("")) {
                dialog_recycler_view10.show()
                tv_departments.visibility = View.GONE
                tv_departments2.visibility = View.GONE
                tv_departments3.visibility = View.GONE
                tv_departments4.visibility = View.GONE
                tv_departments5.visibility = View.GONE
                tv_departments6.visibility = View.GONE
                tv_departments7.visibility = View.GONE
                tv_departments8.visibility = View.GONE
                tv_departments9.visibility = View.GONE
                tv_departments10.visibility = View.GONE
            } else {
                Toast.makeText(this@AddJobActivity, "Please Select Departments", Toast.LENGTH_SHORT).show()
            }
        }

        admin_job_id.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val admin_job_id_stg = admin_job_id.text.toString().trim()

                Log.w("ADMINJOBID", "*** " + admin_job_id_stg)
                CallCheckJobIdAPI(admin_job_id_stg)
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }
            override fun afterTextChanged(s: Editable) {
            }
        })
        admin_termsdate1.setOnClickListener(this)
        admin_termsdate11.setOnClickListener(this)
        admin_termsdate21.setOnClickListener(this)
        admin_termsdate31.setOnClickListener(this)
        admin_termsdate51.setOnClickListener(this)
        admin_termsdate61.setOnClickListener(this)
        admin_termsdate71.setOnClickListener(this)
        admin_termsdate81.setOnClickListener(this)
        admin_termsdate91.setOnClickListener(this)
        admin_termsdate101.setOnClickListener(this)

        admin_termsdate2.setOnClickListener(this)
        admin_termsdate12.setOnClickListener(this)
        admin_termsdate22.setOnClickListener(this)
        admin_termsdate32.setOnClickListener(this)
        admin_termsdate52.setOnClickListener(this)
        admin_termsdate62.setOnClickListener(this)
        admin_termsdate72.setOnClickListener(this)
        admin_termsdate82.setOnClickListener(this)
        admin_termsdate92.setOnClickListener(this)
        admin_termsdate102.setOnClickListener(this)

        admin_termsdate3.setOnClickListener(this)
        admin_termsdate13.setOnClickListener(this)
        admin_termsdate23.setOnClickListener(this)
        admin_termsdate33.setOnClickListener(this)
        admin_termsdate53.setOnClickListener(this)
        admin_termsdate63.setOnClickListener(this)
        admin_termsdate73.setOnClickListener(this)
        admin_termsdate83.setOnClickListener(this)
        admin_termsdate93.setOnClickListener(this)
        admin_termsdate103.setOnClickListener(this)

        admin_termsdate4.setOnClickListener(this)
        admin_termsdate14.setOnClickListener(this)
        admin_termsdate24.setOnClickListener(this)
        admin_termsdate34.setOnClickListener(this)
        admin_termsdate54.setOnClickListener(this)
        admin_termsdate64.setOnClickListener(this)
        admin_termsdate74.setOnClickListener(this)
        admin_termsdate84.setOnClickListener(this)
        admin_termsdate94.setOnClickListener(this)
        admin_termsdate104.setOnClickListener(this)

        admin_termsdate5.setOnClickListener(this)
        admin_termsdate15.setOnClickListener(this)
        admin_termsdate25.setOnClickListener(this)
        admin_termsdate35.setOnClickListener(this)
        admin_termsdate55.setOnClickListener(this)
        admin_termsdate65.setOnClickListener(this)
        admin_termsdate75.setOnClickListener(this)
        admin_termsdate85.setOnClickListener(this)
        admin_termsdate95.setOnClickListener(this)
        admin_termsdate105.setOnClickListener(this)

        admin_termsdate6.setOnClickListener(this)
        admin_termsdate16.setOnClickListener(this)
        admin_termsdate26.setOnClickListener(this)
        admin_termsdate36.setOnClickListener(this)
        admin_termsdate56.setOnClickListener(this)
        admin_termsdate66.setOnClickListener(this)
        admin_termsdate76.setOnClickListener(this)
        admin_termsdate86.setOnClickListener(this)
        admin_termsdate96.setOnClickListener(this)
        admin_termsdate106.setOnClickListener(this)

        admin_termsdate7.setOnClickListener(this)
        admin_termsdate17.setOnClickListener(this)
        admin_termsdate27.setOnClickListener(this)
        admin_termsdate37.setOnClickListener(this)
        admin_termsdate57.setOnClickListener(this)
        admin_termsdate67.setOnClickListener(this)
        admin_termsdate77.setOnClickListener(this)
        admin_termsdate87.setOnClickListener(this)
        admin_termsdate97.setOnClickListener(this)
        admin_termsdate107.setOnClickListener(this)

        admin_termsdate8.setOnClickListener(this)
        admin_termsdate18.setOnClickListener(this)
        admin_termsdate28.setOnClickListener(this)
        admin_termsdate38.setOnClickListener(this)
        admin_termsdate58.setOnClickListener(this)
        admin_termsdate68.setOnClickListener(this)
        admin_termsdate78.setOnClickListener(this)
        admin_termsdate88.setOnClickListener(this)
        admin_termsdate98.setOnClickListener(this)
        admin_termsdate108.setOnClickListener(this)

        admin_termsdate9.setOnClickListener(this)
        admin_termsdate19.setOnClickListener(this)
        admin_termsdate29.setOnClickListener(this)
        admin_termsdate39.setOnClickListener(this)
        admin_termsdate59.setOnClickListener(this)
        admin_termsdate69.setOnClickListener(this)
        admin_termsdate79.setOnClickListener(this)
        admin_termsdate89.setOnClickListener(this)
        admin_termsdate99.setOnClickListener(this)
        admin_termsdate109.setOnClickListener(this)

        admin_termsdate10.setOnClickListener(this)
        admin_termsdate110.setOnClickListener(this)
        admin_termsdate210.setOnClickListener(this)
        admin_termsdate310.setOnClickListener(this)
        admin_termsdate510.setOnClickListener(this)
        admin_termsdate610.setOnClickListener(this)
        admin_termsdate710.setOnClickListener(this)
        admin_termsdate810.setOnClickListener(this)
        admin_termsdate910.setOnClickListener(this)
        admin_termsdate1010.setOnClickListener(this)
        jobsheetsdropdown()
        optionlistsdropdown()
        admin_optionlist.setOnClickListener {
            Log.w("ADMINOPTIONLIST", "ADMINOPTIONLIST")
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id = optiontypelist.get(i).option_id!!
                        Log.w("ADMINOPTIONLISToption_id", option_id)
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist1.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist1.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id1 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist2.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist2.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id2 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist3.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist3.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id3 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist5.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist5.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id5 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist6.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist6.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id6 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist7.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist7.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id7 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist8.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist8.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id8 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist9.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist9.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id9 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }
        admin_optionlist10.setOnClickListener {
            if (!dialog_list_option_type.isShowing)
                dialog_list_option_type.show()
            val option_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, optionliststringlist)
            list_items_optionlist_type1.adapter = option_array_adapter
            list_items_optionlist_type1.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_optionlist_type1.getItemAtPosition(i).toString()
                admin_optionlist10.setText(selected_name)
                for (i in 0 until optiontypelist.size) {
                    if (optiontypelist.get(i).type!!.toString().equals(selected_name)) {
                        option_id10 = optiontypelist.get(i).option_id!!
                    }
                }
                dialog_list_option_type.dismiss()
            }
        }

        typeofpaper.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.setOnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name.visibility = View.GONE
                    ll_subtxt_name.visibility = View.GONE
                    ll_weight.visibility = View.GONE
                    ll_size.visibility = View.GONE
                    ll_other_desc.visibility = View.VISIBLE
                    other_val_job = "Other"

                } else {
                    ll_txt_name.visibility = View.VISIBLE
                    ll_subtxt_name.visibility = View.VISIBLE
                    ll_weight.visibility = View.VISIBLE
                    ll_size.visibility = View.VISIBLE
                    ll_other_desc.visibility = View.GONE
                    other_desc.setText("")
                    other_val_job = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid = papertypelist.get(i).id!!
                        txtname.setText("")
                        subtxtname.setText("")
                        sizetxt.setText("")
                        weight.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }

        }
        typeofpaper1.setOnClickListener {
            Log.w("TYPEOFPAPERPART1", "TYPEOFPAPERPART1")
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper1.setText(selected_name)
                if (selected_name.equals("Other")) {
                    ll_txt_name_one.visibility = View.GONE
                    ll_subtxt_name_one.visibility = View.GONE
                    ll_weight_one.visibility = View.GONE
                    ll_size_one.visibility = View.GONE
                    ll_other_desc_one.visibility = View.VISIBLE
                    other_val_job_1 = "Other"
                } else {
                    ll_txt_name_one.visibility = View.VISIBLE
                    ll_subtxt_name_one.visibility = View.VISIBLE
                    ll_weight_one.visibility = View.VISIBLE
                    ll_size_one.visibility = View.VISIBLE
                    ll_other_desc_one.visibility = View.GONE
                    other_desc_one.setText("")
                    other_val_job_1 = ""
                }
                dialog_list_type2.dismiss()
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid1 = papertypelist.get(i).id!!
                        txtname1.setText("")
                        subtxtname1.setText("")
                        sizetxt1.setText("")
                        weight1.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        typeofpaper2.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper2.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name_two.visibility = View.GONE
                    ll_subtxt_name_two.visibility = View.GONE
                    ll_weight_two.visibility = View.GONE
                    ll_size_two.visibility = View.GONE
                    ll_other_desc_two.visibility = View.VISIBLE
                    other_val_job_2 = "Other"
                } else {
                    ll_txt_name_two.visibility = View.VISIBLE
                    ll_subtxt_name_two.visibility = View.VISIBLE
                    ll_weight_two.visibility = View.VISIBLE
                    ll_size_two.visibility = View.VISIBLE
                    ll_other_desc_two.visibility = View.GONE
                    other_desc_two.setText("")
                    other_val_job_2 = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid2 = papertypelist.get(i).id!!

                        txtname2.setText("")
                        subtxtname2.setText("")
                        sizetxt2.setText("")
                        weight2.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        typeofpaper3.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper3.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name_three.visibility = View.GONE
                    ll_subtxt_name_three.visibility = View.GONE
                    ll_weight_three.visibility = View.GONE
                    ll_size_three.visibility = View.GONE
                    ll_other_desc_three.visibility = View.VISIBLE
                    other_val_job_3 = "Other"
                } else {
                    ll_txt_name_three.visibility = View.VISIBLE
                    ll_subtxt_name_three.visibility = View.VISIBLE
                    ll_weight_three.visibility = View.VISIBLE
                    ll_size_three.visibility = View.VISIBLE
                    ll_other_desc_three.visibility = View.GONE
                    other_desc_three.setText("")
                    other_val_job_3 = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid3 = papertypelist.get(i).id!!
                        txtname3.setText("")
                        subtxtname3.setText("")
                        sizetxt3.setText("")
                        weight3.setText("")

                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        typeofpaper5.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper5.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name_five.visibility = View.GONE
                    ll_subtxt_name_five.visibility = View.GONE
                    ll_weight_five.visibility = View.GONE
                    ll_size_five.visibility = View.GONE
                    ll_other_desc_five.visibility = View.VISIBLE
                    other_val_job_5 = "Other"
                } else {
                    ll_txt_name_five.visibility = View.VISIBLE
                    ll_subtxt_name_five.visibility = View.VISIBLE
                    ll_weight_five.visibility = View.VISIBLE
                    ll_size_five.visibility = View.VISIBLE
                    ll_other_desc_five.visibility = View.GONE
                    other_desc_five.setText("")
                    other_val_job_5 = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid5 = papertypelist.get(i).id!!
                        txtname5.setText("")
                        subtxtname5.setText("")
                        sizetxt5.setText("")
                        weight5.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        typeofpaper6.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper6.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name_six.visibility = View.GONE
                    ll_subtxt_name_six.visibility = View.GONE
                    ll_weight_six.visibility = View.GONE
                    ll_size_six.visibility = View.GONE
                    ll_other_desc_six.visibility = View.VISIBLE
                    other_val_job_6 = "Other"
                } else {
                    ll_txt_name_six.visibility = View.VISIBLE
                    ll_subtxt_name_six.visibility = View.VISIBLE
                    ll_weight_six.visibility = View.VISIBLE
                    ll_size_six.visibility = View.VISIBLE
                    ll_other_desc_six.visibility = View.GONE
                    other_desc_six.setText("")
                    other_val_job_6 = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid6 = papertypelist.get(i).id!!
                        txtname6.setText("")
                        subtxtname6.setText("")
                        sizetxt6.setText("")
                        weight6.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        typeofpaper7.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper7.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name_seven.visibility = View.GONE
                    ll_subtxt_name_seven.visibility = View.GONE
                    ll_weight_seven.visibility = View.GONE
                    ll_size_seven.visibility = View.GONE
                    ll_other_desc_seven.visibility = View.VISIBLE
                    other_val_job_7 = "Other"
                } else {
                    ll_txt_name_seven.visibility = View.VISIBLE
                    ll_subtxt_name_seven.visibility = View.VISIBLE
                    ll_weight_seven.visibility = View.VISIBLE
                    ll_size_seven.visibility = View.VISIBLE
                    ll_other_desc_seven.visibility = View.GONE
                    other_desc_seven.setText("")
                    other_val_job_7 = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid7 = papertypelist.get(i).id!!
                        txtname7.setText("")
                        subtxtname7.setText("")
                        sizetxt7.setText("")
                        weight7.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        typeofpaper8.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper8.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name_eight.visibility = View.GONE
                    ll_subtxt_name_eight.visibility = View.GONE
                    ll_weight_eight.visibility = View.GONE
                    ll_size_eight.visibility = View.GONE
                    ll_other_desc_eight.visibility = View.VISIBLE
                    other_val_job_8 = "Other"
                } else {
                    ll_txt_name_eight.visibility = View.VISIBLE
                    ll_subtxt_name_eight.visibility = View.VISIBLE
                    ll_weight_eight.visibility = View.VISIBLE
                    ll_size_eight.visibility = View.VISIBLE
                    ll_other_desc_eight.visibility = View.GONE
                    other_desc_eight.setText("")
                    other_val_job_8 = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid8 = papertypelist.get(i).id!!
                        txtname8.setText("")
                        subtxtname8.setText("")
                        sizetxt8.setText("")
                        weight8.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        typeofpaper9.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper9.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name_nine.visibility = View.GONE
                    ll_subtxt_name_nine.visibility = View.GONE
                    ll_weight_nine.visibility = View.GONE
                    ll_size_nine.visibility = View.GONE
                    ll_other_desc_nine.visibility = View.VISIBLE
                    other_val_job_9 = "Other"
                } else {
                    ll_txt_name_nine.visibility = View.VISIBLE
                    ll_subtxt_name_nine.visibility = View.VISIBLE
                    ll_weight_nine.visibility = View.VISIBLE
                    ll_size_nine.visibility = View.VISIBLE
                    ll_other_desc_nine.visibility = View.GONE
                    other_desc_nine.setText("")
                    other_val_job_9 = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid9 = papertypelist.get(i).id!!
                        txtname9.setText("")
                        subtxtname9.setText("")
                        sizetxt9.setText("")
                        weight9.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        typeofpaper10.setOnClickListener {
            if (!dialog_list_type2.isShowing)
                dialog_list_type2.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, papertypestringlist)
            list_items_list_type2.adapter = state_array_adapter
            list_items_list_type2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type2.getItemAtPosition(i).toString()
                typeofpaper10.setText(selected_name)
                dialog_list_type2.dismiss()
                if (selected_name.equals("Other")) {
                    ll_txt_name_ten.visibility = View.GONE
                    ll_subtxt_name_ten.visibility = View.GONE
                    ll_weight_ten.visibility = View.GONE
                    ll_size_ten.visibility = View.GONE
                    ll_other_desc_ten.visibility = View.VISIBLE
                    other_val_job_10 = "Other"
                } else {
                    ll_txt_name_ten.visibility = View.VISIBLE
                    ll_subtxt_name_ten.visibility = View.VISIBLE
                    ll_weight_ten.visibility = View.VISIBLE
                    ll_size_ten.visibility = View.VISIBLE
                    ll_other_desc_ten.visibility = View.GONE
                    other_desc_ten.setText("")
                    other_val_job_10 = ""
                }
                for (i in 0 until papertypelist.size) {
                    if (papertypelist.get(i).paper_name!!.toString().equals(selected_name)) {
                        paperid10 = papertypelist.get(i).id!!
                        txtname10.setText("")
                        subtxtname10.setText("")
                        sizetxt10.setText("")
                        weight10.setText("")
                        texttypes(papertypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.setOnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid = texttypelist.get(i).id!!
                        subtxtname.setText("")
                        sizetxt.setText("")
                        weight.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }

        }
        txtname1.setOnClickListener {
            Log.w("TEXTNAMEPART1", "TEXTNAMEPART1")
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname1.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid1 = texttypelist.get(i).id!!
                        subtxtname1.setText("")
                        sizetxt1.setText("")
                        weight1.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname2.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname2.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid2 = texttypelist.get(i).id!!
                        subtxtname2.setText("")
                        sizetxt2.setText("")
                        weight2.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname3.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname3.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid3 = texttypelist.get(i).id!!
                        subtxtname3.setText("")
                        sizetxt3.setText("")
                        weight3.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname5.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname5.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid5 = texttypelist.get(i).id!!
                        subtxtname5.setText("")
                        sizetxt5.setText("")
                        weight5.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname6.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname6.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid6 = texttypelist.get(i).id!!
                        subtxtname6.setText("")
                        sizetxt6.setText("")
                        weight6.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname7.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname7.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid7 = texttypelist.get(i).id!!
                        subtxtname7.setText("")
                        sizetxt7.setText("")
                        weight7.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname8.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname8.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid8 = texttypelist.get(i).id!!
                        subtxtname8.setText("")
                        sizetxt8.setText("")
                        weight8.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname9.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname9.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid9 = texttypelist.get(i).id!!
                        subtxtname9.setText("")
                        sizetxt9.setText("")
                        weight9.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        txtname10.setOnClickListener {
            if (!dialog_list_type3.isShowing)
                dialog_list_type3.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, texttypelisttringlist)
            list_items_list_type3_txt.adapter = state_array_adapter
            list_items_list_type3_txt.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type3_txt.getItemAtPosition(i).toString()
                txtname10.setText(selected_name)
                dialog_list_type3.dismiss()
                for (i in 0 until texttypelist.size) {
                    if (texttypelist.get(i).text_name!!.toString().equals(selected_name)) {
                        txtid10 = texttypelist.get(i).id!!
                        subtxtname10.setText("")
                        sizetxt10.setText("")
                        weight10.setText("")
                        SubTextTypeResponse(texttypelist.get(i).id!!)
                    }
                }
            }
        }
        subtxtname.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid, weightid, subtxtid, paperid, txtid, "0")
                        sizetxt.setText("")
                        weight.setText("")
                        weightsizeAPi(subtxtid)
                    }
                }

            }
        }
        subtxtname1.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname1.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid1 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid1, weightid1, subtxtid1, paperid1, txtid1, "1")
                        sizetxt1.setText("")
                        weight1.setText("")
                        weightsizeAPi(subtxtid1)
                    }
                }
            }
        }
        subtxtname2.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname2.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid2 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid2, weightid2, subtxtid2, paperid2, txtid2, "2")
                        sizetxt2.setText("")
                        weight2.setText("")
                        weightsizeAPi(subtxtid2)
                    }
                }
            }
        }
        subtxtname3.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname3.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid3 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid3, weightid3, subtxtid3, paperid3, txtid3, "3")
                        sizetxt3.setText("")
                        weight3.setText("")
                        weightsizeAPi(subtxtid3)
                    }
                }
            }
        }
        subtxtname5.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname5.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid5 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid5, weightid5, subtxtid5, paperid5, txtid5, "5")
                        sizetxt5.setText("")
                        weight5.setText("")
                        weightsizeAPi(subtxtid5)
                    }
                }
            }
        }
        subtxtname6.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname6.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid6 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid6, weightid6, subtxtid6, paperid6, txtid6, "6")
                        sizetxt6.setText("")
                        weight6.setText("")
                        weightsizeAPi(subtxtid6)
                    }
                }
            }
        }
        subtxtname7.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname7.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid7 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid7, weightid7, subtxtid7, paperid7, txtid7, "7")
                        sizetxt7.setText("")
                        weight7.setText("")
                        weightsizeAPi(subtxtid7)
                    }
                }
            }
        }
        subtxtname8.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname8.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid8 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid8, weightid8, subtxtid8, paperid8, txtid8, "8")
                        sizetxt8.setText("")
                        weight8.setText("")
                        weightsizeAPi(subtxtid8)
                    }
                }
            }
        }

        subtxtname9.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname9.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid9 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid9, weightid9, subtxtid9, paperid9, txtid9, "9")
                        sizetxt9.setText("")
                        weight9.setText("")
                        weightsizeAPi(subtxtid9)
                    }
                }
            }
        }
        subtxtname10.setOnClickListener {
            if (!dialog_list_subtxt.isShowing)
                dialog_list_subtxt.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, subtexttypelisttringlist)
            list_items_list_type4.adapter = state_array_adapter
            list_items_list_type4.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type4.getItemAtPosition(i).toString()
                subtxtname10.setText(selected_name)
                dialog_list_subtxt.dismiss()
                for (i in 0 until subtexttypelist.size) {
                    if (subtexttypelist.get(i).sub_text_name!!.toString().equals(selected_name)) {
                        subtxtid10 = subtexttypelist.get(i).id!!
                        CallAvailableSheetsAPI(sizeid10, weightid10, subtxtid10, paperid10, txtid10, "10")
                        sizetxt10.setText("")
                        weight10.setText("")
                        weightsizeAPi(subtxtid10)
                    }
                }
            }
        }
        sizetxt.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid, weightid, subtxtid, paperid1, txtid, "0")
                    }
                }
            }
        }
        sizetxt1.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt1.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid1 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid1, weightid1, subtxtid1, paperid1, txtid1, "1")
                    }
                }
            }
        }
        sizetxt2.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt2.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid2 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid2, weightid2, subtxtid2, paperid2, txtid2, "2")
                    }
                }
            }
        }
        sizetxt3.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt3.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid3 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid3, weightid3, subtxtid3, paperid3, txtid3, "3")
                    }
                }
            }
        }
        sizetxt5.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt5.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid5 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid5, weightid5, subtxtid5, paperid5, txtid5, "5")
                    }
                }
            }
        }
        sizetxt6.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt6.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid6 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid6, weightid6, subtxtid6, paperid6, txtid6, "6")
                    }
                }
            }
        }
        sizetxt7.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt7.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid7 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid7, weightid7, subtxtid7, paperid7, txtid7, "7")
                    }
                }
            }
        }
        sizetxt8.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt8.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid8 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid8, weightid8, subtxtid8, paperid8, txtid8, "8")
                    }
                }
            }
        }
        sizetxt9.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt9.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid9 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid9, weightid9, subtxtid9, paperid9, txtid9, "9")
                    }
                }
            }
        }
        sizetxt10.setOnClickListener {
            if (!dialog_list_sizes.isShowing)
                dialog_list_sizes.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, sizelist)
            list_items_list_type5.adapter = state_array_adapter
            list_items_list_type5.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type5.getItemAtPosition(i).toString()
                sizetxt10.setText(selected_name)
                dialog_list_sizes.dismiss()
                for (i in 0 until sizelisttype.size) {
                    if ((sizelisttype.get(i).height!!.toString() + "*" + sizelisttype.get(i).width!!.toString()).equals(selected_name)) {
                        sizeid10 = sizelisttype.get(i).id!!
                        CallAvailableSheetsAPI(sizeid10, weightid10, subtxtid10, paperid10, txtid10, "10")
                    }
                }
            }
        }
        weight.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid = weightlisttype.get(i).id!!
                        sizetxt.setText("")
                        CallAvailableSheetsAPI(sizeid, weightid, subtxtid, paperid, txtid, "0")
                    }
                }
            }
        }
        weight1.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight1.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid1 = weightlisttype.get(i).id!!
                        sizetxt1.setText("")
                        CallAvailableSheetsAPI(sizeid1, weightid1, subtxtid1, paperid1, txtid1, "1")
                    }
                }
            }
        }
        weight2.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight2.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid2 = weightlisttype.get(i).id!!
                        sizetxt2.setText("")
                        CallAvailableSheetsAPI(sizeid2, weightid2, subtxtid2, paperid2, txtid2, "2")
                    }
                }
            }
        }
        weight3.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight3.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid3 = weightlisttype.get(i).id!!
                        sizetxt3.setText("")
                        CallAvailableSheetsAPI(sizeid3, weightid3, subtxtid3, paperid3, txtid3, "3")
                    }
                }
            }
        }
        weight5.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight5.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid5 = weightlisttype.get(i).id!!
                        sizetxt5.setText("")
                        CallAvailableSheetsAPI(sizeid5, weightid5, subtxtid5, paperid5, txtid5, "5")
                    }
                }
            }
        }
        weight6.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight6.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid6 = weightlisttype.get(i).id!!
                        sizetxt6.setText("")
                        CallAvailableSheetsAPI(sizeid6, weightid6, subtxtid6, paperid6, txtid6, "6")
                    }
                }
            }
        }
        weight7.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight7.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid7 = weightlisttype.get(i).id!!
                        sizetxt7.setText("")
                        CallAvailableSheetsAPI(sizeid7, weightid7, subtxtid7, paperid7, txtid7, "7")
                    }
                }
            }
        }
        weight8.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight8.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid8 = weightlisttype.get(i).id!!
                        sizetxt8.setText("")
                        CallAvailableSheetsAPI(sizeid8, weightid8, subtxtid8, paperid8, txtid8, "8")
                    }
                }
            }
        }
        weight9.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight9.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid9 = weightlisttype.get(i).id!!
                        sizetxt9.setText("")
                        CallAvailableSheetsAPI(sizeid9, weightid9, subtxtid9, paperid9, txtid9, "9")
                    }
                }
            }
        }
        weight10.setOnClickListener {
            if (!dialog_list_weights.isShowing)
                dialog_list_weights.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, weightlist)
            list_items_list_type6.adapter = state_array_adapter
            list_items_list_type6.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list_type6.getItemAtPosition(i).toString()
                weight10.setText(selected_name)
                dialog_list_weights.dismiss()
                for (i in 0 until weightlisttype.size) {
                    if (weightlisttype.get(i).weight!!.toString().equals(selected_name)) {
                        weightid10 = weightlisttype.get(i).id!!
                        sizetxt10.setText("")
                        CallAvailableSheetsAPI(sizeid10, weightid10, subtxtid10, paperid10, txtid10, "10")
                    }
                }
            }
        }
        val array = arrayOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
        val array2 = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        admin_job_part_id.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array)
            list_items_list.adapter = state_array_adapter
            list_items_list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_job_part = list_items_list.getItemAtPosition(i).toString()
                admin_job_part_id.setText(selected_job_part)
                final_job_parts = selected_job_part.toString()
                if (selected_job_part == "1") {
                    btn_jobpart1.visibility = View.VISIBLE
                    ll_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.GONE
                    btn_jobpart3.visibility = View.GONE
                    btn_jobpart4.visibility = View.GONE
                    btn_jobpart5.visibility = View.GONE
                    btn_jobpart6.visibility = View.GONE
                    btn_jobpart7.visibility = View.GONE
                    btn_jobpart8.visibility = View.GONE
                    btn_jobpart9.visibility = View.GONE
                    btn_jobpart10.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "2") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.GONE
                    btn_jobpart4.visibility = View.GONE
                    btn_jobpart5.visibility = View.GONE
                    btn_jobpart6.visibility = View.GONE
                    btn_jobpart7.visibility = View.GONE
                    btn_jobpart8.visibility = View.GONE
                    btn_jobpart9.visibility = View.GONE
                    btn_jobpart10.visibility = View.GONE
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "3") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.VISIBLE
                    btn_jobpart4.visibility = View.GONE
                    btn_jobpart5.visibility = View.GONE
                    btn_jobpart6.visibility = View.GONE
                    btn_jobpart7.visibility = View.GONE
                    btn_jobpart8.visibility = View.GONE
                    btn_jobpart9.visibility = View.GONE
                    btn_jobpart10.visibility = View.GONE
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "4") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.VISIBLE
                    btn_jobpart4.visibility = View.VISIBLE
                    btn_jobpart5.visibility = View.GONE
                    btn_jobpart6.visibility = View.GONE
                    btn_jobpart7.visibility = View.GONE
                    btn_jobpart8.visibility = View.GONE
                    btn_jobpart9.visibility = View.GONE
                    btn_jobpart10.visibility = View.GONE
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "5") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.VISIBLE
                    btn_jobpart4.visibility = View.VISIBLE
                    btn_jobpart5.visibility = View.VISIBLE
                    btn_jobpart6.visibility = View.GONE
                    btn_jobpart7.visibility = View.GONE
                    btn_jobpart8.visibility = View.GONE
                    btn_jobpart9.visibility = View.GONE
                    btn_jobpart10.visibility = View.GONE
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "6") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.VISIBLE
                    btn_jobpart4.visibility = View.VISIBLE
                    btn_jobpart5.visibility = View.VISIBLE
                    btn_jobpart6.visibility = View.VISIBLE
                    btn_jobpart7.visibility = View.GONE
                    btn_jobpart8.visibility = View.GONE
                    btn_jobpart9.visibility = View.GONE
                    btn_jobpart10.visibility = View.GONE
                    Log.e("btn_jobpart10_selection", "100")
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "7") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.VISIBLE
                    btn_jobpart4.visibility = View.VISIBLE
                    btn_jobpart5.visibility = View.VISIBLE
                    btn_jobpart6.visibility = View.VISIBLE
                    btn_jobpart7.visibility = View.VISIBLE
                    btn_jobpart8.visibility = View.GONE
                    btn_jobpart9.visibility = View.GONE
                    btn_jobpart10.visibility = View.GONE
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "8") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.VISIBLE
                    btn_jobpart4.visibility = View.VISIBLE
                    btn_jobpart5.visibility = View.VISIBLE
                    btn_jobpart6.visibility = View.VISIBLE
                    btn_jobpart7.visibility = View.VISIBLE
                    btn_jobpart8.visibility = View.VISIBLE
                    btn_jobpart9.visibility = View.GONE
                    btn_jobpart10.visibility = View.GONE
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "9") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.VISIBLE
                    btn_jobpart4.visibility = View.VISIBLE
                    btn_jobpart5.visibility = View.VISIBLE
                    btn_jobpart6.visibility = View.VISIBLE
                    btn_jobpart7.visibility = View.VISIBLE
                    btn_jobpart8.visibility = View.VISIBLE
                    btn_jobpart9.visibility = View.VISIBLE
                    btn_jobpart10.visibility = View.GONE
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                } else if (selected_job_part == "10") {
                    btn_jobpart1.visibility = View.VISIBLE
                    btn_jobpart2.visibility = View.VISIBLE
                    btn_jobpart3.visibility = View.VISIBLE
                    btn_jobpart4.visibility = View.VISIBLE
                    btn_jobpart5.visibility = View.VISIBLE
                    btn_jobpart6.visibility = View.VISIBLE
                    btn_jobpart7.visibility = View.VISIBLE
                    btn_jobpart8.visibility = View.VISIBLE
                    btn_jobpart9.visibility = View.VISIBLE
                    btn_jobpart10.visibility = View.VISIBLE
                    ll_jobpart1.visibility = View.GONE
                    ll_jobpart2.visibility = View.GONE
                    ll_jobpart3.visibility = View.GONE
                    ll_jobpart4.visibility = View.GONE
                    ll_jobpart5.visibility = View.GONE
                    ll_jobpart6.visibility = View.GONE
                    ll_jobpart7.visibility = View.GONE
                    ll_jobpart8.visibility = View.GONE
                    ll_jobpart9.visibility = View.GONE
                    ll_jobpart10.visibility = View.GONE
                }
                dialog_list.dismiss()
            }
        }

        admin_job_terms.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.GONE
                    ll_subterms3.visibility = View.GONE
                    ll_subterms4.visibility = View.GONE
                    ll_subterms5.visibility = View.GONE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.GONE
                    ll_subterms4.visibility = View.GONE
                    ll_subterms5.visibility = View.GONE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.GONE
                    ll_subterms5.visibility = View.GONE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.GONE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.GONE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.GONE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.VISIBLE
                    ll_subterms8.visibility = View.GONE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.VISIBLE
                    ll_subterms8.visibility = View.VISIBLE
                    ll_subterms9.visibility = View.GONE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.VISIBLE
                    ll_subterms8.visibility = View.VISIBLE
                    ll_subterms9.visibility = View.VISIBLE
                    ll_subterms10.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms1.visibility = View.VISIBLE
                    ll_subterms2.visibility = View.VISIBLE
                    ll_subterms3.visibility = View.VISIBLE
                    ll_subterms4.visibility = View.VISIBLE
                    ll_subterms5.visibility = View.VISIBLE
                    ll_subterms6.visibility = View.VISIBLE
                    ll_subterms7.visibility = View.VISIBLE
                    ll_subterms8.visibility = View.VISIBLE
                    ll_subterms9.visibility = View.VISIBLE
                    ll_subterms10.visibility = View.VISIBLE
                }
                termsquantity = ""
                termsdate = ""
                dialog_list.dismiss()
            }
        }
        admin_job_terms1.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms1.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.GONE
                    ll_subterms13.visibility = View.GONE
                    ll_subterms14.visibility = View.GONE
                    ll_subterms15.visibility = View.GONE
                    ll_subterms16.visibility = View.GONE
                    ll_subterms17.visibility = View.GONE
                    ll_subterms18.visibility = View.GONE
                    ll_subterms19.visibility = View.GONE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.GONE
                    ll_subterms14.visibility = View.GONE
                    ll_subterms15.visibility = View.GONE
                    ll_subterms16.visibility = View.GONE
                    ll_subterms17.visibility = View.GONE
                    ll_subterms18.visibility = View.GONE
                    ll_subterms19.visibility = View.GONE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.VISIBLE
                    ll_subterms14.visibility = View.GONE
                    ll_subterms15.visibility = View.GONE
                    ll_subterms16.visibility = View.GONE
                    ll_subterms17.visibility = View.GONE
                    ll_subterms18.visibility = View.GONE
                    ll_subterms19.visibility = View.GONE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.VISIBLE
                    ll_subterms14.visibility = View.VISIBLE
                    ll_subterms15.visibility = View.GONE
                    ll_subterms16.visibility = View.GONE
                    ll_subterms17.visibility = View.GONE
                    ll_subterms18.visibility = View.GONE
                    ll_subterms19.visibility = View.GONE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.VISIBLE
                    ll_subterms14.visibility = View.VISIBLE
                    ll_subterms15.visibility = View.VISIBLE
                    ll_subterms16.visibility = View.GONE
                    ll_subterms17.visibility = View.GONE
                    ll_subterms18.visibility = View.GONE
                    ll_subterms19.visibility = View.GONE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.VISIBLE
                    ll_subterms14.visibility = View.VISIBLE
                    ll_subterms15.visibility = View.VISIBLE
                    ll_subterms16.visibility = View.VISIBLE
                    ll_subterms17.visibility = View.GONE
                    ll_subterms18.visibility = View.GONE
                    ll_subterms19.visibility = View.GONE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.VISIBLE
                    ll_subterms14.visibility = View.VISIBLE
                    ll_subterms15.visibility = View.VISIBLE
                    ll_subterms16.visibility = View.VISIBLE
                    ll_subterms17.visibility = View.VISIBLE
                    ll_subterms18.visibility = View.GONE
                    ll_subterms19.visibility = View.GONE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.VISIBLE
                    ll_subterms14.visibility = View.VISIBLE
                    ll_subterms15.visibility = View.VISIBLE
                    ll_subterms16.visibility = View.VISIBLE
                    ll_subterms17.visibility = View.VISIBLE
                    ll_subterms18.visibility = View.VISIBLE
                    ll_subterms19.visibility = View.GONE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.VISIBLE
                    ll_subterms14.visibility = View.VISIBLE
                    ll_subterms15.visibility = View.VISIBLE
                    ll_subterms16.visibility = View.VISIBLE
                    ll_subterms17.visibility = View.VISIBLE
                    ll_subterms18.visibility = View.VISIBLE
                    ll_subterms19.visibility = View.VISIBLE
                    ll_subterms110.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms11.visibility = View.VISIBLE
                    ll_subterms12.visibility = View.VISIBLE
                    ll_subterms13.visibility = View.VISIBLE
                    ll_subterms14.visibility = View.VISIBLE
                    ll_subterms15.visibility = View.VISIBLE
                    ll_subterms16.visibility = View.VISIBLE
                    ll_subterms17.visibility = View.VISIBLE
                    ll_subterms18.visibility = View.VISIBLE
                    ll_subterms19.visibility = View.VISIBLE
                    ll_subterms110.visibility = View.VISIBLE
                }
                termsquantity1 = ""
                termsdate1 = ""
                dialog_list.dismiss()

            }
        }
        admin_job_terms2.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms2.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.GONE
                    ll_subterms23.visibility = View.GONE
                    ll_subterms24.visibility = View.GONE
                    ll_subterms25.visibility = View.GONE
                    ll_subterms26.visibility = View.GONE
                    ll_subterms27.visibility = View.GONE
                    ll_subterms28.visibility = View.GONE
                    ll_subterms29.visibility = View.GONE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.GONE
                    ll_subterms24.visibility = View.GONE
                    ll_subterms25.visibility = View.GONE
                    ll_subterms26.visibility = View.GONE
                    ll_subterms27.visibility = View.GONE
                    ll_subterms28.visibility = View.GONE
                    ll_subterms29.visibility = View.GONE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.VISIBLE
                    ll_subterms24.visibility = View.GONE
                    ll_subterms25.visibility = View.GONE
                    ll_subterms26.visibility = View.GONE
                    ll_subterms27.visibility = View.GONE
                    ll_subterms28.visibility = View.GONE
                    ll_subterms29.visibility = View.GONE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.VISIBLE
                    ll_subterms24.visibility = View.VISIBLE
                    ll_subterms25.visibility = View.GONE
                    ll_subterms26.visibility = View.GONE
                    ll_subterms27.visibility = View.GONE
                    ll_subterms28.visibility = View.GONE
                    ll_subterms29.visibility = View.GONE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.VISIBLE
                    ll_subterms24.visibility = View.VISIBLE
                    ll_subterms25.visibility = View.VISIBLE
                    ll_subterms26.visibility = View.GONE
                    ll_subterms27.visibility = View.GONE
                    ll_subterms28.visibility = View.GONE
                    ll_subterms29.visibility = View.GONE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.VISIBLE
                    ll_subterms24.visibility = View.VISIBLE
                    ll_subterms25.visibility = View.VISIBLE
                    ll_subterms26.visibility = View.VISIBLE
                    ll_subterms27.visibility = View.GONE
                    ll_subterms28.visibility = View.GONE
                    ll_subterms29.visibility = View.GONE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.VISIBLE
                    ll_subterms24.visibility = View.VISIBLE
                    ll_subterms25.visibility = View.VISIBLE
                    ll_subterms26.visibility = View.VISIBLE
                    ll_subterms27.visibility = View.VISIBLE
                    ll_subterms28.visibility = View.GONE
                    ll_subterms29.visibility = View.GONE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.VISIBLE
                    ll_subterms24.visibility = View.VISIBLE
                    ll_subterms25.visibility = View.VISIBLE
                    ll_subterms26.visibility = View.VISIBLE
                    ll_subterms27.visibility = View.VISIBLE
                    ll_subterms28.visibility = View.VISIBLE
                    ll_subterms29.visibility = View.GONE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.VISIBLE
                    ll_subterms24.visibility = View.VISIBLE
                    ll_subterms25.visibility = View.VISIBLE
                    ll_subterms26.visibility = View.VISIBLE
                    ll_subterms27.visibility = View.VISIBLE
                    ll_subterms28.visibility = View.VISIBLE
                    ll_subterms29.visibility = View.VISIBLE
                    ll_subterms210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms21.visibility = View.VISIBLE
                    ll_subterms22.visibility = View.VISIBLE
                    ll_subterms23.visibility = View.VISIBLE
                    ll_subterms24.visibility = View.VISIBLE
                    ll_subterms25.visibility = View.VISIBLE
                    ll_subterms26.visibility = View.VISIBLE
                    ll_subterms27.visibility = View.VISIBLE
                    ll_subterms28.visibility = View.VISIBLE
                    ll_subterms29.visibility = View.VISIBLE
                    ll_subterms210.visibility = View.VISIBLE
                }
                termsquantity2 = ""
                termsdate2 = ""
                dialog_list.dismiss()

            }
        }
        admin_job_terms3.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms3.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.GONE
                    ll_subterms33.visibility = View.GONE
                    ll_subterms34.visibility = View.GONE
                    ll_subterms35.visibility = View.GONE
                    ll_subterms36.visibility = View.GONE
                    ll_subterms37.visibility = View.GONE
                    ll_subterms38.visibility = View.GONE
                    ll_subterms39.visibility = View.GONE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.GONE
                    ll_subterms34.visibility = View.GONE
                    ll_subterms35.visibility = View.GONE
                    ll_subterms36.visibility = View.GONE
                    ll_subterms37.visibility = View.GONE
                    ll_subterms38.visibility = View.GONE
                    ll_subterms39.visibility = View.GONE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.VISIBLE
                    ll_subterms34.visibility = View.GONE
                    ll_subterms35.visibility = View.GONE
                    ll_subterms36.visibility = View.GONE
                    ll_subterms37.visibility = View.GONE
                    ll_subterms38.visibility = View.GONE
                    ll_subterms39.visibility = View.GONE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.VISIBLE
                    ll_subterms34.visibility = View.VISIBLE
                    ll_subterms35.visibility = View.GONE
                    ll_subterms36.visibility = View.GONE
                    ll_subterms37.visibility = View.GONE
                    ll_subterms38.visibility = View.GONE
                    ll_subterms39.visibility = View.GONE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.VISIBLE
                    ll_subterms34.visibility = View.VISIBLE
                    ll_subterms35.visibility = View.VISIBLE
                    ll_subterms36.visibility = View.GONE
                    ll_subterms37.visibility = View.GONE
                    ll_subterms38.visibility = View.GONE
                    ll_subterms39.visibility = View.GONE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.VISIBLE
                    ll_subterms34.visibility = View.VISIBLE
                    ll_subterms35.visibility = View.VISIBLE
                    ll_subterms36.visibility = View.VISIBLE
                    ll_subterms37.visibility = View.GONE
                    ll_subterms38.visibility = View.GONE
                    ll_subterms39.visibility = View.GONE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.VISIBLE
                    ll_subterms34.visibility = View.VISIBLE
                    ll_subterms35.visibility = View.VISIBLE
                    ll_subterms36.visibility = View.VISIBLE
                    ll_subterms37.visibility = View.VISIBLE
                    ll_subterms38.visibility = View.GONE
                    ll_subterms39.visibility = View.GONE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.VISIBLE
                    ll_subterms34.visibility = View.VISIBLE
                    ll_subterms35.visibility = View.VISIBLE
                    ll_subterms36.visibility = View.VISIBLE
                    ll_subterms37.visibility = View.VISIBLE
                    ll_subterms38.visibility = View.VISIBLE
                    ll_subterms39.visibility = View.GONE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.VISIBLE
                    ll_subterms34.visibility = View.VISIBLE
                    ll_subterms35.visibility = View.VISIBLE
                    ll_subterms36.visibility = View.VISIBLE
                    ll_subterms37.visibility = View.VISIBLE
                    ll_subterms38.visibility = View.VISIBLE
                    ll_subterms39.visibility = View.VISIBLE
                    ll_subterms310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms31.visibility = View.VISIBLE
                    ll_subterms32.visibility = View.VISIBLE
                    ll_subterms33.visibility = View.VISIBLE
                    ll_subterms34.visibility = View.VISIBLE
                    ll_subterms35.visibility = View.VISIBLE
                    ll_subterms36.visibility = View.VISIBLE
                    ll_subterms37.visibility = View.VISIBLE
                    ll_subterms38.visibility = View.VISIBLE
                    ll_subterms39.visibility = View.VISIBLE
                    ll_subterms310.visibility = View.VISIBLE
                }
                termsquantity3 = ""
                termsdate3 = ""
                dialog_list.dismiss()

            }
        }
        admin_job_terms5.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms5.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.GONE
                    ll_subterms53.visibility = View.GONE
                    ll_subterms54.visibility = View.GONE
                    ll_subterms55.visibility = View.GONE
                    ll_subterms56.visibility = View.GONE
                    ll_subterms57.visibility = View.GONE
                    ll_subterms58.visibility = View.GONE
                    ll_subterms59.visibility = View.GONE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.GONE
                    ll_subterms54.visibility = View.GONE
                    ll_subterms55.visibility = View.GONE
                    ll_subterms56.visibility = View.GONE
                    ll_subterms57.visibility = View.GONE
                    ll_subterms58.visibility = View.GONE
                    ll_subterms59.visibility = View.GONE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.VISIBLE
                    ll_subterms54.visibility = View.GONE
                    ll_subterms55.visibility = View.GONE
                    ll_subterms56.visibility = View.GONE
                    ll_subterms57.visibility = View.GONE
                    ll_subterms58.visibility = View.GONE
                    ll_subterms59.visibility = View.GONE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.VISIBLE
                    ll_subterms54.visibility = View.VISIBLE
                    ll_subterms55.visibility = View.GONE
                    ll_subterms56.visibility = View.GONE
                    ll_subterms57.visibility = View.GONE
                    ll_subterms58.visibility = View.GONE
                    ll_subterms59.visibility = View.GONE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.VISIBLE
                    ll_subterms54.visibility = View.VISIBLE
                    ll_subterms55.visibility = View.VISIBLE
                    ll_subterms56.visibility = View.GONE
                    ll_subterms57.visibility = View.GONE
                    ll_subterms58.visibility = View.GONE
                    ll_subterms59.visibility = View.GONE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.VISIBLE
                    ll_subterms54.visibility = View.VISIBLE
                    ll_subterms55.visibility = View.VISIBLE
                    ll_subterms56.visibility = View.VISIBLE
                    ll_subterms57.visibility = View.GONE
                    ll_subterms58.visibility = View.GONE
                    ll_subterms59.visibility = View.GONE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.VISIBLE
                    ll_subterms54.visibility = View.VISIBLE
                    ll_subterms55.visibility = View.VISIBLE
                    ll_subterms56.visibility = View.VISIBLE
                    ll_subterms57.visibility = View.VISIBLE
                    ll_subterms58.visibility = View.GONE
                    ll_subterms59.visibility = View.GONE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.VISIBLE
                    ll_subterms54.visibility = View.VISIBLE
                    ll_subterms55.visibility = View.VISIBLE
                    ll_subterms56.visibility = View.VISIBLE
                    ll_subterms57.visibility = View.VISIBLE
                    ll_subterms58.visibility = View.VISIBLE
                    ll_subterms59.visibility = View.GONE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.VISIBLE
                    ll_subterms54.visibility = View.VISIBLE
                    ll_subterms55.visibility = View.VISIBLE
                    ll_subterms56.visibility = View.VISIBLE
                    ll_subterms57.visibility = View.VISIBLE
                    ll_subterms58.visibility = View.VISIBLE
                    ll_subterms59.visibility = View.VISIBLE
                    ll_subterms510.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms51.visibility = View.VISIBLE
                    ll_subterms52.visibility = View.VISIBLE
                    ll_subterms53.visibility = View.VISIBLE
                    ll_subterms54.visibility = View.VISIBLE
                    ll_subterms55.visibility = View.VISIBLE
                    ll_subterms56.visibility = View.VISIBLE
                    ll_subterms57.visibility = View.VISIBLE
                    ll_subterms58.visibility = View.VISIBLE
                    ll_subterms59.visibility = View.VISIBLE
                    ll_subterms510.visibility = View.VISIBLE
                }
                termsquantity5 = ""
                termsdate5 = ""
                dialog_list.dismiss()

            }
        }

        admin_job_terms6.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms6.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.GONE
                    ll_subterms63.visibility = View.GONE
                    ll_subterms64.visibility = View.GONE
                    ll_subterms65.visibility = View.GONE
                    ll_subterms66.visibility = View.GONE
                    ll_subterms67.visibility = View.GONE
                    ll_subterms68.visibility = View.GONE
                    ll_subterms69.visibility = View.GONE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.GONE
                    ll_subterms64.visibility = View.GONE
                    ll_subterms65.visibility = View.GONE
                    ll_subterms66.visibility = View.GONE
                    ll_subterms67.visibility = View.GONE
                    ll_subterms68.visibility = View.GONE
                    ll_subterms69.visibility = View.GONE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.VISIBLE
                    ll_subterms64.visibility = View.GONE
                    ll_subterms65.visibility = View.GONE
                    ll_subterms66.visibility = View.GONE
                    ll_subterms67.visibility = View.GONE
                    ll_subterms68.visibility = View.GONE
                    ll_subterms69.visibility = View.GONE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.VISIBLE
                    ll_subterms64.visibility = View.VISIBLE
                    ll_subterms65.visibility = View.GONE
                    ll_subterms66.visibility = View.GONE
                    ll_subterms67.visibility = View.GONE
                    ll_subterms68.visibility = View.GONE
                    ll_subterms69.visibility = View.GONE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.VISIBLE
                    ll_subterms64.visibility = View.VISIBLE
                    ll_subterms65.visibility = View.VISIBLE
                    ll_subterms66.visibility = View.GONE
                    ll_subterms67.visibility = View.GONE
                    ll_subterms68.visibility = View.GONE
                    ll_subterms69.visibility = View.GONE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.VISIBLE
                    ll_subterms64.visibility = View.VISIBLE
                    ll_subterms65.visibility = View.VISIBLE
                    ll_subterms66.visibility = View.VISIBLE
                    ll_subterms67.visibility = View.GONE
                    ll_subterms68.visibility = View.GONE
                    ll_subterms69.visibility = View.GONE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.VISIBLE
                    ll_subterms64.visibility = View.VISIBLE
                    ll_subterms65.visibility = View.VISIBLE
                    ll_subterms66.visibility = View.VISIBLE
                    ll_subterms67.visibility = View.VISIBLE
                    ll_subterms68.visibility = View.GONE
                    ll_subterms69.visibility = View.GONE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.VISIBLE
                    ll_subterms64.visibility = View.VISIBLE
                    ll_subterms65.visibility = View.VISIBLE
                    ll_subterms66.visibility = View.VISIBLE
                    ll_subterms67.visibility = View.VISIBLE
                    ll_subterms68.visibility = View.VISIBLE
                    ll_subterms69.visibility = View.GONE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.VISIBLE
                    ll_subterms64.visibility = View.VISIBLE
                    ll_subterms65.visibility = View.VISIBLE
                    ll_subterms66.visibility = View.VISIBLE
                    ll_subterms67.visibility = View.VISIBLE
                    ll_subterms68.visibility = View.VISIBLE
                    ll_subterms69.visibility = View.VISIBLE
                    ll_subterms610.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms61.visibility = View.VISIBLE
                    ll_subterms62.visibility = View.VISIBLE
                    ll_subterms63.visibility = View.VISIBLE
                    ll_subterms64.visibility = View.VISIBLE
                    ll_subterms65.visibility = View.VISIBLE
                    ll_subterms66.visibility = View.VISIBLE
                    ll_subterms67.visibility = View.VISIBLE
                    ll_subterms68.visibility = View.VISIBLE
                    ll_subterms69.visibility = View.VISIBLE
                    ll_subterms610.visibility = View.VISIBLE
                }
                termsquantity6 = ""
                termsdate6 = ""
                dialog_list.dismiss()
            }
        }

        admin_job_terms7.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms7.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.GONE
                    ll_subterms73.visibility = View.GONE
                    ll_subterms74.visibility = View.GONE
                    ll_subterms75.visibility = View.GONE
                    ll_subterms76.visibility = View.GONE
                    ll_subterms77.visibility = View.GONE
                    ll_subterms78.visibility = View.GONE
                    ll_subterms79.visibility = View.GONE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.GONE
                    ll_subterms74.visibility = View.GONE
                    ll_subterms75.visibility = View.GONE
                    ll_subterms76.visibility = View.GONE
                    ll_subterms77.visibility = View.GONE
                    ll_subterms78.visibility = View.GONE
                    ll_subterms79.visibility = View.GONE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.VISIBLE
                    ll_subterms74.visibility = View.GONE
                    ll_subterms75.visibility = View.GONE
                    ll_subterms76.visibility = View.GONE
                    ll_subterms77.visibility = View.GONE
                    ll_subterms78.visibility = View.GONE
                    ll_subterms79.visibility = View.GONE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.VISIBLE
                    ll_subterms74.visibility = View.VISIBLE
                    ll_subterms75.visibility = View.GONE
                    ll_subterms76.visibility = View.GONE
                    ll_subterms77.visibility = View.GONE
                    ll_subterms78.visibility = View.GONE
                    ll_subterms79.visibility = View.GONE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.VISIBLE
                    ll_subterms74.visibility = View.VISIBLE
                    ll_subterms75.visibility = View.VISIBLE
                    ll_subterms76.visibility = View.GONE
                    ll_subterms77.visibility = View.GONE
                    ll_subterms78.visibility = View.GONE
                    ll_subterms79.visibility = View.GONE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.VISIBLE
                    ll_subterms74.visibility = View.VISIBLE
                    ll_subterms75.visibility = View.VISIBLE
                    ll_subterms76.visibility = View.VISIBLE
                    ll_subterms77.visibility = View.GONE
                    ll_subterms78.visibility = View.GONE
                    ll_subterms79.visibility = View.GONE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.VISIBLE
                    ll_subterms74.visibility = View.VISIBLE
                    ll_subterms75.visibility = View.VISIBLE
                    ll_subterms76.visibility = View.VISIBLE
                    ll_subterms77.visibility = View.VISIBLE
                    ll_subterms78.visibility = View.GONE
                    ll_subterms79.visibility = View.GONE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.VISIBLE
                    ll_subterms74.visibility = View.VISIBLE
                    ll_subterms75.visibility = View.VISIBLE
                    ll_subterms76.visibility = View.VISIBLE
                    ll_subterms77.visibility = View.VISIBLE
                    ll_subterms78.visibility = View.VISIBLE
                    ll_subterms79.visibility = View.GONE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.VISIBLE
                    ll_subterms74.visibility = View.VISIBLE
                    ll_subterms75.visibility = View.VISIBLE
                    ll_subterms76.visibility = View.VISIBLE
                    ll_subterms77.visibility = View.VISIBLE
                    ll_subterms78.visibility = View.VISIBLE
                    ll_subterms79.visibility = View.VISIBLE
                    ll_subterms710.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms71.visibility = View.VISIBLE
                    ll_subterms72.visibility = View.VISIBLE
                    ll_subterms73.visibility = View.VISIBLE
                    ll_subterms74.visibility = View.VISIBLE
                    ll_subterms75.visibility = View.VISIBLE
                    ll_subterms76.visibility = View.VISIBLE
                    ll_subterms77.visibility = View.VISIBLE
                    ll_subterms78.visibility = View.VISIBLE
                    ll_subterms79.visibility = View.VISIBLE
                    ll_subterms710.visibility = View.VISIBLE
                }
                termsquantity7 = ""
                termsdate7 = ""
                dialog_list.dismiss()

            }
        }

        admin_job_terms8.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms8.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.GONE
                    ll_subterms83.visibility = View.GONE
                    ll_subterms84.visibility = View.GONE
                    ll_subterms85.visibility = View.GONE
                    ll_subterms86.visibility = View.GONE
                    ll_subterms87.visibility = View.GONE
                    ll_subterms88.visibility = View.GONE
                    ll_subterms89.visibility = View.GONE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.GONE
                    ll_subterms84.visibility = View.GONE
                    ll_subterms85.visibility = View.GONE
                    ll_subterms86.visibility = View.GONE
                    ll_subterms87.visibility = View.GONE
                    ll_subterms88.visibility = View.GONE
                    ll_subterms89.visibility = View.GONE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.VISIBLE
                    ll_subterms84.visibility = View.GONE
                    ll_subterms85.visibility = View.GONE
                    ll_subterms86.visibility = View.GONE
                    ll_subterms87.visibility = View.GONE
                    ll_subterms88.visibility = View.GONE
                    ll_subterms89.visibility = View.GONE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.VISIBLE
                    ll_subterms84.visibility = View.VISIBLE
                    ll_subterms85.visibility = View.GONE
                    ll_subterms86.visibility = View.GONE
                    ll_subterms87.visibility = View.GONE
                    ll_subterms88.visibility = View.GONE
                    ll_subterms89.visibility = View.GONE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.VISIBLE
                    ll_subterms84.visibility = View.VISIBLE
                    ll_subterms85.visibility = View.VISIBLE
                    ll_subterms86.visibility = View.GONE
                    ll_subterms87.visibility = View.GONE
                    ll_subterms88.visibility = View.GONE
                    ll_subterms89.visibility = View.GONE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.VISIBLE
                    ll_subterms84.visibility = View.VISIBLE
                    ll_subterms85.visibility = View.VISIBLE
                    ll_subterms86.visibility = View.VISIBLE
                    ll_subterms87.visibility = View.GONE
                    ll_subterms88.visibility = View.GONE
                    ll_subterms89.visibility = View.GONE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.VISIBLE
                    ll_subterms84.visibility = View.VISIBLE
                    ll_subterms85.visibility = View.VISIBLE
                    ll_subterms86.visibility = View.VISIBLE
                    ll_subterms87.visibility = View.VISIBLE
                    ll_subterms88.visibility = View.GONE
                    ll_subterms89.visibility = View.GONE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.VISIBLE
                    ll_subterms84.visibility = View.VISIBLE
                    ll_subterms85.visibility = View.VISIBLE
                    ll_subterms86.visibility = View.VISIBLE
                    ll_subterms87.visibility = View.VISIBLE
                    ll_subterms88.visibility = View.VISIBLE
                    ll_subterms89.visibility = View.GONE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.VISIBLE
                    ll_subterms84.visibility = View.VISIBLE
                    ll_subterms85.visibility = View.VISIBLE
                    ll_subterms86.visibility = View.VISIBLE
                    ll_subterms87.visibility = View.VISIBLE
                    ll_subterms88.visibility = View.VISIBLE
                    ll_subterms89.visibility = View.VISIBLE
                    ll_subterms810.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms81.visibility = View.VISIBLE
                    ll_subterms82.visibility = View.VISIBLE
                    ll_subterms83.visibility = View.VISIBLE
                    ll_subterms84.visibility = View.VISIBLE
                    ll_subterms85.visibility = View.VISIBLE
                    ll_subterms86.visibility = View.VISIBLE
                    ll_subterms87.visibility = View.VISIBLE
                    ll_subterms88.visibility = View.VISIBLE
                    ll_subterms89.visibility = View.VISIBLE
                    ll_subterms810.visibility = View.VISIBLE
                }
                termsquantity8 = ""
                termsdate8 = ""
                dialog_list.dismiss()

            }
        }
        admin_job_terms9.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms9.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.GONE
                    ll_subterms93.visibility = View.GONE
                    ll_subterms94.visibility = View.GONE
                    ll_subterms95.visibility = View.GONE
                    ll_subterms96.visibility = View.GONE
                    ll_subterms97.visibility = View.GONE
                    ll_subterms98.visibility = View.GONE
                    ll_subterms99.visibility = View.GONE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.GONE
                    ll_subterms94.visibility = View.GONE
                    ll_subterms95.visibility = View.GONE
                    ll_subterms96.visibility = View.GONE
                    ll_subterms97.visibility = View.GONE
                    ll_subterms98.visibility = View.GONE
                    ll_subterms99.visibility = View.GONE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.VISIBLE
                    ll_subterms94.visibility = View.GONE
                    ll_subterms95.visibility = View.GONE
                    ll_subterms96.visibility = View.GONE
                    ll_subterms97.visibility = View.GONE
                    ll_subterms98.visibility = View.GONE
                    ll_subterms99.visibility = View.GONE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.VISIBLE
                    ll_subterms94.visibility = View.VISIBLE
                    ll_subterms95.visibility = View.GONE
                    ll_subterms96.visibility = View.GONE
                    ll_subterms97.visibility = View.GONE
                    ll_subterms98.visibility = View.GONE
                    ll_subterms99.visibility = View.GONE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.VISIBLE
                    ll_subterms94.visibility = View.VISIBLE
                    ll_subterms95.visibility = View.VISIBLE
                    ll_subterms96.visibility = View.GONE
                    ll_subterms97.visibility = View.GONE
                    ll_subterms98.visibility = View.GONE
                    ll_subterms99.visibility = View.GONE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.VISIBLE
                    ll_subterms94.visibility = View.VISIBLE
                    ll_subterms95.visibility = View.VISIBLE
                    ll_subterms96.visibility = View.VISIBLE
                    ll_subterms97.visibility = View.GONE
                    ll_subterms98.visibility = View.GONE
                    ll_subterms99.visibility = View.GONE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.VISIBLE
                    ll_subterms94.visibility = View.VISIBLE
                    ll_subterms95.visibility = View.VISIBLE
                    ll_subterms96.visibility = View.VISIBLE
                    ll_subterms97.visibility = View.VISIBLE
                    ll_subterms98.visibility = View.GONE
                    ll_subterms99.visibility = View.GONE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.VISIBLE
                    ll_subterms94.visibility = View.VISIBLE
                    ll_subterms95.visibility = View.VISIBLE
                    ll_subterms96.visibility = View.VISIBLE
                    ll_subterms97.visibility = View.VISIBLE
                    ll_subterms98.visibility = View.VISIBLE
                    ll_subterms99.visibility = View.GONE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.VISIBLE
                    ll_subterms94.visibility = View.VISIBLE
                    ll_subterms95.visibility = View.VISIBLE
                    ll_subterms96.visibility = View.VISIBLE
                    ll_subterms97.visibility = View.VISIBLE
                    ll_subterms98.visibility = View.VISIBLE
                    ll_subterms99.visibility = View.VISIBLE
                    ll_subterms910.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms91.visibility = View.VISIBLE
                    ll_subterms92.visibility = View.VISIBLE
                    ll_subterms93.visibility = View.VISIBLE
                    ll_subterms94.visibility = View.VISIBLE
                    ll_subterms95.visibility = View.VISIBLE
                    ll_subterms96.visibility = View.VISIBLE
                    ll_subterms97.visibility = View.VISIBLE
                    ll_subterms98.visibility = View.VISIBLE
                    ll_subterms99.visibility = View.VISIBLE
                    ll_subterms910.visibility = View.VISIBLE
                }
                termsquantity9 = ""
                termsdate9 = ""
                dialog_list.dismiss()
            }
        }

        admin_job_terms10.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_job_terms10.setText(selected_name)
                if (selected_name == "1") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.GONE
                    ll_subterms103.visibility = View.GONE
                    ll_subterms104.visibility = View.GONE
                    ll_subterms105.visibility = View.GONE
                    ll_subterms106.visibility = View.GONE
                    ll_subterms107.visibility = View.GONE
                    ll_subterms108.visibility = View.GONE
                    ll_subterms109.visibility = View.GONE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.GONE
                    ll_subterms104.visibility = View.GONE
                    ll_subterms105.visibility = View.GONE
                    ll_subterms106.visibility = View.GONE
                    ll_subterms107.visibility = View.GONE
                    ll_subterms108.visibility = View.GONE
                    ll_subterms109.visibility = View.GONE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.VISIBLE
                    ll_subterms104.visibility = View.GONE
                    ll_subterms105.visibility = View.GONE
                    ll_subterms106.visibility = View.GONE
                    ll_subterms107.visibility = View.GONE
                    ll_subterms108.visibility = View.GONE
                    ll_subterms109.visibility = View.GONE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.VISIBLE
                    ll_subterms104.visibility = View.VISIBLE
                    ll_subterms105.visibility = View.GONE
                    ll_subterms106.visibility = View.GONE
                    ll_subterms107.visibility = View.GONE
                    ll_subterms108.visibility = View.GONE
                    ll_subterms109.visibility = View.GONE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.VISIBLE
                    ll_subterms104.visibility = View.VISIBLE
                    ll_subterms105.visibility = View.VISIBLE
                    ll_subterms106.visibility = View.GONE
                    ll_subterms107.visibility = View.GONE
                    ll_subterms108.visibility = View.GONE
                    ll_subterms109.visibility = View.GONE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.VISIBLE
                    ll_subterms104.visibility = View.VISIBLE
                    ll_subterms105.visibility = View.VISIBLE
                    ll_subterms106.visibility = View.VISIBLE
                    ll_subterms107.visibility = View.GONE
                    ll_subterms108.visibility = View.GONE
                    ll_subterms109.visibility = View.GONE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.VISIBLE
                    ll_subterms104.visibility = View.VISIBLE
                    ll_subterms105.visibility = View.VISIBLE
                    ll_subterms106.visibility = View.VISIBLE
                    ll_subterms107.visibility = View.VISIBLE
                    ll_subterms108.visibility = View.GONE
                    ll_subterms109.visibility = View.GONE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.VISIBLE
                    ll_subterms104.visibility = View.VISIBLE
                    ll_subterms105.visibility = View.VISIBLE
                    ll_subterms106.visibility = View.VISIBLE
                    ll_subterms107.visibility = View.VISIBLE
                    ll_subterms108.visibility = View.VISIBLE
                    ll_subterms109.visibility = View.GONE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.VISIBLE
                    ll_subterms104.visibility = View.VISIBLE
                    ll_subterms105.visibility = View.VISIBLE
                    ll_subterms106.visibility = View.VISIBLE
                    ll_subterms107.visibility = View.VISIBLE
                    ll_subterms108.visibility = View.VISIBLE
                    ll_subterms109.visibility = View.VISIBLE
                    ll_subterms1010.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_subterms101.visibility = View.VISIBLE
                    ll_subterms102.visibility = View.VISIBLE
                    ll_subterms103.visibility = View.VISIBLE
                    ll_subterms104.visibility = View.VISIBLE
                    ll_subterms105.visibility = View.VISIBLE
                    ll_subterms106.visibility = View.VISIBLE
                    ll_subterms107.visibility = View.VISIBLE
                    ll_subterms108.visibility = View.VISIBLE
                    ll_subterms109.visibility = View.VISIBLE
                    ll_subterms1010.visibility = View.VISIBLE
                }
                termsquantity10 = ""
                termsdate10 = ""
                dialog_list.dismiss()

            }
        }
        admin_departments.setListener(this)
        admin_departments2.setListener(this)
        admin_departments3.setListener(this)
        admin_departments4.setListener(this)
        admin_departments5.setListener(this)
        admin_departments6.setListener(this)
        admin_departments7.setListener(this)
        admin_departments8.setListener(this)
        admin_departments9.setListener(this)
        admin_departments10.setListener(this)
        CallDepartmentAPI()
        btn_submit_id.setOnClickListener {
            admin_customer_stg = customer_search_id
            admin_job_type_stg = admin_job_type_id.text.toString()
            admin_job_stg = admin_job_id.text.toString()
            admin_job_part_stg = admin_job_part_id.text.toString()
            admin_Pactivity_stg = admin_Pactivity_id.text.toString()
            admin_Estart_date_stg = admin_Estart_date_id.text.toString()
            admin_description_stg = admin_description_id.text.toString()
            admin_Pdata_stg = admin_Pdata_id.text.toString()
            admin_Estart_time_stg = admin_Estart_time_id.text.toString()
            admin_schedule_stg = admin_schedule_id.text.toString()
            admin_Cstart_date_stg = admin_Cstart_date_id.text.toString()
            admin_due_time_stg = admin_due_time_id.text.toString()
            admin_Lact_code_stg = admin_Lact_code_id.text.toString()
            admin_tasks_stg = admin_tasks.text.toString()
            Log.d("DATE_STRING", "" + admin_Estart_date_stg + "-----" + admin_Pdata_stg + "--------" + admin_customer_stg)
            createJSONArray()
        }
        admin_Estart_date_id.setOnClickListener {
            showDateTimePicker(admin_Estart_date_id.id)
        }
        admin_Pdata_id.setOnClickListener {
            showDateTimePicker(admin_Pdata_id.id)
        }
        admin_Cstart_date_id.setOnClickListener {
            showDateTimePicker(admin_Estart_date_id.id)
        }
        admin_Estart_time_id.setOnClickListener {
            val mcurrentTime = Calendar.getInstance()
            val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
            val minute = mcurrentTime.get(Calendar.MINUTE)
            val mTimePicker = TimePickerDialog(this@AddJobActivity, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                admin_Estart_time_id.text = selectedHour.toString() + ":" + selectedMinute
            }, hour, minute, true)//Yes 24 hour time
            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }
        admin_job_type_id.isFocusable = false
        admin_job_type_id.isFocusableInTouchMode = false
        admin_job_type_id.setOnClickListener {
            if (!dialog_list_type.isShowing)
                dialog_list_type.show()
        }
        admin_due_time_id.setOnClickListener {
            val mcurrentTime = Calendar.getInstance()
            val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
            val minute = mcurrentTime.get(Calendar.MINUTE)

            val mTimePicker = TimePickerDialog(this@AddJobActivity, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                admin_due_time_id.text = selectedHour.toString() + ":" + selectedMinute
            }, hour, minute, true)//Yes 24 hour time
            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }
        mCustomerNamesArray = ArrayList()
        mCustomerIdsArray = ArrayList()
        CallgetCustomersAPI()
        customer_search_adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, mCustomerNamesArray)
        admin_customer_id.setOnItemClickListener { parent, view, position, id ->
            aPosition = mCustomerNamesArray.indexOf(customer_search_adapter.getItem(position).toString())
            customer_search_id = mCustomerIdsArray.get(aPosition)
        }
    }

    private fun JobOneDropswidgets() {
        job_title_one_id = findViewById(R.id.job_title_one_id)
        job_title_two_id = findViewById(R.id.job_title_two_id)
        job_title_three_id = findViewById(R.id.job_title_three_id)
        job_title_four_id = findViewById(R.id.job_title_four_id)
        job_desc_one_id = findViewById(R.id.job_desc_one_id)
        job_desc_two_id = findViewById(R.id.job_desc_two_id)
        job_desc_three_id = findViewById(R.id.job_desc_three_id)
        job_desc_four_id = findViewById(R.id.job_desc_four_id)
        job_desc_five_id = findViewById(R.id.job_desc_five_id)
        job_desc_six_id = findViewById(R.id.job_desc_six_id)
        job_desc_seven_id = findViewById(R.id.job_desc_seven_id)
        job_desc_eight_id = findViewById(R.id.job_desc_eight_id)
        job_desc_nine_id = findViewById(R.id.job_desc_nine_id)
        job_desc_ten_id = findViewById(R.id.job_desc_ten_id)

        ll_shipping_terms_1 = findViewById(R.id.ll_shipping_terms_1)
        admin_shipping_terms1 = findViewById(R.id.admin_shipping_terms1)
        ll_shipp_120 = findViewById(R.id.ll_shipp_120)
        ll_ship_subterms_121 = findViewById(R.id.ll_ship_subterms_121)
        ll_shipp_subterms122 = findViewById(R.id.ll_shipp_subterms122)
        ll_shipp_subterms123 = findViewById(R.id.ll_shipp_subterms123)
        ll_shipp_subterms124 = findViewById(R.id.ll_shipp_subterms124)
        ll_shipp_subterms125 = findViewById(R.id.ll_shipp_subterms125)
        ll_shipp_subterms126 = findViewById(R.id.ll_shipp_subterms126)
        ll_shipp_subterms127 = findViewById(R.id.ll_shipp_subterms127)
        ll_shipp_subterms128 = findViewById(R.id.ll_shipp_subterms128)
        ll_shipp_subterms129 = findViewById(R.id.ll_shipp_subterms129)
        ll_shipp_subterms1210 = findViewById(R.id.ll_shipp_subterms1210)
        admin_shipp_termssitem121 = findViewById(R.id.admin_shipp_termssitem121)
        admin_shipp_termsdate121 = findViewById(R.id.admin_shipp_termsdate121)
        admin_shipp_termssitem122 = findViewById(R.id.admin_shipp_termssitem122)
        admin_shipp_termsdate122 = findViewById(R.id.admin_shipp_termsdate122)
        admin_shipp_termssitem123 = findViewById(R.id.admin_shipp_termssitem123)
        admin_shipp_termsdate123 = findViewById(R.id.admin_shipp_termsdate123)
        admin_shipp_termssitem124 = findViewById(R.id.admin_shipp_termssitem124)
        admin_shipp_termsdate124 = findViewById(R.id.admin_shipp_termsdate124)
        admin_shipp_termssitem125 = findViewById(R.id.admin_shipp_termssitem125)
        admin_shipp_termsdate125 = findViewById(R.id.admin_shipp_termsdate125)
        admin_shipp_termssitem126 = findViewById(R.id.admin_shipp_termssitem126)
        admin_shipp_termsdate126 = findViewById(R.id.admin_shipp_termsdate126)
        admin_shipp_termssitem127 = findViewById(R.id.admin_shipp_termssitem127)
        admin_shipp_termsdate127 = findViewById(R.id.admin_shipp_termsdate127)
        admin_shipp_termssitem128 = findViewById(R.id.admin_shipp_termssitem128)
        admin_shipp_termsdate128 = findViewById(R.id.admin_shipp_termsdate128)
        admin_shipp_termssitem129 = findViewById(R.id.admin_shipp_termssitem129)
        admin_shipp_termsdate129 = findViewById(R.id.admin_shipp_termsdate129)
        admin_shipp_termssitem1210 = findViewById(R.id.admin_shipp_termssitem1210)
        admin_shipp_termsdate1210 = findViewById(R.id.admin_shipp_termsdate1210)

        admin_shipping_terms1.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms1.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.GONE
                    ll_shipp_subterms123.visibility = View.GONE
                    ll_shipp_subterms124.visibility = View.GONE
                    ll_shipp_subterms125.visibility = View.GONE
                    ll_shipp_subterms126.visibility = View.GONE
                    ll_shipp_subterms127.visibility = View.GONE
                    ll_shipp_subterms128.visibility = View.GONE
                    ll_shipp_subterms129.visibility = View.GONE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.GONE
                    ll_shipp_subterms124.visibility = View.GONE
                    ll_shipp_subterms125.visibility = View.GONE
                    ll_shipp_subterms126.visibility = View.GONE
                    ll_shipp_subterms127.visibility = View.GONE
                    ll_shipp_subterms128.visibility = View.GONE
                    ll_shipp_subterms129.visibility = View.GONE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.VISIBLE
                    ll_shipp_subterms124.visibility = View.GONE
                    ll_shipp_subterms125.visibility = View.GONE
                    ll_shipp_subterms126.visibility = View.GONE
                    ll_shipp_subterms127.visibility = View.GONE
                    ll_shipp_subterms128.visibility = View.GONE
                    ll_shipp_subterms129.visibility = View.GONE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.VISIBLE
                    ll_shipp_subterms124.visibility = View.VISIBLE
                    ll_shipp_subterms125.visibility = View.GONE
                    ll_shipp_subterms126.visibility = View.GONE
                    ll_shipp_subterms127.visibility = View.GONE
                    ll_shipp_subterms128.visibility = View.GONE
                    ll_shipp_subterms129.visibility = View.GONE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.VISIBLE
                    ll_shipp_subterms124.visibility = View.VISIBLE
                    ll_shipp_subterms125.visibility = View.VISIBLE
                    ll_shipp_subterms126.visibility = View.GONE
                    ll_shipp_subterms127.visibility = View.GONE
                    ll_shipp_subterms128.visibility = View.GONE
                    ll_shipp_subterms129.visibility = View.GONE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.VISIBLE
                    ll_shipp_subterms124.visibility = View.VISIBLE
                    ll_shipp_subterms125.visibility = View.VISIBLE
                    ll_shipp_subterms126.visibility = View.VISIBLE
                    ll_shipp_subterms127.visibility = View.GONE
                    ll_shipp_subterms128.visibility = View.GONE
                    ll_shipp_subterms129.visibility = View.GONE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.VISIBLE
                    ll_shipp_subterms124.visibility = View.VISIBLE
                    ll_shipp_subterms125.visibility = View.VISIBLE
                    ll_shipp_subterms126.visibility = View.VISIBLE
                    ll_shipp_subterms127.visibility = View.VISIBLE
                    ll_shipp_subterms128.visibility = View.GONE
                    ll_shipp_subterms129.visibility = View.GONE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.VISIBLE
                    ll_shipp_subterms124.visibility = View.VISIBLE
                    ll_shipp_subterms125.visibility = View.VISIBLE
                    ll_shipp_subterms126.visibility = View.VISIBLE
                    ll_shipp_subterms127.visibility = View.VISIBLE
                    ll_shipp_subterms128.visibility = View.VISIBLE
                    ll_shipp_subterms129.visibility = View.GONE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.VISIBLE
                    ll_shipp_subterms124.visibility = View.VISIBLE
                    ll_shipp_subterms125.visibility = View.VISIBLE
                    ll_shipp_subterms126.visibility = View.VISIBLE
                    ll_shipp_subterms127.visibility = View.VISIBLE
                    ll_shipp_subterms128.visibility = View.VISIBLE
                    ll_shipp_subterms129.visibility = View.VISIBLE
                    ll_shipp_subterms1210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_121.visibility = View.VISIBLE
                    ll_shipp_subterms122.visibility = View.VISIBLE
                    ll_shipp_subterms123.visibility = View.VISIBLE
                    ll_shipp_subterms124.visibility = View.VISIBLE
                    ll_shipp_subterms125.visibility = View.VISIBLE
                    ll_shipp_subterms126.visibility = View.VISIBLE
                    ll_shipp_subterms127.visibility = View.VISIBLE
                    ll_shipp_subterms128.visibility = View.VISIBLE
                    ll_shipp_subterms129.visibility = View.VISIBLE
                    ll_shipp_subterms1210.visibility = View.VISIBLE
                }
                shipping_termsdate1 = ""
                shipping_qty1 = ""
                dialog_list.dismiss()

            }
        }
        admin_delivery_terms1 = findViewById(R.id.admin_delivery_terms1)
        ll_delivery_terms_1 = findViewById(R.id.ll_delivery_terms_1)
        ll_delivery_130 = findViewById(R.id.ll_delivery_130)
        ll_delivery_subterms_131 = findViewById(R.id.ll_delivery_subterms_131)
        ll_delivery_subterms132 = findViewById(R.id.ll_delivery_subterms132)
        ll_delivery_subterms133 = findViewById(R.id.ll_delivery_subterms133)
        ll_delivery_subterms134 = findViewById(R.id.ll_delivery_subterms134)
        ll_delivery_subterms135 = findViewById(R.id.ll_delivery_subterms135)
        ll_delivery_subterms136 = findViewById(R.id.ll_delivery_subterms136)
        ll_delivery_subterms137 = findViewById(R.id.ll_delivery_subterms137)
        ll_delivery_subterms138 = findViewById(R.id.ll_delivery_subterms138)
        ll_delivery_subterms139 = findViewById(R.id.ll_delivery_subterms139)
        ll_delivery_subterms1310 = findViewById(R.id.ll_delivery_subterms1310)
        admin_delivery_termssitem131 = findViewById(R.id.admin_delivery_termssitem131)
        admin_delivery_termsdate131 = findViewById(R.id.admin_delivery_termsdate131)
        admin_delivery_termssitem132 = findViewById(R.id.admin_delivery_termssitem132)
        admin_delivery_termsdate132 = findViewById(R.id.admin_delivery_termsdate132)
        admin_delivery_termssitem133 = findViewById(R.id.admin_delivery_termssitem133)
        admin_delivery_termsdate133 = findViewById(R.id.admin_delivery_termsdate133)
        admin_delivery_termssitem134 = findViewById(R.id.admin_delivery_termssitem134)
        admin_delivery_termsdate134 = findViewById(R.id.admin_delivery_termsdate134)
        admin_delivery_termssitem135 = findViewById(R.id.admin_delivery_termssitem135)
        admin_delivery_termsdate135 = findViewById(R.id.admin_delivery_termsdate135)
        admin_delivery_termssitem136 = findViewById(R.id.admin_delivery_termssitem136)
        admin_delivery_termsdate136 = findViewById(R.id.admin_delivery_termsdate136)
        admin_delivery_termssitem137 = findViewById(R.id.admin_delivery_termssitem137)
        admin_delivery_termsdate137 = findViewById(R.id.admin_delivery_termsdate137)
        admin_delivery_termssitem138 = findViewById(R.id.admin_delivery_termssitem138)
        admin_delivery_termsdate138 = findViewById(R.id.admin_delivery_termsdate138)
        admin_delivery_termssitem139 = findViewById(R.id.admin_delivery_termssitem139)
        admin_delivery_termsdate139 = findViewById(R.id.admin_delivery_termsdate139)
        admin_delivery_termssitem1310 = findViewById(R.id.admin_delivery_termssitem1310)
        admin_delivery_termsdate1310 = findViewById(R.id.admin_delivery_termsdate1310)

        admin_delivery_terms1.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms1.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.GONE
                    ll_delivery_subterms133.visibility = View.GONE
                    ll_delivery_subterms134.visibility = View.GONE
                    ll_delivery_subterms135.visibility = View.GONE
                    ll_delivery_subterms136.visibility = View.GONE
                    ll_delivery_subterms137.visibility = View.GONE
                    ll_delivery_subterms138.visibility = View.GONE
                    ll_delivery_subterms139.visibility = View.GONE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.GONE
                    ll_delivery_subterms134.visibility = View.GONE
                    ll_delivery_subterms135.visibility = View.GONE
                    ll_delivery_subterms136.visibility = View.GONE
                    ll_delivery_subterms137.visibility = View.GONE
                    ll_delivery_subterms138.visibility = View.GONE
                    ll_delivery_subterms139.visibility = View.GONE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.VISIBLE
                    ll_delivery_subterms134.visibility = View.GONE
                    ll_delivery_subterms135.visibility = View.GONE
                    ll_delivery_subterms136.visibility = View.GONE
                    ll_delivery_subterms137.visibility = View.GONE
                    ll_delivery_subterms138.visibility = View.GONE
                    ll_delivery_subterms139.visibility = View.GONE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.VISIBLE
                    ll_delivery_subterms134.visibility = View.VISIBLE
                    ll_delivery_subterms135.visibility = View.GONE
                    ll_delivery_subterms136.visibility = View.GONE
                    ll_delivery_subterms137.visibility = View.GONE
                    ll_delivery_subterms138.visibility = View.GONE
                    ll_delivery_subterms139.visibility = View.GONE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.VISIBLE
                    ll_delivery_subterms134.visibility = View.VISIBLE
                    ll_delivery_subterms135.visibility = View.VISIBLE
                    ll_delivery_subterms136.visibility = View.GONE
                    ll_delivery_subterms137.visibility = View.GONE
                    ll_delivery_subterms138.visibility = View.GONE
                    ll_delivery_subterms139.visibility = View.GONE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.VISIBLE
                    ll_delivery_subterms134.visibility = View.VISIBLE
                    ll_delivery_subterms135.visibility = View.VISIBLE
                    ll_delivery_subterms136.visibility = View.VISIBLE
                    ll_delivery_subterms137.visibility = View.GONE
                    ll_delivery_subterms138.visibility = View.GONE
                    ll_delivery_subterms139.visibility = View.GONE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.VISIBLE
                    ll_delivery_subterms134.visibility = View.VISIBLE
                    ll_delivery_subterms135.visibility = View.VISIBLE
                    ll_delivery_subterms136.visibility = View.VISIBLE
                    ll_delivery_subterms137.visibility = View.VISIBLE
                    ll_delivery_subterms138.visibility = View.GONE
                    ll_delivery_subterms139.visibility = View.GONE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.VISIBLE
                    ll_delivery_subterms134.visibility = View.VISIBLE
                    ll_delivery_subterms135.visibility = View.VISIBLE
                    ll_delivery_subterms136.visibility = View.VISIBLE
                    ll_delivery_subterms137.visibility = View.VISIBLE
                    ll_delivery_subterms138.visibility = View.VISIBLE
                    ll_delivery_subterms139.visibility = View.GONE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.VISIBLE
                    ll_delivery_subterms134.visibility = View.VISIBLE
                    ll_delivery_subterms135.visibility = View.VISIBLE
                    ll_delivery_subterms136.visibility = View.VISIBLE
                    ll_delivery_subterms137.visibility = View.VISIBLE
                    ll_delivery_subterms138.visibility = View.VISIBLE
                    ll_delivery_subterms139.visibility = View.VISIBLE
                    ll_delivery_subterms1310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_131.visibility = View.VISIBLE
                    ll_delivery_subterms132.visibility = View.VISIBLE
                    ll_delivery_subterms133.visibility = View.VISIBLE
                    ll_delivery_subterms134.visibility = View.VISIBLE
                    ll_delivery_subterms135.visibility = View.VISIBLE
                    ll_delivery_subterms136.visibility = View.VISIBLE
                    ll_delivery_subterms137.visibility = View.VISIBLE
                    ll_delivery_subterms138.visibility = View.VISIBLE
                    ll_delivery_subterms139.visibility = View.VISIBLE
                    ll_delivery_subterms1310.visibility = View.VISIBLE
                }
                delivery_termsdate1 = ""
                delivery_qty1 = ""
                dialog_list.dismiss()

            }
        }
        admin_shipp_termsdate121.setOnClickListener(this)
        admin_shipp_termsdate122.setOnClickListener(this)
        admin_shipp_termsdate123.setOnClickListener(this)
        admin_shipp_termsdate124.setOnClickListener(this)
        admin_shipp_termsdate125.setOnClickListener(this)
        admin_shipp_termsdate126.setOnClickListener(this)
        admin_shipp_termsdate127.setOnClickListener(this)
        admin_shipp_termsdate128.setOnClickListener(this)
        admin_shipp_termsdate129.setOnClickListener(this)
        admin_shipp_termsdate1210.setOnClickListener(this)
        admin_delivery_termsdate131.setOnClickListener(this)
        admin_delivery_termsdate132.setOnClickListener(this)
        admin_delivery_termsdate133.setOnClickListener(this)
        admin_delivery_termsdate134.setOnClickListener(this)
        admin_delivery_termsdate135.setOnClickListener(this)
        admin_delivery_termsdate136.setOnClickListener(this)
        admin_delivery_termsdate137.setOnClickListener(this)
        admin_delivery_termsdate138.setOnClickListener(this)
        admin_delivery_termsdate139.setOnClickListener(this)
        admin_delivery_termsdate1310.setOnClickListener(this)
    }
    private fun JobTwoDropswidgets() {
        admin_shipping_terms2 = findViewById(R.id.admin_shipping_terms2)
        ll_shipping_two = findViewById(R.id.ll_shipping_two)
        ll_shipp_220 = findViewById(R.id.ll_shipp_220)
        ll_ship_subterms_221 = findViewById(R.id.ll_ship_subterms_221)
        ll_shipp_subterms222 = findViewById(R.id.ll_shipp_subterms222)
        ll_shipp_subterms223 = findViewById(R.id.ll_shipp_subterms223)
        ll_shipp_subterms224 = findViewById(R.id.ll_shipp_subterms224)
        ll_shipp_subterms225 = findViewById(R.id.ll_shipp_subterms225)
        ll_shipp_subterms226 = findViewById(R.id.ll_shipp_subterms226)
        ll_shipp_subterms227 = findViewById(R.id.ll_shipp_subterms227)
        ll_shipp_subterms228 = findViewById(R.id.ll_shipp_subterms228)
        ll_shipp_subterms229 = findViewById(R.id.ll_shipp_subterms229)
        ll_shipp_subterms2210 = findViewById(R.id.ll_shipp_subterms2210)
        admin_shipp_termssitem221 = findViewById(R.id.admin_shipp_termssitem221)
        admin_shipp_termsdate221 = findViewById(R.id.admin_shipp_termsdate221)
        admin_shipp_termssitem222 = findViewById(R.id.admin_shipp_termssitem222)
        admin_shipp_termsdate222 = findViewById(R.id.admin_shipp_termsdate222)
        admin_shipp_termssitem223 = findViewById(R.id.admin_shipp_termssitem223)
        admin_shipp_termsdate223 = findViewById(R.id.admin_shipp_termsdate223)
        admin_shipp_termssitem224 = findViewById(R.id.admin_shipp_termssitem224)
        admin_shipp_termsdate224 = findViewById(R.id.admin_shipp_termsdate224)
        admin_shipp_termssitem225 = findViewById(R.id.admin_shipp_termssitem225)
        admin_shipp_termsdate225 = findViewById(R.id.admin_shipp_termsdate225)
        admin_shipp_termssitem226 = findViewById(R.id.admin_shipp_termssitem226)
        admin_shipp_termsdate226 = findViewById(R.id.admin_shipp_termsdate226)
        admin_shipp_termssitem227 = findViewById(R.id.admin_shipp_termssitem227)
        admin_shipp_termsdate227 = findViewById(R.id.admin_shipp_termsdate227)
        admin_shipp_termssitem228 = findViewById(R.id.admin_shipp_termssitem228)
        admin_shipp_termsdate228 = findViewById(R.id.admin_shipp_termsdate228)
        admin_shipp_termssitem229 = findViewById(R.id.admin_shipp_termssitem229)
        admin_shipp_termsdate229 = findViewById(R.id.admin_shipp_termsdate229)
        admin_shipp_termssitem2210 = findViewById(R.id.admin_shipp_termssitem2210)
        admin_shipp_termsdate2210 = findViewById(R.id.admin_shipp_termsdate2210)

        admin_shipping_terms2.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms2.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.GONE
                    ll_shipp_subterms223.visibility = View.GONE
                    ll_shipp_subterms224.visibility = View.GONE
                    ll_shipp_subterms225.visibility = View.GONE
                    ll_shipp_subterms226.visibility = View.GONE
                    ll_shipp_subterms227.visibility = View.GONE
                    ll_shipp_subterms228.visibility = View.GONE
                    ll_shipp_subterms229.visibility = View.GONE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.GONE
                    ll_shipp_subterms224.visibility = View.GONE
                    ll_shipp_subterms225.visibility = View.GONE
                    ll_shipp_subterms226.visibility = View.GONE
                    ll_shipp_subterms227.visibility = View.GONE
                    ll_shipp_subterms228.visibility = View.GONE
                    ll_shipp_subterms229.visibility = View.GONE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.VISIBLE
                    ll_shipp_subterms224.visibility = View.GONE
                    ll_shipp_subterms225.visibility = View.GONE
                    ll_shipp_subterms226.visibility = View.GONE
                    ll_shipp_subterms227.visibility = View.GONE
                    ll_shipp_subterms228.visibility = View.GONE
                    ll_shipp_subterms229.visibility = View.GONE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.VISIBLE
                    ll_shipp_subterms224.visibility = View.VISIBLE
                    ll_shipp_subterms225.visibility = View.GONE
                    ll_shipp_subterms226.visibility = View.GONE
                    ll_shipp_subterms227.visibility = View.GONE
                    ll_shipp_subterms228.visibility = View.GONE
                    ll_shipp_subterms229.visibility = View.GONE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.VISIBLE
                    ll_shipp_subterms224.visibility = View.VISIBLE
                    ll_shipp_subterms225.visibility = View.VISIBLE
                    ll_shipp_subterms226.visibility = View.GONE
                    ll_shipp_subterms227.visibility = View.GONE
                    ll_shipp_subterms228.visibility = View.GONE
                    ll_shipp_subterms229.visibility = View.GONE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.VISIBLE
                    ll_shipp_subterms224.visibility = View.VISIBLE
                    ll_shipp_subterms225.visibility = View.VISIBLE
                    ll_shipp_subterms226.visibility = View.VISIBLE
                    ll_shipp_subterms227.visibility = View.GONE
                    ll_shipp_subterms228.visibility = View.GONE
                    ll_shipp_subterms229.visibility = View.GONE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.VISIBLE
                    ll_shipp_subterms224.visibility = View.VISIBLE
                    ll_shipp_subterms225.visibility = View.VISIBLE
                    ll_shipp_subterms226.visibility = View.VISIBLE
                    ll_shipp_subterms227.visibility = View.VISIBLE
                    ll_shipp_subterms228.visibility = View.GONE
                    ll_shipp_subterms229.visibility = View.GONE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.VISIBLE
                    ll_shipp_subterms224.visibility = View.VISIBLE
                    ll_shipp_subterms225.visibility = View.VISIBLE
                    ll_shipp_subterms226.visibility = View.VISIBLE
                    ll_shipp_subterms227.visibility = View.VISIBLE
                    ll_shipp_subterms228.visibility = View.VISIBLE
                    ll_shipp_subterms229.visibility = View.GONE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.VISIBLE
                    ll_shipp_subterms224.visibility = View.VISIBLE
                    ll_shipp_subterms225.visibility = View.VISIBLE
                    ll_shipp_subterms226.visibility = View.VISIBLE
                    ll_shipp_subterms227.visibility = View.VISIBLE
                    ll_shipp_subterms228.visibility = View.VISIBLE
                    ll_shipp_subterms229.visibility = View.VISIBLE
                    ll_shipp_subterms2210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_221.visibility = View.VISIBLE
                    ll_shipp_subterms222.visibility = View.VISIBLE
                    ll_shipp_subterms223.visibility = View.VISIBLE
                    ll_shipp_subterms224.visibility = View.VISIBLE
                    ll_shipp_subterms225.visibility = View.VISIBLE
                    ll_shipp_subterms226.visibility = View.VISIBLE
                    ll_shipp_subterms227.visibility = View.VISIBLE
                    ll_shipp_subterms228.visibility = View.VISIBLE
                    ll_shipp_subterms229.visibility = View.VISIBLE
                    ll_shipp_subterms2210.visibility = View.VISIBLE
                }
                shipping_termsdate2 = ""
                shipping_qty2 = ""
                dialog_list.dismiss()
            }
        }
        admin_delivery_terms2 = findViewById(R.id.admin_delivery_terms2)
        ll_delivery_230 = findViewById(R.id.ll_delivery_230)
        ll_delivery_terms_2 = findViewById(R.id.ll_delivery_terms_2)
        ll_delivery_subterms_231 = findViewById(R.id.ll_delivery_subterms_231)
        ll_delivery_subterms232 = findViewById(R.id.ll_delivery_subterms232)
        ll_delivery_subterms233 = findViewById(R.id.ll_delivery_subterms233)
        ll_delivery_subterms234 = findViewById(R.id.ll_delivery_subterms234)
        ll_delivery_subterms235 = findViewById(R.id.ll_delivery_subterms235)
        ll_delivery_subterms236 = findViewById(R.id.ll_delivery_subterms236)
        ll_delivery_subterms237 = findViewById(R.id.ll_delivery_subterms237)
        ll_delivery_subterms238 = findViewById(R.id.ll_delivery_subterms238)
        ll_delivery_subterms239 = findViewById(R.id.ll_delivery_subterms239)
        ll_delivery_subterms2310 = findViewById(R.id.ll_delivery_subterms2310)
        admin_delivery_termssitem231 = findViewById(R.id.admin_delivery_termssitem231)
        admin_delivery_termsdate231 = findViewById(R.id.admin_delivery_termsdate231)
        admin_delivery_termssitem232 = findViewById(R.id.admin_delivery_termssitem232)
        admin_delivery_termsdate232 = findViewById(R.id.admin_delivery_termsdate232)
        admin_delivery_termssitem233 = findViewById(R.id.admin_delivery_termssitem233)
        admin_delivery_termsdate233 = findViewById(R.id.admin_delivery_termsdate233)
        admin_delivery_termssitem234 = findViewById(R.id.admin_delivery_termssitem234)
        admin_delivery_termsdate234 = findViewById(R.id.admin_delivery_termsdate234)
        admin_delivery_termssitem235 = findViewById(R.id.admin_delivery_termssitem235)
        admin_delivery_termsdate235 = findViewById(R.id.admin_delivery_termsdate235)
        admin_delivery_termssitem236 = findViewById(R.id.admin_delivery_termssitem236)
        admin_delivery_termsdate236 = findViewById(R.id.admin_delivery_termsdate236)
        admin_delivery_termssitem237 = findViewById(R.id.admin_delivery_termssitem237)
        admin_delivery_termsdate237 = findViewById(R.id.admin_delivery_termsdate237)
        admin_delivery_termssitem238 = findViewById(R.id.admin_delivery_termssitem238)
        admin_delivery_termsdate238 = findViewById(R.id.admin_delivery_termsdate238)
        admin_delivery_termssitem239 = findViewById(R.id.admin_delivery_termssitem239)
        admin_delivery_termsdate239 = findViewById(R.id.admin_delivery_termsdate239)
        admin_delivery_termssitem2310 = findViewById(R.id.admin_delivery_termssitem2310)
        admin_delivery_termsdate2310 = findViewById(R.id.admin_delivery_termsdate2310)
        admin_delivery_terms2.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms2.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.GONE
                    ll_delivery_subterms233.visibility = View.GONE
                    ll_delivery_subterms234.visibility = View.GONE
                    ll_delivery_subterms235.visibility = View.GONE
                    ll_delivery_subterms236.visibility = View.GONE
                    ll_delivery_subterms237.visibility = View.GONE
                    ll_delivery_subterms238.visibility = View.GONE
                    ll_delivery_subterms239.visibility = View.GONE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.GONE
                    ll_delivery_subterms234.visibility = View.GONE
                    ll_delivery_subterms235.visibility = View.GONE
                    ll_delivery_subterms236.visibility = View.GONE
                    ll_delivery_subterms237.visibility = View.GONE
                    ll_delivery_subterms238.visibility = View.GONE
                    ll_delivery_subterms239.visibility = View.GONE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.VISIBLE
                    ll_delivery_subterms234.visibility = View.GONE
                    ll_delivery_subterms235.visibility = View.GONE
                    ll_delivery_subterms236.visibility = View.GONE
                    ll_delivery_subterms237.visibility = View.GONE
                    ll_delivery_subterms238.visibility = View.GONE
                    ll_delivery_subterms239.visibility = View.GONE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.VISIBLE
                    ll_delivery_subterms234.visibility = View.VISIBLE
                    ll_delivery_subterms235.visibility = View.GONE
                    ll_delivery_subterms236.visibility = View.GONE
                    ll_delivery_subterms237.visibility = View.GONE
                    ll_delivery_subterms238.visibility = View.GONE
                    ll_delivery_subterms239.visibility = View.GONE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.VISIBLE
                    ll_delivery_subterms234.visibility = View.VISIBLE
                    ll_delivery_subterms235.visibility = View.VISIBLE
                    ll_delivery_subterms236.visibility = View.GONE
                    ll_delivery_subterms237.visibility = View.GONE
                    ll_delivery_subterms238.visibility = View.GONE
                    ll_delivery_subterms239.visibility = View.GONE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.VISIBLE
                    ll_delivery_subterms234.visibility = View.VISIBLE
                    ll_delivery_subterms235.visibility = View.VISIBLE
                    ll_delivery_subterms236.visibility = View.VISIBLE
                    ll_delivery_subterms237.visibility = View.GONE
                    ll_delivery_subterms238.visibility = View.GONE
                    ll_delivery_subterms239.visibility = View.GONE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.VISIBLE
                    ll_delivery_subterms234.visibility = View.VISIBLE
                    ll_delivery_subterms235.visibility = View.VISIBLE
                    ll_delivery_subterms236.visibility = View.VISIBLE
                    ll_delivery_subterms237.visibility = View.VISIBLE
                    ll_delivery_subterms238.visibility = View.GONE
                    ll_delivery_subterms239.visibility = View.GONE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.VISIBLE
                    ll_delivery_subterms234.visibility = View.VISIBLE
                    ll_delivery_subterms235.visibility = View.VISIBLE
                    ll_delivery_subterms236.visibility = View.VISIBLE
                    ll_delivery_subterms237.visibility = View.VISIBLE
                    ll_delivery_subterms238.visibility = View.VISIBLE
                    ll_delivery_subterms239.visibility = View.GONE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.VISIBLE
                    ll_delivery_subterms234.visibility = View.VISIBLE
                    ll_delivery_subterms235.visibility = View.VISIBLE
                    ll_delivery_subterms236.visibility = View.VISIBLE
                    ll_delivery_subterms237.visibility = View.VISIBLE
                    ll_delivery_subterms238.visibility = View.VISIBLE
                    ll_delivery_subterms239.visibility = View.VISIBLE
                    ll_delivery_subterms2310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_231.visibility = View.VISIBLE
                    ll_delivery_subterms232.visibility = View.VISIBLE
                    ll_delivery_subterms233.visibility = View.VISIBLE
                    ll_delivery_subterms234.visibility = View.VISIBLE
                    ll_delivery_subterms235.visibility = View.VISIBLE
                    ll_delivery_subterms236.visibility = View.VISIBLE
                    ll_delivery_subterms237.visibility = View.VISIBLE
                    ll_delivery_subterms238.visibility = View.VISIBLE
                    ll_delivery_subterms239.visibility = View.VISIBLE
                    ll_delivery_subterms2310.visibility = View.VISIBLE
                }
                delivery_termsdate2 = ""
                delivery_qty2 = ""
                dialog_list.dismiss()

            }
        }
        admin_shipp_termsdate221.setOnClickListener(this)
        admin_shipp_termsdate222.setOnClickListener(this)
        admin_shipp_termsdate223.setOnClickListener(this)
        admin_shipp_termsdate224.setOnClickListener(this)
        admin_shipp_termsdate225.setOnClickListener(this)
        admin_shipp_termsdate226.setOnClickListener(this)
        admin_shipp_termsdate227.setOnClickListener(this)
        admin_shipp_termsdate228.setOnClickListener(this)
        admin_shipp_termsdate229.setOnClickListener(this)
        admin_shipp_termsdate2210.setOnClickListener(this)
        admin_delivery_termsdate231.setOnClickListener(this)
        admin_delivery_termsdate232.setOnClickListener(this)
        admin_delivery_termsdate233.setOnClickListener(this)
        admin_delivery_termsdate234.setOnClickListener(this)
        admin_delivery_termsdate235.setOnClickListener(this)
        admin_delivery_termsdate236.setOnClickListener(this)
        admin_delivery_termsdate237.setOnClickListener(this)
        admin_delivery_termsdate238.setOnClickListener(this)
        admin_delivery_termsdate239.setOnClickListener(this)
        admin_delivery_termsdate2310.setOnClickListener(this)
    }

    private fun JobThreeDropswidgets() {
        admin_shipping_terms3 = findViewById(R.id.admin_shipping_terms3)
        ll_shipping_three = findViewById(R.id.ll_shipping_three)
        ll_shipp_320 = findViewById(R.id.ll_shipp_320)
        ll_ship_subterms_321 = findViewById(R.id.ll_ship_subterms_321)
        ll_shipp_subterms322 = findViewById(R.id.ll_shipp_subterms322)
        ll_shipp_subterms323 = findViewById(R.id.ll_shipp_subterms323)
        ll_shipp_subterms324 = findViewById(R.id.ll_shipp_subterms324)
        ll_shipp_subterms325 = findViewById(R.id.ll_shipp_subterms325)
        ll_shipp_subterms326 = findViewById(R.id.ll_shipp_subterms326)
        ll_shipp_subterms327 = findViewById(R.id.ll_shipp_subterms327)
        ll_shipp_subterms328 = findViewById(R.id.ll_shipp_subterms328)
        ll_shipp_subterms329 = findViewById(R.id.ll_shipp_subterms329)
        ll_shipp_subterms3210 = findViewById(R.id.ll_shipp_subterms3210)
        admin_shipp_termssitem321 = findViewById(R.id.admin_shipp_termssitem321)
        admin_shipp_termsdate321 = findViewById(R.id.admin_shipp_termsdate321)
        admin_shipp_termssitem322 = findViewById(R.id.admin_shipp_termssitem322)
        admin_shipp_termsdate322 = findViewById(R.id.admin_shipp_termsdate322)
        admin_shipp_termssitem323 = findViewById(R.id.admin_shipp_termssitem323)
        admin_shipp_termsdate323 = findViewById(R.id.admin_shipp_termsdate323)
        admin_shipp_termssitem324 = findViewById(R.id.admin_shipp_termssitem324)
        admin_shipp_termsdate324 = findViewById(R.id.admin_shipp_termsdate324)
        admin_shipp_termssitem325 = findViewById(R.id.admin_shipp_termssitem325)
        admin_shipp_termsdate325 = findViewById(R.id.admin_shipp_termsdate325)
        admin_shipp_termssitem326 = findViewById(R.id.admin_shipp_termssitem326)
        admin_shipp_termsdate326 = findViewById(R.id.admin_shipp_termsdate326)
        admin_shipp_termssitem327 = findViewById(R.id.admin_shipp_termssitem327)
        admin_shipp_termsdate327 = findViewById(R.id.admin_shipp_termsdate327)
        admin_shipp_termssitem328 = findViewById(R.id.admin_shipp_termssitem328)
        admin_shipp_termsdate328 = findViewById(R.id.admin_shipp_termsdate328)
        admin_shipp_termssitem329 = findViewById(R.id.admin_shipp_termssitem329)
        admin_shipp_termsdate329 = findViewById(R.id.admin_shipp_termsdate329)
        admin_shipp_termssitem3210 = findViewById(R.id.admin_shipp_termssitem3210)
        admin_shipp_termsdate3210 = findViewById(R.id.admin_shipp_termsdate3210)
        admin_shipping_terms3.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms3.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.GONE
                    ll_shipp_subterms323.visibility = View.GONE
                    ll_shipp_subterms324.visibility = View.GONE
                    ll_shipp_subterms325.visibility = View.GONE
                    ll_shipp_subterms326.visibility = View.GONE
                    ll_shipp_subterms327.visibility = View.GONE
                    ll_shipp_subterms328.visibility = View.GONE
                    ll_shipp_subterms329.visibility = View.GONE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.GONE
                    ll_shipp_subterms324.visibility = View.GONE
                    ll_shipp_subterms325.visibility = View.GONE
                    ll_shipp_subterms326.visibility = View.GONE
                    ll_shipp_subterms327.visibility = View.GONE
                    ll_shipp_subterms328.visibility = View.GONE
                    ll_shipp_subterms329.visibility = View.GONE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.VISIBLE
                    ll_shipp_subterms324.visibility = View.GONE
                    ll_shipp_subterms325.visibility = View.GONE
                    ll_shipp_subterms326.visibility = View.GONE
                    ll_shipp_subterms327.visibility = View.GONE
                    ll_shipp_subterms328.visibility = View.GONE
                    ll_shipp_subterms329.visibility = View.GONE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.VISIBLE
                    ll_shipp_subterms324.visibility = View.VISIBLE
                    ll_shipp_subterms325.visibility = View.GONE
                    ll_shipp_subterms326.visibility = View.GONE
                    ll_shipp_subterms327.visibility = View.GONE
                    ll_shipp_subterms328.visibility = View.GONE
                    ll_shipp_subterms329.visibility = View.GONE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.VISIBLE
                    ll_shipp_subterms324.visibility = View.VISIBLE
                    ll_shipp_subterms325.visibility = View.VISIBLE
                    ll_shipp_subterms326.visibility = View.GONE
                    ll_shipp_subterms327.visibility = View.GONE
                    ll_shipp_subterms328.visibility = View.GONE
                    ll_shipp_subterms329.visibility = View.GONE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.VISIBLE
                    ll_shipp_subterms324.visibility = View.VISIBLE
                    ll_shipp_subterms325.visibility = View.VISIBLE
                    ll_shipp_subterms326.visibility = View.VISIBLE
                    ll_shipp_subterms327.visibility = View.GONE
                    ll_shipp_subterms328.visibility = View.GONE
                    ll_shipp_subterms329.visibility = View.GONE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.VISIBLE
                    ll_shipp_subterms324.visibility = View.VISIBLE
                    ll_shipp_subterms325.visibility = View.VISIBLE
                    ll_shipp_subterms326.visibility = View.VISIBLE
                    ll_shipp_subterms327.visibility = View.VISIBLE
                    ll_shipp_subterms328.visibility = View.GONE
                    ll_shipp_subterms329.visibility = View.GONE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.VISIBLE
                    ll_shipp_subterms324.visibility = View.VISIBLE
                    ll_shipp_subterms325.visibility = View.VISIBLE
                    ll_shipp_subterms326.visibility = View.VISIBLE
                    ll_shipp_subterms327.visibility = View.VISIBLE
                    ll_shipp_subterms328.visibility = View.VISIBLE
                    ll_shipp_subterms329.visibility = View.GONE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.VISIBLE
                    ll_shipp_subterms324.visibility = View.VISIBLE
                    ll_shipp_subterms325.visibility = View.VISIBLE
                    ll_shipp_subterms326.visibility = View.VISIBLE
                    ll_shipp_subterms327.visibility = View.VISIBLE
                    ll_shipp_subterms328.visibility = View.VISIBLE
                    ll_shipp_subterms329.visibility = View.VISIBLE
                    ll_shipp_subterms3210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_321.visibility = View.VISIBLE
                    ll_shipp_subterms322.visibility = View.VISIBLE
                    ll_shipp_subterms323.visibility = View.VISIBLE
                    ll_shipp_subterms324.visibility = View.VISIBLE
                    ll_shipp_subterms325.visibility = View.VISIBLE
                    ll_shipp_subterms326.visibility = View.VISIBLE
                    ll_shipp_subterms327.visibility = View.VISIBLE
                    ll_shipp_subterms328.visibility = View.VISIBLE
                    ll_shipp_subterms329.visibility = View.VISIBLE
                    ll_shipp_subterms3210.visibility = View.VISIBLE
                }
                shipping_termsdate3 = ""
                shipping_qty3 = ""
                dialog_list.dismiss()
            }
        }
        admin_delivery_terms3 = findViewById(R.id.admin_delivery_terms3)
        ll_delivery_terms_3 = findViewById(R.id.ll_delivery_terms_3)
        ll_delivery_330 = findViewById(R.id.ll_delivery_330)
        ll_delivery_subterms_331 = findViewById(R.id.ll_delivery_subterms_331)
        ll_delivery_subterms332 = findViewById(R.id.ll_delivery_subterms332)
        ll_delivery_subterms333 = findViewById(R.id.ll_delivery_subterms333)
        ll_delivery_subterms334 = findViewById(R.id.ll_delivery_subterms334)
        ll_delivery_subterms335 = findViewById(R.id.ll_delivery_subterms335)
        ll_delivery_subterms336 = findViewById(R.id.ll_delivery_subterms336)
        ll_delivery_subterms337 = findViewById(R.id.ll_delivery_subterms337)
        ll_delivery_subterms338 = findViewById(R.id.ll_delivery_subterms338)
        ll_delivery_subterms339 = findViewById(R.id.ll_delivery_subterms339)
        ll_delivery_subterms3310 = findViewById(R.id.ll_delivery_subterms3310)
        admin_delivery_termssitem331 = findViewById(R.id.admin_delivery_termssitem331)
        admin_delivery_termsdate331 = findViewById(R.id.admin_delivery_termsdate331)
        admin_delivery_termssitem332 = findViewById(R.id.admin_delivery_termssitem332)
        admin_delivery_termsdate332 = findViewById(R.id.admin_delivery_termsdate332)
        admin_delivery_termssitem333 = findViewById(R.id.admin_delivery_termssitem333)
        admin_delivery_termsdate333 = findViewById(R.id.admin_delivery_termsdate333)
        admin_delivery_termssitem334 = findViewById(R.id.admin_delivery_termssitem334)
        admin_delivery_termsdate334 = findViewById(R.id.admin_delivery_termsdate334)
        admin_delivery_termssitem335 = findViewById(R.id.admin_delivery_termssitem335)
        admin_delivery_termsdate335 = findViewById(R.id.admin_delivery_termsdate335)
        admin_delivery_termssitem336 = findViewById(R.id.admin_delivery_termssitem336)
        admin_delivery_termsdate336 = findViewById(R.id.admin_delivery_termsdate336)
        admin_delivery_termssitem337 = findViewById(R.id.admin_delivery_termssitem337)
        admin_delivery_termsdate337 = findViewById(R.id.admin_delivery_termsdate337)
        admin_delivery_termssitem338 = findViewById(R.id.admin_delivery_termssitem338)
        admin_delivery_termsdate338 = findViewById(R.id.admin_delivery_termsdate338)
        admin_delivery_termssitem339 = findViewById(R.id.admin_delivery_termssitem339)
        admin_delivery_termsdate339 = findViewById(R.id.admin_delivery_termsdate339)
        admin_delivery_termssitem3310 = findViewById(R.id.admin_delivery_termssitem3310)
        admin_delivery_termsdate3310 = findViewById(R.id.admin_delivery_termsdate3310)
        admin_delivery_terms3.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms3.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.GONE
                    ll_delivery_subterms333.visibility = View.GONE
                    ll_delivery_subterms334.visibility = View.GONE
                    ll_delivery_subterms335.visibility = View.GONE
                    ll_delivery_subterms336.visibility = View.GONE
                    ll_delivery_subterms337.visibility = View.GONE
                    ll_delivery_subterms338.visibility = View.GONE
                    ll_delivery_subterms339.visibility = View.GONE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.GONE
                    ll_delivery_subterms334.visibility = View.GONE
                    ll_delivery_subterms335.visibility = View.GONE
                    ll_delivery_subterms336.visibility = View.GONE
                    ll_delivery_subterms337.visibility = View.GONE
                    ll_delivery_subterms338.visibility = View.GONE
                    ll_delivery_subterms339.visibility = View.GONE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.VISIBLE
                    ll_delivery_subterms334.visibility = View.GONE
                    ll_delivery_subterms335.visibility = View.GONE
                    ll_delivery_subterms336.visibility = View.GONE
                    ll_delivery_subterms337.visibility = View.GONE
                    ll_delivery_subterms338.visibility = View.GONE
                    ll_delivery_subterms339.visibility = View.GONE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.VISIBLE
                    ll_delivery_subterms334.visibility = View.VISIBLE
                    ll_delivery_subterms335.visibility = View.GONE
                    ll_delivery_subterms336.visibility = View.GONE
                    ll_delivery_subterms337.visibility = View.GONE
                    ll_delivery_subterms338.visibility = View.GONE
                    ll_delivery_subterms339.visibility = View.GONE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.VISIBLE
                    ll_delivery_subterms334.visibility = View.VISIBLE
                    ll_delivery_subterms335.visibility = View.VISIBLE
                    ll_delivery_subterms336.visibility = View.GONE
                    ll_delivery_subterms337.visibility = View.GONE
                    ll_delivery_subterms338.visibility = View.GONE
                    ll_delivery_subterms339.visibility = View.GONE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.VISIBLE
                    ll_delivery_subterms334.visibility = View.VISIBLE
                    ll_delivery_subterms335.visibility = View.VISIBLE
                    ll_delivery_subterms336.visibility = View.VISIBLE
                    ll_delivery_subterms337.visibility = View.GONE
                    ll_delivery_subterms338.visibility = View.GONE
                    ll_delivery_subterms339.visibility = View.GONE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.VISIBLE
                    ll_delivery_subterms334.visibility = View.VISIBLE
                    ll_delivery_subterms335.visibility = View.VISIBLE
                    ll_delivery_subterms336.visibility = View.VISIBLE
                    ll_delivery_subterms337.visibility = View.VISIBLE
                    ll_delivery_subterms338.visibility = View.GONE
                    ll_delivery_subterms339.visibility = View.GONE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.VISIBLE
                    ll_delivery_subterms334.visibility = View.VISIBLE
                    ll_delivery_subterms335.visibility = View.VISIBLE
                    ll_delivery_subterms336.visibility = View.VISIBLE
                    ll_delivery_subterms337.visibility = View.VISIBLE
                    ll_delivery_subterms338.visibility = View.VISIBLE
                    ll_delivery_subterms339.visibility = View.GONE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.VISIBLE
                    ll_delivery_subterms334.visibility = View.VISIBLE
                    ll_delivery_subterms335.visibility = View.VISIBLE
                    ll_delivery_subterms336.visibility = View.VISIBLE
                    ll_delivery_subterms337.visibility = View.VISIBLE
                    ll_delivery_subterms338.visibility = View.VISIBLE
                    ll_delivery_subterms339.visibility = View.VISIBLE
                    ll_delivery_subterms3310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_331.visibility = View.VISIBLE
                    ll_delivery_subterms332.visibility = View.VISIBLE
                    ll_delivery_subterms333.visibility = View.VISIBLE
                    ll_delivery_subterms334.visibility = View.VISIBLE
                    ll_delivery_subterms335.visibility = View.VISIBLE
                    ll_delivery_subterms336.visibility = View.VISIBLE
                    ll_delivery_subterms337.visibility = View.VISIBLE
                    ll_delivery_subterms338.visibility = View.VISIBLE
                    ll_delivery_subterms339.visibility = View.VISIBLE
                    ll_delivery_subterms3310.visibility = View.VISIBLE
                }
                delivery_termsdate3 = ""
                delivery_qty3 = ""
                dialog_list.dismiss()
            }
        }

        admin_shipp_termsdate321.setOnClickListener(this)
        admin_shipp_termsdate322.setOnClickListener(this)
        admin_shipp_termsdate323.setOnClickListener(this)
        admin_shipp_termsdate324.setOnClickListener(this)
        admin_shipp_termsdate325.setOnClickListener(this)
        admin_shipp_termsdate326.setOnClickListener(this)
        admin_shipp_termsdate327.setOnClickListener(this)
        admin_shipp_termsdate328.setOnClickListener(this)
        admin_shipp_termsdate329.setOnClickListener(this)
        admin_shipp_termsdate3210.setOnClickListener(this)
        admin_delivery_termsdate331.setOnClickListener(this)
        admin_delivery_termsdate332.setOnClickListener(this)
        admin_delivery_termsdate333.setOnClickListener(this)
        admin_delivery_termsdate334.setOnClickListener(this)
        admin_delivery_termsdate335.setOnClickListener(this)
        admin_delivery_termsdate336.setOnClickListener(this)
        admin_delivery_termsdate337.setOnClickListener(this)
        admin_delivery_termsdate338.setOnClickListener(this)
        admin_delivery_termsdate339.setOnClickListener(this)
        admin_delivery_termsdate3310.setOnClickListener(this)
    }

    private fun JobFourDropswidgets() {
        admin_shipping_terms4 = findViewById(R.id.admin_shipping_terms4)
        ll_shipp_420 = findViewById(R.id.ll_shipp_420)
        ll_shipping_four = findViewById(R.id.ll_shipping_four)
        ll_delivery_terms_4 = findViewById(R.id.ll_delivery_terms_4)
        ll_ship_subterms_421 = findViewById(R.id.ll_ship_subterms_421)
        ll_shipp_subterms422 = findViewById(R.id.ll_shipp_subterms422)
        ll_shipp_subterms423 = findViewById(R.id.ll_shipp_subterms423)
        ll_shipp_subterms424 = findViewById(R.id.ll_shipp_subterms424)
        ll_shipp_subterms425 = findViewById(R.id.ll_shipp_subterms425)
        ll_shipp_subterms426 = findViewById(R.id.ll_shipp_subterms426)
        ll_shipp_subterms427 = findViewById(R.id.ll_shipp_subterms427)
        ll_shipp_subterms428 = findViewById(R.id.ll_shipp_subterms428)
        ll_shipp_subterms429 = findViewById(R.id.ll_shipp_subterms429)
        ll_shipp_subterms4210 = findViewById(R.id.ll_shipp_subterms4210)
        admin_shipp_termssitem421 = findViewById(R.id.admin_shipp_termssitem421)
        admin_shipp_termsdate421 = findViewById(R.id.admin_shipp_termsdate421)
        admin_shipp_termssitem422 = findViewById(R.id.admin_shipp_termssitem422)
        admin_shipp_termsdate422 = findViewById(R.id.admin_shipp_termsdate422)
        admin_shipp_termssitem423 = findViewById(R.id.admin_shipp_termssitem423)
        admin_shipp_termsdate423 = findViewById(R.id.admin_shipp_termsdate423)
        admin_shipp_termssitem424 = findViewById(R.id.admin_shipp_termssitem424)
        admin_shipp_termsdate424 = findViewById(R.id.admin_shipp_termsdate424)
        admin_shipp_termssitem425 = findViewById(R.id.admin_shipp_termssitem425)
        admin_shipp_termsdate425 = findViewById(R.id.admin_shipp_termsdate425)
        admin_shipp_termssitem426 = findViewById(R.id.admin_shipp_termssitem426)
        admin_shipp_termsdate426 = findViewById(R.id.admin_shipp_termsdate426)
        admin_shipp_termssitem427 = findViewById(R.id.admin_shipp_termssitem427)
        admin_shipp_termsdate427 = findViewById(R.id.admin_shipp_termsdate427)
        admin_shipp_termssitem428 = findViewById(R.id.admin_shipp_termssitem428)
        admin_shipp_termsdate428 = findViewById(R.id.admin_shipp_termsdate428)
        admin_shipp_termssitem429 = findViewById(R.id.admin_shipp_termssitem429)
        admin_shipp_termsdate429 = findViewById(R.id.admin_shipp_termsdate429)
        admin_shipp_termssitem4210 = findViewById(R.id.admin_shipp_termssitem4210)
        admin_shipp_termsdate4210 = findViewById(R.id.admin_shipp_termsdate4210)
        admin_shipping_terms4.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms4.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.GONE
                    ll_shipp_subterms423.visibility = View.GONE
                    ll_shipp_subterms424.visibility = View.GONE
                    ll_shipp_subterms425.visibility = View.GONE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.GONE
                    ll_shipp_subterms424.visibility = View.GONE
                    ll_shipp_subterms425.visibility = View.GONE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.GONE
                    ll_shipp_subterms425.visibility = View.GONE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.GONE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.GONE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.GONE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.VISIBLE
                    ll_shipp_subterms428.visibility = View.GONE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.VISIBLE
                    ll_shipp_subterms428.visibility = View.VISIBLE
                    ll_shipp_subterms429.visibility = View.GONE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.VISIBLE
                    ll_shipp_subterms428.visibility = View.VISIBLE
                    ll_shipp_subterms429.visibility = View.VISIBLE
                    ll_shipp_subterms4210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_421.visibility = View.VISIBLE
                    ll_shipp_subterms422.visibility = View.VISIBLE
                    ll_shipp_subterms423.visibility = View.VISIBLE
                    ll_shipp_subterms424.visibility = View.VISIBLE
                    ll_shipp_subterms425.visibility = View.VISIBLE
                    ll_shipp_subterms426.visibility = View.VISIBLE
                    ll_shipp_subterms427.visibility = View.VISIBLE
                    ll_shipp_subterms428.visibility = View.VISIBLE
                    ll_shipp_subterms429.visibility = View.VISIBLE
                    ll_shipp_subterms4210.visibility = View.VISIBLE
                }
                shipping_termsdate4 = ""
                shipping_qty4 = ""
                dialog_list.dismiss()

            }
        }

        admin_delivery_terms4 = findViewById(R.id.admin_delivery_terms4)
        ll_delivery_430 = findViewById(R.id.ll_delivery_430)
        ll_delivery_subterms_431 = findViewById(R.id.ll_delivery_subterms_431)
        ll_delivery_subterms432 = findViewById(R.id.ll_delivery_subterms432)
        ll_delivery_subterms433 = findViewById(R.id.ll_delivery_subterms433)
        ll_delivery_subterms434 = findViewById(R.id.ll_delivery_subterms434)
        ll_delivery_subterms435 = findViewById(R.id.ll_delivery_subterms435)
        ll_delivery_subterms436 = findViewById(R.id.ll_delivery_subterms436)
        ll_delivery_subterms437 = findViewById(R.id.ll_delivery_subterms437)
        ll_delivery_subterms438 = findViewById(R.id.ll_delivery_subterms438)
        ll_delivery_subterms439 = findViewById(R.id.ll_delivery_subterms439)
        ll_delivery_subterms4310 = findViewById(R.id.ll_delivery_subterms4310)
        admin_delivery_termssitem431 = findViewById(R.id.admin_delivery_termssitem431)
        admin_delivery_termsdate431 = findViewById(R.id.admin_delivery_termsdate431)
        admin_delivery_termssitem432 = findViewById(R.id.admin_delivery_termssitem432)
        admin_delivery_termsdate432 = findViewById(R.id.admin_delivery_termsdate432)
        admin_delivery_termssitem433 = findViewById(R.id.admin_delivery_termssitem433)
        admin_delivery_termsdate433 = findViewById(R.id.admin_delivery_termsdate433)
        admin_delivery_termssitem434 = findViewById(R.id.admin_delivery_termssitem434)
        admin_delivery_termsdate434 = findViewById(R.id.admin_delivery_termsdate434)
        admin_delivery_termssitem435 = findViewById(R.id.admin_delivery_termssitem435)
        admin_delivery_termsdate435 = findViewById(R.id.admin_delivery_termsdate435)
        admin_delivery_termssitem436 = findViewById(R.id.admin_delivery_termssitem436)
        admin_delivery_termsdate436 = findViewById(R.id.admin_delivery_termsdate436)
        admin_delivery_termssitem437 = findViewById(R.id.admin_delivery_termssitem437)
        admin_delivery_termsdate437 = findViewById(R.id.admin_delivery_termsdate437)
        admin_delivery_termssitem438 = findViewById(R.id.admin_delivery_termssitem438)
        admin_delivery_termsdate438 = findViewById(R.id.admin_delivery_termsdate438)
        admin_delivery_termssitem439 = findViewById(R.id.admin_delivery_termssitem439)
        admin_delivery_termsdate439 = findViewById(R.id.admin_delivery_termsdate439)
        admin_delivery_termssitem4310 = findViewById(R.id.admin_delivery_termssitem4310)
        admin_delivery_termsdate4310 = findViewById(R.id.admin_delivery_termsdate4310)
        admin_delivery_terms4.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms4.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.GONE
                    ll_delivery_subterms433.visibility = View.GONE
                    ll_delivery_subterms434.visibility = View.GONE
                    ll_delivery_subterms435.visibility = View.GONE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.GONE
                    ll_delivery_subterms434.visibility = View.GONE
                    ll_delivery_subterms435.visibility = View.GONE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.GONE
                    ll_delivery_subterms435.visibility = View.GONE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.GONE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.GONE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.GONE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.VISIBLE
                    ll_delivery_subterms438.visibility = View.GONE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.VISIBLE
                    ll_delivery_subterms438.visibility = View.VISIBLE
                    ll_delivery_subterms439.visibility = View.GONE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.VISIBLE
                    ll_delivery_subterms438.visibility = View.VISIBLE
                    ll_delivery_subterms439.visibility = View.VISIBLE
                    ll_delivery_subterms4310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_431.visibility = View.VISIBLE
                    ll_delivery_subterms432.visibility = View.VISIBLE
                    ll_delivery_subterms433.visibility = View.VISIBLE
                    ll_delivery_subterms434.visibility = View.VISIBLE
                    ll_delivery_subterms435.visibility = View.VISIBLE
                    ll_delivery_subterms436.visibility = View.VISIBLE
                    ll_delivery_subterms437.visibility = View.VISIBLE
                    ll_delivery_subterms438.visibility = View.VISIBLE
                    ll_delivery_subterms439.visibility = View.VISIBLE
                    ll_delivery_subterms4310.visibility = View.VISIBLE
                }
                delivery_termsdate4 = ""
                delivery_qty4 = ""
                dialog_list.dismiss()
            }
        }
        admin_shipp_termsdate421.setOnClickListener(this)
        admin_shipp_termsdate422.setOnClickListener(this)
        admin_shipp_termsdate423.setOnClickListener(this)
        admin_shipp_termsdate424.setOnClickListener(this)
        admin_shipp_termsdate425.setOnClickListener(this)
        admin_shipp_termsdate426.setOnClickListener(this)
        admin_shipp_termsdate427.setOnClickListener(this)
        admin_shipp_termsdate428.setOnClickListener(this)
        admin_shipp_termsdate429.setOnClickListener(this)
        admin_shipp_termsdate4210.setOnClickListener(this)

        admin_delivery_termsdate431.setOnClickListener(this)
        admin_delivery_termsdate432.setOnClickListener(this)
        admin_delivery_termsdate433.setOnClickListener(this)
        admin_delivery_termsdate434.setOnClickListener(this)
        admin_delivery_termsdate435.setOnClickListener(this)
        admin_delivery_termsdate436.setOnClickListener(this)
        admin_delivery_termsdate437.setOnClickListener(this)
        admin_delivery_termsdate438.setOnClickListener(this)
        admin_delivery_termsdate439.setOnClickListener(this)
        admin_delivery_termsdate4310.setOnClickListener(this)
    }

    private fun JobFiveDropswidgets() {
        admin_shipping_terms5 = findViewById(R.id.admin_shipping_terms5)
        ll_shipp_520 = findViewById(R.id.ll_shipp_520)
        ll_shipping_five = findViewById(R.id.ll_shipping_five)
        ll_delivery_terms_5 = findViewById(R.id.ll_delivery_terms_5)
        ll_ship_subterms_521 = findViewById(R.id.ll_ship_subterms_521)
        ll_shipp_subterms522 = findViewById(R.id.ll_shipp_subterms522)
        ll_shipp_subterms523 = findViewById(R.id.ll_shipp_subterms523)
        ll_shipp_subterms524 = findViewById(R.id.ll_shipp_subterms524)
        ll_shipp_subterms525 = findViewById(R.id.ll_shipp_subterms525)
        ll_shipp_subterms526 = findViewById(R.id.ll_shipp_subterms526)
        ll_shipp_subterms527 = findViewById(R.id.ll_shipp_subterms527)
        ll_shipp_subterms528 = findViewById(R.id.ll_shipp_subterms528)
        ll_shipp_subterms529 = findViewById(R.id.ll_shipp_subterms529)
        ll_shipp_subterms5210 = findViewById(R.id.ll_shipp_subterms5210)
        admin_shipp_termssitem521 = findViewById(R.id.admin_shipp_termssitem521)
        admin_shipp_termsdate521 = findViewById(R.id.admin_shipp_termsdate521)
        admin_shipp_termssitem522 = findViewById(R.id.admin_shipp_termssitem522)
        admin_shipp_termsdate522 = findViewById(R.id.admin_shipp_termsdate522)
        admin_shipp_termssitem523 = findViewById(R.id.admin_shipp_termssitem523)
        admin_shipp_termsdate523 = findViewById(R.id.admin_shipp_termsdate523)
        admin_shipp_termssitem524 = findViewById(R.id.admin_shipp_termssitem524)
        admin_shipp_termsdate524 = findViewById(R.id.admin_shipp_termsdate524)
        admin_shipp_termssitem525 = findViewById(R.id.admin_shipp_termssitem525)
        admin_shipp_termsdate525 = findViewById(R.id.admin_shipp_termsdate525)
        admin_shipp_termssitem526 = findViewById(R.id.admin_shipp_termssitem526)
        admin_shipp_termsdate526 = findViewById(R.id.admin_shipp_termsdate526)
        admin_shipp_termssitem527 = findViewById(R.id.admin_shipp_termssitem527)
        admin_shipp_termsdate527 = findViewById(R.id.admin_shipp_termsdate527)
        admin_shipp_termssitem528 = findViewById(R.id.admin_shipp_termssitem528)
        admin_shipp_termsdate528 = findViewById(R.id.admin_shipp_termsdate528)
        admin_shipp_termssitem529 = findViewById(R.id.admin_shipp_termssitem529)
        admin_shipp_termsdate529 = findViewById(R.id.admin_shipp_termsdate529)
        admin_shipp_termssitem5210 = findViewById(R.id.admin_shipp_termssitem5210)
        admin_shipp_termsdate5210 = findViewById(R.id.admin_shipp_termsdate5210)

        admin_shipping_terms5.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms5.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.GONE
                    ll_shipp_subterms523.visibility = View.GONE
                    ll_shipp_subterms524.visibility = View.GONE
                    ll_shipp_subterms525.visibility = View.GONE
                    ll_shipp_subterms526.visibility = View.GONE
                    ll_shipp_subterms527.visibility = View.GONE
                    ll_shipp_subterms528.visibility = View.GONE
                    ll_shipp_subterms529.visibility = View.GONE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.GONE
                    ll_shipp_subterms524.visibility = View.GONE
                    ll_shipp_subterms525.visibility = View.GONE
                    ll_shipp_subterms526.visibility = View.GONE
                    ll_shipp_subterms527.visibility = View.GONE
                    ll_shipp_subterms528.visibility = View.GONE
                    ll_shipp_subterms529.visibility = View.GONE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.VISIBLE
                    ll_shipp_subterms524.visibility = View.GONE
                    ll_shipp_subterms525.visibility = View.GONE
                    ll_shipp_subterms526.visibility = View.GONE
                    ll_shipp_subterms527.visibility = View.GONE
                    ll_shipp_subterms528.visibility = View.GONE
                    ll_shipp_subterms529.visibility = View.GONE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.VISIBLE
                    ll_shipp_subterms524.visibility = View.VISIBLE
                    ll_shipp_subterms525.visibility = View.GONE
                    ll_shipp_subterms526.visibility = View.GONE
                    ll_shipp_subterms527.visibility = View.GONE
                    ll_shipp_subterms528.visibility = View.GONE
                    ll_shipp_subterms529.visibility = View.GONE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.VISIBLE
                    ll_shipp_subterms524.visibility = View.VISIBLE
                    ll_shipp_subterms525.visibility = View.VISIBLE
                    ll_shipp_subterms526.visibility = View.GONE
                    ll_shipp_subterms527.visibility = View.GONE
                    ll_shipp_subterms528.visibility = View.GONE
                    ll_shipp_subterms529.visibility = View.GONE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.VISIBLE
                    ll_shipp_subterms524.visibility = View.VISIBLE
                    ll_shipp_subterms525.visibility = View.VISIBLE
                    ll_shipp_subterms526.visibility = View.VISIBLE
                    ll_shipp_subterms527.visibility = View.GONE
                    ll_shipp_subterms528.visibility = View.GONE
                    ll_shipp_subterms529.visibility = View.GONE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.VISIBLE
                    ll_shipp_subterms524.visibility = View.VISIBLE
                    ll_shipp_subterms525.visibility = View.VISIBLE
                    ll_shipp_subterms526.visibility = View.VISIBLE
                    ll_shipp_subterms527.visibility = View.VISIBLE
                    ll_shipp_subterms528.visibility = View.GONE
                    ll_shipp_subterms529.visibility = View.GONE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.VISIBLE
                    ll_shipp_subterms524.visibility = View.VISIBLE
                    ll_shipp_subterms525.visibility = View.VISIBLE
                    ll_shipp_subterms526.visibility = View.VISIBLE
                    ll_shipp_subterms527.visibility = View.VISIBLE
                    ll_shipp_subterms528.visibility = View.VISIBLE
                    ll_shipp_subterms529.visibility = View.GONE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.VISIBLE
                    ll_shipp_subterms524.visibility = View.VISIBLE
                    ll_shipp_subterms525.visibility = View.VISIBLE
                    ll_shipp_subterms526.visibility = View.VISIBLE
                    ll_shipp_subterms527.visibility = View.VISIBLE
                    ll_shipp_subterms528.visibility = View.VISIBLE
                    ll_shipp_subterms529.visibility = View.VISIBLE
                    ll_shipp_subterms5210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_521.visibility = View.VISIBLE
                    ll_shipp_subterms522.visibility = View.VISIBLE
                    ll_shipp_subterms523.visibility = View.VISIBLE
                    ll_shipp_subterms524.visibility = View.VISIBLE
                    ll_shipp_subterms525.visibility = View.VISIBLE
                    ll_shipp_subterms526.visibility = View.VISIBLE
                    ll_shipp_subterms527.visibility = View.VISIBLE
                    ll_shipp_subterms528.visibility = View.VISIBLE
                    ll_shipp_subterms529.visibility = View.VISIBLE
                    ll_shipp_subterms5210.visibility = View.VISIBLE
                }
                shipping_termsdate5 = ""
                shipping_qty5 = ""
                dialog_list.dismiss()
            }
        }

        admin_delivery_terms5 = findViewById(R.id.admin_delivery_terms5)
        ll_delivery_530 = findViewById(R.id.ll_delivery_530)
        ll_delivery_subterms_531 = findViewById(R.id.ll_delivery_subterms_531)
        ll_delivery_subterms532 = findViewById(R.id.ll_delivery_subterms532)
        ll_delivery_subterms533 = findViewById(R.id.ll_delivery_subterms533)
        ll_delivery_subterms534 = findViewById(R.id.ll_delivery_subterms534)
        ll_delivery_subterms535 = findViewById(R.id.ll_delivery_subterms535)
        ll_delivery_subterms536 = findViewById(R.id.ll_delivery_subterms536)
        ll_delivery_subterms537 = findViewById(R.id.ll_delivery_subterms537)
        ll_delivery_subterms538 = findViewById(R.id.ll_delivery_subterms538)
        ll_delivery_subterms539 = findViewById(R.id.ll_delivery_subterms539)
        ll_delivery_subterms5310 = findViewById(R.id.ll_delivery_subterms5310)
        admin_delivery_termssitem531 = findViewById(R.id.admin_delivery_termssitem531)
        admin_delivery_termsdate531 = findViewById(R.id.admin_delivery_termsdate531)
        admin_delivery_termssitem532 = findViewById(R.id.admin_delivery_termssitem532)
        admin_delivery_termsdate532 = findViewById(R.id.admin_delivery_termsdate532)
        admin_delivery_termssitem533 = findViewById(R.id.admin_delivery_termssitem533)
        admin_delivery_termsdate533 = findViewById(R.id.admin_delivery_termsdate533)
        admin_delivery_termssitem534 = findViewById(R.id.admin_delivery_termssitem534)
        admin_delivery_termsdate534 = findViewById(R.id.admin_delivery_termsdate534)
        admin_delivery_termssitem535 = findViewById(R.id.admin_delivery_termssitem535)
        admin_delivery_termsdate535 = findViewById(R.id.admin_delivery_termsdate535)
        admin_delivery_termssitem536 = findViewById(R.id.admin_delivery_termssitem536)
        admin_delivery_termsdate536 = findViewById(R.id.admin_delivery_termsdate536)
        admin_delivery_termssitem537 = findViewById(R.id.admin_delivery_termssitem537)
        admin_delivery_termsdate537 = findViewById(R.id.admin_delivery_termsdate537)
        admin_delivery_termssitem538 = findViewById(R.id.admin_delivery_termssitem538)
        admin_delivery_termsdate538 = findViewById(R.id.admin_delivery_termsdate538)
        admin_delivery_termssitem539 = findViewById(R.id.admin_delivery_termssitem539)
        admin_delivery_termsdate539 = findViewById(R.id.admin_delivery_termsdate539)
        admin_delivery_termssitem5310 = findViewById(R.id.admin_delivery_termssitem5310)
        admin_delivery_termsdate5310 = findViewById(R.id.admin_delivery_termsdate5310)

        admin_delivery_terms5.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms5.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.GONE
                    ll_delivery_subterms533.visibility = View.GONE
                    ll_delivery_subterms534.visibility = View.GONE
                    ll_delivery_subterms535.visibility = View.GONE
                    ll_delivery_subterms536.visibility = View.GONE
                    ll_delivery_subterms537.visibility = View.GONE
                    ll_delivery_subterms538.visibility = View.GONE
                    ll_delivery_subterms539.visibility = View.GONE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.GONE
                    ll_delivery_subterms534.visibility = View.GONE
                    ll_delivery_subterms535.visibility = View.GONE
                    ll_delivery_subterms536.visibility = View.GONE
                    ll_delivery_subterms537.visibility = View.GONE
                    ll_delivery_subterms538.visibility = View.GONE
                    ll_delivery_subterms539.visibility = View.GONE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.VISIBLE
                    ll_delivery_subterms534.visibility = View.GONE
                    ll_delivery_subterms535.visibility = View.GONE
                    ll_delivery_subterms536.visibility = View.GONE
                    ll_delivery_subterms537.visibility = View.GONE
                    ll_delivery_subterms538.visibility = View.GONE
                    ll_delivery_subterms539.visibility = View.GONE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.VISIBLE
                    ll_delivery_subterms534.visibility = View.VISIBLE
                    ll_delivery_subterms535.visibility = View.GONE
                    ll_delivery_subterms536.visibility = View.GONE
                    ll_delivery_subterms537.visibility = View.GONE
                    ll_delivery_subterms538.visibility = View.GONE
                    ll_delivery_subterms539.visibility = View.GONE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.VISIBLE
                    ll_delivery_subterms534.visibility = View.VISIBLE
                    ll_delivery_subterms535.visibility = View.VISIBLE
                    ll_delivery_subterms536.visibility = View.GONE
                    ll_delivery_subterms537.visibility = View.GONE
                    ll_delivery_subterms538.visibility = View.GONE
                    ll_delivery_subterms539.visibility = View.GONE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.VISIBLE
                    ll_delivery_subterms534.visibility = View.VISIBLE
                    ll_delivery_subterms535.visibility = View.VISIBLE
                    ll_delivery_subterms536.visibility = View.VISIBLE
                    ll_delivery_subterms537.visibility = View.GONE
                    ll_delivery_subterms538.visibility = View.GONE
                    ll_delivery_subterms539.visibility = View.GONE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.VISIBLE
                    ll_delivery_subterms534.visibility = View.VISIBLE
                    ll_delivery_subterms535.visibility = View.VISIBLE
                    ll_delivery_subterms536.visibility = View.VISIBLE
                    ll_delivery_subterms537.visibility = View.VISIBLE
                    ll_delivery_subterms538.visibility = View.GONE
                    ll_delivery_subterms539.visibility = View.GONE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.VISIBLE
                    ll_delivery_subterms534.visibility = View.VISIBLE
                    ll_delivery_subterms535.visibility = View.VISIBLE
                    ll_delivery_subterms536.visibility = View.VISIBLE
                    ll_delivery_subterms537.visibility = View.VISIBLE
                    ll_delivery_subterms538.visibility = View.VISIBLE
                    ll_delivery_subterms539.visibility = View.GONE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.VISIBLE
                    ll_delivery_subterms534.visibility = View.VISIBLE
                    ll_delivery_subterms535.visibility = View.VISIBLE
                    ll_delivery_subterms536.visibility = View.VISIBLE
                    ll_delivery_subterms537.visibility = View.VISIBLE
                    ll_delivery_subterms538.visibility = View.VISIBLE
                    ll_delivery_subterms539.visibility = View.VISIBLE
                    ll_delivery_subterms5310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_531.visibility = View.VISIBLE
                    ll_delivery_subterms532.visibility = View.VISIBLE
                    ll_delivery_subterms533.visibility = View.VISIBLE
                    ll_delivery_subterms534.visibility = View.VISIBLE
                    ll_delivery_subterms535.visibility = View.VISIBLE
                    ll_delivery_subterms536.visibility = View.VISIBLE
                    ll_delivery_subterms537.visibility = View.VISIBLE
                    ll_delivery_subterms538.visibility = View.VISIBLE
                    ll_delivery_subterms539.visibility = View.VISIBLE
                    ll_delivery_subterms5310.visibility = View.VISIBLE
                }
                delivery_termsdate5 = ""
                delivery_qty5 = ""
                dialog_list.dismiss()
            }
        }

        admin_shipp_termsdate521.setOnClickListener(this)
        admin_shipp_termsdate522.setOnClickListener(this)
        admin_shipp_termsdate523.setOnClickListener(this)
        admin_shipp_termsdate524.setOnClickListener(this)
        admin_shipp_termsdate525.setOnClickListener(this)
        admin_shipp_termsdate526.setOnClickListener(this)
        admin_shipp_termsdate527.setOnClickListener(this)
        admin_shipp_termsdate528.setOnClickListener(this)
        admin_shipp_termsdate529.setOnClickListener(this)
        admin_shipp_termsdate5210.setOnClickListener(this)

        admin_delivery_termsdate531.setOnClickListener(this)
        admin_delivery_termsdate532.setOnClickListener(this)
        admin_delivery_termsdate533.setOnClickListener(this)
        admin_delivery_termsdate534.setOnClickListener(this)
        admin_delivery_termsdate535.setOnClickListener(this)
        admin_delivery_termsdate536.setOnClickListener(this)
        admin_delivery_termsdate537.setOnClickListener(this)
        admin_delivery_termsdate538.setOnClickListener(this)
        admin_delivery_termsdate539.setOnClickListener(this)
        admin_delivery_termsdate5310.setOnClickListener(this)
    }

    private fun JobSixDropswidgets() {
        admin_shipping_terms6 = findViewById(R.id.admin_shipping_terms6)
        ll_shipp_620 = findViewById(R.id.ll_shipp_620)
        ll_shipping_six = findViewById(R.id.ll_shipping_six)
        ll_delivery_terms_6 = findViewById(R.id.ll_delivery_terms_6)
        ll_ship_subterms_621 = findViewById(R.id.ll_ship_subterms_621)
        ll_shipp_subterms622 = findViewById(R.id.ll_shipp_subterms622)
        ll_shipp_subterms623 = findViewById(R.id.ll_shipp_subterms623)
        ll_shipp_subterms624 = findViewById(R.id.ll_shipp_subterms624)
        ll_shipp_subterms625 = findViewById(R.id.ll_shipp_subterms625)
        ll_shipp_subterms626 = findViewById(R.id.ll_shipp_subterms626)
        ll_shipp_subterms627 = findViewById(R.id.ll_shipp_subterms627)
        ll_shipp_subterms628 = findViewById(R.id.ll_shipp_subterms628)
        ll_shipp_subterms629 = findViewById(R.id.ll_shipp_subterms629)
        ll_shipp_subterms6210 = findViewById(R.id.ll_shipp_subterms6210)
        admin_shipp_termssitem621 = findViewById(R.id.admin_shipp_termssitem621)
        admin_shipp_termsdate621 = findViewById(R.id.admin_shipp_termsdate621)
        admin_shipp_termssitem622 = findViewById(R.id.admin_shipp_termssitem622)
        admin_shipp_termsdate622 = findViewById(R.id.admin_shipp_termsdate622)
        admin_shipp_termssitem623 = findViewById(R.id.admin_shipp_termssitem623)
        admin_shipp_termsdate623 = findViewById(R.id.admin_shipp_termsdate623)
        admin_shipp_termssitem624 = findViewById(R.id.admin_shipp_termssitem624)
        admin_shipp_termsdate624 = findViewById(R.id.admin_shipp_termsdate624)
        admin_shipp_termssitem625 = findViewById(R.id.admin_shipp_termssitem625)
        admin_shipp_termsdate625 = findViewById(R.id.admin_shipp_termsdate625)
        admin_shipp_termssitem626 = findViewById(R.id.admin_shipp_termssitem626)
        admin_shipp_termsdate626 = findViewById(R.id.admin_shipp_termsdate626)
        admin_shipp_termssitem627 = findViewById(R.id.admin_shipp_termssitem627)
        admin_shipp_termsdate627 = findViewById(R.id.admin_shipp_termsdate627)
        admin_shipp_termssitem628 = findViewById(R.id.admin_shipp_termssitem628)
        admin_shipp_termsdate628 = findViewById(R.id.admin_shipp_termsdate628)
        admin_shipp_termssitem629 = findViewById(R.id.admin_shipp_termssitem629)
        admin_shipp_termsdate629 = findViewById(R.id.admin_shipp_termsdate629)
        admin_shipp_termssitem6210 = findViewById(R.id.admin_shipp_termssitem6210)
        admin_shipp_termsdate6210 = findViewById(R.id.admin_shipp_termsdate6210)

        admin_shipping_terms6.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms6.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.GONE
                    ll_shipp_subterms623.visibility = View.GONE
                    ll_shipp_subterms624.visibility = View.GONE
                    ll_shipp_subterms625.visibility = View.GONE
                    ll_shipp_subterms626.visibility = View.GONE
                    ll_shipp_subterms627.visibility = View.GONE
                    ll_shipp_subterms628.visibility = View.GONE
                    ll_shipp_subterms629.visibility = View.GONE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.GONE
                    ll_shipp_subterms624.visibility = View.GONE
                    ll_shipp_subterms625.visibility = View.GONE
                    ll_shipp_subterms626.visibility = View.GONE
                    ll_shipp_subterms627.visibility = View.GONE
                    ll_shipp_subterms628.visibility = View.GONE
                    ll_shipp_subterms629.visibility = View.GONE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.VISIBLE
                    ll_shipp_subterms624.visibility = View.GONE
                    ll_shipp_subterms625.visibility = View.GONE
                    ll_shipp_subterms626.visibility = View.GONE
                    ll_shipp_subterms627.visibility = View.GONE
                    ll_shipp_subterms628.visibility = View.GONE
                    ll_shipp_subterms629.visibility = View.GONE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.VISIBLE
                    ll_shipp_subterms624.visibility = View.VISIBLE
                    ll_shipp_subterms625.visibility = View.GONE
                    ll_shipp_subterms626.visibility = View.GONE
                    ll_shipp_subterms627.visibility = View.GONE
                    ll_shipp_subterms628.visibility = View.GONE
                    ll_shipp_subterms629.visibility = View.GONE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.VISIBLE
                    ll_shipp_subterms624.visibility = View.VISIBLE
                    ll_shipp_subterms625.visibility = View.VISIBLE
                    ll_shipp_subterms626.visibility = View.GONE
                    ll_shipp_subterms627.visibility = View.GONE
                    ll_shipp_subterms628.visibility = View.GONE
                    ll_shipp_subterms629.visibility = View.GONE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.VISIBLE
                    ll_shipp_subterms624.visibility = View.VISIBLE
                    ll_shipp_subterms625.visibility = View.VISIBLE
                    ll_shipp_subterms626.visibility = View.VISIBLE
                    ll_shipp_subterms627.visibility = View.GONE
                    ll_shipp_subterms628.visibility = View.GONE
                    ll_shipp_subterms629.visibility = View.GONE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.VISIBLE
                    ll_shipp_subterms624.visibility = View.VISIBLE
                    ll_shipp_subterms625.visibility = View.VISIBLE
                    ll_shipp_subterms626.visibility = View.VISIBLE
                    ll_shipp_subterms627.visibility = View.VISIBLE
                    ll_shipp_subterms628.visibility = View.GONE
                    ll_shipp_subterms629.visibility = View.GONE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.VISIBLE
                    ll_shipp_subterms624.visibility = View.VISIBLE
                    ll_shipp_subterms625.visibility = View.VISIBLE
                    ll_shipp_subterms626.visibility = View.VISIBLE
                    ll_shipp_subterms627.visibility = View.VISIBLE
                    ll_shipp_subterms628.visibility = View.VISIBLE
                    ll_shipp_subterms629.visibility = View.GONE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.VISIBLE
                    ll_shipp_subterms624.visibility = View.VISIBLE
                    ll_shipp_subterms625.visibility = View.VISIBLE
                    ll_shipp_subterms626.visibility = View.VISIBLE
                    ll_shipp_subterms627.visibility = View.VISIBLE
                    ll_shipp_subterms628.visibility = View.VISIBLE
                    ll_shipp_subterms629.visibility = View.VISIBLE
                    ll_shipp_subterms6210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_621.visibility = View.VISIBLE
                    ll_shipp_subterms622.visibility = View.VISIBLE
                    ll_shipp_subterms623.visibility = View.VISIBLE
                    ll_shipp_subterms624.visibility = View.VISIBLE
                    ll_shipp_subterms625.visibility = View.VISIBLE
                    ll_shipp_subterms626.visibility = View.VISIBLE
                    ll_shipp_subterms627.visibility = View.VISIBLE
                    ll_shipp_subterms628.visibility = View.VISIBLE
                    ll_shipp_subterms629.visibility = View.VISIBLE
                    ll_shipp_subterms6210.visibility = View.VISIBLE
                }
                shipping_termsdate6 = ""
                shipping_qty6 = ""
                dialog_list.dismiss()

            }
        }

        admin_delivery_terms6 = findViewById(R.id.admin_delivery_terms6)
        ll_delivery_630 = findViewById(R.id.ll_delivery_630)
        ll_delivery_subterms_631 = findViewById(R.id.ll_delivery_subterms_631)
        ll_delivery_subterms632 = findViewById(R.id.ll_delivery_subterms632)
        ll_delivery_subterms633 = findViewById(R.id.ll_delivery_subterms633)
        ll_delivery_subterms634 = findViewById(R.id.ll_delivery_subterms634)
        ll_delivery_subterms635 = findViewById(R.id.ll_delivery_subterms635)
        ll_delivery_subterms636 = findViewById(R.id.ll_delivery_subterms636)
        ll_delivery_subterms637 = findViewById(R.id.ll_delivery_subterms637)
        ll_delivery_subterms638 = findViewById(R.id.ll_delivery_subterms638)
        ll_delivery_subterms639 = findViewById(R.id.ll_delivery_subterms639)
        ll_delivery_subterms6310 = findViewById(R.id.ll_delivery_subterms6310)
        admin_delivery_termssitem631 = findViewById(R.id.admin_delivery_termssitem631)
        admin_delivery_termsdate631 = findViewById(R.id.admin_delivery_termsdate631)
        admin_delivery_termssitem632 = findViewById(R.id.admin_delivery_termssitem632)
        admin_delivery_termsdate632 = findViewById(R.id.admin_delivery_termsdate632)
        admin_delivery_termssitem633 = findViewById(R.id.admin_delivery_termssitem633)
        admin_delivery_termsdate633 = findViewById(R.id.admin_delivery_termsdate633)
        admin_delivery_termssitem634 = findViewById(R.id.admin_delivery_termssitem634)
        admin_delivery_termsdate634 = findViewById(R.id.admin_delivery_termsdate634)
        admin_delivery_termssitem635 = findViewById(R.id.admin_delivery_termssitem635)
        admin_delivery_termsdate635 = findViewById(R.id.admin_delivery_termsdate635)
        admin_delivery_termssitem636 = findViewById(R.id.admin_delivery_termssitem636)
        admin_delivery_termsdate636 = findViewById(R.id.admin_delivery_termsdate636)
        admin_delivery_termssitem637 = findViewById(R.id.admin_delivery_termssitem637)
        admin_delivery_termsdate637 = findViewById(R.id.admin_delivery_termsdate637)
        admin_delivery_termssitem638 = findViewById(R.id.admin_delivery_termssitem638)
        admin_delivery_termsdate638 = findViewById(R.id.admin_delivery_termsdate638)
        admin_delivery_termssitem639 = findViewById(R.id.admin_delivery_termssitem639)
        admin_delivery_termsdate639 = findViewById(R.id.admin_delivery_termsdate639)
        admin_delivery_termssitem6310 = findViewById(R.id.admin_delivery_termssitem6310)
        admin_delivery_termsdate6310 = findViewById(R.id.admin_delivery_termsdate6310)

        admin_delivery_terms6.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms6.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.GONE
                    ll_delivery_subterms633.visibility = View.GONE
                    ll_delivery_subterms634.visibility = View.GONE
                    ll_delivery_subterms635.visibility = View.GONE
                    ll_delivery_subterms636.visibility = View.GONE
                    ll_delivery_subterms637.visibility = View.GONE
                    ll_delivery_subterms638.visibility = View.GONE
                    ll_delivery_subterms639.visibility = View.GONE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.GONE
                    ll_delivery_subterms634.visibility = View.GONE
                    ll_delivery_subterms635.visibility = View.GONE
                    ll_delivery_subterms636.visibility = View.GONE
                    ll_delivery_subterms637.visibility = View.GONE
                    ll_delivery_subterms638.visibility = View.GONE
                    ll_delivery_subterms639.visibility = View.GONE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.VISIBLE
                    ll_delivery_subterms634.visibility = View.GONE
                    ll_delivery_subterms635.visibility = View.GONE
                    ll_delivery_subterms636.visibility = View.GONE
                    ll_delivery_subterms637.visibility = View.GONE
                    ll_delivery_subterms638.visibility = View.GONE
                    ll_delivery_subterms639.visibility = View.GONE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.VISIBLE
                    ll_delivery_subterms634.visibility = View.VISIBLE
                    ll_delivery_subterms635.visibility = View.GONE
                    ll_delivery_subterms636.visibility = View.GONE
                    ll_delivery_subterms637.visibility = View.GONE
                    ll_delivery_subterms638.visibility = View.GONE
                    ll_delivery_subterms639.visibility = View.GONE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.VISIBLE
                    ll_delivery_subterms634.visibility = View.VISIBLE
                    ll_delivery_subterms635.visibility = View.VISIBLE
                    ll_delivery_subterms636.visibility = View.GONE
                    ll_delivery_subterms637.visibility = View.GONE
                    ll_delivery_subterms638.visibility = View.GONE
                    ll_delivery_subterms639.visibility = View.GONE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.VISIBLE
                    ll_delivery_subterms634.visibility = View.VISIBLE
                    ll_delivery_subterms635.visibility = View.VISIBLE
                    ll_delivery_subterms636.visibility = View.VISIBLE
                    ll_delivery_subterms637.visibility = View.GONE
                    ll_delivery_subterms638.visibility = View.GONE
                    ll_delivery_subterms639.visibility = View.GONE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.VISIBLE
                    ll_delivery_subterms634.visibility = View.VISIBLE
                    ll_delivery_subterms635.visibility = View.VISIBLE
                    ll_delivery_subterms636.visibility = View.VISIBLE
                    ll_delivery_subterms637.visibility = View.VISIBLE
                    ll_delivery_subterms638.visibility = View.GONE
                    ll_delivery_subterms639.visibility = View.GONE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.VISIBLE
                    ll_delivery_subterms634.visibility = View.VISIBLE
                    ll_delivery_subterms635.visibility = View.VISIBLE
                    ll_delivery_subterms636.visibility = View.VISIBLE
                    ll_delivery_subterms637.visibility = View.VISIBLE
                    ll_delivery_subterms638.visibility = View.VISIBLE
                    ll_delivery_subterms639.visibility = View.GONE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.VISIBLE
                    ll_delivery_subterms634.visibility = View.VISIBLE
                    ll_delivery_subterms635.visibility = View.VISIBLE
                    ll_delivery_subterms636.visibility = View.VISIBLE
                    ll_delivery_subterms637.visibility = View.VISIBLE
                    ll_delivery_subterms638.visibility = View.VISIBLE
                    ll_delivery_subterms639.visibility = View.VISIBLE
                    ll_delivery_subterms6310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_631.visibility = View.VISIBLE
                    ll_delivery_subterms632.visibility = View.VISIBLE
                    ll_delivery_subterms633.visibility = View.VISIBLE
                    ll_delivery_subterms634.visibility = View.VISIBLE
                    ll_delivery_subterms635.visibility = View.VISIBLE
                    ll_delivery_subterms636.visibility = View.VISIBLE
                    ll_delivery_subterms637.visibility = View.VISIBLE
                    ll_delivery_subterms638.visibility = View.VISIBLE
                    ll_delivery_subterms639.visibility = View.VISIBLE
                    ll_delivery_subterms6310.visibility = View.VISIBLE
                }
                delivery_termsdate6 = ""
                delivery_qty6 = ""
                dialog_list.dismiss()

            }
        }

        admin_shipp_termsdate621.setOnClickListener(this)
        admin_shipp_termsdate622.setOnClickListener(this)
        admin_shipp_termsdate623.setOnClickListener(this)
        admin_shipp_termsdate624.setOnClickListener(this)
        admin_shipp_termsdate625.setOnClickListener(this)
        admin_shipp_termsdate626.setOnClickListener(this)
        admin_shipp_termsdate627.setOnClickListener(this)
        admin_shipp_termsdate628.setOnClickListener(this)
        admin_shipp_termsdate629.setOnClickListener(this)
        admin_shipp_termsdate6210.setOnClickListener(this)

        admin_delivery_termsdate631.setOnClickListener(this)
        admin_delivery_termsdate632.setOnClickListener(this)
        admin_delivery_termsdate633.setOnClickListener(this)
        admin_delivery_termsdate634.setOnClickListener(this)
        admin_delivery_termsdate635.setOnClickListener(this)
        admin_delivery_termsdate636.setOnClickListener(this)
        admin_delivery_termsdate637.setOnClickListener(this)
        admin_delivery_termsdate638.setOnClickListener(this)
        admin_delivery_termsdate639.setOnClickListener(this)
        admin_delivery_termsdate6310.setOnClickListener(this)
    }

    private fun JobSevenDropswidgets() {
        admin_shipping_terms7 = findViewById(R.id.admin_shipping_terms7)
        ll_shipp_720 = findViewById(R.id.ll_shipp_720)
        ll_shipping_seven = findViewById(R.id.ll_shipping_seven)
        ll_delivery_terms_7 = findViewById(R.id.ll_delivery_terms_7)
        ll_ship_subterms_721 = findViewById(R.id.ll_ship_subterms_721)
        ll_shipp_subterms722 = findViewById(R.id.ll_shipp_subterms722)
        ll_shipp_subterms723 = findViewById(R.id.ll_shipp_subterms723)
        ll_shipp_subterms724 = findViewById(R.id.ll_shipp_subterms724)
        ll_shipp_subterms725 = findViewById(R.id.ll_shipp_subterms725)
        ll_shipp_subterms726 = findViewById(R.id.ll_shipp_subterms726)
        ll_shipp_subterms727 = findViewById(R.id.ll_shipp_subterms727)
        ll_shipp_subterms728 = findViewById(R.id.ll_shipp_subterms728)
        ll_shipp_subterms729 = findViewById(R.id.ll_shipp_subterms729)
        ll_shipp_subterms7210 = findViewById(R.id.ll_shipp_subterms7210)
        admin_shipp_termssitem721 = findViewById(R.id.admin_shipp_termssitem721)
        admin_shipp_termsdate721 = findViewById(R.id.admin_shipp_termsdate721)
        admin_shipp_termssitem722 = findViewById(R.id.admin_shipp_termssitem722)
        admin_shipp_termsdate722 = findViewById(R.id.admin_shipp_termsdate722)
        admin_shipp_termssitem723 = findViewById(R.id.admin_shipp_termssitem723)
        admin_shipp_termsdate723 = findViewById(R.id.admin_shipp_termsdate723)
        admin_shipp_termssitem724 = findViewById(R.id.admin_shipp_termssitem724)
        admin_shipp_termsdate724 = findViewById(R.id.admin_shipp_termsdate724)
        admin_shipp_termssitem725 = findViewById(R.id.admin_shipp_termssitem725)
        admin_shipp_termsdate725 = findViewById(R.id.admin_shipp_termsdate725)
        admin_shipp_termssitem726 = findViewById(R.id.admin_shipp_termssitem726)
        admin_shipp_termsdate726 = findViewById(R.id.admin_shipp_termsdate726)
        admin_shipp_termssitem727 = findViewById(R.id.admin_shipp_termssitem727)
        admin_shipp_termsdate727 = findViewById(R.id.admin_shipp_termsdate727)
        admin_shipp_termssitem728 = findViewById(R.id.admin_shipp_termssitem728)
        admin_shipp_termsdate728 = findViewById(R.id.admin_shipp_termsdate728)
        admin_shipp_termssitem729 = findViewById(R.id.admin_shipp_termssitem729)
        admin_shipp_termsdate729 = findViewById(R.id.admin_shipp_termsdate729)
        admin_shipp_termssitem7210 = findViewById(R.id.admin_shipp_termssitem7210)
        admin_shipp_termsdate7210 = findViewById(R.id.admin_shipp_termsdate7210)
        admin_shipping_terms7.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms7.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.GONE
                    ll_shipp_subterms723.visibility = View.GONE
                    ll_shipp_subterms724.visibility = View.GONE
                    ll_shipp_subterms725.visibility = View.GONE
                    ll_shipp_subterms726.visibility = View.GONE
                    ll_shipp_subterms727.visibility = View.GONE
                    ll_shipp_subterms728.visibility = View.GONE
                    ll_shipp_subterms729.visibility = View.GONE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.GONE
                    ll_shipp_subterms724.visibility = View.GONE
                    ll_shipp_subterms725.visibility = View.GONE
                    ll_shipp_subterms726.visibility = View.GONE
                    ll_shipp_subterms727.visibility = View.GONE
                    ll_shipp_subterms728.visibility = View.GONE
                    ll_shipp_subterms729.visibility = View.GONE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.VISIBLE
                    ll_shipp_subterms724.visibility = View.GONE
                    ll_shipp_subterms725.visibility = View.GONE
                    ll_shipp_subterms726.visibility = View.GONE
                    ll_shipp_subterms727.visibility = View.GONE
                    ll_shipp_subterms728.visibility = View.GONE
                    ll_shipp_subterms729.visibility = View.GONE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.VISIBLE
                    ll_shipp_subterms724.visibility = View.VISIBLE
                    ll_shipp_subterms725.visibility = View.GONE
                    ll_shipp_subterms726.visibility = View.GONE
                    ll_shipp_subterms727.visibility = View.GONE
                    ll_shipp_subterms728.visibility = View.GONE
                    ll_shipp_subterms729.visibility = View.GONE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.VISIBLE
                    ll_shipp_subterms724.visibility = View.VISIBLE
                    ll_shipp_subterms725.visibility = View.VISIBLE
                    ll_shipp_subterms726.visibility = View.GONE
                    ll_shipp_subterms727.visibility = View.GONE
                    ll_shipp_subterms728.visibility = View.GONE
                    ll_shipp_subterms729.visibility = View.GONE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.VISIBLE
                    ll_shipp_subterms724.visibility = View.VISIBLE
                    ll_shipp_subterms725.visibility = View.VISIBLE
                    ll_shipp_subterms726.visibility = View.VISIBLE
                    ll_shipp_subterms727.visibility = View.GONE
                    ll_shipp_subterms728.visibility = View.GONE
                    ll_shipp_subterms729.visibility = View.GONE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.VISIBLE
                    ll_shipp_subterms724.visibility = View.VISIBLE
                    ll_shipp_subterms725.visibility = View.VISIBLE
                    ll_shipp_subterms726.visibility = View.VISIBLE
                    ll_shipp_subterms727.visibility = View.VISIBLE
                    ll_shipp_subterms728.visibility = View.GONE
                    ll_shipp_subterms729.visibility = View.GONE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.VISIBLE
                    ll_shipp_subterms724.visibility = View.VISIBLE
                    ll_shipp_subterms725.visibility = View.VISIBLE
                    ll_shipp_subterms726.visibility = View.VISIBLE
                    ll_shipp_subterms727.visibility = View.VISIBLE
                    ll_shipp_subterms728.visibility = View.VISIBLE
                    ll_shipp_subterms729.visibility = View.GONE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.VISIBLE
                    ll_shipp_subterms724.visibility = View.VISIBLE
                    ll_shipp_subterms725.visibility = View.VISIBLE
                    ll_shipp_subterms726.visibility = View.VISIBLE
                    ll_shipp_subterms727.visibility = View.VISIBLE
                    ll_shipp_subterms728.visibility = View.VISIBLE
                    ll_shipp_subterms729.visibility = View.VISIBLE
                    ll_shipp_subterms7210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_721.visibility = View.VISIBLE
                    ll_shipp_subterms722.visibility = View.VISIBLE
                    ll_shipp_subterms723.visibility = View.VISIBLE
                    ll_shipp_subterms724.visibility = View.VISIBLE
                    ll_shipp_subterms725.visibility = View.VISIBLE
                    ll_shipp_subterms726.visibility = View.VISIBLE
                    ll_shipp_subterms727.visibility = View.VISIBLE
                    ll_shipp_subterms728.visibility = View.VISIBLE
                    ll_shipp_subterms729.visibility = View.VISIBLE
                    ll_shipp_subterms7210.visibility = View.VISIBLE
                }
                shipping_termsdate7 = ""
                shipping_qty7 = ""
                dialog_list.dismiss()

            }
        }
        admin_delivery_terms7 = findViewById(R.id.admin_delivery_terms7)
        ll_delivery_730 = findViewById(R.id.ll_delivery_730)
        ll_delivery_subterms_731 = findViewById(R.id.ll_delivery_subterms_731)
        ll_delivery_subterms732 = findViewById(R.id.ll_delivery_subterms732)
        ll_delivery_subterms733 = findViewById(R.id.ll_delivery_subterms733)
        ll_delivery_subterms734 = findViewById(R.id.ll_delivery_subterms734)
        ll_delivery_subterms735 = findViewById(R.id.ll_delivery_subterms735)
        ll_delivery_subterms736 = findViewById(R.id.ll_delivery_subterms736)
        ll_delivery_subterms737 = findViewById(R.id.ll_delivery_subterms737)
        ll_delivery_subterms738 = findViewById(R.id.ll_delivery_subterms738)
        ll_delivery_subterms739 = findViewById(R.id.ll_delivery_subterms739)
        ll_delivery_subterms7310 = findViewById(R.id.ll_delivery_subterms7310)
        admin_delivery_termssitem731 = findViewById(R.id.admin_delivery_termssitem731)
        admin_delivery_termsdate731 = findViewById(R.id.admin_delivery_termsdate731)
        admin_delivery_termssitem732 = findViewById(R.id.admin_delivery_termssitem732)
        admin_delivery_termsdate732 = findViewById(R.id.admin_delivery_termsdate732)
        admin_delivery_termssitem733 = findViewById(R.id.admin_delivery_termssitem733)
        admin_delivery_termsdate733 = findViewById(R.id.admin_delivery_termsdate733)
        admin_delivery_termssitem734 = findViewById(R.id.admin_delivery_termssitem734)
        admin_delivery_termsdate734 = findViewById(R.id.admin_delivery_termsdate734)
        admin_delivery_termssitem735 = findViewById(R.id.admin_delivery_termssitem735)
        admin_delivery_termsdate735 = findViewById(R.id.admin_delivery_termsdate735)
        admin_delivery_termssitem736 = findViewById(R.id.admin_delivery_termssitem736)
        admin_delivery_termsdate736 = findViewById(R.id.admin_delivery_termsdate736)
        admin_delivery_termssitem737 = findViewById(R.id.admin_delivery_termssitem737)
        admin_delivery_termsdate737 = findViewById(R.id.admin_delivery_termsdate737)
        admin_delivery_termssitem738 = findViewById(R.id.admin_delivery_termssitem738)
        admin_delivery_termsdate738 = findViewById(R.id.admin_delivery_termsdate738)
        admin_delivery_termssitem739 = findViewById(R.id.admin_delivery_termssitem739)
        admin_delivery_termsdate739 = findViewById(R.id.admin_delivery_termsdate739)
        admin_delivery_termssitem7310 = findViewById(R.id.admin_delivery_termssitem7310)
        admin_delivery_termsdate7310 = findViewById(R.id.admin_delivery_termsdate7310)

        admin_delivery_terms7.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms7.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.GONE
                    ll_delivery_subterms733.visibility = View.GONE
                    ll_delivery_subterms734.visibility = View.GONE
                    ll_delivery_subterms735.visibility = View.GONE
                    ll_delivery_subterms736.visibility = View.GONE
                    ll_delivery_subterms737.visibility = View.GONE
                    ll_delivery_subterms738.visibility = View.GONE
                    ll_delivery_subterms739.visibility = View.GONE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.GONE
                    ll_delivery_subterms734.visibility = View.GONE
                    ll_delivery_subterms735.visibility = View.GONE
                    ll_delivery_subterms736.visibility = View.GONE
                    ll_delivery_subterms737.visibility = View.GONE
                    ll_delivery_subterms738.visibility = View.GONE
                    ll_delivery_subterms739.visibility = View.GONE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.VISIBLE
                    ll_delivery_subterms734.visibility = View.GONE
                    ll_delivery_subterms735.visibility = View.GONE
                    ll_delivery_subterms736.visibility = View.GONE
                    ll_delivery_subterms737.visibility = View.GONE
                    ll_delivery_subterms738.visibility = View.GONE
                    ll_delivery_subterms739.visibility = View.GONE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.VISIBLE
                    ll_delivery_subterms734.visibility = View.VISIBLE
                    ll_delivery_subterms735.visibility = View.GONE
                    ll_delivery_subterms736.visibility = View.GONE
                    ll_delivery_subterms737.visibility = View.GONE
                    ll_delivery_subterms738.visibility = View.GONE
                    ll_delivery_subterms739.visibility = View.GONE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.VISIBLE
                    ll_delivery_subterms734.visibility = View.VISIBLE
                    ll_delivery_subterms735.visibility = View.VISIBLE
                    ll_delivery_subterms736.visibility = View.GONE
                    ll_delivery_subterms737.visibility = View.GONE
                    ll_delivery_subterms738.visibility = View.GONE
                    ll_delivery_subterms739.visibility = View.GONE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.VISIBLE
                    ll_delivery_subterms734.visibility = View.VISIBLE
                    ll_delivery_subterms735.visibility = View.VISIBLE
                    ll_delivery_subterms736.visibility = View.VISIBLE
                    ll_delivery_subterms737.visibility = View.GONE
                    ll_delivery_subterms738.visibility = View.GONE
                    ll_delivery_subterms739.visibility = View.GONE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.VISIBLE
                    ll_delivery_subterms734.visibility = View.VISIBLE
                    ll_delivery_subterms735.visibility = View.VISIBLE
                    ll_delivery_subterms736.visibility = View.VISIBLE
                    ll_delivery_subterms737.visibility = View.VISIBLE
                    ll_delivery_subterms738.visibility = View.GONE
                    ll_delivery_subterms739.visibility = View.GONE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.VISIBLE
                    ll_delivery_subterms734.visibility = View.VISIBLE
                    ll_delivery_subterms735.visibility = View.VISIBLE
                    ll_delivery_subterms736.visibility = View.VISIBLE
                    ll_delivery_subterms737.visibility = View.VISIBLE
                    ll_delivery_subterms738.visibility = View.VISIBLE
                    ll_delivery_subterms739.visibility = View.GONE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.VISIBLE
                    ll_delivery_subterms734.visibility = View.VISIBLE
                    ll_delivery_subterms735.visibility = View.VISIBLE
                    ll_delivery_subterms736.visibility = View.VISIBLE
                    ll_delivery_subterms737.visibility = View.VISIBLE
                    ll_delivery_subterms738.visibility = View.VISIBLE
                    ll_delivery_subterms739.visibility = View.VISIBLE
                    ll_delivery_subterms7310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_731.visibility = View.VISIBLE
                    ll_delivery_subterms732.visibility = View.VISIBLE
                    ll_delivery_subterms733.visibility = View.VISIBLE
                    ll_delivery_subterms734.visibility = View.VISIBLE
                    ll_delivery_subterms735.visibility = View.VISIBLE
                    ll_delivery_subterms736.visibility = View.VISIBLE
                    ll_delivery_subterms737.visibility = View.VISIBLE
                    ll_delivery_subterms738.visibility = View.VISIBLE
                    ll_delivery_subterms739.visibility = View.VISIBLE
                    ll_delivery_subterms7310.visibility = View.VISIBLE
                }
                delivery_termsdate7 = ""
                delivery_qty7 = ""
                dialog_list.dismiss()

            }
        }
        admin_shipp_termsdate721.setOnClickListener(this)
        admin_shipp_termsdate722.setOnClickListener(this)
        admin_shipp_termsdate723.setOnClickListener(this)
        admin_shipp_termsdate724.setOnClickListener(this)
        admin_shipp_termsdate725.setOnClickListener(this)
        admin_shipp_termsdate726.setOnClickListener(this)
        admin_shipp_termsdate727.setOnClickListener(this)
        admin_shipp_termsdate728.setOnClickListener(this)
        admin_shipp_termsdate729.setOnClickListener(this)
        admin_shipp_termsdate7210.setOnClickListener(this)
        admin_delivery_termsdate731.setOnClickListener(this)
        admin_delivery_termsdate732.setOnClickListener(this)
        admin_delivery_termsdate733.setOnClickListener(this)
        admin_delivery_termsdate734.setOnClickListener(this)
        admin_delivery_termsdate735.setOnClickListener(this)
        admin_delivery_termsdate736.setOnClickListener(this)
        admin_delivery_termsdate737.setOnClickListener(this)
        admin_delivery_termsdate738.setOnClickListener(this)
        admin_delivery_termsdate739.setOnClickListener(this)
        admin_delivery_termsdate7310.setOnClickListener(this)
    }

    private fun JobEightDropswidgets() {
        admin_shipping_terms8 = findViewById(R.id.admin_shipping_terms8)
        ll_shipp_820 = findViewById(R.id.ll_shipp_820)
        ll_shipping_eight = findViewById(R.id.ll_shipping_eight)
        ll_delivery_terms_8 = findViewById(R.id.ll_delivery_terms_8)
        ll_ship_subterms_821 = findViewById(R.id.ll_ship_subterms_821)
        ll_shipp_subterms822 = findViewById(R.id.ll_shipp_subterms822)
        ll_shipp_subterms823 = findViewById(R.id.ll_shipp_subterms823)
        ll_shipp_subterms824 = findViewById(R.id.ll_shipp_subterms824)
        ll_shipp_subterms825 = findViewById(R.id.ll_shipp_subterms825)
        ll_shipp_subterms826 = findViewById(R.id.ll_shipp_subterms826)
        ll_shipp_subterms827 = findViewById(R.id.ll_shipp_subterms827)
        ll_shipp_subterms828 = findViewById(R.id.ll_shipp_subterms828)
        ll_shipp_subterms829 = findViewById(R.id.ll_shipp_subterms829)
        ll_shipp_subterms8210 = findViewById(R.id.ll_shipp_subterms8210)
        admin_shipp_termssitem821 = findViewById(R.id.admin_shipp_termssitem821)
        admin_shipp_termsdate821 = findViewById(R.id.admin_shipp_termsdate821)
        admin_shipp_termssitem822 = findViewById(R.id.admin_shipp_termssitem822)
        admin_shipp_termsdate822 = findViewById(R.id.admin_shipp_termsdate822)
        admin_shipp_termssitem823 = findViewById(R.id.admin_shipp_termssitem823)
        admin_shipp_termsdate823 = findViewById(R.id.admin_shipp_termsdate823)
        admin_shipp_termssitem824 = findViewById(R.id.admin_shipp_termssitem824)
        admin_shipp_termsdate824 = findViewById(R.id.admin_shipp_termsdate824)
        admin_shipp_termssitem825 = findViewById(R.id.admin_shipp_termssitem825)
        admin_shipp_termsdate825 = findViewById(R.id.admin_shipp_termsdate825)
        admin_shipp_termssitem826 = findViewById(R.id.admin_shipp_termssitem826)
        admin_shipp_termsdate826 = findViewById(R.id.admin_shipp_termsdate826)
        admin_shipp_termssitem827 = findViewById(R.id.admin_shipp_termssitem827)
        admin_shipp_termsdate827 = findViewById(R.id.admin_shipp_termsdate827)
        admin_shipp_termssitem828 = findViewById(R.id.admin_shipp_termssitem828)
        admin_shipp_termsdate828 = findViewById(R.id.admin_shipp_termsdate828)
        admin_shipp_termssitem829 = findViewById(R.id.admin_shipp_termssitem829)
        admin_shipp_termsdate829 = findViewById(R.id.admin_shipp_termsdate829)
        admin_shipp_termssitem8210 = findViewById(R.id.admin_shipp_termssitem8210)
        admin_shipp_termsdate8210 = findViewById(R.id.admin_shipp_termsdate8210)
        admin_shipping_terms8.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms8.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.GONE
                    ll_shipp_subterms823.visibility = View.GONE
                    ll_shipp_subterms824.visibility = View.GONE
                    ll_shipp_subterms825.visibility = View.GONE
                    ll_shipp_subterms826.visibility = View.GONE
                    ll_shipp_subterms827.visibility = View.GONE
                    ll_shipp_subterms828.visibility = View.GONE
                    ll_shipp_subterms829.visibility = View.GONE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.GONE
                    ll_shipp_subterms824.visibility = View.GONE
                    ll_shipp_subterms825.visibility = View.GONE
                    ll_shipp_subterms826.visibility = View.GONE
                    ll_shipp_subterms827.visibility = View.GONE
                    ll_shipp_subterms828.visibility = View.GONE
                    ll_shipp_subterms829.visibility = View.GONE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.VISIBLE
                    ll_shipp_subterms824.visibility = View.GONE
                    ll_shipp_subterms825.visibility = View.GONE
                    ll_shipp_subterms826.visibility = View.GONE
                    ll_shipp_subterms827.visibility = View.GONE
                    ll_shipp_subterms828.visibility = View.GONE
                    ll_shipp_subterms829.visibility = View.GONE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.VISIBLE
                    ll_shipp_subterms824.visibility = View.VISIBLE
                    ll_shipp_subterms825.visibility = View.GONE
                    ll_shipp_subterms826.visibility = View.GONE
                    ll_shipp_subterms827.visibility = View.GONE
                    ll_shipp_subterms828.visibility = View.GONE
                    ll_shipp_subterms829.visibility = View.GONE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.VISIBLE
                    ll_shipp_subterms824.visibility = View.VISIBLE
                    ll_shipp_subterms825.visibility = View.VISIBLE
                    ll_shipp_subterms826.visibility = View.GONE
                    ll_shipp_subterms827.visibility = View.GONE
                    ll_shipp_subterms828.visibility = View.GONE
                    ll_shipp_subterms829.visibility = View.GONE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.VISIBLE
                    ll_shipp_subterms824.visibility = View.VISIBLE
                    ll_shipp_subterms825.visibility = View.VISIBLE
                    ll_shipp_subterms826.visibility = View.VISIBLE
                    ll_shipp_subterms827.visibility = View.GONE
                    ll_shipp_subterms828.visibility = View.GONE
                    ll_shipp_subterms829.visibility = View.GONE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.VISIBLE
                    ll_shipp_subterms824.visibility = View.VISIBLE
                    ll_shipp_subterms825.visibility = View.VISIBLE
                    ll_shipp_subterms826.visibility = View.VISIBLE
                    ll_shipp_subterms827.visibility = View.VISIBLE
                    ll_shipp_subterms828.visibility = View.GONE
                    ll_shipp_subterms829.visibility = View.GONE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.VISIBLE
                    ll_shipp_subterms824.visibility = View.VISIBLE
                    ll_shipp_subterms825.visibility = View.VISIBLE
                    ll_shipp_subterms826.visibility = View.VISIBLE
                    ll_shipp_subterms827.visibility = View.VISIBLE
                    ll_shipp_subterms828.visibility = View.VISIBLE
                    ll_shipp_subterms829.visibility = View.GONE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.VISIBLE
                    ll_shipp_subterms824.visibility = View.VISIBLE
                    ll_shipp_subterms825.visibility = View.VISIBLE
                    ll_shipp_subterms826.visibility = View.VISIBLE
                    ll_shipp_subterms827.visibility = View.VISIBLE
                    ll_shipp_subterms828.visibility = View.VISIBLE
                    ll_shipp_subterms829.visibility = View.VISIBLE
                    ll_shipp_subterms8210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_821.visibility = View.VISIBLE
                    ll_shipp_subterms822.visibility = View.VISIBLE
                    ll_shipp_subterms823.visibility = View.VISIBLE
                    ll_shipp_subterms824.visibility = View.VISIBLE
                    ll_shipp_subterms825.visibility = View.VISIBLE
                    ll_shipp_subterms826.visibility = View.VISIBLE
                    ll_shipp_subterms827.visibility = View.VISIBLE
                    ll_shipp_subterms828.visibility = View.VISIBLE
                    ll_shipp_subterms829.visibility = View.VISIBLE
                    ll_shipp_subterms8210.visibility = View.VISIBLE
                }
                shipping_termsdate8 = ""
                shipping_qty8 = ""
                dialog_list.dismiss()
            }
        }

        admin_delivery_terms8 = findViewById(R.id.admin_delivery_terms8)
        ll_delivery_830 = findViewById(R.id.ll_delivery_830)
        ll_delivery_subterms_831 = findViewById(R.id.ll_delivery_subterms_831)
        ll_delivery_subterms832 = findViewById(R.id.ll_delivery_subterms832)
        ll_delivery_subterms833 = findViewById(R.id.ll_delivery_subterms833)
        ll_delivery_subterms834 = findViewById(R.id.ll_delivery_subterms834)
        ll_delivery_subterms835 = findViewById(R.id.ll_delivery_subterms835)
        ll_delivery_subterms836 = findViewById(R.id.ll_delivery_subterms836)
        ll_delivery_subterms837 = findViewById(R.id.ll_delivery_subterms837)
        ll_delivery_subterms838 = findViewById(R.id.ll_delivery_subterms838)
        ll_delivery_subterms839 = findViewById(R.id.ll_delivery_subterms839)
        ll_delivery_subterms8310 = findViewById(R.id.ll_delivery_subterms8310)
        admin_delivery_termssitem831 = findViewById(R.id.admin_delivery_termssitem831)
        admin_delivery_termsdate831 = findViewById(R.id.admin_delivery_termsdate831)
        admin_delivery_termssitem832 = findViewById(R.id.admin_delivery_termssitem832)
        admin_delivery_termsdate832 = findViewById(R.id.admin_delivery_termsdate832)
        admin_delivery_termssitem833 = findViewById(R.id.admin_delivery_termssitem833)
        admin_delivery_termsdate833 = findViewById(R.id.admin_delivery_termsdate833)
        admin_delivery_termssitem834 = findViewById(R.id.admin_delivery_termssitem834)
        admin_delivery_termsdate834 = findViewById(R.id.admin_delivery_termsdate834)
        admin_delivery_termssitem835 = findViewById(R.id.admin_delivery_termssitem835)
        admin_delivery_termsdate835 = findViewById(R.id.admin_delivery_termsdate835)
        admin_delivery_termssitem836 = findViewById(R.id.admin_delivery_termssitem836)
        admin_delivery_termsdate836 = findViewById(R.id.admin_delivery_termsdate836)
        admin_delivery_termssitem837 = findViewById(R.id.admin_delivery_termssitem837)
        admin_delivery_termsdate837 = findViewById(R.id.admin_delivery_termsdate837)
        admin_delivery_termssitem838 = findViewById(R.id.admin_delivery_termssitem838)
        admin_delivery_termsdate838 = findViewById(R.id.admin_delivery_termsdate838)
        admin_delivery_termssitem839 = findViewById(R.id.admin_delivery_termssitem839)
        admin_delivery_termsdate839 = findViewById(R.id.admin_delivery_termsdate839)
        admin_delivery_termssitem8310 = findViewById(R.id.admin_delivery_termssitem8310)
        admin_delivery_termsdate8310 = findViewById(R.id.admin_delivery_termsdate8310)
        admin_delivery_terms8.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms8.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.GONE
                    ll_delivery_subterms833.visibility = View.GONE
                    ll_delivery_subterms834.visibility = View.GONE
                    ll_delivery_subterms835.visibility = View.GONE
                    ll_delivery_subterms836.visibility = View.GONE
                    ll_delivery_subterms837.visibility = View.GONE
                    ll_delivery_subterms838.visibility = View.GONE
                    ll_delivery_subterms839.visibility = View.GONE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.GONE
                    ll_delivery_subterms834.visibility = View.GONE
                    ll_delivery_subterms835.visibility = View.GONE
                    ll_delivery_subterms836.visibility = View.GONE
                    ll_delivery_subterms837.visibility = View.GONE
                    ll_delivery_subterms838.visibility = View.GONE
                    ll_delivery_subterms839.visibility = View.GONE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.VISIBLE
                    ll_delivery_subterms834.visibility = View.GONE
                    ll_delivery_subterms835.visibility = View.GONE
                    ll_delivery_subterms836.visibility = View.GONE
                    ll_delivery_subterms837.visibility = View.GONE
                    ll_delivery_subterms838.visibility = View.GONE
                    ll_delivery_subterms839.visibility = View.GONE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.VISIBLE
                    ll_delivery_subterms834.visibility = View.VISIBLE
                    ll_delivery_subterms835.visibility = View.GONE
                    ll_delivery_subterms836.visibility = View.GONE
                    ll_delivery_subterms837.visibility = View.GONE
                    ll_delivery_subterms838.visibility = View.GONE
                    ll_delivery_subterms839.visibility = View.GONE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.VISIBLE
                    ll_delivery_subterms834.visibility = View.VISIBLE
                    ll_delivery_subterms835.visibility = View.VISIBLE
                    ll_delivery_subterms836.visibility = View.GONE
                    ll_delivery_subterms837.visibility = View.GONE
                    ll_delivery_subterms838.visibility = View.GONE
                    ll_delivery_subterms839.visibility = View.GONE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.VISIBLE
                    ll_delivery_subterms834.visibility = View.VISIBLE
                    ll_delivery_subterms835.visibility = View.VISIBLE
                    ll_delivery_subterms836.visibility = View.VISIBLE
                    ll_delivery_subterms837.visibility = View.GONE
                    ll_delivery_subterms838.visibility = View.GONE
                    ll_delivery_subterms839.visibility = View.GONE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.VISIBLE
                    ll_delivery_subterms834.visibility = View.VISIBLE
                    ll_delivery_subterms835.visibility = View.VISIBLE
                    ll_delivery_subterms836.visibility = View.VISIBLE
                    ll_delivery_subterms837.visibility = View.VISIBLE
                    ll_delivery_subterms838.visibility = View.GONE
                    ll_delivery_subterms839.visibility = View.GONE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.VISIBLE
                    ll_delivery_subterms834.visibility = View.VISIBLE
                    ll_delivery_subterms835.visibility = View.VISIBLE
                    ll_delivery_subterms836.visibility = View.VISIBLE
                    ll_delivery_subterms837.visibility = View.VISIBLE
                    ll_delivery_subterms838.visibility = View.VISIBLE
                    ll_delivery_subterms839.visibility = View.GONE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.VISIBLE
                    ll_delivery_subterms834.visibility = View.VISIBLE
                    ll_delivery_subterms835.visibility = View.VISIBLE
                    ll_delivery_subterms836.visibility = View.VISIBLE
                    ll_delivery_subterms837.visibility = View.VISIBLE
                    ll_delivery_subterms838.visibility = View.VISIBLE
                    ll_delivery_subterms839.visibility = View.VISIBLE
                    ll_delivery_subterms8310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_831.visibility = View.VISIBLE
                    ll_delivery_subterms832.visibility = View.VISIBLE
                    ll_delivery_subterms833.visibility = View.VISIBLE
                    ll_delivery_subterms834.visibility = View.VISIBLE
                    ll_delivery_subterms835.visibility = View.VISIBLE
                    ll_delivery_subterms836.visibility = View.VISIBLE
                    ll_delivery_subterms837.visibility = View.VISIBLE
                    ll_delivery_subterms838.visibility = View.VISIBLE
                    ll_delivery_subterms839.visibility = View.VISIBLE
                    ll_delivery_subterms8310.visibility = View.VISIBLE
                }
                delivery_termsdate8 = ""
                delivery_qty8 = ""
                dialog_list.dismiss()
            }
        }
        admin_shipp_termsdate821.setOnClickListener(this)
        admin_shipp_termsdate822.setOnClickListener(this)
        admin_shipp_termsdate823.setOnClickListener(this)
        admin_shipp_termsdate824.setOnClickListener(this)
        admin_shipp_termsdate825.setOnClickListener(this)
        admin_shipp_termsdate826.setOnClickListener(this)
        admin_shipp_termsdate827.setOnClickListener(this)
        admin_shipp_termsdate828.setOnClickListener(this)
        admin_shipp_termsdate829.setOnClickListener(this)
        admin_shipp_termsdate8210.setOnClickListener(this)
        admin_delivery_termsdate831.setOnClickListener(this)
        admin_delivery_termsdate832.setOnClickListener(this)
        admin_delivery_termsdate833.setOnClickListener(this)
        admin_delivery_termsdate834.setOnClickListener(this)
        admin_delivery_termsdate835.setOnClickListener(this)
        admin_delivery_termsdate836.setOnClickListener(this)
        admin_delivery_termsdate837.setOnClickListener(this)
        admin_delivery_termsdate838.setOnClickListener(this)
        admin_delivery_termsdate839.setOnClickListener(this)
        admin_delivery_termsdate8310.setOnClickListener(this)
    }
    private fun JobNineDropswidgets() {
        admin_shipping_terms9 = findViewById(R.id.admin_shipping_terms9)
        ll_shipp_920 = findViewById(R.id.ll_shipp_920)
        ll_shipping_nine = findViewById(R.id.ll_shipping_nine)
        ll_delivery_terms_9 = findViewById(R.id.ll_delivery_terms_9)
        ll_ship_subterms_921 = findViewById(R.id.ll_ship_subterms_921)
        ll_shipp_subterms922 = findViewById(R.id.ll_shipp_subterms922)
        ll_shipp_subterms923 = findViewById(R.id.ll_shipp_subterms923)
        ll_shipp_subterms924 = findViewById(R.id.ll_shipp_subterms924)
        ll_shipp_subterms925 = findViewById(R.id.ll_shipp_subterms925)
        ll_shipp_subterms926 = findViewById(R.id.ll_shipp_subterms926)
        ll_shipp_subterms927 = findViewById(R.id.ll_shipp_subterms927)
        ll_shipp_subterms928 = findViewById(R.id.ll_shipp_subterms928)
        ll_shipp_subterms929 = findViewById(R.id.ll_shipp_subterms929)
        ll_shipp_subterms9210 = findViewById(R.id.ll_shipp_subterms9210)
        admin_shipp_termssitem921 = findViewById(R.id.admin_shipp_termssitem921)
        admin_shipp_termsdate921 = findViewById(R.id.admin_shipp_termsdate921)
        admin_shipp_termssitem922 = findViewById(R.id.admin_shipp_termssitem922)
        admin_shipp_termsdate922 = findViewById(R.id.admin_shipp_termsdate922)
        admin_shipp_termssitem923 = findViewById(R.id.admin_shipp_termssitem923)
        admin_shipp_termsdate923 = findViewById(R.id.admin_shipp_termsdate923)
        admin_shipp_termssitem924 = findViewById(R.id.admin_shipp_termssitem924)
        admin_shipp_termsdate924 = findViewById(R.id.admin_shipp_termsdate924)
        admin_shipp_termssitem925 = findViewById(R.id.admin_shipp_termssitem925)
        admin_shipp_termsdate925 = findViewById(R.id.admin_shipp_termsdate925)
        admin_shipp_termssitem926 = findViewById(R.id.admin_shipp_termssitem926)
        admin_shipp_termsdate926 = findViewById(R.id.admin_shipp_termsdate926)
        admin_shipp_termssitem927 = findViewById(R.id.admin_shipp_termssitem927)
        admin_shipp_termsdate927 = findViewById(R.id.admin_shipp_termsdate927)
        admin_shipp_termssitem928 = findViewById(R.id.admin_shipp_termssitem928)
        admin_shipp_termsdate928 = findViewById(R.id.admin_shipp_termsdate928)
        admin_shipp_termssitem929 = findViewById(R.id.admin_shipp_termssitem929)
        admin_shipp_termsdate929 = findViewById(R.id.admin_shipp_termsdate929)
        admin_shipp_termssitem9210 = findViewById(R.id.admin_shipp_termssitem9210)
        admin_shipp_termsdate9210 = findViewById(R.id.admin_shipp_termsdate9210)
        admin_shipping_terms9.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms9.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.GONE
                    ll_shipp_subterms923.visibility = View.GONE
                    ll_shipp_subterms924.visibility = View.GONE
                    ll_shipp_subterms925.visibility = View.GONE
                    ll_shipp_subterms926.visibility = View.GONE
                    ll_shipp_subterms927.visibility = View.GONE
                    ll_shipp_subterms928.visibility = View.GONE
                    ll_shipp_subterms929.visibility = View.GONE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.GONE
                    ll_shipp_subterms924.visibility = View.GONE
                    ll_shipp_subterms925.visibility = View.GONE
                    ll_shipp_subterms926.visibility = View.GONE
                    ll_shipp_subterms927.visibility = View.GONE
                    ll_shipp_subterms928.visibility = View.GONE
                    ll_shipp_subterms929.visibility = View.GONE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.VISIBLE
                    ll_shipp_subterms924.visibility = View.GONE
                    ll_shipp_subterms925.visibility = View.GONE
                    ll_shipp_subterms926.visibility = View.GONE
                    ll_shipp_subterms927.visibility = View.GONE
                    ll_shipp_subterms928.visibility = View.GONE
                    ll_shipp_subterms929.visibility = View.GONE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.VISIBLE
                    ll_shipp_subterms924.visibility = View.VISIBLE
                    ll_shipp_subterms925.visibility = View.GONE
                    ll_shipp_subterms926.visibility = View.GONE
                    ll_shipp_subterms927.visibility = View.GONE
                    ll_shipp_subterms928.visibility = View.GONE
                    ll_shipp_subterms929.visibility = View.GONE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.VISIBLE
                    ll_shipp_subterms924.visibility = View.VISIBLE
                    ll_shipp_subterms925.visibility = View.VISIBLE
                    ll_shipp_subterms926.visibility = View.GONE
                    ll_shipp_subterms927.visibility = View.GONE
                    ll_shipp_subterms928.visibility = View.GONE
                    ll_shipp_subterms929.visibility = View.GONE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.VISIBLE
                    ll_shipp_subterms924.visibility = View.VISIBLE
                    ll_shipp_subterms925.visibility = View.VISIBLE
                    ll_shipp_subterms926.visibility = View.VISIBLE
                    ll_shipp_subterms927.visibility = View.GONE
                    ll_shipp_subterms928.visibility = View.GONE
                    ll_shipp_subterms929.visibility = View.GONE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.VISIBLE
                    ll_shipp_subterms924.visibility = View.VISIBLE
                    ll_shipp_subterms925.visibility = View.VISIBLE
                    ll_shipp_subterms926.visibility = View.VISIBLE
                    ll_shipp_subterms927.visibility = View.VISIBLE
                    ll_shipp_subterms928.visibility = View.GONE
                    ll_shipp_subterms929.visibility = View.GONE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.VISIBLE
                    ll_shipp_subterms924.visibility = View.VISIBLE
                    ll_shipp_subterms925.visibility = View.VISIBLE
                    ll_shipp_subterms926.visibility = View.VISIBLE
                    ll_shipp_subterms927.visibility = View.VISIBLE
                    ll_shipp_subterms928.visibility = View.VISIBLE
                    ll_shipp_subterms929.visibility = View.GONE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.VISIBLE
                    ll_shipp_subterms924.visibility = View.VISIBLE
                    ll_shipp_subterms925.visibility = View.VISIBLE
                    ll_shipp_subterms926.visibility = View.VISIBLE
                    ll_shipp_subterms927.visibility = View.VISIBLE
                    ll_shipp_subterms928.visibility = View.VISIBLE
                    ll_shipp_subterms929.visibility = View.VISIBLE
                    ll_shipp_subterms9210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_921.visibility = View.VISIBLE
                    ll_shipp_subterms922.visibility = View.VISIBLE
                    ll_shipp_subterms923.visibility = View.VISIBLE
                    ll_shipp_subterms924.visibility = View.VISIBLE
                    ll_shipp_subterms925.visibility = View.VISIBLE
                    ll_shipp_subterms926.visibility = View.VISIBLE
                    ll_shipp_subterms927.visibility = View.VISIBLE
                    ll_shipp_subterms928.visibility = View.VISIBLE
                    ll_shipp_subterms929.visibility = View.VISIBLE
                    ll_shipp_subterms9210.visibility = View.VISIBLE
                }
                shipping_termsdate9 = ""
                shipping_qty9 = ""
                dialog_list.dismiss()
            }
        }
        admin_delivery_terms9 = findViewById(R.id.admin_delivery_terms9)
        ll_delivery_930 = findViewById(R.id.ll_delivery_930)
        ll_delivery_subterms_931 = findViewById(R.id.ll_delivery_subterms_931)
        ll_delivery_subterms932 = findViewById(R.id.ll_delivery_subterms932)
        ll_delivery_subterms933 = findViewById(R.id.ll_delivery_subterms933)
        ll_delivery_subterms934 = findViewById(R.id.ll_delivery_subterms934)
        ll_delivery_subterms935 = findViewById(R.id.ll_delivery_subterms935)
        ll_delivery_subterms936 = findViewById(R.id.ll_delivery_subterms936)
        ll_delivery_subterms937 = findViewById(R.id.ll_delivery_subterms937)
        ll_delivery_subterms938 = findViewById(R.id.ll_delivery_subterms938)
        ll_delivery_subterms939 = findViewById(R.id.ll_delivery_subterms939)
        ll_delivery_subterms9310 = findViewById(R.id.ll_delivery_subterms9310)
        admin_delivery_termssitem931 = findViewById(R.id.admin_delivery_termssitem931)
        admin_delivery_termsdate931 = findViewById(R.id.admin_delivery_termsdate931)
        admin_delivery_termssitem932 = findViewById(R.id.admin_delivery_termssitem932)
        admin_delivery_termsdate932 = findViewById(R.id.admin_delivery_termsdate932)
        admin_delivery_termssitem933 = findViewById(R.id.admin_delivery_termssitem933)
        admin_delivery_termsdate933 = findViewById(R.id.admin_delivery_termsdate933)
        admin_delivery_termssitem934 = findViewById(R.id.admin_delivery_termssitem934)
        admin_delivery_termsdate934 = findViewById(R.id.admin_delivery_termsdate934)
        admin_delivery_termssitem935 = findViewById(R.id.admin_delivery_termssitem935)
        admin_delivery_termsdate935 = findViewById(R.id.admin_delivery_termsdate935)
        admin_delivery_termssitem936 = findViewById(R.id.admin_delivery_termssitem936)
        admin_delivery_termsdate936 = findViewById(R.id.admin_delivery_termsdate936)
        admin_delivery_termssitem937 = findViewById(R.id.admin_delivery_termssitem937)
        admin_delivery_termsdate937 = findViewById(R.id.admin_delivery_termsdate937)
        admin_delivery_termssitem938 = findViewById(R.id.admin_delivery_termssitem938)
        admin_delivery_termsdate938 = findViewById(R.id.admin_delivery_termsdate938)
        admin_delivery_termssitem939 = findViewById(R.id.admin_delivery_termssitem939)
        admin_delivery_termsdate939 = findViewById(R.id.admin_delivery_termsdate939)
        admin_delivery_termssitem9310 = findViewById(R.id.admin_delivery_termssitem9310)
        admin_delivery_termsdate9310 = findViewById(R.id.admin_delivery_termsdate9310)
        admin_delivery_terms9.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms9.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.GONE
                    ll_delivery_subterms933.visibility = View.GONE
                    ll_delivery_subterms934.visibility = View.GONE
                    ll_delivery_subterms935.visibility = View.GONE
                    ll_delivery_subterms936.visibility = View.GONE
                    ll_delivery_subterms937.visibility = View.GONE
                    ll_delivery_subterms938.visibility = View.GONE
                    ll_delivery_subterms939.visibility = View.GONE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.GONE
                    ll_delivery_subterms934.visibility = View.GONE
                    ll_delivery_subterms935.visibility = View.GONE
                    ll_delivery_subterms936.visibility = View.GONE
                    ll_delivery_subterms937.visibility = View.GONE
                    ll_delivery_subterms938.visibility = View.GONE
                    ll_delivery_subterms939.visibility = View.GONE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.VISIBLE
                    ll_delivery_subterms934.visibility = View.GONE
                    ll_delivery_subterms935.visibility = View.GONE
                    ll_delivery_subterms936.visibility = View.GONE
                    ll_delivery_subterms937.visibility = View.GONE
                    ll_delivery_subterms938.visibility = View.GONE
                    ll_delivery_subterms939.visibility = View.GONE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.VISIBLE
                    ll_delivery_subterms934.visibility = View.VISIBLE
                    ll_delivery_subterms935.visibility = View.GONE
                    ll_delivery_subterms936.visibility = View.GONE
                    ll_delivery_subterms937.visibility = View.GONE
                    ll_delivery_subterms938.visibility = View.GONE
                    ll_delivery_subterms939.visibility = View.GONE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.VISIBLE
                    ll_delivery_subterms934.visibility = View.VISIBLE
                    ll_delivery_subterms935.visibility = View.VISIBLE
                    ll_delivery_subterms936.visibility = View.GONE
                    ll_delivery_subterms937.visibility = View.GONE
                    ll_delivery_subterms938.visibility = View.GONE
                    ll_delivery_subterms939.visibility = View.GONE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.VISIBLE
                    ll_delivery_subterms934.visibility = View.VISIBLE
                    ll_delivery_subterms935.visibility = View.VISIBLE
                    ll_delivery_subterms936.visibility = View.VISIBLE
                    ll_delivery_subterms937.visibility = View.GONE
                    ll_delivery_subterms938.visibility = View.GONE
                    ll_delivery_subterms939.visibility = View.GONE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.VISIBLE
                    ll_delivery_subterms934.visibility = View.VISIBLE
                    ll_delivery_subterms935.visibility = View.VISIBLE
                    ll_delivery_subterms936.visibility = View.VISIBLE
                    ll_delivery_subterms937.visibility = View.VISIBLE
                    ll_delivery_subterms938.visibility = View.GONE
                    ll_delivery_subterms939.visibility = View.GONE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.VISIBLE
                    ll_delivery_subterms934.visibility = View.VISIBLE
                    ll_delivery_subterms935.visibility = View.VISIBLE
                    ll_delivery_subterms936.visibility = View.VISIBLE
                    ll_delivery_subterms937.visibility = View.VISIBLE
                    ll_delivery_subterms938.visibility = View.VISIBLE
                    ll_delivery_subterms939.visibility = View.GONE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.VISIBLE
                    ll_delivery_subterms934.visibility = View.VISIBLE
                    ll_delivery_subterms935.visibility = View.VISIBLE
                    ll_delivery_subterms936.visibility = View.VISIBLE
                    ll_delivery_subterms937.visibility = View.VISIBLE
                    ll_delivery_subterms938.visibility = View.VISIBLE
                    ll_delivery_subterms939.visibility = View.VISIBLE
                    ll_delivery_subterms9310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_931.visibility = View.VISIBLE
                    ll_delivery_subterms932.visibility = View.VISIBLE
                    ll_delivery_subterms933.visibility = View.VISIBLE
                    ll_delivery_subterms934.visibility = View.VISIBLE
                    ll_delivery_subterms935.visibility = View.VISIBLE
                    ll_delivery_subterms936.visibility = View.VISIBLE
                    ll_delivery_subterms937.visibility = View.VISIBLE
                    ll_delivery_subterms938.visibility = View.VISIBLE
                    ll_delivery_subterms939.visibility = View.VISIBLE
                    ll_delivery_subterms9310.visibility = View.VISIBLE
                }
                delivery_termsdate9 = ""
                delivery_qty9 = ""
                dialog_list.dismiss()
            }
        }
        admin_shipp_termsdate921.setOnClickListener(this)
        admin_shipp_termsdate922.setOnClickListener(this)
        admin_shipp_termsdate923.setOnClickListener(this)
        admin_shipp_termsdate924.setOnClickListener(this)
        admin_shipp_termsdate925.setOnClickListener(this)
        admin_shipp_termsdate926.setOnClickListener(this)
        admin_shipp_termsdate927.setOnClickListener(this)
        admin_shipp_termsdate928.setOnClickListener(this)
        admin_shipp_termsdate929.setOnClickListener(this)
        admin_shipp_termsdate9210.setOnClickListener(this)
        admin_delivery_termsdate931.setOnClickListener(this)
        admin_delivery_termsdate932.setOnClickListener(this)
        admin_delivery_termsdate933.setOnClickListener(this)
        admin_delivery_termsdate934.setOnClickListener(this)
        admin_delivery_termsdate935.setOnClickListener(this)
        admin_delivery_termsdate936.setOnClickListener(this)
        admin_delivery_termsdate937.setOnClickListener(this)
        admin_delivery_termsdate938.setOnClickListener(this)
        admin_delivery_termsdate939.setOnClickListener(this)
        admin_delivery_termsdate9310.setOnClickListener(this)
    }
    private fun JobTenDropswidgets() {
        admin_shipping_terms10 = findViewById(R.id.admin_shipping_terms10)
        ll_shipp_1020 = findViewById(R.id.ll_shipp_1020)
        ll_shipping_ten = findViewById(R.id.ll_shipping_ten)
        ll_delivery_terms_10 = findViewById(R.id.ll_delivery_terms_10)
        ll_ship_subterms_1021 = findViewById(R.id.ll_ship_subterms_1021)
        ll_shipp_subterms1022 = findViewById(R.id.ll_shipp_subterms1022)
        ll_shipp_subterms1023 = findViewById(R.id.ll_shipp_subterms1023)
        ll_shipp_subterms1024 = findViewById(R.id.ll_shipp_subterms1024)
        ll_shipp_subterms1025 = findViewById(R.id.ll_shipp_subterms1025)
        ll_shipp_subterms1026 = findViewById(R.id.ll_shipp_subterms1026)
        ll_shipp_subterms1027 = findViewById(R.id.ll_shipp_subterms1027)
        ll_shipp_subterms1028 = findViewById(R.id.ll_shipp_subterms1028)
        ll_shipp_subterms1029 = findViewById(R.id.ll_shipp_subterms1029)
        ll_shipp_subterms10210 = findViewById(R.id.ll_shipp_subterms10210)
        admin_shipp_termssitem1021 = findViewById(R.id.admin_shipp_termssitem1021)
        admin_shipp_termsdate1021 = findViewById(R.id.admin_shipp_termsdate1021)
        admin_shipp_termssitem1022 = findViewById(R.id.admin_shipp_termssitem1022)
        admin_shipp_termsdate1022 = findViewById(R.id.admin_shipp_termsdate1022)
        admin_shipp_termssitem1023 = findViewById(R.id.admin_shipp_termssitem1023)
        admin_shipp_termsdate1023 = findViewById(R.id.admin_shipp_termsdate1023)
        admin_shipp_termssitem1024 = findViewById(R.id.admin_shipp_termssitem1024)
        admin_shipp_termsdate1024 = findViewById(R.id.admin_shipp_termsdate1024)
        admin_shipp_termssitem1025 = findViewById(R.id.admin_shipp_termssitem1025)
        admin_shipp_termsdate1025 = findViewById(R.id.admin_shipp_termsdate1025)
        admin_shipp_termssitem1026 = findViewById(R.id.admin_shipp_termssitem1026)
        admin_shipp_termsdate1026 = findViewById(R.id.admin_shipp_termsdate1026)
        admin_shipp_termssitem1027 = findViewById(R.id.admin_shipp_termssitem1027)
        admin_shipp_termsdate1027 = findViewById(R.id.admin_shipp_termsdate1027)
        admin_shipp_termssitem1028 = findViewById(R.id.admin_shipp_termssitem1028)
        admin_shipp_termsdate1028 = findViewById(R.id.admin_shipp_termsdate1028)
        admin_shipp_termssitem1029 = findViewById(R.id.admin_shipp_termssitem1029)
        admin_shipp_termsdate1029 = findViewById(R.id.admin_shipp_termsdate1029)
        admin_shipp_termssitem10210 = findViewById(R.id.admin_shipp_termssitem10210)
        admin_shipp_termsdate10210 = findViewById(R.id.admin_shipp_termsdate10210)
        admin_shipping_terms10.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_shipping_terms10.setText(selected_name)
                if (selected_name == "1") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.GONE
                    ll_shipp_subterms1023.visibility = View.GONE
                    ll_shipp_subterms1024.visibility = View.GONE
                    ll_shipp_subterms1025.visibility = View.GONE
                    ll_shipp_subterms1026.visibility = View.GONE
                    ll_shipp_subterms1027.visibility = View.GONE
                    ll_shipp_subterms1028.visibility = View.GONE
                    ll_shipp_subterms1029.visibility = View.GONE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.GONE
                    ll_shipp_subterms1024.visibility = View.GONE
                    ll_shipp_subterms1025.visibility = View.GONE
                    ll_shipp_subterms1026.visibility = View.GONE
                    ll_shipp_subterms1027.visibility = View.GONE
                    ll_shipp_subterms1028.visibility = View.GONE
                    ll_shipp_subterms1029.visibility = View.GONE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.VISIBLE
                    ll_shipp_subterms1024.visibility = View.GONE
                    ll_shipp_subterms1025.visibility = View.GONE
                    ll_shipp_subterms1026.visibility = View.GONE
                    ll_shipp_subterms1027.visibility = View.GONE
                    ll_shipp_subterms1028.visibility = View.GONE
                    ll_shipp_subterms1029.visibility = View.GONE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.VISIBLE
                    ll_shipp_subterms1024.visibility = View.VISIBLE
                    ll_shipp_subterms1025.visibility = View.GONE
                    ll_shipp_subterms1026.visibility = View.GONE
                    ll_shipp_subterms1027.visibility = View.GONE
                    ll_shipp_subterms1028.visibility = View.GONE
                    ll_shipp_subterms1029.visibility = View.GONE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.VISIBLE
                    ll_shipp_subterms1024.visibility = View.VISIBLE
                    ll_shipp_subterms1025.visibility = View.VISIBLE
                    ll_shipp_subterms1026.visibility = View.GONE
                    ll_shipp_subterms1027.visibility = View.GONE
                    ll_shipp_subterms1028.visibility = View.GONE
                    ll_shipp_subterms1029.visibility = View.GONE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.VISIBLE
                    ll_shipp_subterms1024.visibility = View.VISIBLE
                    ll_shipp_subterms1025.visibility = View.VISIBLE
                    ll_shipp_subterms1026.visibility = View.VISIBLE
                    ll_shipp_subterms1027.visibility = View.GONE
                    ll_shipp_subterms1028.visibility = View.GONE
                    ll_shipp_subterms1029.visibility = View.GONE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.VISIBLE
                    ll_shipp_subterms1024.visibility = View.VISIBLE
                    ll_shipp_subterms1025.visibility = View.VISIBLE
                    ll_shipp_subterms1026.visibility = View.VISIBLE
                    ll_shipp_subterms1027.visibility = View.VISIBLE
                    ll_shipp_subterms1028.visibility = View.GONE
                    ll_shipp_subterms1029.visibility = View.GONE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.VISIBLE
                    ll_shipp_subterms1024.visibility = View.VISIBLE
                    ll_shipp_subterms1025.visibility = View.VISIBLE
                    ll_shipp_subterms1026.visibility = View.VISIBLE
                    ll_shipp_subterms1027.visibility = View.VISIBLE
                    ll_shipp_subterms1028.visibility = View.VISIBLE
                    ll_shipp_subterms1029.visibility = View.GONE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.VISIBLE
                    ll_shipp_subterms1024.visibility = View.VISIBLE
                    ll_shipp_subterms1025.visibility = View.VISIBLE
                    ll_shipp_subterms1026.visibility = View.VISIBLE
                    ll_shipp_subterms1027.visibility = View.VISIBLE
                    ll_shipp_subterms1028.visibility = View.VISIBLE
                    ll_shipp_subterms1029.visibility = View.VISIBLE
                    ll_shipp_subterms10210.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_ship_subterms_1021.visibility = View.VISIBLE
                    ll_shipp_subterms1022.visibility = View.VISIBLE
                    ll_shipp_subterms1023.visibility = View.VISIBLE
                    ll_shipp_subterms1024.visibility = View.VISIBLE
                    ll_shipp_subterms1025.visibility = View.VISIBLE
                    ll_shipp_subterms1026.visibility = View.VISIBLE
                    ll_shipp_subterms1027.visibility = View.VISIBLE
                    ll_shipp_subterms1028.visibility = View.VISIBLE
                    ll_shipp_subterms1029.visibility = View.VISIBLE
                    ll_shipp_subterms10210.visibility = View.VISIBLE
                }
                shipping_termsdate10 = ""
                shipping_qty10 = ""
                dialog_list.dismiss()
            }
        }
        admin_delivery_terms10 = findViewById(R.id.admin_delivery_terms10)
        ll_delivery_1030 = findViewById(R.id.ll_delivery_1030)
        ll_delivery_subterms_1031 = findViewById(R.id.ll_delivery_subterms_1031)
        ll_delivery_subterms1032 = findViewById(R.id.ll_delivery_subterms1032)
        ll_delivery_subterms1033 = findViewById(R.id.ll_delivery_subterms1033)
        ll_delivery_subterms1034 = findViewById(R.id.ll_delivery_subterms1034)
        ll_delivery_subterms1035 = findViewById(R.id.ll_delivery_subterms1035)
        ll_delivery_subterms1036 = findViewById(R.id.ll_delivery_subterms1036)
        ll_delivery_subterms1037 = findViewById(R.id.ll_delivery_subterms1037)
        ll_delivery_subterms1038 = findViewById(R.id.ll_delivery_subterms1038)
        ll_delivery_subterms1039 = findViewById(R.id.ll_delivery_subterms1039)
        ll_delivery_subterms10310 = findViewById(R.id.ll_delivery_subterms10310)
        admin_delivery_termssitem1031 = findViewById(R.id.admin_delivery_termssitem1031)
        admin_delivery_termsdate1031 = findViewById(R.id.admin_delivery_termsdate1031)
        admin_delivery_termssitem1032 = findViewById(R.id.admin_delivery_termssitem1032)
        admin_delivery_termsdate1032 = findViewById(R.id.admin_delivery_termsdate1032)
        admin_delivery_termssitem1033 = findViewById(R.id.admin_delivery_termssitem1033)
        admin_delivery_termsdate1033 = findViewById(R.id.admin_delivery_termsdate1033)
        admin_delivery_termssitem1034 = findViewById(R.id.admin_delivery_termssitem1034)
        admin_delivery_termsdate1034 = findViewById(R.id.admin_delivery_termsdate1034)
        admin_delivery_termssitem1035 = findViewById(R.id.admin_delivery_termssitem1035)
        admin_delivery_termsdate1035 = findViewById(R.id.admin_delivery_termsdate1035)
        admin_delivery_termssitem1036 = findViewById(R.id.admin_delivery_termssitem1036)
        admin_delivery_termsdate1036 = findViewById(R.id.admin_delivery_termsdate1036)
        admin_delivery_termssitem1037 = findViewById(R.id.admin_delivery_termssitem1037)
        admin_delivery_termsdate1037 = findViewById(R.id.admin_delivery_termsdate1037)
        admin_delivery_termssitem1038 = findViewById(R.id.admin_delivery_termssitem1038)
        admin_delivery_termsdate1038 = findViewById(R.id.admin_delivery_termsdate1038)
        admin_delivery_termssitem1039 = findViewById(R.id.admin_delivery_termssitem1039)
        admin_delivery_termsdate1039 = findViewById(R.id.admin_delivery_termsdate1039)
        admin_delivery_termssitem10310 = findViewById(R.id.admin_delivery_termssitem10310)
        admin_delivery_termsdate10310 = findViewById(R.id.admin_delivery_termsdate10310)
        admin_delivery_terms10.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter(this@AddJobActivity, R.layout.simple_spinner_item, array2)
            list_items_list2.adapter = state_array_adapter
            list_items_list2.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val selected_name = list_items_list2.getItemAtPosition(i).toString()
                admin_delivery_terms10.setText(selected_name)
                if (selected_name == "1") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.GONE
                    ll_delivery_subterms1033.visibility = View.GONE
                    ll_delivery_subterms1034.visibility = View.GONE
                    ll_delivery_subterms1035.visibility = View.GONE
                    ll_delivery_subterms1036.visibility = View.GONE
                    ll_delivery_subterms1037.visibility = View.GONE
                    ll_delivery_subterms1038.visibility = View.GONE
                    ll_delivery_subterms1039.visibility = View.GONE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "2") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.GONE
                    ll_delivery_subterms1034.visibility = View.GONE
                    ll_delivery_subterms1035.visibility = View.GONE
                    ll_delivery_subterms1036.visibility = View.GONE
                    ll_delivery_subterms1037.visibility = View.GONE
                    ll_delivery_subterms1038.visibility = View.GONE
                    ll_delivery_subterms1039.visibility = View.GONE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "3") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.VISIBLE
                    ll_delivery_subterms1034.visibility = View.GONE
                    ll_delivery_subterms1035.visibility = View.GONE
                    ll_delivery_subterms1036.visibility = View.GONE
                    ll_delivery_subterms1037.visibility = View.GONE
                    ll_delivery_subterms1038.visibility = View.GONE
                    ll_delivery_subterms1039.visibility = View.GONE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "4") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.VISIBLE
                    ll_delivery_subterms1034.visibility = View.VISIBLE
                    ll_delivery_subterms1035.visibility = View.GONE
                    ll_delivery_subterms1036.visibility = View.GONE
                    ll_delivery_subterms1037.visibility = View.GONE
                    ll_delivery_subterms1038.visibility = View.GONE
                    ll_delivery_subterms1039.visibility = View.GONE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "5") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.VISIBLE
                    ll_delivery_subterms1034.visibility = View.VISIBLE
                    ll_delivery_subterms1035.visibility = View.VISIBLE
                    ll_delivery_subterms1036.visibility = View.GONE
                    ll_delivery_subterms1037.visibility = View.GONE
                    ll_delivery_subterms1038.visibility = View.GONE
                    ll_delivery_subterms1039.visibility = View.GONE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "6") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.VISIBLE
                    ll_delivery_subterms1034.visibility = View.VISIBLE
                    ll_delivery_subterms1035.visibility = View.VISIBLE
                    ll_delivery_subterms1036.visibility = View.VISIBLE
                    ll_delivery_subterms1037.visibility = View.GONE
                    ll_delivery_subterms1038.visibility = View.GONE
                    ll_delivery_subterms1039.visibility = View.GONE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "7") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.VISIBLE
                    ll_delivery_subterms1034.visibility = View.VISIBLE
                    ll_delivery_subterms1035.visibility = View.VISIBLE
                    ll_delivery_subterms1036.visibility = View.VISIBLE
                    ll_delivery_subterms1037.visibility = View.VISIBLE
                    ll_delivery_subterms1038.visibility = View.GONE
                    ll_delivery_subterms1039.visibility = View.GONE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "8") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.VISIBLE
                    ll_delivery_subterms1034.visibility = View.VISIBLE
                    ll_delivery_subterms1035.visibility = View.VISIBLE
                    ll_delivery_subterms1036.visibility = View.VISIBLE
                    ll_delivery_subterms1037.visibility = View.VISIBLE
                    ll_delivery_subterms1038.visibility = View.VISIBLE
                    ll_delivery_subterms1039.visibility = View.GONE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "9") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.VISIBLE
                    ll_delivery_subterms1034.visibility = View.VISIBLE
                    ll_delivery_subterms1035.visibility = View.VISIBLE
                    ll_delivery_subterms1036.visibility = View.VISIBLE
                    ll_delivery_subterms1037.visibility = View.VISIBLE
                    ll_delivery_subterms1038.visibility = View.VISIBLE
                    ll_delivery_subterms1039.visibility = View.VISIBLE
                    ll_delivery_subterms10310.visibility = View.GONE
                } else if (selected_name == "10") {
                    ll_delivery_subterms_1031.visibility = View.VISIBLE
                    ll_delivery_subterms1032.visibility = View.VISIBLE
                    ll_delivery_subterms1033.visibility = View.VISIBLE
                    ll_delivery_subterms1034.visibility = View.VISIBLE
                    ll_delivery_subterms1035.visibility = View.VISIBLE
                    ll_delivery_subterms1036.visibility = View.VISIBLE
                    ll_delivery_subterms1037.visibility = View.VISIBLE
                    ll_delivery_subterms1038.visibility = View.VISIBLE
                    ll_delivery_subterms1039.visibility = View.VISIBLE
                    ll_delivery_subterms10310.visibility = View.VISIBLE
                }
                delivery_termsdate10 = ""
                delivery_qty10 = ""
                dialog_list.dismiss()
            }
        }
        admin_shipp_termsdate1021.setOnClickListener(this)
        admin_shipp_termsdate1022.setOnClickListener(this)
        admin_shipp_termsdate1023.setOnClickListener(this)
        admin_shipp_termsdate1024.setOnClickListener(this)
        admin_shipp_termsdate1025.setOnClickListener(this)
        admin_shipp_termsdate1026.setOnClickListener(this)
        admin_shipp_termsdate1027.setOnClickListener(this)
        admin_shipp_termsdate1028.setOnClickListener(this)
        admin_shipp_termsdate1029.setOnClickListener(this)
        admin_shipp_termsdate10210.setOnClickListener(this)
        admin_delivery_termsdate1031.setOnClickListener(this)
        admin_delivery_termsdate1032.setOnClickListener(this)
        admin_delivery_termsdate1033.setOnClickListener(this)
        admin_delivery_termsdate1034.setOnClickListener(this)
        admin_delivery_termsdate1035.setOnClickListener(this)
        admin_delivery_termsdate1036.setOnClickListener(this)
        admin_delivery_termsdate1037.setOnClickListener(this)
        admin_delivery_termsdate1038.setOnClickListener(this)
        admin_delivery_termsdate1039.setOnClickListener(this)
        admin_delivery_termsdate10310.setOnClickListener(this)
    }
    val deptarray = JSONArray()
    val taskarray = JSONArray()
    private fun createJSONArray() {
        val jsonArray = JSONObject()
        val jsonArray1 = JSONArray()
        val jsonArray2 = JSONArray()
        val jsonArray3 = JSONArray()
        val jsonArray4 = JSONArray()
        val jsonArray5 = JSONArray()
        val jsonArray6 = JSONArray()
        val jsonArray7 = JSONArray()
        val jsonArray8 = JSONArray()
        val jsonArray9 = JSONArray()
        val jsonArray10 = JSONArray()
        for (i in 0 until al1.size) {
            jsonArray1.put(i, al1.get(i))
        }
        for (i in 0 until al2.size) {
            jsonArray2.put(i, al2.get(i))
        }
        for (i in 0 until al3.size) {
            jsonArray3.put(i, al3.get(i))
        }
        for (i in 0 until al4.size) {
            jsonArray4.put(i, al4.get(i))
        }
        for (i in 0 until al5.size) {
            jsonArray5.put(i, al5.get(i))
        }
        for (i in 0 until al6.size) {
            jsonArray6.put(i, al6.get(i))
        }
        for (i in 0 until al7.size) {
            jsonArray7.put(i, al7.get(i))
        }
        for (i in 0 until al8.size) {
            jsonArray8.put(i, al8.get(i))
        }
        for (i in 0 until al9.size) {
            jsonArray9.put(i, al9.get(i))
        }
        for (i in 0 until al10.size) {
            jsonArray10.put(i, al10.get(i))
        }
        if (final_job_parts.equals("1")) {
            val jsonArray2 = JSONArray()
            val jsonArray3 = JSONArray()
            val jsonArray4 = JSONArray()
            val jsonArray5 = JSONArray()
            val jsonArray6 = JSONArray()
            val jsonArray7 = JSONArray()
            val jsonArray8 = JSONArray()
            val jsonArray9 = JSONArray()
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("2")) {
            val jsonArray3 = JSONArray()
            val jsonArray4 = JSONArray()
            val jsonArray5 = JSONArray()
            val jsonArray6 = JSONArray()
            val jsonArray7 = JSONArray()
            val jsonArray8 = JSONArray()
            val jsonArray9 = JSONArray()
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("3")) {
            val jsonArray4 = JSONArray()
            val jsonArray5 = JSONArray()
            val jsonArray6 = JSONArray()
            val jsonArray7 = JSONArray()
            val jsonArray8 = JSONArray()
            val jsonArray9 = JSONArray()
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("4")) {
            val jsonArray5 = JSONArray()
            val jsonArray6 = JSONArray()
            val jsonArray7 = JSONArray()
            val jsonArray8 = JSONArray()
            val jsonArray9 = JSONArray()
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("5")) {
            val jsonArray6 = JSONArray()
            val jsonArray7 = JSONArray()
            val jsonArray8 = JSONArray()
            val jsonArray9 = JSONArray()
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("6")) {
            val jsonArray7 = JSONArray()
            val jsonArray8 = JSONArray()
            val jsonArray9 = JSONArray()
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("7")) {
            val jsonArray8 = JSONArray()
            val jsonArray9 = JSONArray()
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("8")) {
            val jsonArray9 = JSONArray()
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("9")) {
            val jsonArray10 = JSONArray()
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        } else if (final_job_parts.equals("5")) {
            deptarray.put(0, jsonArray1)
            deptarray.put(1, jsonArray2)
            deptarray.put(2, jsonArray3)
            deptarray.put(3, jsonArray4)
            deptarray.put(4, jsonArray5)
            deptarray.put(5, jsonArray6)
            deptarray.put(6, jsonArray7)
            deptarray.put(7, jsonArray8)
            deptarray.put(8, jsonArray9)
            deptarray.put(9, jsonArray10)
        }
        val jsonArray11 = JSONArray()
        val jsonArray12 = JSONArray()
        val jsonArray13 = JSONArray()
        val jsonArray14 = JSONArray()
        val jsonArray15 = JSONArray()
        val jsonArray16 = JSONArray()
        val jsonArray17 = JSONArray()
        val jsonArray18 = JSONArray()
        val jsonArray19 = JSONArray()
        val jsonArray110 = JSONArray()
        for (i in 0 until mSingleTasklistandCat.size) {
            jsonArray11.put(i, mSingleTasklistandCat.get(i))
        }
        for (i in 0 until mSingleTasklistandCat2.size) {
            jsonArray12.put(i, mSingleTasklistandCat2.get(i))
        }
        for (i in 0 until mSingleTasklistandCat3.size) {
            jsonArray13.put(i, mSingleTasklistandCat3.get(i))
        }
        for (i in 0 until mSingleTasklistandCat4.size) {
            jsonArray14.put(i, mSingleTasklistandCat4.get(i))
        }
        for (i in 0 until mSingleTasklistandCat5.size) {
            jsonArray15.put(i, mSingleTasklistandCat5.get(i))
        }
        for (i in 0 until mSingleTasklistandCat6.size) {
            jsonArray16.put(i, mSingleTasklistandCat6.get(i))
        }
        for (i in 0 until mSingleTasklistandCat7.size) {
            jsonArray17.put(i, mSingleTasklistandCat7.get(i))
        }
        for (i in 0 until mSingleTasklistandCat8.size) {
            jsonArray18.put(i, mSingleTasklistandCat8.get(i))
        }
        for (i in 0 until mSingleTasklistandCat10.size) {
            jsonArray19.put(i, mSingleTasklistandCat10.get(i))
        }
        for (i in 0 until mSingleTasklistandCat10.size) {
            jsonArray110.put(i, mSingleTasklistandCat10.get(i))
        }
        if (final_job_parts.equals("1")) {
            val jsonArray12 = JSONArray()
            val jsonArray13 = JSONArray()
            val jsonArray14 = JSONArray()
            val jsonArray15 = JSONArray()
            val jsonArray16 = JSONArray()
            val jsonArray17 = JSONArray()
            val jsonArray18 = JSONArray()
            val jsonArray19 = JSONArray()
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("2")) {
            val jsonArray13 = JSONArray()
            val jsonArray14 = JSONArray()
            val jsonArray15 = JSONArray()
            val jsonArray16 = JSONArray()
            val jsonArray17 = JSONArray()
            val jsonArray18 = JSONArray()
            val jsonArray19 = JSONArray()
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("3")) {
            val jsonArray14 = JSONArray()
            val jsonArray15 = JSONArray()
            val jsonArray16 = JSONArray()
            val jsonArray17 = JSONArray()
            val jsonArray18 = JSONArray()
            val jsonArray19 = JSONArray()
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("4")) {
            val jsonArray15 = JSONArray()
            val jsonArray16 = JSONArray()
            val jsonArray17 = JSONArray()
            val jsonArray18 = JSONArray()
            val jsonArray19 = JSONArray()
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("5")) {
            val jsonArray16 = JSONArray()
            val jsonArray17 = JSONArray()
            val jsonArray18 = JSONArray()
            val jsonArray19 = JSONArray()
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("6")) {
            val jsonArray17 = JSONArray()
            val jsonArray18 = JSONArray()
            val jsonArray19 = JSONArray()
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("7")) {
            val jsonArray18 = JSONArray()
            val jsonArray19 = JSONArray()
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("8")) {
            val jsonArray19 = JSONArray()
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("9")) {
            val jsonArray110 = JSONArray()
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        } else if (final_job_parts.equals("10")) {
            taskarray.put(0, jsonArray11)
            taskarray.put(1, jsonArray12)
            taskarray.put(2, jsonArray13)
            taskarray.put(3, jsonArray14)
            taskarray.put(4, jsonArray15)
            taskarray.put(5, jsonArray16)
            taskarray.put(6, jsonArray17)
            taskarray.put(7, jsonArray18)
            taskarray.put(8, jsonArray19)
            taskarray.put(9, jsonArray110)
        }
        jsonArray.put("department_ids", deptarray)
        jsonArray.put("task_ids", taskarray)
        termsdate = admin_termsdate1.text.toString() + "," + admin_termsdate2.text.toString() + "," + admin_termsdate3.text.toString() + "," +
                "" + admin_termsdate4.text.toString() + "," + admin_termsdate5.text.toString() + "," + admin_termsdate6.text.toString() + "," + admin_termsdate7.text.toString() + "," +
                "" + admin_termsdate8.text.toString() + "," + admin_termsdate9.text.toString() + "," + "" + admin_termsdate10.text.toString() + ","
        termsdate1 = admin_termsdate11.text.toString() + "," + admin_termsdate12.text.toString() + "," + admin_termsdate13.text.toString() + "," +
                "" + admin_termsdate14.text.toString() + "," + admin_termsdate15.text.toString() + "," + admin_termsdate16.text.toString() + "," + admin_termsdate17.text.toString() + "," +
                "" + admin_termsdate18.text.toString() + "," + admin_termsdate19.text.toString() + "," + "" + admin_termsdate110.text.toString() + ","
        termsdate2 = admin_termsdate21.text.toString() + "," + admin_termsdate22.text.toString() + "," + admin_termsdate23.text.toString() + "," +
                "" + admin_termsdate24.text.toString() + "," + admin_termsdate25.text.toString() + "," + admin_termsdate26.text.toString() + "," + admin_termsdate27.text.toString() + "," +
                "" + admin_termsdate28.text.toString() + "," + admin_termsdate29.text.toString() + "," + "" + admin_termsdate210.text.toString() + ","
        termsdate3 = admin_termsdate31.text.toString() + "," + admin_termsdate32.text.toString() + "," + admin_termsdate33.text.toString() + "," +
                "" + admin_termsdate34.text.toString() + "," + admin_termsdate35.text.toString() + "," + admin_termsdate36.text.toString() + "," + admin_termsdate37.text.toString() + "," +
                "" + admin_termsdate38.text.toString() + "," + admin_termsdate39.text.toString() + "," + "" + admin_termsdate310.text.toString() + ","
        termsdate5 = admin_termsdate51.text.toString() + "," + admin_termsdate52.text.toString() + "," + admin_termsdate53.text.toString() + "," +
                "" + admin_termsdate54.text.toString() + "," + admin_termsdate55.text.toString() + "," + admin_termsdate56.text.toString() + "," + admin_termsdate57.text.toString() + "," +
                "" + admin_termsdate58.text.toString() + "," + admin_termsdate59.text.toString() + "," + "" + admin_termsdate510.text.toString() + ","
        termsdate6 = admin_termsdate61.text.toString() + "," + admin_termsdate62.text.toString() + "," + admin_termsdate63.text.toString() + "," +
                "" + admin_termsdate64.text.toString() + "," + admin_termsdate65.text.toString() + "," + admin_termsdate66.text.toString() + "," + admin_termsdate67.text.toString() + "," +
                "" + admin_termsdate68.text.toString() + "," + admin_termsdate69.text.toString() + "," + "" + admin_termsdate610.text.toString() + ","
        termsdate7 = admin_termsdate71.text.toString() + "," + admin_termsdate72.text.toString() + "," + admin_termsdate73.text.toString() + "," +
                "" + admin_termsdate74.text.toString() + "," + admin_termsdate75.text.toString() + "," + admin_termsdate76.text.toString() + "," + admin_termsdate77.text.toString() + "," +
                "" + admin_termsdate78.text.toString() + "," + admin_termsdate79.text.toString() + "," + "" + admin_termsdate710.text.toString() + ","
        termsdate8 = admin_termsdate81.text.toString() + "," + admin_termsdate82.text.toString() + "," + admin_termsdate83.text.toString() + "," +
                "" + admin_termsdate84.text.toString() + "," + admin_termsdate85.text.toString() + "," + admin_termsdate86.text.toString() + "," + admin_termsdate87.text.toString() + "," +
                "" + admin_termsdate88.text.toString() + "," + admin_termsdate89.text.toString() + "," + "" + admin_termsdate810.text.toString() + ","

        termsdate9 = admin_termsdate91.text.toString() + "," + admin_termsdate92.text.toString() + "," + admin_termsdate93.text.toString() + "," +
                "" + admin_termsdate94.text.toString() + "," + admin_termsdate95.text.toString() + "," + admin_termsdate96.text.toString() + "," + admin_termsdate97.text.toString() + "," +
                "" + admin_termsdate98.text.toString() + "," + admin_termsdate99.text.toString() + "," + "" + admin_termsdate910.text.toString() + ","
        termsdate10 = admin_termsdate101.text.toString() + "," + admin_termsdate102.text.toString() + "," + admin_termsdate103.text.toString() + "," +
                "" + admin_termsdate104.text.toString() + "," + admin_termsdate105.text.toString() + "," + admin_termsdate106.text.toString() + "," + admin_termsdate107.text.toString() + "," +
                "" + admin_termsdate108.text.toString() + "," + admin_termsdate109.text.toString() + "," + "" + admin_termsdate1010.text.toString() + ","
        termsquantity = admin_termssitem1.text.toString() + "," + admin_termssitem2.text.toString() + "," + admin_termssitem3.text.toString() + "," +
                "" + admin_termssitem4.text.toString() + "," + admin_termssitem5.text.toString() + "," + admin_termssitem6.text.toString() + "," + admin_termssitem7.text.toString() + "," +
                "" + admin_termssitem8.text.toString() + "," + admin_termssitem9.text.toString() + "," + "" + admin_termssitem10.text.toString() + ","
        termsquantity1 = admin_termssitem11.text.toString() + "," + admin_termssitem12.text.toString() + "," + admin_termssitem13.text.toString() + "," +
                "" + admin_termssitem14.text.toString() + "," + admin_termssitem15.text.toString() + "," + admin_termssitem16.text.toString() + "," + admin_termssitem17.text.toString() + "," +
                "" + admin_termssitem18.text.toString() + "," + admin_termssitem19.text.toString() + "," + "" + admin_termssitem110.text.toString() + ","

        termsquantity2 = admin_termssitem21.text.toString() + "," + admin_termssitem22.text.toString() + "," + admin_termssitem23.text.toString() + "," +
                "" + admin_termssitem24.text.toString() + "," + admin_termssitem25.text.toString() + "," + admin_termssitem26.text.toString() + "," + admin_termssitem27.text.toString() + "," +
                "" + admin_termssitem28.text.toString() + "," + admin_termssitem29.text.toString() + "," + "" + admin_termssitem210.text.toString() + ","
        termsquantity3 = admin_termssitem31.text.toString() + "," + admin_termssitem32.text.toString() + "," + admin_termssitem33.text.toString() + "," +
                "" + admin_termssitem34.text.toString() + "," + admin_termssitem35.text.toString() + "," + admin_termssitem36.text.toString() + "," + admin_termssitem37.text.toString() + "," +
                "" + admin_termssitem38.text.toString() + "," + admin_termssitem39.text.toString() + "," + "" + admin_termssitem310.text.toString() + ","
        termsquantity5 = admin_termssitem51.text.toString() + "," + admin_termssitem52.text.toString() + "," + admin_termssitem53.text.toString() + "," +
                "" + admin_termssitem54.text.toString() + "," + admin_termssitem55.text.toString() + "," + admin_termssitem56.text.toString() + "," + admin_termssitem57.text.toString() + "," +
                "" + admin_termssitem58.text.toString() + "," + admin_termssitem59.text.toString() + "," + "" + admin_termssitem510.text.toString() + ","
        termsquantity6 = admin_termssitem61.text.toString() + "," + admin_termssitem62.text.toString() + "," + admin_termssitem63.text.toString() + "," +
                "" + admin_termssitem64.text.toString() + "," + admin_termssitem65.text.toString() + "," + admin_termssitem66.text.toString() + "," + admin_termssitem67.text.toString() + "," +
                "" + admin_termssitem68.text.toString() + "," + admin_termssitem69.text.toString() + "," + "" + admin_termssitem610.text.toString() + ","
        termsquantity7 = admin_termssitem71.text.toString() + "," + admin_termssitem72.text.toString() + "," + admin_termssitem73.text.toString() + "," +
                "" + admin_termssitem74.text.toString() + "," + admin_termssitem75.text.toString() + "," + admin_termssitem76.text.toString() + "," + admin_termssitem77.text.toString() + "," +
                "" + admin_termssitem78.text.toString() + "," + admin_termssitem79.text.toString() + "," + "" + admin_termssitem710.text.toString() + ","
        termsquantity8 = admin_termssitem81.text.toString() + "," + admin_termssitem82.text.toString() + "," + admin_termssitem83.text.toString() + "," +
                "" + admin_termssitem84.text.toString() + "," + admin_termssitem85.text.toString() + "," + admin_termssitem86.text.toString() + "," + admin_termssitem87.text.toString() + "," +
                "" + admin_termssitem88.text.toString() + "," + admin_termssitem89.text.toString() + "," + "" + admin_termssitem810.text.toString() + ","
        termsquantity9 = admin_termssitem91.text.toString() + "," + admin_termssitem92.text.toString() + "," + admin_termssitem93.text.toString() + "," +
                "" + admin_termssitem94.text.toString() + "," + admin_termssitem95.text.toString() + "," + admin_termssitem96.text.toString() + "," + admin_termssitem97.text.toString() + "," +
                "" + admin_termssitem98.text.toString() + "," + admin_termssitem99.text.toString() + "," + "" + admin_termssitem910.text.toString() + ","
        termsquantity10 = admin_termssitem101.text.toString() + "," + admin_termssitem102.text.toString() + "," + admin_termssitem103.text.toString() + "," +
                "" + admin_termssitem104.text.toString() + "," + admin_termssitem105.text.toString() + "," + admin_termssitem106.text.toString() + "," + admin_termssitem107.text.toString() + "," +
                "" + admin_termssitem108.text.toString() + "," + admin_termssitem109.text.toString() + "," + "" + admin_termssitem1010.text.toString() + ","
        ///new shipping and Delivery qty and dates
        shipping_termsdate1 = admin_shipp_termsdate121.text.toString() + "," + admin_shipp_termsdate122.text.toString() + "," + admin_shipp_termsdate123.text.toString() + "," +
                "" + admin_shipp_termsdate124.text.toString() + "," + admin_shipp_termsdate125.text.toString() + "," + admin_shipp_termsdate126.text.toString() + "," + admin_shipp_termsdate127.text.toString() + "," +
                "" + admin_shipp_termsdate128.text.toString() + "," + admin_shipp_termsdate129.text.toString() + "," + "" + admin_shipp_termsdate1210.text.toString() + ","
        shipping_termsdate2 = admin_shipp_termsdate221.text.toString() + "," + admin_shipp_termsdate222.text.toString() + "," + admin_shipp_termsdate223.text.toString() + "," +
                "" + admin_shipp_termsdate224.text.toString() + "," + admin_shipp_termsdate225.text.toString() + "," + admin_shipp_termsdate226.text.toString() + "," + admin_shipp_termsdate227.text.toString() + "," +
                "" + admin_shipp_termsdate228.text.toString() + "," + admin_shipp_termsdate229.text.toString() + "," + "" + admin_shipp_termsdate2210.text.toString() + ","
        shipping_termsdate3 = admin_shipp_termsdate321.text.toString() + "," + admin_shipp_termsdate322.text.toString() + "," + admin_shipp_termsdate323.text.toString() + "," +
                "" + admin_shipp_termsdate324.text.toString() + "," + admin_shipp_termsdate325.text.toString() + "," + admin_shipp_termsdate326.text.toString() + "," + admin_shipp_termsdate327.text.toString() + "," +
                "" + admin_shipp_termsdate328.text.toString() + "," + admin_shipp_termsdate329.text.toString() + "," + "" + admin_shipp_termsdate3210.text.toString() + ","
        shipping_termsdate4 = admin_shipp_termsdate421.text.toString() + "," + admin_shipp_termsdate422.text.toString() + "," + admin_shipp_termsdate423.text.toString() + "," +
                "" + admin_shipp_termsdate424.text.toString() + "," + admin_shipp_termsdate425.text.toString() + "," + admin_shipp_termsdate426.text.toString() + "," + admin_shipp_termsdate427.text.toString() + "," +
                "" + admin_shipp_termsdate428.text.toString() + "," + admin_shipp_termsdate429.text.toString() + "," + "" + admin_shipp_termsdate4210.text.toString() + ","

        shipping_termsdate5 = admin_shipp_termsdate521.text.toString() + "," + admin_shipp_termsdate522.text.toString() + "," + admin_shipp_termsdate523.text.toString() + "," +
                "" + admin_shipp_termsdate524.text.toString() + "," + admin_shipp_termsdate525.text.toString() + "," + admin_shipp_termsdate526.text.toString() + "," + admin_shipp_termsdate527.text.toString() + "," +
                "" + admin_shipp_termsdate528.text.toString() + "," + admin_shipp_termsdate529.text.toString() + "," + "" + admin_shipp_termsdate5210.text.toString() + ","
        shipping_termsdate6 = admin_shipp_termsdate621.text.toString() + "," + admin_shipp_termsdate622.text.toString() + "," + admin_shipp_termsdate623.text.toString() + "," +
                "" + admin_shipp_termsdate624.text.toString() + "," + admin_shipp_termsdate625.text.toString() + "," + admin_shipp_termsdate626.text.toString() + "," + admin_shipp_termsdate627.text.toString() + "," +
                "" + admin_shipp_termsdate628.text.toString() + "," + admin_shipp_termsdate629.text.toString() + "," + "" + admin_shipp_termsdate6210.text.toString() + ","
        shipping_termsdate7 = admin_shipp_termsdate721.text.toString() + "," + admin_shipp_termsdate722.text.toString() + "," + admin_shipp_termsdate723.text.toString() + "," +
                "" + admin_shipp_termsdate724.text.toString() + "," + admin_shipp_termsdate725.text.toString() + "," + admin_shipp_termsdate726.text.toString() + "," + admin_shipp_termsdate727.text.toString() + "," +
                "" + admin_shipp_termsdate728.text.toString() + "," + admin_shipp_termsdate729.text.toString() + "," + "" + admin_shipp_termsdate7210.text.toString() + ","
        shipping_termsdate8 = admin_shipp_termsdate821.text.toString() + "," + admin_shipp_termsdate822.text.toString() + "," + admin_shipp_termsdate823.text.toString() + "," +
                "" + admin_shipp_termsdate824.text.toString() + "," + admin_shipp_termsdate825.text.toString() + "," + admin_shipp_termsdate826.text.toString() + "," + admin_shipp_termsdate827.text.toString() + "," +
                "" + admin_shipp_termsdate828.text.toString() + "," + admin_shipp_termsdate829.text.toString() + "," + "" + admin_shipp_termsdate8210.text.toString() + ","
        shipping_termsdate9 = admin_shipp_termsdate921.text.toString() + "," + admin_shipp_termsdate922.text.toString() + "," + admin_shipp_termsdate923.text.toString() + "," +
                "" + admin_shipp_termsdate924.text.toString() + "," + admin_shipp_termsdate925.text.toString() + "," + admin_shipp_termsdate926.text.toString() + "," + admin_shipp_termsdate927.text.toString() + "," +
                "" + admin_shipp_termsdate928.text.toString() + "," + admin_shipp_termsdate929.text.toString() + "," + "" + admin_shipp_termsdate9210.text.toString() + ","
        shipping_termsdate10 = admin_shipp_termsdate1021.text.toString() + "," + admin_shipp_termsdate1022.text.toString() + "," + admin_shipp_termsdate1023.text.toString() + "," +
                "" + admin_shipp_termsdate1024.text.toString() + "," + admin_shipp_termsdate1025.text.toString() + "," + admin_shipp_termsdate1026.text.toString() + "," + admin_shipp_termsdate1027.text.toString() + "," +
                "" + admin_shipp_termsdate1028.text.toString() + "," + admin_shipp_termsdate1029.text.toString() + "," + "" + admin_shipp_termsdate10210.text.toString() + ","
        delivery_termsdate1 = admin_delivery_termsdate131.text.toString() + "," + admin_delivery_termsdate132.text.toString() + "," + admin_delivery_termsdate133.text.toString() + "," +
                "" + admin_delivery_termsdate134.text.toString() + "," + admin_delivery_termsdate135.text.toString() + "," + admin_delivery_termsdate136.text.toString() + "," + admin_delivery_termsdate137.text.toString() + "," +
                "" + admin_delivery_termsdate138.text.toString() + "," + admin_delivery_termsdate139.text.toString() + "," + "" + admin_delivery_termsdate1310.text.toString() + ","
        delivery_termsdate2 = admin_delivery_termsdate231.text.toString() + "," + admin_delivery_termsdate232.text.toString() + "," + admin_delivery_termsdate233.text.toString() + "," +
                "" + admin_delivery_termsdate234.text.toString() + "," + admin_delivery_termsdate235.text.toString() + "," + admin_delivery_termsdate236.text.toString() + "," + admin_delivery_termsdate237.text.toString() + "," +
                "" + admin_delivery_termsdate238.text.toString() + "," + admin_delivery_termsdate239.text.toString() + "," + "" + admin_delivery_termsdate2310.text.toString() + ","
        delivery_termsdate3 = admin_delivery_termsdate331.text.toString() + "," + admin_delivery_termsdate332.text.toString() + "," + admin_delivery_termsdate333.text.toString() + "," +
                "" + admin_delivery_termsdate334.text.toString() + "," + admin_delivery_termsdate335.text.toString() + "," + admin_delivery_termsdate336.text.toString() + "," + admin_delivery_termsdate337.text.toString() + "," +
                "" + admin_delivery_termsdate338.text.toString() + "," + admin_delivery_termsdate339.text.toString() + "," + "" + admin_delivery_termsdate3310.text.toString() + ","
        delivery_termsdate4 = admin_delivery_termsdate431.text.toString() + "," + admin_delivery_termsdate432.text.toString() + "," + admin_delivery_termsdate433.text.toString() + "," +
                "" + admin_delivery_termsdate434.text.toString() + "," + admin_delivery_termsdate435.text.toString() + "," + admin_delivery_termsdate436.text.toString() + "," + admin_delivery_termsdate437.text.toString() + "," +
                "" + admin_delivery_termsdate438.text.toString() + "," + admin_delivery_termsdate439.text.toString() + "," + "" + admin_delivery_termsdate4310.text.toString() + ","
        delivery_termsdate5 = admin_delivery_termsdate531.text.toString() + "," + admin_delivery_termsdate532.text.toString() + "," + admin_delivery_termsdate533.text.toString() + "," +
                "" + admin_delivery_termsdate534.text.toString() + "," + admin_delivery_termsdate535.text.toString() + "," + admin_delivery_termsdate536.text.toString() + "," + admin_delivery_termsdate537.text.toString() + "," +
                "" + admin_delivery_termsdate538.text.toString() + "," + admin_delivery_termsdate539.text.toString() + "," + "" + admin_delivery_termsdate5310.text.toString() + ","
        delivery_termsdate6 = admin_delivery_termsdate631.text.toString() + "," + admin_delivery_termsdate632.text.toString() + "," + admin_delivery_termsdate633.text.toString() + "," +
                "" + admin_delivery_termsdate634.text.toString() + "," + admin_delivery_termsdate635.text.toString() + "," + admin_delivery_termsdate636.text.toString() + "," + admin_delivery_termsdate637.text.toString() + "," +
                "" + admin_delivery_termsdate638.text.toString() + "," + admin_delivery_termsdate639.text.toString() + "," + "" + admin_delivery_termsdate6310.text.toString() + ","
        delivery_termsdate7 = admin_delivery_termsdate731.text.toString() + "," + admin_delivery_termsdate732.text.toString() + "," + admin_delivery_termsdate733.text.toString() + "," +
                "" + admin_delivery_termsdate734.text.toString() + "," + admin_delivery_termsdate735.text.toString() + "," + admin_delivery_termsdate736.text.toString() + "," + admin_delivery_termsdate737.text.toString() + "," +
                "" + admin_delivery_termsdate738.text.toString() + "," + admin_delivery_termsdate739.text.toString() + "," + "" + admin_delivery_termsdate7310.text.toString() + ","
        delivery_termsdate8 = admin_delivery_termsdate831.text.toString() + "," + admin_delivery_termsdate832.text.toString() + "," + admin_delivery_termsdate833.text.toString() + "," +
                "" + admin_delivery_termsdate834.text.toString() + "," + admin_delivery_termsdate835.text.toString() + "," + admin_delivery_termsdate836.text.toString() + "," + admin_delivery_termsdate837.text.toString() + "," +
                "" + admin_delivery_termsdate838.text.toString() + "," + admin_delivery_termsdate839.text.toString() + "," + "" + admin_delivery_termsdate8310.text.toString() + ","
        delivery_termsdate9 = admin_delivery_termsdate931.text.toString() + "," + admin_delivery_termsdate932.text.toString() + "," + admin_delivery_termsdate933.text.toString() + "," +
                "" + admin_delivery_termsdate934.text.toString() + "," + admin_delivery_termsdate935.text.toString() + "," + admin_delivery_termsdate936.text.toString() + "," + admin_delivery_termsdate937.text.toString() + "," +
                "" + admin_delivery_termsdate938.text.toString() + "," + admin_delivery_termsdate939.text.toString() + "," + "" + admin_delivery_termsdate9310.text.toString() + ","
        delivery_termsdate10 = admin_delivery_termsdate1031.text.toString() + "," + admin_delivery_termsdate1032.text.toString() + "," + admin_delivery_termsdate1033.text.toString() + "," +
                "" + admin_delivery_termsdate10310.text.toString() + "," + admin_delivery_termsdate1035.text.toString() + "," + admin_delivery_termsdate1036.text.toString() + "," + admin_delivery_termsdate1037.text.toString() + "," +
                "" + admin_delivery_termsdate1038.text.toString() + "," + admin_delivery_termsdate1039.text.toString() + "," + "" + admin_delivery_termsdate10310.text.toString() + ","
        shipping_qty1 = admin_shipp_termssitem121.text.toString() + "," + admin_shipp_termssitem122.text.toString() + "," + admin_shipp_termssitem123.text.toString() + "," +
                "" + admin_shipp_termssitem124.text.toString() + "," + admin_shipp_termssitem125.text.toString() + "," + admin_shipp_termssitem126.text.toString() + "," + admin_shipp_termssitem127.text.toString() + "," +
                "" + admin_shipp_termssitem128.text.toString() + "," + admin_shipp_termssitem129.text.toString() + "," + "" + admin_shipp_termssitem1210.text.toString() + ","
        shipping_qty2 = admin_shipp_termssitem221.text.toString() + "," + admin_shipp_termssitem222.text.toString() + "," + admin_shipp_termssitem223.text.toString() + "," +
                "" + admin_shipp_termssitem224.text.toString() + "," + admin_shipp_termssitem225.text.toString() + "," + admin_shipp_termssitem226.text.toString() + "," + admin_shipp_termsdate427.text.toString() + "," +
                "" + admin_shipp_termssitem228.text.toString() + "," + admin_shipp_termssitem229.text.toString() + "," + "" + admin_shipp_termssitem2210.text.toString() + ","
        shipping_qty3 = admin_shipp_termssitem321.text.toString() + "," + admin_shipp_termssitem322.text.toString() + "," + admin_shipp_termssitem323.text.toString() + "," +
                "" + admin_shipp_termssitem324.text.toString() + "," + admin_shipp_termssitem325.text.toString() + "," + admin_shipp_termssitem326.text.toString() + "," + admin_shipp_termsdate427.text.toString() + "," +
                "" + admin_shipp_termssitem328.text.toString() + "," + admin_shipp_termssitem329.text.toString() + "," + "" + admin_shipp_termssitem3210.text.toString() + ","
        shipping_qty4 = admin_shipp_termssitem421.text.toString() + "," + admin_shipp_termssitem422.text.toString() + "," + admin_shipp_termssitem423.text.toString() + "," +
                "" + admin_shipp_termssitem424.text.toString() + "," + admin_shipp_termssitem425.text.toString() + "," + admin_shipp_termssitem426.text.toString() + "," + admin_shipp_termssitem427.text.toString() + "," +
                "" + admin_shipp_termssitem428.text.toString() + "," + admin_shipp_termssitem429.text.toString() + "," + "" + admin_shipp_termssitem4210.text.toString() + ","
        shipping_qty5 = admin_shipp_termssitem521.text.toString() + "," + admin_shipp_termssitem522.text.toString() + "," + admin_shipp_termssitem523.text.toString() + "," +
                "" + admin_shipp_termssitem524.text.toString() + "," + admin_shipp_termssitem525.text.toString() + "," + admin_shipp_termssitem526.text.toString() + "," + admin_shipp_termssitem527.text.toString() + "," +
                "" + admin_shipp_termssitem528.text.toString() + "," + admin_shipp_termssitem529.text.toString() + "," + "" + admin_shipp_termssitem5210.text.toString() + ","
        shipping_qty6 = admin_shipp_termssitem621.text.toString() + "," + admin_shipp_termssitem622.text.toString() + "," + admin_shipp_termssitem623.text.toString() + "," +
                "" + admin_shipp_termssitem624.text.toString() + "," + admin_shipp_termssitem625.text.toString() + "," + admin_shipp_termssitem626.text.toString() + "," + admin_shipp_termssitem627.text.toString() + "," +
                "" + admin_shipp_termssitem628.text.toString() + "," + admin_shipp_termssitem629.text.toString() + "," + "" + admin_shipp_termssitem6210.text.toString() + ","
        shipping_qty7 = admin_shipp_termssitem721.text.toString() + "," + admin_shipp_termssitem722.text.toString() + "," + admin_shipp_termssitem723.text.toString() + "," +
                "" + admin_shipp_termssitem724.text.toString() + "," + admin_shipp_termssitem725.text.toString() + "," + admin_shipp_termssitem726.text.toString() + "," + admin_shipp_termssitem727.text.toString() + "," +
                "" + admin_shipp_termssitem728.text.toString() + "," + admin_shipp_termssitem729.text.toString() + "," + "" + admin_shipp_termssitem7210.text.toString() + ","
        shipping_qty8 = admin_shipp_termssitem821.text.toString() + "," + admin_shipp_termssitem822.text.toString() + "," + admin_shipp_termssitem823.text.toString() + "," +
                "" + admin_shipp_termssitem824.text.toString() + "," + admin_shipp_termssitem825.text.toString() + "," + admin_shipp_termssitem826.text.toString() + "," + admin_shipp_termssitem827.text.toString() + "," +
                "" + admin_shipp_termssitem828.text.toString() + "," + admin_shipp_termssitem829.text.toString() + "," + "" + admin_shipp_termssitem8210.text.toString() + ","
        shipping_qty9 = admin_shipp_termssitem921.text.toString() + "," + admin_shipp_termssitem922.text.toString() + "," + admin_shipp_termssitem923.text.toString() + "," +
                "" + admin_shipp_termssitem924.text.toString() + "," + admin_shipp_termssitem925.text.toString() + "," + admin_shipp_termssitem926.text.toString() + "," + admin_shipp_termssitem927.text.toString() + "," +
                "" + admin_shipp_termssitem928.text.toString() + "," + admin_shipp_termssitem929.text.toString() + "," + "" + admin_shipp_termssitem9210.text.toString() + ","
        shipping_qty10 = admin_shipp_termssitem1021.text.toString() + "," + admin_shipp_termssitem1022.text.toString() + "," + admin_shipp_termssitem1023.text.toString() + "," +
                "" + admin_shipp_termssitem10210.text.toString() + "," + admin_shipp_termssitem1025.text.toString() + "," + admin_shipp_termssitem1026.text.toString() + "," + admin_shipp_termssitem1027.text.toString() + "," +
                "" + admin_shipp_termssitem1028.text.toString() + "," + admin_shipp_termssitem1029.text.toString() + "," + "" + admin_shipp_termssitem10210.text.toString() + ","
        delivery_qty1 = admin_delivery_termssitem131.text.toString() + "," + admin_delivery_termssitem132.text.toString() + "," + admin_delivery_termssitem133.text.toString() + "," +
                "" + admin_delivery_termssitem134.text.toString() + "," + admin_delivery_termssitem135.text.toString() + "," + admin_delivery_termssitem136.text.toString() + "," + admin_delivery_termssitem137.text.toString() + "," +
                "" + admin_delivery_termssitem138.text.toString() + "," + admin_delivery_termssitem139.text.toString() + "," + "" + admin_delivery_termssitem1310.text.toString() + ","
        delivery_qty2 = admin_delivery_termssitem231.text.toString() + "," + admin_delivery_termssitem232.text.toString() + "," + admin_delivery_termssitem233.text.toString() + "," +
                "" + admin_delivery_termssitem234.text.toString() + "," + admin_delivery_termssitem235.text.toString() + "," + admin_delivery_termssitem236.text.toString() + "," + admin_delivery_termssitem237.text.toString() + "," +
                "" + admin_delivery_termssitem238.text.toString() + "," + admin_delivery_termssitem239.text.toString() + "," + "" + admin_delivery_termssitem2310.text.toString() + ","
        delivery_qty3 = admin_delivery_termssitem331.text.toString() + "," + admin_delivery_termssitem332.text.toString() + "," + admin_delivery_termssitem333.text.toString() + "," +
                "" + admin_delivery_termssitem334.text.toString() + "," + admin_delivery_termssitem335.text.toString() + "," + admin_delivery_termssitem336.text.toString() + "," + admin_delivery_termssitem337.text.toString() + "," +
                "" + admin_delivery_termssitem338.text.toString() + "," + admin_delivery_termssitem339.text.toString() + "," + "" + admin_delivery_termssitem3310.text.toString() + ","

        delivery_qty4 = admin_delivery_termssitem431.text.toString() + "," + admin_delivery_termssitem432.text.toString() + "," + admin_delivery_termssitem433.text.toString() + "," +
                "" + admin_delivery_termssitem434.text.toString() + "," + admin_delivery_termssitem435.text.toString() + "," + admin_delivery_termssitem436.text.toString() + "," + admin_delivery_termssitem437.text.toString() + "," +
                "" + admin_delivery_termssitem438.text.toString() + "," + admin_delivery_termssitem439.text.toString() + "," + "" + admin_delivery_termssitem4310.text.toString() + ","
        delivery_qty5 = admin_delivery_termssitem531.text.toString() + "," + admin_delivery_termssitem532.text.toString() + "," + admin_delivery_termssitem533.text.toString() + "," +
                "" + admin_delivery_termssitem534.text.toString() + "," + admin_delivery_termssitem535.text.toString() + "," + admin_delivery_termssitem536.text.toString() + "," + admin_delivery_termssitem537.text.toString() + "," +
                "" + admin_delivery_termssitem538.text.toString() + "," + admin_delivery_termssitem539.text.toString() + "," + "" + admin_delivery_termssitem5310.text.toString() + ","
        delivery_qty6 = admin_delivery_termssitem631.text.toString() + "," + admin_delivery_termssitem632.text.toString() + "," + admin_delivery_termssitem633.text.toString() + "," +
                "" + admin_delivery_termssitem634.text.toString() + "," + admin_delivery_termssitem635.text.toString() + "," + admin_delivery_termssitem636.text.toString() + "," + admin_delivery_termssitem637.text.toString() + "," +
                "" + admin_delivery_termssitem638.text.toString() + "," + admin_delivery_termssitem639.text.toString() + "," + "" + admin_delivery_termssitem6310.text.toString() + ","
        delivery_qty7 = admin_delivery_termssitem731.text.toString() + "," + admin_delivery_termssitem732.text.toString() + "," + admin_delivery_termssitem733.text.toString() + "," +
                "" + admin_delivery_termssitem734.text.toString() + "," + admin_delivery_termssitem735.text.toString() + "," + admin_delivery_termssitem736.text.toString() + "," + admin_delivery_termssitem737.text.toString() + "," +
                "" + admin_delivery_termssitem738.text.toString() + "," + admin_delivery_termssitem739.text.toString() + "," + "" + admin_delivery_termssitem7310.text.toString() + ","
        delivery_qty8 = admin_delivery_termssitem831.text.toString() + "," + admin_delivery_termssitem832.text.toString() + "," + admin_delivery_termssitem833.text.toString() + "," +
                "" + admin_delivery_termssitem834.text.toString() + "," + admin_delivery_termssitem835.text.toString() + "," + admin_delivery_termssitem836.text.toString() + "," + admin_delivery_termssitem837.text.toString() + "," +
                "" + admin_delivery_termssitem838.text.toString() + "," + admin_delivery_termssitem839.text.toString() + "," + "" + admin_delivery_termssitem8310.text.toString() + ","
        delivery_qty9 = admin_delivery_termssitem931.text.toString() + "," + admin_delivery_termssitem932.text.toString() + "," + admin_delivery_termssitem933.text.toString() + "," +
                "" + admin_delivery_termssitem934.text.toString() + "," + admin_delivery_termssitem935.text.toString() + "," + admin_delivery_termssitem936.text.toString() + "," + admin_delivery_termssitem937.text.toString() + "," +
                "" + admin_delivery_termssitem938.text.toString() + "," + admin_delivery_termssitem939.text.toString() + "," + "" + admin_delivery_termssitem9310.text.toString() + ","
        delivery_qty10 = admin_delivery_termssitem1031.text.toString() + "," + admin_delivery_termssitem1032.text.toString() + "," + admin_delivery_termssitem1033.text.toString() + "," +
                "" + admin_delivery_termssitem1034.text.toString() + "," + admin_delivery_termssitem1035.text.toString() + "," + admin_delivery_termssitem1036.text.toString() + "," + admin_delivery_termssitem1037.text.toString() + "," +
                "" + admin_delivery_termssitem1038.text.toString() + "," + admin_delivery_termssitem1039.text.toString() + "," + "" + admin_delivery_termssitem10310.text.toString() + ","
        val termsdate = termsdate!!.split(",")
        val termsquantity = termsquantity!!.split(",")
        val termsdate1 = termsdate1!!.split(",")
        val termsquantity1 = termsquantity1!!.split(",")
        val termsdate2 = termsdate2!!.split(",")
        val termsquantity2 = termsquantity2!!.split(",")
        val termsdate3 = termsdate3!!.split(",")
        val termsquantity3 = termsquantity3!!.split(",")
        val termsdate5 = termsdate5!!.split(",")
        val termsquantity5 = termsquantity5!!.split(",")
        val termsdate6 = termsdate6!!.split(",")
        val termsquantity6 = termsquantity6!!.split(",")
        val termsdate7 = termsdate7!!.split(",")
        val termsquantity7 = termsquantity7!!.split(",")
        val termsdate8 = termsdate8!!.split(",")
        val termsquantity8 = termsquantity8!!.split(",")
        val termsdate9 = termsdate9!!.split(",")
        val termsquantity9 = termsquantity9!!.split(",")
        val termsdate10 = termsdate10!!.split(",")
        val termsquantity10 = termsquantity10!!.split(",")
        val termsdate_shipping1 = shipping_termsdate1!!.split(",")
        val termsdate_shipping2 = shipping_termsdate2!!.split(",")
        val termsdate_shipping3 = shipping_termsdate3!!.split(",")
        val termsdate_shipping4 = shipping_termsdate4!!.split(",")
        val termsdate_shipping5 = shipping_termsdate5!!.split(",")
        val termsdate_shipping6 = shipping_termsdate6!!.split(",")
        val termsdate_shipping7 = shipping_termsdate7!!.split(",")
        val termsdate_shipping8 = shipping_termsdate8!!.split(",")
        val termsdate_shipping9 = shipping_termsdate9!!.split(",")
        val termsdate_shipping10 = shipping_termsdate10!!.split(",")

        val termsdate_delivery1 = delivery_termsdate1!!.split(",")
        val termsdate_delivery2 = delivery_termsdate2!!.split(",")
        val termsdate_delivery3 = delivery_termsdate3!!.split(",")
        val termsdate_delivery4 = delivery_termsdate4!!.split(",")
        val termsdate_delivery5 = delivery_termsdate5!!.split(",")
        val termsdate_delivery6 = delivery_termsdate6!!.split(",")
        val termsdate_delivery7 = delivery_termsdate7!!.split(",")
        val termsdate_delivery8 = delivery_termsdate8!!.split(",")
        val termsdate_delivery9 = delivery_termsdate9!!.split(",")
        val termsdate_delivery10 = delivery_termsdate10!!.split(",")

        val qty_shipping1 = shipping_qty1!!.split(",")
        val qty_shipping2 = shipping_qty2!!.split(",")
        val qty_shipping3 = shipping_qty3!!.split(",")
        val qty_shipping4 = shipping_qty4!!.split(",")
        val qty_shipping5 = shipping_qty5!!.split(",")
        val qty_shipping6 = shipping_qty6!!.split(",")
        val qty_shipping7 = shipping_qty7!!.split(",")
        val qty_shipping8 = shipping_qty8!!.split(",")
        val qty_shipping9 = shipping_qty9!!.split(",")
        val qty_shipping10 = shipping_qty10!!.split(",")

        val qty_delivery1 = delivery_qty1!!.split(",")
        val qty_delivery2 = delivery_qty2!!.split(",")
        val qty_delivery3 = delivery_qty3!!.split(",")
        val qty_delivery4 = delivery_qty4!!.split(",")
        val qty_delivery5 = delivery_qty5!!.split(",")
        val qty_delivery6 = delivery_qty6!!.split(",")
        val qty_delivery7 = delivery_qty7!!.split(",")
        val qty_delivery8 = delivery_qty8!!.split(",")
        val qty_delivery9 = delivery_qty9!!.split(",")
        val qty_delivery10 = delivery_qty10!!.split(",")
////shipping delivery end
        val termslist = ArrayList<String>()
        val termsdatelist = ArrayList<String>()
        for (i in 0 until termsdate.size) {
            if (termsdate.get(i) != "") {
                termslist.add("\"" + termsdate.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity.size) {
            if (termsquantity.get(i) != "") {
                termsdatelist.add("\"" + termsquantity.get(i) + "\"")
            }
        }
        val termslist1 = ArrayList<String>()
        val termsdatelist1 = ArrayList<String>()
        for (i in 0 until termsdate1.size) {
            if (termsdate1.get(i) != "") {
                termslist1.add("\"" + termsdate1.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity1.size) {
            if (termsquantity1.get(i) != "") {
                termsdatelist1.add("\"" + termsquantity1.get(i) + "\"")
            }
        }
        val termslist2 = ArrayList<String>()
        val termsdatelist2 = ArrayList<String>()
        for (i in 0 until termsdate2.size) {
            if (termsdate2.get(i) != "") {
                termslist2.add("\"" + termsdate2.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity2.size) {
            if (termsquantity2.get(i) != "") {
                termsdatelist2.add("\"" + termsquantity2.get(i) + "\"")
            }
        }
        val termslist3 = ArrayList<String>()
        val termsdatelist3 = ArrayList<String>()

        for (i in 0 until termsdate3.size) {
            if (termsdate3.get(i) != "") {
                termslist3.add("\"" + termsdate3.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity3.size) {
            if (termsquantity3.get(i) != "") {
                termsdatelist3.add("\"" + termsquantity3.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity.size) {
            if (termsquantity.get(i) != "") {
                mailing_qty = mailing_qty!!.toInt() + termsquantity.get(i).toInt()
            }
        }
        Log.w("terms ", ":::Qyab " + mailing_qty)

        val termslist5 = ArrayList<String>()
        val termsdatelist5 = ArrayList<String>()
        for (i in 0 until termsdate5.size) {
            if (termsdate5.get(i) != "") {
                termslist5.add("\"" + termsdate5.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity5.size) {
            if (termsquantity5.get(i) != "") {
                termsdatelist5.add("\"" + termsquantity5.get(i) + "\"")
            }
        }
        val termslist6 = ArrayList<String>()
        val termsdatelist6 = ArrayList<String>()
        for (i in 0 until termsdate6.size) {
            if (termsdate6.get(i) != "") {
                termslist6.add("\"" + termsdate6.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity6.size) {
            if (termsquantity6.get(i) != "") {
                termsdatelist6.add("\"" + termsquantity6.get(i) + "\"")
            }
        }
        val termslist7 = ArrayList<String>()
        val termsdatelist7 = ArrayList<String>()
        for (i in 0 until termsdate7.size) {
            if (termsdate7.get(i) != "") {
                termslist7.add("\"" + termsdate7.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity7.size) {
            if (termsquantity7.get(i) != "") {
                termsdatelist7.add("\"" + termsquantity7.get(i) + "\"")
            }
        }
        val termslist8 = ArrayList<String>()
        val termsdatelist8 = ArrayList<String>()
        for (i in 0 until termsdate8.size) {
            if (termsdate8.get(i) != "") {
                termslist8.add("\"" + termsdate8.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity8.size) {
            if (termsquantity8.get(i) != "") {
                termsdatelist8.add("\"" + termsquantity8.get(i) + "\"")
            }
        }
        val termslist9 = ArrayList<String>()
        val termsdatelist9 = ArrayList<String>()
        for (i in 0 until termsdate9.size) {
            if (termsdate9.get(i) != "") {
                termslist9.add("\"" + termsdate9.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity9.size) {
            if (termsquantity9.get(i) != "") {
                termsdatelist9.add("\"" + termsquantity9.get(i) + "\"")
            }
        }
        val termslist10 = ArrayList<String>()
        val termsdatelist10 = ArrayList<String>()
        for (i in 0 until termsdate10.size) {
            if (termsdate10.get(i) != "") {
                termslist10.add("\"" + termsdate10.get(i) + "\"")
            }
        }
        for (i in 0 until termsquantity10.size) {
            if (termsquantity10.get(i) != "") {
                termsdatelist10.add("\"" + termsquantity10.get(i) + "\"")
            }
        }
        val shipping_qty_list1 = ArrayList<String>()
        val shipping_date1 = ArrayList<String>()
        for (i in 0 until qty_shipping1.size) {
            if (qty_shipping1.get(i) != "") {
                shipping_qty_list1.add("\"" + qty_shipping1.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping1.size) {
            if (termsdate_shipping1.get(i) != "") {
                shipping_date1.add("\"" + termsdate_shipping1.get(i) + "\"")
            }
        }
        val shipping_qty_list2 = ArrayList<String>()
        val shipping_date2 = ArrayList<String>()
        for (i in 0 until qty_shipping2.size) {
            if (qty_shipping2.get(i) != "") {
                shipping_qty_list2.add("\"" + qty_shipping2.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping2.size) {
            if (termsdate_shipping2.get(i) != "") {
                shipping_date2.add("\"" + termsdate_shipping2.get(i) + "\"")
            }
        }
        val shipping_qty_list3 = ArrayList<String>()
        val shipping_date3 = ArrayList<String>()
        for (i in 0 until qty_shipping3.size) {
            if (qty_shipping3.get(i) != "") {
                shipping_qty_list3.add("\"" + qty_shipping3.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping3.size) {
            if (termsdate_shipping3.get(i) != "") {
                shipping_date3.add("\"" + termsdate_shipping3.get(i) + "\"")
            }
        }
        val shipping_qty_list4 = ArrayList<String>()
        val shipping_date4 = ArrayList<String>()
        for (i in 0 until qty_shipping4.size) {
            if (qty_shipping4.get(i) != "") {
                shipping_qty_list4.add("\"" + qty_shipping4.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping4.size) {
            if (termsdate_shipping4.get(i) != "") {
                shipping_date4.add("\"" + termsdate_shipping4.get(i) + "\"")
            }
        }
        val shipping_qty_list5 = ArrayList<String>()
        val shipping_date5 = ArrayList<String>()
        for (i in 0 until qty_shipping5.size) {
            if (qty_shipping5.get(i) != "") {
                shipping_qty_list5.add("\"" + qty_shipping5.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping5.size) {
            if (termsdate_shipping5.get(i) != "") {
                shipping_date5.add("\"" + termsdate_shipping5.get(i) + "\"")
            }
        }
        val shipping_qty_list6 = ArrayList<String>()
        val shipping_date6 = ArrayList<String>()
        for (i in 0 until qty_shipping6.size) {
            if (qty_shipping6.get(i) != "") {
                shipping_qty_list6.add("\"" + qty_shipping6.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping6.size) {
            if (termsdate_shipping6.get(i) != "") {
                shipping_date6.add("\"" + termsdate_shipping6.get(i) + "\"")
            }
        }
        val shipping_qty_list7 = ArrayList<String>()
        val shipping_date7 = ArrayList<String>()
        for (i in 0 until qty_shipping7.size) {
            if (qty_shipping7.get(i) != "") {
                shipping_qty_list7.add("\"" + qty_shipping7.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping7.size) {
            if (termsdate_shipping7.get(i) != "") {
                shipping_date7.add("\"" + termsdate_shipping7.get(i) + "\"")
            }
        }
        val shipping_qty_list8 = ArrayList<String>()
        val shipping_date8 = ArrayList<String>()
        for (i in 0 until qty_shipping8.size) {
            if (qty_shipping8.get(i) != "") {
                shipping_qty_list8.add("\"" + qty_shipping8.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping8.size) {
            if (termsdate_shipping8.get(i) != "") {
                shipping_date8.add("\"" + termsdate_shipping8.get(i) + "\"")
            }
        }
        val shipping_qty_list9 = ArrayList<String>()
        val shipping_date9 = ArrayList<String>()
        for (i in 0 until qty_shipping9.size) {
            if (qty_shipping9.get(i) != "") {
                shipping_qty_list9.add("\"" + qty_shipping9.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping9.size) {
            if (termsdate_shipping9.get(i) != "") {
                shipping_date9.add("\"" + termsdate_shipping9.get(i) + "\"")
            }
        }
        val shipping_qty_list10 = ArrayList<String>()
        val shipping_date10 = ArrayList<String>()
        for (i in 0 until qty_shipping10.size) {
            if (qty_shipping10.get(i) != "") {
                shipping_qty_list10.add("\"" + qty_shipping10.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_shipping10.size) {
            if (termsdate_shipping10.get(i) != "") {
                shipping_date10.add("\"" + termsdate_shipping10.get(i) + "\"")
            }
        }
        val delivery_qty_list1 = ArrayList<String>()
        val delivery_date1 = ArrayList<String>()
        for (i in 0 until qty_delivery1.size) {
            if (qty_delivery1.get(i) != "") {
                delivery_qty_list1.add("\"" + qty_delivery1.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery1.size) {
            if (termsdate_delivery1.get(i) != "") {
                delivery_date1.add("\"" + termsdate_delivery1.get(i) + "\"")
            }
        }
        val delivery_qty_list2 = ArrayList<String>()
        val delivery_date2 = ArrayList<String>()
        for (i in 0 until qty_delivery2.size) {
            if (qty_delivery2.get(i) != "") {
                delivery_qty_list2.add("\"" + qty_delivery2.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery2.size) {
            if (termsdate_delivery2.get(i) != "") {
                delivery_date2.add("\"" + termsdate_delivery2.get(i) + "\"")
            }
        }
        val delivery_qty_list3 = ArrayList<String>()
        val delivery_date3 = ArrayList<String>()
        for (i in 0 until qty_delivery3.size) {
            if (qty_delivery3.get(i) != "") {
                delivery_qty_list3.add("\"" + qty_delivery3.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery3.size) {
            if (termsdate_delivery3.get(i) != "") {
                delivery_date3.add("\"" + termsdate_delivery3.get(i) + "\"")
            }
        }
        val delivery_qty_list4 = ArrayList<String>()
        val delivery_date4 = ArrayList<String>()
        for (i in 0 until qty_delivery4.size) {
            if (qty_delivery4.get(i) != "") {
                delivery_qty_list4.add("\"" + qty_delivery4.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery4.size) {
            if (termsdate_delivery4.get(i) != "") {
                delivery_date4.add("\"" + termsdate_delivery4.get(i) + "\"")
            }
        }
        val delivery_qty_list5 = ArrayList<String>()
        val delivery_date5 = ArrayList<String>()
        for (i in 0 until qty_delivery5.size) {
            if (qty_delivery5.get(i) != "") {
                delivery_qty_list5.add("\"" + qty_delivery5.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery5.size) {
            if (termsdate_delivery5.get(i) != "") {
                delivery_date5.add("\"" + termsdate_delivery5.get(i) + "\"")
            }
        }
        val delivery_qty_list6 = ArrayList<String>()
        val delivery_date6 = ArrayList<String>()
        for (i in 0 until qty_delivery6.size) {
            if (qty_delivery6.get(i) != "") {
                delivery_qty_list6.add("\"" + qty_delivery6.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery6.size) {
            if (termsdate_delivery6.get(i) != "") {
                delivery_date6.add("\"" + termsdate_delivery6.get(i) + "\"")
            }
        }
        val delivery_qty_list7 = ArrayList<String>()
        val delivery_date7 = ArrayList<String>()
        for (i in 0 until qty_delivery7.size) {
            if (qty_delivery7.get(i) != "") {
                delivery_qty_list7.add("\"" + qty_delivery7.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery7.size) {
            if (termsdate_delivery7.get(i) != "") {
                delivery_date7.add("\"" + termsdate_delivery7.get(i) + "\"")
            }
        }
        val delivery_qty_list8 = ArrayList<String>()
        val delivery_date8 = ArrayList<String>()
        for (i in 0 until qty_delivery8.size) {
            if (qty_delivery8.get(i) != "") {
                delivery_qty_list8.add("\"" + qty_delivery8.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery8.size) {
            if (termsdate_delivery8.get(i) != "") {
                delivery_date8.add("\"" + termsdate_delivery8.get(i) + "\"")
            }
        }
        val delivery_qty_list9 = ArrayList<String>()
        val delivery_date9 = ArrayList<String>()
        for (i in 0 until qty_delivery9.size) {
            if (qty_delivery9.get(i) != "") {
                delivery_qty_list9.add("\"" + qty_delivery9.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery9.size) {
            if (termsdate_delivery9.get(i) != "") {
                delivery_date9.add("\"" + termsdate_delivery9.get(i) + "\"")
            }
        }
        val delivery_qty_list10 = ArrayList<String>()
        val delivery_date10 = ArrayList<String>()
        for (i in 0 until qty_delivery10.size) {
            if (qty_delivery10.get(i) != "") {
                delivery_qty_list10.add("\"" + qty_delivery10.get(i) + "\"")
            }
        }
        for (i in 0 until termsdate_delivery10.size) {
            if (termsdate_delivery10.get(i) != "") {
                delivery_date10.add("\"" + termsdate_delivery10.get(i) + "\"")
            }
        }
        val terms = ArrayList<String>()
        terms.add(termsdatelist.toString())
        terms.add(termslist.toString())
        val terms1 = ArrayList<String>()
        terms1.add(termsdatelist1.toString())
        terms1.add(termslist1.toString())
        val terms2 = ArrayList<String>()
        terms2.add(termsdatelist2.toString())
        terms2.add(termslist2.toString())
        val terms3 = ArrayList<String>()
        terms3.add(termsdatelist3.toString())
        terms3.add(termslist3.toString())
        val terms5 = ArrayList<String>()
        terms5.add(termsdatelist5.toString())
        terms5.add(termslist5.toString())
        val terms6 = ArrayList<String>()
        terms6.add(termsdatelist6.toString())
        terms6.add(termslist6.toString())
        val terms7 = ArrayList<String>()
        terms7.add(termsdatelist7.toString())
        terms7.add(termslist7.toString())
        val terms8 = ArrayList<String>()
        terms8.add(termsdatelist8.toString())
        terms8.add(termslist8.toString())
        val terms9 = ArrayList<String>()
        terms9.add(termsdatelist9.toString())
        terms9.add(termslist9.toString())
        val terms10 = ArrayList<String>()
        terms10.add(termsdatelist10.toString())
        terms10.add(termslist10.toString())
        val shipping_terms1 = ArrayList<String>()
        shipping_terms1.add(shipping_qty_list1.toString())
        shipping_terms1.add(shipping_date1.toString())
        val shipping_terms2 = ArrayList<String>()
        shipping_terms2.add(shipping_qty_list2.toString())
        shipping_terms2.add(shipping_date2.toString())
        val shipping_terms3 = ArrayList<String>()
        shipping_terms3.add(shipping_qty_list3.toString())
        shipping_terms3.add(shipping_date3.toString())
        val shipping_terms4 = ArrayList<String>()
        shipping_terms4.add(shipping_qty_list4.toString())
        shipping_terms4.add(shipping_date4.toString())
        val shipping_terms5 = ArrayList<String>()
        shipping_terms5.add(shipping_qty_list5.toString())
        shipping_terms5.add(shipping_date5.toString())
        val shipping_terms6 = ArrayList<String>()
        shipping_terms6.add(shipping_qty_list6.toString())
        shipping_terms6.add(shipping_date6.toString())
        val shipping_terms7 = ArrayList<String>()
        shipping_terms7.add(shipping_qty_list7.toString())
        shipping_terms7.add(shipping_date7.toString())
        val shipping_terms8 = ArrayList<String>()
        shipping_terms8.add(shipping_qty_list8.toString())
        shipping_terms8.add(shipping_date8.toString())
        val shipping_terms9 = ArrayList<String>()
        shipping_terms9.add(shipping_qty_list9.toString())
        shipping_terms9.add(shipping_date9.toString())
        val shipping_terms10 = ArrayList<String>()
        shipping_terms10.add(shipping_qty_list10.toString())
        shipping_terms10.add(shipping_date10.toString())
        val delivery_terms1 = ArrayList<String>()
        delivery_terms1.add(delivery_qty_list1.toString())
        delivery_terms1.add(delivery_date1.toString())
        val delivery_terms2 = ArrayList<String>()
        delivery_terms2.add(delivery_qty_list2.toString())
        delivery_terms2.add(delivery_date2.toString())
        val delivery_terms3 = ArrayList<String>()
        delivery_terms3.add(delivery_qty_list3.toString())
        delivery_terms3.add(delivery_date3.toString())
        val delivery_terms4 = ArrayList<String>()
        delivery_terms4.add(delivery_qty_list4.toString())
        delivery_terms4.add(delivery_date4.toString())
        val delivery_terms5 = ArrayList<String>()
        delivery_terms5.add(delivery_qty_list5.toString())
        delivery_terms5.add(delivery_date5.toString())
        val delivery_terms6 = ArrayList<String>()
        delivery_terms6.add(delivery_qty_list6.toString())
        delivery_terms6.add(delivery_date6.toString())
        val delivery_terms7 = ArrayList<String>()
        delivery_terms7.add(delivery_qty_list7.toString())
        delivery_terms7.add(delivery_date7.toString())
        val delivery_terms8 = ArrayList<String>()
        delivery_terms8.add(delivery_qty_list8.toString())
        delivery_terms8.add(delivery_date8.toString())
        val delivery_terms9 = ArrayList<String>()
        delivery_terms9.add(delivery_qty_list9.toString())
        delivery_terms9.add(delivery_date9.toString())
        val delivery_terms10 = ArrayList<String>()
        delivery_terms10.add(delivery_qty_list10.toString())
        delivery_terms10.add(delivery_date10.toString())
        val all_terms_array = ArrayList<String>()
        all_terms_array.add(terms1.toString())
        all_terms_array.add(terms2.toString())
        all_terms_array.add(terms3.toString())
        all_terms_array.add(terms5.toString())
        all_terms_array.add(terms6.toString())
        all_terms_array.add(terms7.toString())
        all_terms_array.add(terms8.toString())
        all_terms_array.add(terms9.toString())
        all_terms_array.add(terms10.toString())

        val all_shipping_terms_array = ArrayList<String>()
        all_shipping_terms_array.add(shipping_terms1.toString())
        all_shipping_terms_array.add(shipping_terms2.toString())
        all_shipping_terms_array.add(shipping_terms3.toString())
        all_shipping_terms_array.add(shipping_terms4.toString())
        all_shipping_terms_array.add(shipping_terms5.toString())
        all_shipping_terms_array.add(shipping_terms6.toString())
        all_shipping_terms_array.add(shipping_terms7.toString())
        all_shipping_terms_array.add(shipping_terms8.toString())
        all_shipping_terms_array.add(shipping_terms9.toString())
        all_shipping_terms_array.add(shipping_terms10.toString())

        val all_delivery_terms_array = ArrayList<String>()
        all_delivery_terms_array.add(delivery_terms1.toString())
        all_delivery_terms_array.add(delivery_terms2.toString())
        all_delivery_terms_array.add(delivery_terms3.toString())
        all_delivery_terms_array.add(delivery_terms4.toString())
        all_delivery_terms_array.add(delivery_terms5.toString())
        all_delivery_terms_array.add(delivery_terms6.toString())
        all_delivery_terms_array.add(delivery_terms7.toString())
        all_delivery_terms_array.add(delivery_terms8.toString())
        all_delivery_terms_array.add(delivery_terms9.toString())
        all_delivery_terms_array.add(delivery_terms10.toString())
        if (allotsheets.text.toString() != "") {
            allotedsheets = allotsheets.text.toString()
        }
        if (hasValidCredentials()) {
            if (final_job_parts.equals("1")) {
                if (!job_mailing_1.equals("")) {
                    if (admin_job_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()

                        Log.e("mailing_1", "job 1")
                        return
                    } else {
                        Toast.makeText(this, "mailing", Toast.LENGTH_SHORT).show()
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    } else {
                        Toast.makeText(this, "shipp", Toast.LENGTH_SHORT).show()

                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("2")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms1.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("3")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms1.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    } else {
                        Toast.makeText(this, "mailing", Toast.LENGTH_SHORT).show()
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_3.equals("")) {
                    if (admin_job_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_3.equals("")) {
                    if (admin_shipping_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_3.equals("")) {
                    if (admin_delivery_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_three_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 3 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("4")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms1.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        Log.e("mailing_4", "job 4")
                        return
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_3.equals("")) {
                    if (admin_job_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_3.equals("")) {
                    if (admin_shipping_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_3.equals("")) {
                    if (admin_delivery_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    } else {

                    }
                }
                if (!job_mailing_4.equals("")) {
                    if (admin_job_terms.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_4.equals("")) {
                    if (admin_shipping_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_4.equals("")) {
                    if (admin_delivery_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_three_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 3 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_four_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 4 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("5")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    } else {
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_3.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_3.equals("")) {
                    if (admin_shipping_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_3.equals("")) {
                    if (admin_delivery_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_4.equals("")) {
                    if (admin_job_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_4.equals("")) {
                    if (admin_shipping_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_4.equals("")) {
                    if (admin_delivery_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_5.equals("")) {
                    if (admin_job_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_5.equals("")) {
                    if (admin_shipping_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_5.equals("")) {
                    if (admin_delivery_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_three_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 3 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_four_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 4 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_five_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 5 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("6")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_3.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_3.equals("")) {
                    if (admin_shipping_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_3.equals("")) {
                    if (admin_delivery_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_4.equals("")) {
                    if (admin_job_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_4.equals("")) {
                    if (admin_shipping_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_4.equals("")) {
                    if (admin_delivery_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_5.equals("")) {
                    if (admin_job_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_5.equals("")) {
                    if (admin_shipping_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_5.equals("")) {
                    if (admin_delivery_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_6.equals("")) {
                    if (admin_job_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_6.equals("")) {
                    if (admin_shipping_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_6.equals("")) {
                    if (admin_delivery_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_three_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 3 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_four_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 4 Description ", Toast.LENGTH_SHORT).show()
                    return
                }

                if (job_desc_five_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 5 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_six_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 6 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("7")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_3.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_3.equals("")) {
                    if (admin_shipping_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_3.equals("")) {
                    if (admin_delivery_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_4.equals("")) {
                    if (admin_job_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_4.equals("")) {
                    if (admin_shipping_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_4.equals("")) {
                    if (admin_delivery_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_5.equals("")) {
                    if (admin_job_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_5.equals("")) {
                    if (admin_shipping_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_5.equals("")) {
                    if (admin_delivery_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_6.equals("")) {
                    if (admin_job_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_6.equals("")) {
                    if (admin_shipping_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_6.equals("")) {
                    if (admin_delivery_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_7.equals("")) {
                    if (admin_job_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_7.equals("")) {
                    if (admin_shipping_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_7.equals("")) {
                    if (admin_delivery_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_three_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 3 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_four_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 4 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_five_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 5 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_six_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 6 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_seven_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 7 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("8")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    } else {
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    } else {
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_3.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_3.equals("")) {
                    if (admin_shipping_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_3.equals("")) {
                    if (admin_delivery_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_4.equals("")) {
                    if (admin_job_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_4.equals("")) {
                    if (admin_shipping_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_4.equals("")) {
                    if (admin_delivery_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_5.equals("")) {
                    if (admin_job_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_5.equals("")) {
                    if (admin_shipping_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_5.equals("")) {
                    if (admin_delivery_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_6.equals("")) {
                    if (admin_job_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_6.equals("")) {
                    if (admin_shipping_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_6.equals("")) {
                    if (admin_delivery_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_7.equals("")) {
                    if (admin_job_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_7.equals("")) {
                    if (admin_shipping_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_7.equals("")) {
                    if (admin_delivery_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_8.equals("")) {
                    if (admin_job_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_8.equals("")) {
                    if (admin_shipping_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_8.equals("")) {
                    if (admin_delivery_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_three_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 3 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_four_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 4 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_five_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 5 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_six_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 6 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_seven_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 7 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_eight_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 8 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("9")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_3.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_3.equals("")) {
                    if (admin_shipping_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_3.equals("")) {
                    if (admin_delivery_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_4.equals("")) {
                    if (admin_job_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_4.equals("")) {
                    if (admin_shipping_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_4.equals("")) {
                    if (admin_delivery_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_5.equals("")) {
                    if (admin_job_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_5.equals("")) {
                    if (admin_shipping_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_5.equals("")) {
                    if (admin_delivery_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_6.equals("")) {
                    if (admin_job_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_6.equals("")) {
                    if (admin_shipping_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_6.equals("")) {
                    if (admin_delivery_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_7.equals("")) {
                    if (admin_job_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_7.equals("")) {
                    if (admin_shipping_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_7.equals("")) {
                    if (admin_delivery_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_8.equals("")) {
                    if (admin_job_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_8.equals("")) {
                    if (admin_shipping_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_8.equals("")) {
                    if (admin_delivery_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_9.equals("")) {
                    if (admin_job_terms9.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 9 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_9.equals("")) {
                    if (admin_shipping_terms9.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 9 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_9.equals("")) {
                    if (admin_delivery_terms9.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 9 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_three_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 3 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_four_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 4 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_five_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 5 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_six_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 6 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_seven_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 7 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_eight_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 8 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_nine_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 9 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            } else if (final_job_parts.equals("10")) {
                if (!job_mailing_1.equals("")) {
                    if (TextUtils.isEmpty(admin_job_terms.text.toString())) {
                        Toast.makeText(this, "Select Job part 1 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_1.equals("")) {
                    if (admin_shipping_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    } else {
                        Toast.makeText(this, "shipp", Toast.LENGTH_SHORT).show()
                    }
                }
                if (!job_delivery_1.equals("")) {
                    if (admin_delivery_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 1 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_2.equals("")) {
                    if (admin_job_terms1.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_2.equals("")) {
                    if (admin_shipping_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_2.equals("")) {
                    if (admin_delivery_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 2 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_3.equals("")) {
                    if (admin_job_terms2.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_3.equals("")) {
                    if (admin_shipping_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_3.equals("")) {
                    if (admin_delivery_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 3 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_4.equals("")) {
                    if (admin_job_terms3.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_4.equals("")) {
                    if (admin_shipping_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_4.equals("")) {
                    if (admin_delivery_terms4.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 4 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_5.equals("")) {
                    if (admin_job_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_5.equals("")) {
                    if (admin_shipping_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_5.equals("")) {
                    if (admin_delivery_terms5.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 5 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_6.equals("")) {
                    if (admin_job_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_6.equals("")) {
                    if (admin_shipping_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_6.equals("")) {
                    if (admin_delivery_terms6.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 6 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }

                if (!job_mailing_7.equals("")) {
                    if (admin_job_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_7.equals("")) {
                    if (admin_shipping_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_7.equals("")) {
                    if (admin_delivery_terms7.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 7 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }


                if (!job_mailing_8.equals("")) {
                    if (admin_job_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_8.equals("")) {
                    if (admin_shipping_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_8.equals("")) {
                    if (admin_delivery_terms8.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 8 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_9.equals("")) {
                    if (admin_job_terms9.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 9 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_9.equals("")) {
                    if (admin_shipping_terms9.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 9 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_9.equals("")) {
                    if (admin_delivery_terms9.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 9 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_mailing_10.equals("")) {
                    if (admin_job_terms10.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 10 Mailing Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_shipping_10.equals("")) {
                    if (admin_shipping_terms10.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 10 Shipping Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (!job_delivery_10.equals("")) {
                    if (admin_delivery_terms10.text.toString().equals("")) {
                        Toast.makeText(this, "Select Job part 10 Delivery Drops", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                if (job_desc_one_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 1 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_two_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 2 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_three_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 3 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_four_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 4 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_five_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 5 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_six_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 6 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_seven_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 7 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_eight_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 8 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_nine_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 9 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
                if (job_desc_ten_id.text.toString().equals("")) {
                    Toast.makeText(this, "Enter Job Part 10 Description ", Toast.LENGTH_SHORT).show()
                    return
                }
            }
            AddJobCallAPI(deptarray, taskarray, all_terms_array, "[" + terms + "]", all_shipping_terms_array, all_delivery_terms_array)
        }
    }

    override fun selectedIndices(indices: LinkedList<Int>) {
        Log.w("index1", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat.clear()
        job_nameList.clear()
        admin_tasks.setText("")
        mChecked.clear()
        tv_departments.visibility = View.GONE
        tv_departments2.visibility = View.GONE
        tv_departments3.visibility = View.GONE
        tv_departments4.visibility = View.GONE
        tv_departments5.visibility = View.GONE
        tv_departments6.visibility = View.GONE
        tv_departments7.visibility = View.GONE
        tv_departments8.visibility = View.GONE
        tv_departments9.visibility = View.GONE
        tv_departments10.visibility = View.GONE
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 1)
        rv_main_id.adapter = adapter
    }

    override fun selectedStrings(strings: LinkedList<String>) {
        dep_def_ids_flag = false
        if (strings.toString().contains("Mailing")) {
            ll_mailing_terms_1.visibility = View.VISIBLE
            job_mailing_1 = "Mailing"
        } else {
            ll_mailing_terms_1.visibility = View.GONE
            job_mailing_1 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_terms_1.visibility = View.VISIBLE
            job_shipping_1 = "Shipping"

        } else {
            ll_shipping_terms_1.visibility = View.GONE
            job_shipping_1 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_1.visibility = View.VISIBLE
            job_delivery_1 = "Delivery"
        } else {
            ll_delivery_terms_1.visibility = View.GONE
            job_delivery_1 = ""
        }
    }

    override fun selectedIndices2(indices: LinkedList<Int>) {
        Log.w("index2", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat2.clear()
        job_nameList2.clear()
        admin_tasks2.setText("")
        mChecked2.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array2.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array2.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll2", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 2)
        rv_main_id2.adapter = adapter
    }

    override fun selectedStrings2(strings: LinkedList<String>) {
        dep_def_ids_flag2 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_two.visibility = View.VISIBLE
            job_mailing_2 = "Mailing"
        } else {
            ll_miling_two.visibility = View.GONE
            job_mailing_2 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_two.visibility = View.VISIBLE
            job_shipping_2 = "Shipping"
        } else {
            ll_shipping_two.visibility = View.GONE
            job_shipping_2 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_2.visibility = View.VISIBLE
            job_delivery_2 = "Delivery"
        } else {
            ll_delivery_terms_2.visibility = View.GONE
            job_delivery_2 = ""
        }
    }
    override fun selectedIndices3(indices: LinkedList<Int>) {
        Log.w("index3", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat3.clear()
        job_nameList3.clear()
        admin_tasks3.setText("")
        mChecked3.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array3.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array3.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll3", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 3)
        rv_main_id3.adapter = adapter
    }

    override fun selectedStrings3(strings: LinkedList<String>) {
        dep_def_ids_flag3 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_three.visibility = View.VISIBLE
            job_mailing_3 = "Mailing"
        } else {
            ll_miling_three.visibility = View.GONE
            job_mailing_3 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_three.visibility = View.VISIBLE
            job_shipping_3 = "Shipping"
        } else {
            ll_shipping_three.visibility = View.GONE
            job_shipping_3 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_3.visibility = View.VISIBLE
            job_delivery_3 = "Delivery"
        } else {
            ll_delivery_terms_3.visibility = View.GONE
            job_delivery_3 = ""
        }
    }

    override fun selectedIndices4(indices: LinkedList<Int>) {
        Log.w("index4", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat4.clear()
        job_nameList4.clear()
        admin_tasks4.setText("")
        mChecked4.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array4.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array4.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll4", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 4)
        rv_main_id4.adapter = adapter
    }
    override fun selectedStrings4(strings: LinkedList<String>) {
        dep_def_ids_flag4 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_four.visibility = View.VISIBLE
            job_mailing_4 = "Mailing"
        } else {
            ll_miling_four.visibility = View.GONE
            job_mailing_4 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_four.visibility = View.VISIBLE
            job_shipping_4 = "Shipping"
        } else {
            ll_shipping_four.visibility = View.GONE
            job_shipping_4 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_4.visibility = View.VISIBLE
            job_delivery_4 = "Delivery"
        } else {
            ll_delivery_terms_4.visibility = View.GONE
            job_delivery_4 = ""
        }
    }

    override fun selectedIndices5(indices: LinkedList<Int>) {
        Log.w("index5", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat5.clear()
        job_nameList5.clear()
        admin_tasks5.setText("")
        mChecked5.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array5.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array5.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll5", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 5)
        rv_main_id5.adapter = adapter
    }

    override fun selectedStrings5(strings: LinkedList<String>) {
        dep_def_ids_flag5 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_five.visibility = View.VISIBLE
            job_mailing_5 = "Mailing"
        } else {
            ll_miling_five.visibility = View.GONE
            job_mailing_5 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_five.visibility = View.VISIBLE
            job_shipping_5 = "Shipping"
        } else {
            ll_shipping_five.visibility = View.GONE
            job_shipping_5 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_5.visibility = View.VISIBLE
            job_delivery_5 = "Delivery"
        } else {
            ll_delivery_terms_5.visibility = View.GONE
            job_delivery_5 = ""
        }
    }

    override fun selectedIndices6(indices: LinkedList<Int>) {
        Log.w("index5", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat6.clear()
        job_nameList6.clear()
        admin_tasks6.setText("")
        mChecked6.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array6.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array6.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll5", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 6)
        rv_main_id6.adapter = adapter
    }

    override fun selectedStrings6(strings: LinkedList<String>) {
        dep_def_ids_flag6 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_six.visibility = View.VISIBLE
            job_mailing_6 = "Mailing"
        } else {
            ll_miling_six.visibility = View.GONE
            job_mailing_6 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_six.visibility = View.VISIBLE
            job_shipping_6 = "Shipping"
        } else {
            ll_shipping_six.visibility = View.GONE
            job_shipping_6 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_6.visibility = View.VISIBLE
            job_delivery_6 = "Delivery"
        } else {
            ll_delivery_terms_6.visibility = View.GONE
            job_delivery_6 = ""
        }
    }

    override fun selectedIndices7(indices: LinkedList<Int>) {
        Log.w("index5", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat7.clear()
        job_nameList7.clear()
        admin_tasks7.setText("")
        mChecked7.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array7.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array7.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll5", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 7)
        rv_main_id7.adapter = adapter
    }

    override fun selectedStrings7(strings: LinkedList<String>) {
        dep_def_ids_flag7 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_seven.visibility = View.VISIBLE
            job_mailing_7 = "Mailing"
        } else {
            ll_miling_seven.visibility = View.GONE
            job_mailing_7 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_seven.visibility = View.VISIBLE
            job_shipping_7 = "Shipping"
        } else {
            ll_shipping_seven.visibility = View.GONE
            job_shipping_7 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_7.visibility = View.VISIBLE
            job_delivery_7 = "Delivery"

        } else {
            ll_delivery_terms_7.visibility = View.GONE
            job_delivery_7 = ""
        }
    }
    override fun selectedIndices8(indices: LinkedList<Int>) {
        Log.w("index5", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat8.clear()
        job_nameList8.clear()
        admin_tasks8.setText("")
        mChecked8.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array8.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array8.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll5", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 8)
        rv_main_id8.adapter = adapter
    }

    override fun selectedStrings8(strings: LinkedList<String>) {
        dep_def_ids_flag8 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_eight.visibility = View.VISIBLE
            job_mailing_8 = "Mailing"
        } else {
            ll_miling_eight.visibility = View.GONE
            job_mailing_8 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_eight.visibility = View.VISIBLE
            job_shipping_8 = "Shipping"
        } else {
            ll_shipping_eight.visibility = View.GONE
            job_shipping_8 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_8.visibility = View.VISIBLE
            job_delivery_8 = "Delivery"
        } else {
            ll_delivery_terms_8.visibility = View.GONE
            job_delivery_8 = ""
        }
    }
    override fun selectedIndices9(indices: LinkedList<Int>) {
        Log.w("index5", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat9.clear()
        job_nameList9.clear()
        admin_tasks9.setText("")
        mChecked9.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array9.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array9.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll5", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }

        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 9)
        rv_main_id9.adapter = adapter
    }
    override fun selectedStrings9(strings: LinkedList<String>) {
        dep_def_ids_flag9 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_nine.visibility = View.VISIBLE
            job_mailing_9 = "Mailing"
        } else {
            ll_miling_nine.visibility = View.GONE
            job_mailing_9 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_nine.visibility = View.VISIBLE
            job_shipping_9 = "Shipping"
        } else {
            ll_shipping_nine.visibility = View.GONE
            job_shipping_9 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_9.visibility = View.VISIBLE
            job_delivery_9 = "Delivery"
        } else {
            ll_delivery_terms_9.visibility = View.GONE
            job_delivery_9 = ""
        }
    }

    override fun selectedIndices10(indices: LinkedList<Int>) {
        Log.w("index5", "::::" + indices)
        val taskmain = ""
        mSingleTasklistandCat10.clear()
        job_nameList10.clear()
        admin_tasks10.setText("")
        mChecked10.clear()
        val mTaskListarray = ArrayList<ListCellk>()
        val mTaskslistids = ArrayList<ListCellk>()
        departselectedid_array10.clear()
        var departlappend = false
        val departidssb = StringBuilder()
        for (j in 0 until indices.size) {
            val mTaskslistfinal = ArrayList<String>()
            departselectedid_array10.add(mDepartmentListArray.get(indices.get(j)).id.toString())
            Log.e("main_depart_lll5", indices.size.toString() + "---" + mDepartmentListArray.get(indices.get(j)).id.toString())
            var mString = ""
            for (k in 0 until mDepartmentListArray.get(indices.get(j)).tasks!!.size) {
                mTaskslistfinal.add(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString())
                if (mString == mDepartmentListArray.get(indices.get(j)).department_name.toString()) {
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                } else {
                    mString = mDepartmentListArray.get(indices.get(j)).department_name.toString()
                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(indices.get(j)).department_name.toString(), mDepartmentListArray.get(indices.get(j)).tasks!!.get(k).id.toString(), mDepartmentListArray.get(indices.get(j)).id.toString()))
                }
            }
        }
        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 10)
        rv_main_id10.adapter = adapter
    }
    override fun selectedStrings10(strings: LinkedList<String>) {
        dep_def_ids_flag10 = false
        if (strings.toString().contains("Mailing")) {
            ll_miling_ten.visibility = View.VISIBLE
            job_mailing_10 = "Mailing"
        } else {
            ll_miling_ten.visibility = View.GONE
            job_mailing_10 = ""
        }
        if (strings.toString().contains("Shipping")) {
            ll_shipping_ten.visibility = View.VISIBLE
            job_shipping_10 = "Shipping"
        } else {
            ll_shipping_ten.visibility = View.GONE
            job_shipping_10 = ""
        }
        if (strings.toString().contains("Delivery")) {
            ll_delivery_terms_10.visibility = View.VISIBLE
            job_delivery_10 = "Delivery"
        } else {
            ll_delivery_terms_10.visibility = View.GONE
            job_delivery_10 = ""
        }
    }
    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(admin_customer_id.text.toString()))
            admin_customer_id.error = "Customer Required"
        else if (TextUtils.isEmpty(admin_job_type_id.text.toString()))
            admin_job_type_id.error = "Jobtype Required"
        else if (TextUtils.isEmpty(admin_job_id.text.toString()))
            admin_job_id.error = "Jobid Required"
        else if (chkJobIderrorStg.equals("Already Epace Job Number is Exist.")) {
            tv_error_jobID.visibility = View.VISIBLE
            tv_error_jobID.text = "Please Check with other Name"
        } else if (TextUtils.isEmpty(admin_job_part_id.text.toString()))
            admin_job_part_id.error = "Job Part Required"
        else if (TextUtils.isEmpty(admin_Estart_date_id.text.toString()))
            admin_Estart_date_id.error = "StartDate Required"
        else if (TextUtils.isEmpty(admin_description_id.text.toString()))
            admin_description_id.error = "Description Required"
        else if (TextUtils.isEmpty(admin_Pdata_id.text.toString()))
            admin_Pdata_id.error = "Promisedata Required"
        else if (TextUtils.isEmpty(admin_tasks.text.toString()))
            admin_tasks.error = "Please Select Departments and Tasks"
        else
            return true
        return false
    }

    private fun CallDepartmentAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.getDepartmentList()
        Log.d("REQUEST", call.toString() + "")
        mSingleTasklistandCat.clear()
        mSingleTasklistandCat2.clear()
        mSingleTasklistandCat3.clear()
        mSingleTasklistandCat4.clear()
        call.enqueue(object : Callback<GetDepartmentListResponse> {
            override fun onResponse(call: Call<GetDepartmentListResponse>, response: retrofit2.Response<GetDepartmentListResponse>?) {
                if (response != null) {
                    Log.w("Result_Order_details", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.departmentsList != null) {
                        val list: Array<DepatListName>? = response.body()!!.departmentsList!!
                        for (item: DepatListName in list!!.iterator()) {
                            mDepartmentListArray.add(item)
                        }
                        for (i in 0 until mDepartmentListArray.size) {
                            mGetDepartmentList_names.add(mDepartmentListArray.get(i).department_name.toString())
                            mGetDepartmentList_ids.add(mDepartmentListArray.get(i).id.toString())
                            val list: Array<DeprtTaskList>? = mDepartmentListArray.get(i).tasks!!
                            for (item: DeprtTaskList in list!!.iterator()) {
                                mDepartTasksArray.add(item)
                            }
                            mIndexList.add(i)
                        }
                        mSingleArray = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray2 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray3 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray4 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray5 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray6 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray7 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray8 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray9 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        mSingleArray10 = Array(mDepartmentListArray.size, { i -> mDepartmentListArray.get(i).department_name.toString() })
                        admin_departments.setItems(mSingleArray, 1)
                        admin_departments2.setItems(mSingleArray2, 2)
                        admin_departments3.setItems(mSingleArray3, 3)
                        admin_departments4.setItems(mSingleArray4, 4)
                        admin_departments5.setItems(mSingleArray5, 5)
                        admin_departments6.setItems(mSingleArray6, 6)
                        admin_departments7.setItems(mSingleArray7, 7)
                        admin_departments8.setItems(mSingleArray8, 8)
                        admin_departments9.setItems(mSingleArray9, 9)
                        admin_departments10.setItems(mSingleArray10, 10)
                        for (i in 0 until mDepartTasksArray!!.size) {
                            mGetTasksList_names.add(mDepartTasksArray.get(i).task_name.toString())
                            mGetTasksList_ids.add(mDepartTasksArray.get(i).id.toString())
                        }
                        var appendSeparator = false
                        val sb = StringBuilder()
                        for (y in 0 until mGetTasksList_names.size) {
                            if (appendSeparator)
                                sb.append(',') // a comma
                            appendSeparator = true
                            sb.append(mGetTasksList_names.get(y))
                        }
                        admin_tasks.setText(sb)
                        admin_tasks2.setText(sb)
                        admin_tasks3.setText(sb)
                        admin_tasks4.setText(sb)
                        admin_tasks5.setText(sb)
                        admin_tasks6.setText(sb)
                        admin_tasks7.setText(sb)
                        admin_tasks8.setText(sb)
                        admin_tasks9.setText(sb)
                        admin_tasks10.setText(sb)
                        val intArrayv = IntArray(mGetDepartmentList_names.size)
                        val intArrayv1 = IntArray(mGetDepartmentList_names.size)
                        val intArrayv2 = IntArray(mGetDepartmentList_names.size)
                        val intArrayv3 = IntArray(mGetDepartmentList_names.size)
                        val intArrayv5 = IntArray(mGetDepartmentList_names.size)
                        val intArrayv6 = IntArray(mGetDepartmentList_names.size)
                        val intArrayv7 = IntArray(mGetDepartmentList_names.size)
                        val intArrayv8 = IntArray(mGetDepartmentList_names.size)
                        val intArrayv9 = IntArray(mGetDepartmentList_names.size)
                        val intArrayv10 = IntArray(mGetDepartmentList_names.size)
                        for (index in 0 until mIndexList.size) {
                            intArrayv[index] = mIndexList.get(index)
                            intArrayv1[index] = mIndexList.get(index)
                            intArrayv2[index] = mIndexList.get(index)
                            intArrayv3[index] = mIndexList.get(index)
                            intArrayv5[index] = mIndexList.get(index)
                            intArrayv6[index] = mIndexList.get(index)
                            intArrayv7[index] = mIndexList.get(index)
                            intArrayv8[index] = mIndexList.get(index)
                            intArrayv9[index] = mIndexList.get(index)
                            intArrayv10[index] = mIndexList.get(index)
                        }
                        admin_departments.setSelection(intArrayv)
                        admin_departments2.setSelection1(intArrayv1)
                        admin_departments3.setSelection2(intArrayv2)
                        admin_departments4.setSelection3(intArrayv3)
                        admin_departments5.setSelection5(intArrayv5)
                        admin_departments6.setSelection6(intArrayv6)
                        admin_departments7.setSelection7(intArrayv7)
                        admin_departments8.setSelection8(intArrayv8)
                        admin_departments9.setSelection9(intArrayv9)
                        admin_departments10.setSelection10(intArrayv10)
                        job_nameList.clear()
                        job_nameList2.clear()
                        job_nameList3.clear()
                        job_nameList4.clear()
                        val mTaskListarray = ArrayList<ListCellk>()
                        val mTaskListarray2 = ArrayList<ListCellk>()
                        val mTaskListarray3 = ArrayList<ListCellk>()
                        val mTaskListarray4 = ArrayList<ListCellk>()
                        val mTaskListarray5 = ArrayList<ListCellk>()
                        val mTaskListarray6 = ArrayList<ListCellk>()
                        val mTaskListarray7 = ArrayList<ListCellk>()
                        val mTaskListarray8 = ArrayList<ListCellk>()
                        val mTaskListarray9 = ArrayList<ListCellk>()
                        val mTaskListarray10 = ArrayList<ListCellk>()
                        departselectedid_array.clear()
                        departselectedid_array2.clear()
                        departselectedid_array3.clear()
                        departselectedid_array4.clear()
                        for (j in 0 until mDepartmentListArray.size) {
                            val mTaskslistfinal = ArrayList<String>()
                            departselectedid_array.add(mDepartmentListArray.get(j).id.toString())
                            departselectedid_array2.add(mDepartmentListArray.get(j).id.toString())
                            departselectedid_array3.add(mDepartmentListArray.get(j).id.toString())
                            departselectedid_array4.add(mDepartmentListArray.get(j).id.toString())
                            var mString = ""
                            for (k in 0 until mDepartmentListArray.get(j).tasks!!.size) {
                                mTaskslistfinal.add(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString())
                                if (mString == mDepartmentListArray.get(j).department_name.toString()) {
                                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray2.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray3.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray4.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray5.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray6.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray7.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray8.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray9.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray10.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), "", mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mDepartTasksnamesList.add(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString())
                                } else {
                                    mString = mDepartmentListArray.get(j).department_name.toString()
                                    mTaskListarray.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray2.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray3.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray4.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray5.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray6.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray7.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray8.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray9.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                    mTaskListarray10.add(ListCellk(mDepartmentListArray.get(j).tasks!!.get(k).task_name.toString(), mDepartmentListArray.get(j).department_name.toString(), mDepartmentListArray.get(j).tasks!!.get(k).id.toString(), mDepartmentListArray.get(j).id.toString()))
                                }
                            }
                        }
                        if (flag) {
                            defaultSelectedData(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray)
                        }
                        if (dep_def_ids_flag) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList.get(y))
                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat.get(y))
                            }
                            al1 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat.size) {
                                hs.add(mSingleTasklistandCat.get(z).split("_").get(1))
                            }
                            al1.addAll(hs)
                            for (k in 0 until al1.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al1.get(k))
                            }
                            tasksselectedid_only = (idsb.toString())
                            departselectedid_only = (jobid.toString())
                            println(sb)
                            departselectedid_only = (only_dep_ids)
                        }
                        val adapter = ListAdapterk(this@AddJobActivity, mTaskListarray, 1)
                        rv_main_id.adapter = adapter
                        //TEST
                        if (flag2) {
                            defaultSelectedData2(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray2)
                        }
                        if (dep_def_ids_flag2) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only2 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList2.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList2.get(y))

                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat2.get(y))
                            }
                            al2 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat2.size) {
                                hs.add(mSingleTasklistandCat2.get(z).split("_").get(1))
                            }
                            al2.addAll(hs)
                            for (k in 0 until al2.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al2.get(k))
                            }
                            tasksselectedid_only2 = (idsb.toString())
                            departselectedid_only2 = (jobid.toString())
                            println(sb)
                            departselectedid_only2 = (only_dep_ids)
                        }
                        val adapter2 = ListAdapterk(this@AddJobActivity, mTaskListarray2, 2)
                        rv_main_id2.adapter = adapter2
                        //TEST3
                        if (flag3) {
                            defaultSelectedData3(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray3)
                        }
                        if (dep_def_ids_flag3) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()

                            tasksselectedid_only3 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList3.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList3.get(y))
                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat3.get(y))
                            }
                            al3 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat3.size) {
                                hs.add(mSingleTasklistandCat3.get(z).split("_").get(1))
                            }
                            al3.addAll(hs)
                            for (k in 0 until al3.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al3.get(k))
                            }
                            tasksselectedid_only3 = (idsb.toString())
                            departselectedid_only3 = (jobid.toString())
                            println(sb)
                            departselectedid_only3 = (only_dep_ids)
                        }
                        val adapter3 = ListAdapterk(this@AddJobActivity, mTaskListarray3, 3)
                        rv_main_id3.adapter = adapter3
                        //TEST4
                        if (flag4) {
                            defaultSelectedData4(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray4)
                        }
                        if (dep_def_ids_flag4) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only4 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList4.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList4.get(y))
                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat4.get(y))
                            }
                            al4 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat4.size) {
                                hs.add(mSingleTasklistandCat4.get(z).split("_").get(1))
                            }
                            al4.addAll(hs)
                            for (k in 0 until al4.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al4.get(k))
                            }
                            tasksselectedid_only4 = (idsb.toString())
                            departselectedid_only4 = (jobid.toString())
                            println(sb)
                            departselectedid_only4 = (only_dep_ids)
                        }
                        val adapter4 = ListAdapterk(this@AddJobActivity, mTaskListarray4, 4)
                        rv_main_id4.adapter = adapter4
                        if (flag5) {
                            defaultSelectedData5(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray5)
                        }
                        if (dep_def_ids_flag5) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only5 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList5.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList5.get(y))
                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat5.get(y))
                            }
                            al5 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat5.size) {
                                hs.add(mSingleTasklistandCat5.get(z).split("_").get(1))
                            }
                            al5.addAll(hs)
                            for (k in 0 until al5.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al5.get(k))
                            }
                            tasksselectedid_only5 = (idsb.toString())
                            departselectedid_only5 = (jobid.toString())
                            println(sb)
                            departselectedid_only5 = (only_dep_ids)
                        }
                        val adapter5 = ListAdapterk(this@AddJobActivity, mTaskListarray5, 5)
                        rv_main_id5.adapter = adapter5
                        //TEST6
                        if (flag6) {
                            defaultSelectedData6(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray6)
                        }
                        if (dep_def_ids_flag6) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only5 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList6.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList6.get(y))
                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat6.get(y))
                            }
                            al6 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat6.size) {
                                hs.add(mSingleTasklistandCat6.get(z).split("_").get(1))
                            }
                            al6.addAll(hs)
                            for (k in 0 until al6.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al6.get(k))
                            }
                            tasksselectedid_only6 = (idsb.toString())
                            departselectedid_only6 = (jobid.toString())
                            println(sb)
                            departselectedid_only6 = (only_dep_ids)
                        }
                        val adapter6 = ListAdapterk(this@AddJobActivity, mTaskListarray6, 6)
                        rv_main_id6.adapter = adapter6
                        if (flag7) {
                            defaultSelectedData7(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray7)
                        }
                        if (dep_def_ids_flag7) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only7 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList7.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList7.get(y))
                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat7.get(y))
                            }
                            al7 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat7.size) {
                                hs.add(mSingleTasklistandCat7.get(z).split("_").get(1))
                            }
                            al7.addAll(hs)
                            for (k in 0 until al7.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al7.get(k))
                            }
                            tasksselectedid_only7 = (idsb.toString())
                            departselectedid_only7 = (jobid.toString())
                            println(sb)
                            departselectedid_only7 = (only_dep_ids)
                        }
                        val adapter7 = ListAdapterk(this@AddJobActivity, mTaskListarray7, 7)
                        rv_main_id7.adapter = adapter7
                        if (flag8) {
                            defaultSelectedData8(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray)
                        }
                        if (dep_def_ids_flag8) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only8 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList8.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList8.get(y))

                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat8.get(y))
                            }
                            al8 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat8.size) {
                                hs.add(mSingleTasklistandCat8.get(z).split("_").get(1))
                            }
                            al8.addAll(hs)
                            for (k in 0 until al8.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al8.get(k))
                            }
                            tasksselectedid_only8 = (idsb.toString())
                            departselectedid_only8 = (jobid.toString())
                            println(sb)
                            departselectedid_only8 = (only_dep_ids)
                        }
                        val adapter8 = ListAdapterk(this@AddJobActivity, mTaskListarray8, 8)
                        rv_main_id8.adapter = adapter8
                        if (flag9) {
                            defaultSelectedData9(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray)
                        }
                        if (dep_def_ids_flag9) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only9 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()
                            val size = job_nameList9.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList9.get(y))

                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat9.get(y))
                            }
                            al9 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat9.size) {
                                hs.add(mSingleTasklistandCat9.get(z).split("_").get(1))
                            }
                            al9.addAll(hs)
                            for (k in 0 until al9.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al9.get(k))
                            }
                            tasksselectedid_only9 = (idsb.toString())
                            departselectedid_only9 = (jobid.toString())
                            println(sb)
                            departselectedid_only9 = (only_dep_ids)
                        }
                        val adapter9 = ListAdapterk(this@AddJobActivity, mTaskListarray9, 9)
                        rv_main_id9.adapter = adapter9
                        if (flag10) {
                            defaultSelectedData10(mGetDepartmentList_ids, mGetTasksList_ids, mTaskListarray)
                        }
                        if (dep_def_ids_flag10) {
                            val only_dep_ids = mGetDepartmentList_ids.toString()
                            tasksselectedid_only10 = ""
                            val sb = StringBuilder()
                            val idsb = StringBuilder()
                            val jobid = StringBuilder()

                            val size = job_nameList10.size
                            var appendSeparator = false
                            var taslappend = false
                            var jobappend = false
                            for (y in 0 until size) {
                                if (appendSeparator)
                                    sb.append(',') // a comma
                                appendSeparator = true
                                sb.append(job_nameList10.get(y))
                                if (taslappend)
                                    idsb.append(',') // a comma
                                taslappend = true
                                idsb.append(mSingleTasklistandCat10.get(y))
                            }
                            al10 = ArrayList<String>()
                            val hs = HashSet<String>()
                            for (z in 0 until mSingleTasklistandCat10.size) {
                                hs.add(mSingleTasklistandCat10.get(z).split("_").get(1))
                            }
                            al10.addAll(hs)
                            for (k in 0 until al10.size) {
                                if (jobappend)
                                    jobid.append(',') // a comma
                                jobappend = true
                                jobid.append(al10.get(k))
                            }
                            tasksselectedid_only10 = (idsb.toString())
                            departselectedid_only10 = (jobid.toString())
                            println(sb)
                            departselectedid_only10 = (only_dep_ids)
                        }
                        val adapter10 = ListAdapterk(this@AddJobActivity, mTaskListarray10, 10)
                        rv_main_id10.adapter = adapter10
                    } else if (response.body()!!.status.equals("2")) {
                    }

                }
            }
            override fun onFailure(call: Call<GetDepartmentListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }
    private fun callgetJobsAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.getjobTypeList()
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<GetJobTypeListResponse> {
            override fun onResponse(call: Call<GetJobTypeListResponse>, response: retrofit2.Response<GetJobTypeListResponse>?) {
                if (response != null) {
                    mServiceInfo_Jobname_id.clear()
                    Log.w("Result_Order_details", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.jobtypesList != null) {
                        val list: Array<JobTypeResponse>? = response.body()!!.jobtypesList!!
                        for (item in 0 until list!!.size) {
                            mServiceInfo_Jobname.add(list.get(item).job_name!!)
                            mServiceInfo_Jobname_id.add(list.get(item))
                        }
                        if (list.size == 0) {
                        }
                    } else if (response.body()!!.status.equals("2")) {
                    }
                    val adapter = Dataadapter(this@AddJobActivity, mServiceInfo_Jobname_id)
                    list_items_list_type.adapter = adapter
                }
            }
            override fun onFailure(call: Call<GetJobTypeListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun AddJobCallAPI(deptarry: JSONArray, taskarry: JSONArray, termsarray: ArrayList<String>, terms: String, shipping_termsarray: ArrayList<String>, delivery_termsarray: ArrayList<String>) {
        val paper_id_array = ArrayList<String>()
        paper_id_array.add(paperid1)
        paper_id_array.add(paperid2)
        paper_id_array.add(paperid3)
        paper_id_array.add(paperid)
        paper_id_array.add(paperid5)
        paper_id_array.add(paperid6)
        paper_id_array.add(paperid7)
        paper_id_array.add(paperid8)
        paper_id_array.add(paperid9)
        paper_id_array.add(paperid10)

        val option_id_array = ArrayList<String>()
        option_id_array.add(option_id1)
        option_id_array.add(option_id2)
        option_id_array.add(option_id3)
        option_id_array.add(option_id)
        option_id_array.add(option_id5)
        option_id_array.add(option_id6)
        option_id_array.add(option_id7)
        option_id_array.add(option_id8)
        option_id_array.add(option_id9)
        option_id_array.add(option_id10)
        val other_paper_type_array = ArrayList<String>()
        other_paper_type_array.add("\"" + other_desc_one.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc_two.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc_three.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc_five.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc_six.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc_seven.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc_eight.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc_nine.text.toString() + "\"")
        other_paper_type_array.add("\"" + other_desc_ten.text.toString() + "\"")
        val text_id_array = ArrayList<String>()
        val sub_text_id_array = ArrayList<String>()
        val weightid_array = ArrayList<String>()
        val sizeid_array = ArrayList<String>()

        if (other_val_job_1.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid1)
            sub_text_id_array.add(subtxtid1)
            weightid_array.add(weightid1)
            sizeid_array.add(sizeid1)
        }
        if (other_val_job_2.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid2)
            sub_text_id_array.add(subtxtid2)
            weightid_array.add(weightid2)
            sizeid_array.add(sizeid2)
        }
        if (other_val_job_3.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid3)
            sub_text_id_array.add(subtxtid3)
            weightid_array.add(weightid3)
            sizeid_array.add(sizeid3)
        }
        if (other_val_job.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid)
            sub_text_id_array.add(subtxtid)
            weightid_array.add(weightid)
            sizeid_array.add(sizeid)
        }
        if (other_val_job_5.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid5)
            sub_text_id_array.add(subtxtid5)
            weightid_array.add(weightid5)
            sizeid_array.add(sizeid5)
        }
        if (other_val_job_6.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid6)
            sub_text_id_array.add(subtxtid6)
            weightid_array.add(weightid6)
            sizeid_array.add(sizeid6)
        }
        if (other_val_job_7.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid7)
            sub_text_id_array.add(subtxtid7)
            weightid_array.add(weightid7)
            sizeid_array.add(sizeid7)
        }
        if (other_val_job_8.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid8)
            sub_text_id_array.add(subtxtid8)
            weightid_array.add(weightid8)
            sizeid_array.add(sizeid8)
        }
        if (other_val_job_9.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid9)
            sub_text_id_array.add(subtxtid9)
            weightid_array.add(weightid9)
            sizeid_array.add(sizeid9)
        }
        if (other_val_job_10.equals("Other")) {
            text_id_array.add("0")
            sub_text_id_array.add("0")
            weightid_array.add("0")
            sizeid_array.add("0")
        } else {
            text_id_array.add(txtid10)
            sub_text_id_array.add(subtxtid10)
            weightid_array.add(weightid10)
            sizeid_array.add(sizeid10)
        }
        val alloted_sheets_array = ArrayList<String>()
        if (allotsheets1.text.toString() != "") {
            aloted_sheets1 = allotsheets1.text.toString()
        }
        if (allotsheets2.text.toString() != "") {
            aloted_sheets2 = allotsheets2.text.toString()
        }
        if (allotsheets3.text.toString() != "") {
            aloted_sheets3 = allotsheets3.text.toString()
        }
        if (allotsheets.text.toString() != "") {
            aloted_sheets = allotsheets.text.toString()
        }
        if (allotsheets5.text.toString() != "") {
            aloted_sheets5 = allotsheets5.text.toString()
        }
        if (allotsheets6.text.toString() != "") {
            aloted_sheets6 = allotsheets6.text.toString()
        }
        if (allotsheets7.text.toString() != "") {
            aloted_sheets7 = allotsheets7.text.toString()
        }
        if (allotsheets8.text.toString() != "") {
            aloted_sheets8 = allotsheets8.text.toString()
        }
        if (allotsheets9.text.toString() != "") {
            aloted_sheets9 = allotsheets9.text.toString()
        }
        if (allotsheets10.text.toString() != "") {
            aloted_sheets10 = allotsheets10.text.toString()
        }
        alloted_sheets_array.add(aloted_sheets1)
        alloted_sheets_array.add(aloted_sheets2)
        alloted_sheets_array.add(aloted_sheets3)
        alloted_sheets_array.add(aloted_sheets)
        alloted_sheets_array.add(aloted_sheets5)
        alloted_sheets_array.add(aloted_sheets6)
        alloted_sheets_array.add(aloted_sheets7)
        alloted_sheets_array.add(aloted_sheets8)
        alloted_sheets_array.add(aloted_sheets9)
        alloted_sheets_array.add(aloted_sheets10)
        val totalquantity_array = ArrayList<String>()
        if (totalquantity1.text.toString() != "") {
            tot_qty1 = totalquantity1.text.toString()
        }
        if (totalquantity2.text.toString() != "") {
            tot_qty2 = totalquantity2.text.toString()
        }
        if (totalquantity3.text.toString() != "") {
            tot_qty3 = totalquantity3.text.toString()
        }
        if (totalquantity.text.toString() != "") {
            tot_qty = totalquantity.text.toString()
        }
        if (totalquantity5.text.toString() != "") {
            tot_qty5 = totalquantity5.text.toString()
        }
        if (totalquantity6.text.toString() != "") {
            tot_qty6 = totalquantity6.text.toString()
        }
        if (totalquantity7.text.toString() != "") {
            tot_qty7 = totalquantity7.text.toString()
        }
        if (totalquantity8.text.toString() != "") {
            tot_qty8 = totalquantity8.text.toString()
        }
        if (totalquantity9.text.toString() != "") {
            tot_qty9 = totalquantity9.text.toString()
        }
        if (totalquantity10.text.toString() != "") {
            tot_qty10 = totalquantity10.text.toString()
        }
        totalquantity_array.add(tot_qty1)
        totalquantity_array.add(tot_qty2)
        totalquantity_array.add(tot_qty3)
        totalquantity_array.add(tot_qty)
        totalquantity_array.add(tot_qty5)
        totalquantity_array.add(tot_qty6)
        totalquantity_array.add(tot_qty7)
        totalquantity_array.add(tot_qty8)
        totalquantity_array.add(tot_qty9)
        totalquantity_array.add(tot_qty10)
        val no_of_terms_array = ArrayList<String>()
        if (admin_job_terms1.text.toString() != "") {
            admin_terms1 = admin_job_terms1.text.toString()
        }
        if (admin_job_terms2.text.toString() != "") {
            admin_terms2 = admin_job_terms2.text.toString()
        }
        if (admin_job_terms3.text.toString() != "") {
            admin_terms3 = admin_job_terms3.text.toString()
        }
        if (admin_job_terms.text.toString() != "") {
            admin_terms = admin_job_terms.text.toString()
        }
        if (admin_job_terms5.text.toString() != "") {
            admin_terms5 = admin_job_terms5.text.toString()
        }
        if (admin_job_terms6.text.toString() != "") {
            admin_terms6 = admin_job_terms6.text.toString()
        }
        if (admin_job_terms7.text.toString() != "") {
            admin_terms7 = admin_job_terms7.text.toString()
        }
        if (admin_job_terms8.text.toString() != "") {
            admin_terms8 = admin_job_terms8.text.toString()
        }
        if (admin_job_terms9.text.toString() != "") {
            admin_terms9 = admin_job_terms9.text.toString()
        }
        if (admin_job_terms10.text.toString() != "") {
            admin_terms10 = admin_job_terms10.text.toString()
        }
        no_of_terms_array.add(admin_terms1)
        no_of_terms_array.add(admin_terms2)
        no_of_terms_array.add(admin_terms3)
        no_of_terms_array.add(admin_terms)
        no_of_terms_array.add(admin_terms5)
        no_of_terms_array.add(admin_terms6)
        no_of_terms_array.add(admin_terms7)
        no_of_terms_array.add(admin_terms8)
        no_of_terms_array.add(admin_terms9)
        no_of_terms_array.add(admin_terms10)
        Log.w("Arrays ", ":::" + paper_id_array.toString())
        var no_of_delivery_1_stg: String = "0"
        var no_of_delivery_2_stg: String = "0"
        var no_of_delivery_3_stg: String = "0"
        var no_of_delivery_4_stg: String = "0"
        var no_of_delivery_5_stg: String = "0"
        var no_of_delivery_6_stg: String = "0"
        var no_of_delivery_7_stg: String = "0"
        var no_of_delivery_8_stg: String = "0"
        var no_of_delivery_9_stg: String = "0"
        var no_of_delivery_10_stg: String = "0"
        val no_of_shipping_terms = ArrayList<String>()
        if (admin_shipping_terms1.text.toString() != "") {
            no_of_shipping_1_stg = admin_shipping_terms1.text.toString()
        }
        if (admin_shipping_terms2.text.toString() != "") {
            no_of_shipping_2_stg = admin_shipping_terms2.text.toString()
        }
        if (admin_shipping_terms3.text.toString() != "") {
            no_of_shipping_3_stg = admin_shipping_terms3.text.toString()
        }
        if (admin_shipping_terms4.text.toString() != "") {
            no_of_shipping_4_stg = admin_shipping_terms4.text.toString()
        }
        if (admin_shipping_terms5.text.toString() != "") {
            no_of_shipping_5_stg = admin_shipping_terms5.text.toString()
        }
        if (admin_shipping_terms6.text.toString() != "") {
            no_of_shipping_6_stg = admin_shipping_terms6.text.toString()
        }
        if (admin_shipping_terms7.text.toString() != "") {
            no_of_shipping_7_stg = admin_shipping_terms7.text.toString()
        }
        if (admin_shipping_terms8.text.toString() != "") {
            no_of_shipping_8_stg = admin_shipping_terms8.text.toString()
        }
        if (admin_shipping_terms9.text.toString() != "") {
            no_of_shipping_9_stg = admin_shipping_terms9.text.toString()
        }
        if (admin_shipping_terms10.text.toString() != "") {
            no_of_shipping_10_stg = admin_shipping_terms10.text.toString()
        }

        no_of_shipping_terms.add(no_of_shipping_1_stg)
        no_of_shipping_terms.add(no_of_shipping_2_stg)
        no_of_shipping_terms.add(no_of_shipping_3_stg)
        no_of_shipping_terms.add(no_of_shipping_4_stg)
        no_of_shipping_terms.add(no_of_shipping_5_stg)
        no_of_shipping_terms.add(no_of_shipping_6_stg)
        no_of_shipping_terms.add(no_of_shipping_7_stg)
        no_of_shipping_terms.add(no_of_shipping_8_stg)
        no_of_shipping_terms.add(no_of_shipping_9_stg)
        no_of_shipping_terms.add(no_of_shipping_10_stg)

        val no_of_delivery_terms = ArrayList<String>()
        if (admin_delivery_terms1.text.toString() != "") {
            no_of_delivery_1_stg = admin_delivery_terms1.text.toString()
        }
        if (admin_delivery_terms2.text.toString() != "") {
            no_of_delivery_2_stg = admin_delivery_terms2.text.toString()
        }
        if (admin_delivery_terms3.text.toString() != "") {
            no_of_delivery_3_stg = admin_delivery_terms3.text.toString()
        }
        if (admin_delivery_terms4.text.toString() != "") {
            no_of_delivery_4_stg = admin_delivery_terms4.text.toString()
        }
        if (admin_delivery_terms5.text.toString() != "") {
            no_of_delivery_5_stg = admin_delivery_terms5.text.toString()
        }
        if (admin_delivery_terms6.text.toString() != "") {
            no_of_delivery_6_stg = admin_delivery_terms6.text.toString()
        }
        if (admin_delivery_terms7.text.toString() != "") {
            no_of_delivery_7_stg = admin_delivery_terms7.text.toString()
        }
        if (admin_delivery_terms8.text.toString() != "") {
            no_of_delivery_8_stg = admin_delivery_terms8.text.toString()
        }
        if (admin_delivery_terms9.text.toString() != "") {
            no_of_delivery_9_stg = admin_delivery_terms9.text.toString()
        }
        if (admin_delivery_terms10.text.toString() != "") {
            no_of_delivery_10_stg = admin_delivery_terms10.text.toString()
        }
        no_of_delivery_terms.add(no_of_delivery_1_stg)
        no_of_delivery_terms.add(no_of_delivery_2_stg)
        no_of_delivery_terms.add(no_of_delivery_3_stg)
        no_of_delivery_terms.add(no_of_delivery_4_stg)
        no_of_delivery_terms.add(no_of_delivery_5_stg)
        no_of_delivery_terms.add(no_of_delivery_6_stg)
        no_of_delivery_terms.add(no_of_delivery_7_stg)
        no_of_delivery_terms.add(no_of_delivery_8_stg)
        no_of_delivery_terms.add(no_of_delivery_9_stg)
        no_of_delivery_terms.add(no_of_delivery_10_stg)

        val job_part_title_array = ArrayList<String>()
        if (job_title_one_id.text.toString() != "") {
            job_title_1_stg = job_title_one_id.text.toString()
        }
        if (job_title_two_id.text.toString() != "") {
            job_title_2_stg = job_title_two_id.text.toString()
        }
        if (job_title_three_id.text.toString() != "") {
            job_title_3_stg = job_title_three_id.text.toString()
        }
        if (job_title_four_id.text.toString() != "") {
            job_title_4_stg = job_title_four_id.text.toString()
        }
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")
        job_part_title_array.add("\"" + "" + "\"")

        val job_part_desc_array = ArrayList<String>()
        if (job_desc_one_id.text.toString() != "") {
            job_desc_1_stg = job_desc_one_id.text.toString()
        }
        if (job_desc_two_id.text.toString() != "") {
            job_desc_2_stg = job_desc_two_id.text.toString()
        }
        if (job_desc_three_id.text.toString() != "") {
            job_desc_3_stg = job_desc_three_id.text.toString()
        }
        if (job_desc_four_id.text.toString() != "") {
            job_desc_4_stg = job_desc_four_id.text.toString()
        }
        if (job_desc_five_id.text.toString() != "") {
            job_desc_5_stg = job_desc_five_id.text.toString()
        }
        if (job_desc_six_id.text.toString() != "") {
            job_desc_6_stg = job_desc_six_id.text.toString()
        }
        if (job_desc_seven_id.text.toString() != "") {
            job_desc_7_stg = job_desc_seven_id.text.toString()
        }
        if (job_desc_eight_id.text.toString() != "") {
            job_desc_8_stg = job_desc_eight_id.text.toString()
        }
        if (job_desc_nine_id.text.toString() != "") {
            job_desc_9_stg = job_desc_nine_id.text.toString()
        }
        if (job_desc_ten_id.text.toString() != "") {
            job_desc_10_stg = job_desc_ten_id.text.toString()
        }
        job_part_desc_array.add("\"" + job_desc_1_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_2_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_3_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_4_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_5_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_6_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_7_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_8_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_9_stg + "\"")
        job_part_desc_array.add("\"" + job_desc_10_stg + "\"")
        val apiService = ApiInterface.create()
        Log.w("customer_search_id ", customer_search_id + "---" + job_part_title_array)
        val call = apiService.postAddjobData(option_id = option_id_array.toString(), job_id = admin_job_stg!!, job_type_id = mJobType_id, job_part = admin_job_part_stg!!, earliest_start_date = admin_Estart_date_stg!!, custmer = customer_search_id,
                description = admin_description_stg!!, promise_date = admin_Pdata_stg!!, department_ids = deptarry.toString(),
                task_ids = taskarry.toString(), admin_id = admin_id_main, type = "android", no_of_terms = no_of_terms_array.toString(), terms = termsarray.toString(), mailing_qty = totalquantity_array.toString()
                , paper_id = paper_id_array.toString(), text_id = text_id_array.toString(), weight_id = weightid_array.toString(), sub_text_id = sub_text_id_array.toString(), size_id = sizeid_array.toString(), alloted_sheets = alloted_sheets_array.toString(),
                job_title = job_part_title_array.toString(), job_description = job_part_desc_array.toString(), number_of_shipping_terms = no_of_shipping_terms.toString(), number_of_delivery_terms = no_of_delivery_terms.toString(),
                shippingterms = shipping_termsarray.toString(), deliveryterms = delivery_termsarray.toString(), other_paper_type = other_paper_type_array.toString())
        call.enqueue(object : Callback<UserAddJobResponce> {
            override fun onResponse(call: Call<UserAddJobResponce>, response: retrofit2.Response<UserAddJobResponce>?) {
                if (response != null) {
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(applicationContext, response.body()!!.result, Toast.LENGTH_SHORT).show()
                        val home_intent = Intent(this@AddJobActivity, DashboardtListActivity::class.java)
                        home_intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(home_intent)
                    } else if (response.body()!!.status.equals("0")) {
                        Toast.makeText(applicationContext, "Please Search Customer", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            override fun onFailure(call: Call<UserAddJobResponce>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun defaultSelectedData(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        //departselectedid_only = ""
        tasksselectedid_only = ""
        val size = mGetDepartmentList_ids.size
        Log.e("fffffffffff", "Dept-size" + mGetDepartmentList_ids.size)
        Log.e("fffffffffff", "Task-size" + mGetTasksList_ids.size)
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked.put(i, true)
                    job_nameList.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList.get(y))
            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat.get(y))
        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat.size) {
            hs.add(mSingleTasklistandCat.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        Log.e("hashset", hs.size.toString() + "--" + hs.toString())
        tasksselectedid_only = (idsb.toString())
        Log.e("fffffffffff", "Dept" + this.departselectedid_only + "|" + jobid)
        Log.e("fffffffffff", "task" + tasksselectedid_only + "|" + idsb)
        flag = false

    }

    private fun defaultSelectedData2(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        tasksselectedid_only2 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked2.put(i, true)
                    job_nameList2.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat2.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList2.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList2.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList2.get(y))
            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat2.get(y))
        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat2.size) {
            hs.add(mSingleTasklistandCat2.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        tasksselectedid_only2 = (idsb.toString())
        flag2 = false
    }

    private fun defaultSelectedData3(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        //TEST3
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        tasksselectedid_only3 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked3.put(i, true)
                    job_nameList3.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat3.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList3.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList3.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList3.get(y))
            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat3.get(y))
        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat3.size) {
            hs.add(mSingleTasklistandCat3.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        Log.e("hashset3", hs.size.toString() + "--" + hs.toString())
        tasksselectedid_only3 = (idsb.toString())
        flag3 = false
    }

    private fun defaultSelectedData4(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        tasksselectedid_only4 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked4.put(i, true)
                    job_nameList4.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat4.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList4.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList4.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList4.get(y))

            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat4.get(y))

        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat4.size) {
            hs.add(mSingleTasklistandCat4.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        Log.e("hashset4", hs.size.toString() + "--" + hs.toString())
        tasksselectedid_only4 = (idsb.toString())
        flag4 = false
    }

    private fun defaultSelectedData5(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        tasksselectedid_only5 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked5.put(i, true)
                    job_nameList5.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat5.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList5.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList5.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList5.get(y))
            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat5.get(y))
        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat5.size) {
            hs.add(mSingleTasklistandCat5.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        tasksselectedid_only5 = (idsb.toString())
        flag5 = false
    }

    private fun defaultSelectedData6(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        tasksselectedid_only6 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked6.put(i, true)
                    job_nameList6.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat6.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList6.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList6.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList6.get(y))
            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat6.get(y))
        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat6.size) {
            hs.add(mSingleTasklistandCat6.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        tasksselectedid_only6 = (idsb.toString())
        flag6 = false
    }

    private fun defaultSelectedData7(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        tasksselectedid_only7 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked7.put(i, true)
                    job_nameList7.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat7.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList7.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList7.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList7.get(y))
            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat7.get(y))
        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat7.size) {
            hs.add(mSingleTasklistandCat7.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        tasksselectedid_only7 = (idsb.toString())
        flag6 = false
    }

    private fun defaultSelectedData8(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        //departselectedid_only = ""
        tasksselectedid_only8 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked8.put(i, true)
                    job_nameList8.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat8.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList8.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList8.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList8.get(y))
            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat8.get(y))

        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat8.size) {
            hs.add(mSingleTasklistandCat8.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        tasksselectedid_only8 = (idsb.toString())
        flag8 = false
    }

    private fun defaultSelectedData9(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        tasksselectedid_only9 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked9.put(i, true)
                    job_nameList9.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat9.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList9.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList9.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList9.get(y))

            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat9.get(y))

        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat9.size) {
            hs.add(mSingleTasklistandCat9.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        tasksselectedid_only9 = (idsb.toString())
        flag9 = false
    }

    private fun defaultSelectedData10(mGetDepartmentList_ids: ArrayList<String>, mGetTasksList_ids: ArrayList<String>, mTaskListarray: ArrayList<ListCellk>) {
        val sb = StringBuilder()
        val idsb = StringBuilder()
        val jobid = StringBuilder()
        //departselectedid_only = ""
        tasksselectedid_only10 = ""
        val size = mGetDepartmentList_ids.size
        var appendSeparator = false
        var taslappend = false
        var jobappend = false
        for (i in 0 until mTaskListarray.size) {
            for (j in 0 until mGetTasksList_ids.size) {
                if (mTaskListarray.get(i).task_id.equals(mGetTasksList_ids.get(j))) {
                    mChecked10.put(i, true)
                    job_nameList10.add(mTaskListarray.get(i).name)
                    mSingleTasklistandCat10.add(mTaskListarray.get(i).task_id + "_" + mTaskListarray.get(i).cat_id)
                    JobidList10.add(mTaskListarray.get(i).cat_id)
                }
            }
        }
        for (y in 0 until job_nameList10.size) {
            if (appendSeparator)
                sb.append(',') // a comma
            appendSeparator = true
            sb.append(job_nameList10.get(y))
            if (taslappend)
                idsb.append(',') // a comma
            taslappend = true
            idsb.append(mSingleTasklistandCat10.get(y))
        }
        val al = ArrayList<String>()
        val hs = HashSet<String>()
        for (z in 0 until mSingleTasklistandCat10.size) {
            hs.add(mSingleTasklistandCat10.get(z).split("_").get(1))
        }
        al.addAll(hs)
        for (k in 0 until al.size) {
            if (jobappend)
                jobid.append(',') // a comma
            jobappend = true
            jobid.append(al.get(k))
        }
        tasksselectedid_only10 = (idsb.toString())
        flag10 = false
    }
    inner class ListAdapterk(context: Context, items: java.util.ArrayList<ListCellk>, i: Int) : ArrayAdapter<ListCellk>(context, 0, items) {
        internal var inflater: LayoutInflater
        var mtaskslist = ""
        var index = 0
        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            index = i
        }
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)
            if (cell!!.isSectionHeader) {
                v = inflater.inflate(R.layout.section_header, null)
                v!!.isClickable = false
                val header = v.findViewById(R.id.name) as TextView
                header.text = cell.name
            } else {
                v = inflater.inflate(R.layout.select_task_items, null)
                val name = v!!.findViewById(R.id.tv_task_name_id) as TextView
                val category = v.findViewById(R.id.tv_category_id) as TextView
                val chbContent = v.findViewById(R.id.chbContent) as CheckBox
                val chbContent2 = v.findViewById(R.id.chbContent2) as CheckBox
                val chbContent3 = v.findViewById(R.id.chbContent3) as CheckBox
                val chbContent4 = v.findViewById(R.id.chbContent4) as CheckBox
                val chbContent5 = v.findViewById(R.id.chbContent5) as CheckBox
                val chbContent6 = v.findViewById(R.id.chbContent6) as CheckBox
                val chbContent7 = v.findViewById(R.id.chbContent7) as CheckBox
                val chbContent8 = v.findViewById(R.id.chbContent8) as CheckBox
                val chbContent9 = v.findViewById(R.id.chbContent9) as CheckBox
                val chbContent10 = v.findViewById(R.id.chbContent10) as CheckBox
                chbContent.text = cell.name
                category.text = cell.category
                if (mtaskslist.equals(cell.category)) {
                    category.visibility == View.GONE
                } else {
                    mtaskslist = cell.category
                    category.text = cell.category
                }
                if (index == 1) {
                    chbContent.setChecked(if (mChecked.get(position) == true) true else false)
                    chbContent.visibility = View.VISIBLE
                    chbContent.text = cell.name
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.GONE
                } else if (index == 2) {
                    chbContent2.setChecked(if (mChecked2.get(position) == true) true else false)
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.VISIBLE
                    chbContent2.text = cell.name
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.GONE
                } else if (index == 3) {
                    chbContent3.isChecked = mChecked3.get(position) == true
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.VISIBLE
                    chbContent3.text = cell.name
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.GONE
                } else if (index == 4) {
                    chbContent4.isChecked = mChecked4.get(position) == true
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.VISIBLE
                    chbContent4.text = cell.name
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.GONE
                } else if (index == 5) {
                    chbContent4.isChecked = mChecked5.get(position) == true
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.VISIBLE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.GONE
                    chbContent5.text = cell.name
                } else if (index == 6) {
                    chbContent4.isChecked = mChecked6.get(position) == true
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.VISIBLE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.GONE
                    chbContent6.text = cell.name
                } else if (index == 7) {
                    chbContent4.isChecked = mChecked7.get(position) == true
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.VISIBLE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.GONE
                    chbContent7.text = cell.name
                } else if (index == 8) {
                    chbContent4.isChecked = mChecked8.get(position) == true
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.VISIBLE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.GONE
                    chbContent8.text = cell.name
                } else if (index == 9) {
                    chbContent4.isChecked = mChecked9.get(position) == true
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.VISIBLE
                    chbContent10.visibility = View.GONE
                    chbContent9.text = cell.name
                } else if (index == 10) {
                    chbContent4.isChecked = mChecked10.get(position) == true
                    chbContent.visibility = View.GONE
                    chbContent2.visibility = View.GONE
                    chbContent3.visibility = View.GONE
                    chbContent4.visibility = View.GONE
                    chbContent5.visibility = View.GONE
                    chbContent6.visibility = View.GONE
                    chbContent7.visibility = View.GONE
                    chbContent8.visibility = View.GONE
                    chbContent9.visibility = View.GONE
                    chbContent10.visibility = View.VISIBLE
                    chbContent10.text = cell.name
                }
                chbContent.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        mChecked.put(position, isChecked)
                        job_nameList.add(cell.name)
                        mSingleTasklistandCat.add(cell.task_id + "_" + cell.cat_id)
                        JobidList.add(cell.cat_id)
                    } else {
                        mChecked.delete(position)
                        job_nameList.remove(cell.name)
                        mSingleTasklistandCat.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList.remove(cell.cat_id)
                    }
                }
                chbContent2.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        mChecked2.put(position, isChecked)
                        job_nameList2.add(cell.name)
                        mSingleTasklistandCat2.add(cell.task_id + "_" + cell.cat_id)
                        JobidList2.add(cell.cat_id)
                    } else {
                        mChecked2.delete(position)
                        job_nameList2.remove(cell.name)
                        mSingleTasklistandCat2.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList2.remove(cell.cat_id)
                    }
                }
                chbContent3.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        JobidList3.add(cell.cat_id)
                        mChecked3.put(position, isChecked)
                        job_nameList3.add(cell.name)
                        mSingleTasklistandCat3.add(cell.task_id + "_" + cell.cat_id)
                        JobidList3.add(cell.cat_id)
                    } else {
                        mChecked3.delete(position)
                        job_nameList3.remove(cell.name)
                        mSingleTasklistandCat3.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList3.remove(cell.cat_id)
                    }
                }
                chbContent4.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        JobidList4.add(cell.cat_id)
                        mChecked4.put(position, isChecked)
                        job_nameList4.add(cell.name)
                        mSingleTasklistandCat4.add(cell.task_id + "_" + cell.cat_id)
                        JobidList4.add(cell.cat_id)
                    } else {
                        mChecked4.delete(position)
                        job_nameList4.remove(cell.name)
                        mSingleTasklistandCat4.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList4.remove(cell.cat_id)
                    }
                }
                chbContent5.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        JobidList5.add(cell.cat_id)
                        mChecked5.put(position, isChecked)
                        job_nameList5.add(cell.name)
                        mSingleTasklistandCat5.add(cell.task_id + "_" + cell.cat_id)
                        JobidList5.add(cell.cat_id)
                    } else {
                        mChecked5.delete(position)
                        job_nameList5.remove(cell.name)
                        mSingleTasklistandCat5.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList5.remove(cell.cat_id)
                    }
                }
                chbContent6.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        JobidList6.add(cell.cat_id)
                        mChecked6.put(position, isChecked)
                        job_nameList6.add(cell.name)
                        mSingleTasklistandCat6.add(cell.task_id + "_" + cell.cat_id)
                        JobidList6.add(cell.cat_id)
                    } else {
                        mChecked6.delete(position)
                        job_nameList6.remove(cell.name)
                        mSingleTasklistandCat6.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList6.remove(cell.cat_id)
                    }
                }
                chbContent7.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        JobidList7.add(cell.cat_id)
                        mChecked7.put(position, isChecked)
                        job_nameList7.add(cell.name)
                        mSingleTasklistandCat7.add(cell.task_id + "_" + cell.cat_id)
                        JobidList7.add(cell.cat_id)
                    } else {
                        mChecked7.delete(position)
                        job_nameList7.remove(cell.name)
                        mSingleTasklistandCat7.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList7.remove(cell.cat_id)
                    }
                }
                chbContent8.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        JobidList8.add(cell.cat_id)
                        mChecked8.put(position, isChecked)
                        job_nameList8.add(cell.name)
                        mSingleTasklistandCat8.add(cell.task_id + "_" + cell.cat_id)
                        JobidList8.add(cell.cat_id)
                    } else {
                        mChecked8.delete(position)
                        job_nameList8.remove(cell.name)
                        mSingleTasklistandCat8.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList8.remove(cell.cat_id)
                    }
                }
                chbContent9.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        JobidList9.add(cell.cat_id)
                        mChecked9.put(position, isChecked)
                        job_nameList9.add(cell.name)
                        mSingleTasklistandCat9.add(cell.task_id + "_" + cell.cat_id)
                        JobidList9.add(cell.cat_id)
                    } else {
                        mChecked9.delete(position)
                        job_nameList9.remove(cell.name)
                        mSingleTasklistandCat9.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList9.remove(cell.cat_id)
                    }
                }
                chbContent10.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        JobidList10.add(cell.cat_id)
                        mChecked10.put(position, isChecked)
                        job_nameList10.add(cell.name)
                        mSingleTasklistandCat10.add(cell.task_id + "_" + cell.cat_id)
                        JobidList10.add(cell.cat_id)
                    } else {
                        mChecked10.delete(position)
                        job_nameList10.remove(cell.name)
                        mSingleTasklistandCat10.remove(cell.task_id + "_" + cell.cat_id)
                        JobidList10.remove(cell.cat_id)
                    }
                }
            }
            return v
        }
    }
    inner class Dataadapter(context: Context, items: java.util.ArrayList<JobTypeResponse>) : ArrayAdapter<JobTypeResponse>(context, 0, items) {
        internal var inflater: LayoutInflater
        var mtaskslist = ""
        init {
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var v = convertView
            val cell = getItem(position)
            v = inflater.inflate(R.layout.section_header, null)
            val name = v!!.findViewById(R.id.name) as TextView
            name.text = cell.job_name
            name.setOnClickListener {
                mJobType_id = cell.id.toString()
                dialog_list_type.dismiss()
                admin_job_type_id.setText(cell.job_name)
            }
            return v
        }
    }
    var date: Calendar = Calendar.getInstance()
    fun showDateTimePicker(id: Int) {
        val currentDate = Calendar.getInstance()
        val date = Calendar.getInstance()
        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
            date.set(year, monthofyear, dayofmonth)
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourofday, minute ->
                date.set(Calendar.HOUR_OF_DAY, hourofday)
                date.set(Calendar.MINUTE, minute)
                val myFormat = "MM/dd/yyyy HH:mm" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.UK)
                val date2 = sdf.format(date.time)
                if (id == R.id.admin_Estart_date_id) {
                    admin_Estart_date_id.text = date2
                } else if (id == R.id.admin_Pdata_id) {
                    admin_Pdata_id.text = date2
                } else {
                }
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show()

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show()
    }
    private fun jobsheetsdropdown() {
        sizelisttype.clear()
        sizelist.clear()
        weightlisttype.clear()
        weightlist.clear()
        papertypelist.clear()
        papertypestringlist.clear()
        val apiService = ApiInterface.create()
        val call = apiService.jobsheetsdropdown()
        call.enqueue(object : Callback<PaperTypeResponse> {
            override fun onResponse(call: Call<PaperTypeResponse>, response: retrofit2.Response<PaperTypeResponse>?) {
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.sheetsList != null) {
                        if (response.body()!!.sheetsList!!.papertypes != null) {
                            val list: Array<Papertypes>? = response.body()!!.sheetsList!!.papertypes
                            for (item: Papertypes in list!!.iterator()) {
                                papertypelist.add(item)
                            }
                            for (i in 0 until papertypelist.size) {
                                papertypestringlist.add(papertypelist.get(i).paper_name!!)
                            }
                        }
                    }
                } else {
                    Toast.makeText(applicationContext, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call<PaperTypeResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun optionlistsdropdown() {
        optionliststringlist.clear()
        papertypelist.clear()
        papertypestringlist.clear()
        val apiService = ApiInterface.create()
        val call = apiService.optionListdropdown()
        call.enqueue(object : Callback<OptionListResponse> {
            override fun onResponse(call: Call<OptionListResponse>, response: retrofit2.Response<OptionListResponse>?) {
                if (response != null) {
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {
                        val list: Array<OptiondataListResponse>? = response.body()!!.data!!
                        for (item: OptiondataListResponse in list!!.iterator()) {
                            optiontypelist.add(item)
                        }
                        for (i in 0 until optiontypelist.size) {
                            optionliststringlist.add(optiontypelist.get(i).type!!)
                        }
                    }else{
                    }
                }
            }
            override fun onFailure(call: Call<OptionListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed"+t.message)
            }
        })
    }
    private fun weightsizeAPi(sub_text_id: String) {
        sizelisttype.clear()
        sizelist.clear()
        weightlisttype.clear()
        weightlist.clear()
        Log.w("WEIGHTSIZESUBTEXTID", "*** " + sub_text_id)
        val apiService = ApiInterface.create()
        val call = apiService.weightandsize(sub_text_id)
        call.enqueue(object : Callback<WeightSizeResponse> {
            override fun onResponse(call: Call<WeightSizeResponse>, response: retrofit2.Response<WeightSizeResponse>?) {
                Log.w("WEIGHTSIZESTATUS", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.weightandsizelist != null) {
                        if (response.body()!!.weightandsizelist!!.sizes != null) {
                            val list: Array<Sizes>? = response.body()!!.weightandsizelist!!.sizes
                            for (item: Sizes in list!!.iterator()) {
                                sizelisttype.add(item)
                            }
                            for (i in 0 until response.body()!!.weightandsizelist!!.sizes!!.size) {
                                sizelist.add(response.body()!!.weightandsizelist!!.sizes!!.get(i).height + "*" + response.body()!!.weightandsizelist!!.sizes!!.get(i).width)
                            }
                        }
                        if (response.body()!!.weightandsizelist!!.weights != null) {
                            val list: Array<Weights>? = response.body()!!.weightandsizelist!!.weights
                            for (item: Weights in list!!.iterator()) {
                                weightlisttype.add(item)
                            }
                            for (i in 0 until response.body()!!.weightandsizelist!!.weights!!.size) {
                                weightlist.add(response.body()!!.weightandsizelist!!.weights!!.get(i).weight!!)
                            }
                        }
                    }
                } else {
                    Toast.makeText(applicationContext, "Failed to Respond Data", Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call<WeightSizeResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun texttypes(id: String) {
        texttypelist.clear()
        texttypelisttringlist.clear()
        val apiService = ApiInterface.create()
        val call = apiService.texttypes(id)
        call.enqueue(object : Callback<TextTypeResponse> {
            override fun onResponse(call: Call<TextTypeResponse>, response: retrofit2.Response<TextTypeResponse>?) {
                Log.w("TEXTTYPESTATUS", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.texttypes != null) {
                        val list: Array<Texttypes>? = response.body()!!.texttypes!!
                        for (item: Texttypes in list!!.iterator()) {
                            texttypelist.add(item)
                        }
                        for (i in 0 until texttypelist.size) {
                            texttypelisttringlist.add(texttypelist.get(i).text_name!!)
                        }
                    }
                }
            }
            override fun onFailure(call: Call<TextTypeResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun SubTextTypeResponse(id: String) {
        subtexttypelist.clear()
        subtexttypelisttringlist.clear()
        val apiService = ApiInterface.create()
        val call = apiService.subtexttypes(id)
        call.enqueue(object : Callback<SubTextTypeResponse> {
            override fun onResponse(call: Call<SubTextTypeResponse>, response: retrofit2.Response<SubTextTypeResponse>?) {
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.subtexttypes != null) {
                        val list: Array<Subtexttypes>? = response.body()!!.subtexttypes!!
                        for (item: Subtexttypes in list!!.iterator()) {
                            subtexttypelist.add(item)
                        }
                        for (i in 0 until subtexttypelist.size) {
                            subtexttypelisttringlist.add(subtexttypelist.get(i).sub_text_name!!)
                        }
                    }
                }
            }
            override fun onFailure(call: Call<SubTextTypeResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun CallgetCustomersAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.getCustomers()
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<GetCustomersListResponse> {
            override fun onResponse(call: Call<GetCustomersListResponse>, response: retrofit2.Response<GetCustomersListResponse>?) {
                if (response != null) {
                    if (response.body()!!.status.equals("1") && response.body()!!.customers != null) {
                        val list: Array<CustomersListName>? = response.body()!!.customers!!
                        for (item: CustomersListName in list!!.iterator()) {
                            mCustomerNamesListArray.add(item)
                            mCustomerNamesArray.add(item.c_name!!)
                            mCustomerIdsArray.add(item.id!!)
                        }
                        admin_customer_id.setAdapter(customer_search_adapter)
                    } else if (response.body()!!.status.equals("2")) {
                        admin_customer_id.setText("")
                    }
                }
            }
            override fun onFailure(call: Call<GetCustomersListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }
    private fun CallAvailableSheetsAPI(size_id: String, weight_id: String, sub_text_id: String, paper_id: String, text_id: String, job_part: String) {
        val apiService = ApiInterface.create()
        val call = apiService.getAvailableSheets(size_id, weight_id, sub_text_id, paper_id, text_id)
        call.enqueue(object : Callback<GetAvailableSheetsResponse> {
            override fun onResponse(call: Call<GetAvailableSheetsResponse>, response: retrofit2.Response<GetAvailableSheetsResponse>?) {
                Log.w("AVAILABLESHEETS_STATUS", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.no_of_sheets != null) {
                        val no_f_sheets = response.body()!!.no_of_sheets!!
                        Log.w("no_f_sheets", no_f_sheets)
                        when(job_part.toInt()) {
                            0 -> {tv_availablesheets.text = no_f_sheets}
                            1 -> {tv_availablesheets1.text = no_f_sheets}
                            2 -> {tv_availablesheets2.text = no_f_sheets}
                            3 -> {tv_availablesheets3.text = no_f_sheets}
                            4 -> {tv_availablesheets.text = no_f_sheets}
                            5 -> {tv_availablesheets5.text = no_f_sheets}
                            6 -> {tv_availablesheets6.text = no_f_sheets}
                            7 -> {tv_availablesheets7.text = no_f_sheets}
                            8 -> {tv_availablesheets8.text = no_f_sheets}
                            9 -> {tv_availablesheets9.text = no_f_sheets}
                            10 -> {tv_availablesheets10.text = no_f_sheets}
                            else -> println("Number too high")
                        }
                    }
                } else {
                    tv_availablesheets.text = response.body()!!.no_of_sheets!!
                }
            }
            override fun onFailure(call: Call<GetAvailableSheetsResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun CallCheckJobIdAPI(job_id: String) {
        val apiService = ApiInterface.create()
        val call = apiService.checkJobId(job_id)
        call.enqueue(object : Callback<CheckJobIdResponse> {
            override fun onResponse(call: Call<CheckJobIdResponse>, response: retrofit2.Response<CheckJobIdResponse>?) {
                Log.w("CheckJobId", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    Log.w("CheckJobId_STATUS1", "*** " + response.body()!!.result)
                    tv_error_jobID.visibility = View.GONE
                    chkJobIderrorStg = ""
                } else if (response.body()!!.status.equals("2")) {
                    chkJobIderrorStg = response.body()!!.result
                    Log.w("CheckJobId_STATUS2", "*** " + response.body()!!.result)
                    tv_error_jobID.visibility = View.VISIBLE
                    tv_error_jobID.text = response.body()!!.result
                }
            }
            override fun onFailure(call: Call<CheckJobIdResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    override fun onClick(v: View?) {
        showDateTimePicker_Terms(v!!.id)
    }
    fun showDateTimePicker_Terms(id: Int) {
        val currentDate = Calendar.getInstance()
        val date = Calendar.getInstance()
        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
            date.set(year, monthofyear, dayofmonth)
                val myFormat = "MM/dd/yyyy" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.UK)
                val date2 = sdf.format(date.time)
            when(id) {
                R.id.admin_termsdate1 ->{admin_termsdate1.setText(date2)}
                R.id.admin_termsdate11 ->{admin_termsdate11.setText(date2)}
                R.id.admin_termsdate21 ->{admin_termsdate21.setText(date2)}
                R.id.admin_termsdate31 ->{admin_termsdate31.setText(date2)}
                R.id.admin_termsdate51 ->{admin_termsdate51.setText(date2)}
                R.id.admin_termsdate61 ->{admin_termsdate61.setText(date2)}
                R.id.admin_termsdate71 ->{admin_termsdate71.setText(date2)}
                R.id.admin_termsdate81 ->{admin_termsdate81.setText(date2)}
                R.id.admin_termsdate91 ->{admin_termsdate91.setText(date2)}
                R.id.admin_termsdate101 ->{admin_termsdate101.setText(date2)}
                R.id.admin_termsdate2 ->{admin_termsdate2.setText(date2)}
                R.id.admin_termsdate12 ->{admin_termsdate12.setText(date2)}
                R.id.admin_termsdate22 ->{admin_termsdate22.setText(date2)}
                R.id.admin_termsdate32 ->{admin_termsdate32.setText(date2)}
                R.id.admin_termsdate3 ->{admin_termsdate3.setText(date2)}
                R.id.admin_termsdate13 ->{admin_termsdate13.setText(date2)}
                R.id.admin_termsdate23 ->{admin_termsdate23.setText(date2)}
                R.id.admin_termsdate33 ->{admin_termsdate33.setText(date2)}
                R.id.admin_termsdate4 ->{admin_termsdate4.setText(date2)}
                R.id.admin_termsdate14 ->{admin_termsdate14.setText(date2)}
                R.id.admin_termsdate24 ->{admin_termsdate24.setText(date2)}
                R.id.admin_termsdate34 ->{admin_termsdate34.setText(date2)}
                R.id.admin_termsdate5 ->{admin_termsdate5.setText(date2)}
                R.id.admin_termsdate15 ->{admin_termsdate15.setText(date2)}
                R.id.admin_termsdate25 ->{admin_termsdate25.setText(date2)}
                R.id.admin_termsdate35 ->{admin_termsdate35.setText(date2)}
                R.id.admin_termsdate6 ->{admin_termsdate6.setText(date2)}
                R.id.admin_termsdate16 ->{admin_termsdate16.setText(date2)}
                R.id.admin_termsdate26 ->{admin_termsdate26.setText(date2)}
                R.id.admin_termsdate36 ->{admin_termsdate36.setText(date2)}
                R.id.admin_termsdate7 ->{admin_termsdate7.setText(date2)}
                R.id.admin_termsdate17 ->{admin_termsdate17.setText(date2)}
                R.id.admin_termsdate27 ->{admin_termsdate27.setText(date2)}
                R.id.admin_termsdate37 ->{admin_termsdate37.setText(date2)}
                R.id.admin_termsdate8 ->{admin_termsdate8.setText(date2)}
                R.id.admin_termsdate18 ->{admin_termsdate18.setText(date2)}
                R.id.admin_termsdate28 ->{admin_termsdate28.setText(date2)}
                R.id.admin_termsdate38 ->{admin_termsdate38.setText(date2)}
                R.id.admin_termsdate9 ->{admin_termsdate9.setText(date2)}
                R.id.admin_termsdate19 ->{admin_termsdate19.setText(date2)}
                R.id.admin_termsdate29 ->{admin_termsdate29.setText(date2)}
                R.id.admin_termsdate39 ->{admin_termsdate39.setText(date2)}
                R.id.admin_termsdate10 ->{admin_termsdate10.setText(date2)}
                R.id.admin_termsdate110 ->{admin_termsdate110.setText(date2)}
                R.id.admin_termsdate210 ->{admin_termsdate210.setText(date2)}
                R.id.admin_termsdate310 ->{admin_termsdate310.setText(date2)}
                R.id.admin_shipp_termsdate121 ->{admin_shipp_termsdate121.setText(date2)}
                R.id.admin_shipp_termsdate122 ->{admin_shipp_termsdate122.setText(date2)}
                R.id.admin_shipp_termsdate123 ->{admin_shipp_termsdate123.setText(date2)}
                R.id.admin_shipp_termsdate124 ->{admin_shipp_termsdate124.setText(date2)}
                R.id.admin_shipp_termsdate125 ->{admin_shipp_termsdate125.setText(date2)}
                R.id.admin_shipp_termsdate126 ->{admin_shipp_termsdate126.setText(date2)}
                R.id.admin_shipp_termsdate127 ->{admin_shipp_termsdate127.setText(date2)}
                R.id.admin_shipp_termsdate128 ->{admin_shipp_termsdate128.setText(date2)}
                R.id.admin_shipp_termsdate129 ->{admin_shipp_termsdate129.setText(date2)}
                R.id.admin_shipp_termsdate1210 ->{admin_shipp_termsdate1210.setText(date2)}
                R.id.admin_delivery_termsdate131 ->{admin_delivery_termsdate131.setText(date2)}
                R.id.admin_delivery_termsdate132 ->{admin_delivery_termsdate132.setText(date2)}
                R.id.admin_delivery_termsdate133 ->{admin_delivery_termsdate133.setText(date2)}
                R.id.admin_delivery_termsdate134 ->{admin_delivery_termsdate134.setText(date2)}
                R.id.admin_delivery_termsdate135 ->{admin_delivery_termsdate135.setText(date2)}
                R.id.admin_delivery_termsdate136 ->{admin_delivery_termsdate136.setText(date2)}
                R.id.admin_delivery_termsdate137 ->{admin_delivery_termsdate137.setText(date2)}
                R.id.admin_delivery_termsdate138 ->{admin_delivery_termsdate138.setText(date2)}
                R.id.admin_delivery_termsdate139 ->{admin_delivery_termsdate139.setText(date2)}
                R.id.admin_delivery_termsdate1310 ->{admin_delivery_termsdate1310.setText(date2)}
                R.id.admin_shipp_termsdate221 ->{admin_shipp_termsdate221.setText(date2)}
                R.id.admin_shipp_termsdate222 ->{admin_shipp_termsdate222.setText(date2)}
                R.id.admin_shipp_termsdate223 ->{admin_shipp_termsdate223.setText(date2)}
                R.id.admin_shipp_termsdate224 ->{admin_shipp_termsdate224.setText(date2)}
                R.id.admin_shipp_termsdate225 ->{admin_shipp_termsdate225.setText(date2)}
                R.id.admin_shipp_termsdate226 ->{admin_shipp_termsdate226.setText(date2)}
                R.id.admin_shipp_termsdate227 ->{admin_shipp_termsdate227.setText(date2)}
                R.id.admin_shipp_termsdate228 ->{admin_shipp_termsdate228.setText(date2)}
                R.id.admin_shipp_termsdate229 ->{admin_shipp_termsdate229.setText(date2)}
                R.id.admin_shipp_termsdate2210 ->{admin_shipp_termsdate2210.setText(date2)}
                R.id.admin_delivery_termsdate231 ->{admin_delivery_termsdate231.setText(date2)}
                R.id.admin_delivery_termsdate232 ->{admin_delivery_termsdate232.setText(date2)}
                R.id.admin_delivery_termsdate233 ->{admin_delivery_termsdate233.setText(date2)}
                R.id.admin_delivery_termsdate234 ->{admin_delivery_termsdate234.setText(date2)}
                R.id.admin_delivery_termsdate235 ->{admin_delivery_termsdate235.setText(date2)}
                R.id.admin_delivery_termsdate236 ->{admin_delivery_termsdate236.setText(date2)}
                R.id.admin_delivery_termsdate237 ->{admin_delivery_termsdate237.setText(date2)}
                R.id.admin_delivery_termsdate238 ->{admin_delivery_termsdate238.setText(date2)}
                R.id.admin_delivery_termsdate239 ->{admin_delivery_termsdate239.setText(date2)}
                R.id.admin_delivery_termsdate2310 ->{admin_delivery_termsdate2310.setText(date2)}
                R.id.admin_shipp_termsdate321 ->{admin_shipp_termsdate321.setText(date2)}
                R.id.admin_shipp_termsdate322 ->{admin_shipp_termsdate322.setText(date2)}
                R.id.admin_shipp_termsdate323 ->{admin_shipp_termsdate323.setText(date2)}
                R.id.admin_shipp_termsdate324 ->{admin_shipp_termsdate324.setText(date2)}
                R.id.admin_shipp_termsdate325 ->{admin_shipp_termsdate325.setText(date2)}
                R.id.admin_shipp_termsdate326 ->{admin_shipp_termsdate326.setText(date2)}
                R.id.admin_shipp_termsdate327 ->{admin_shipp_termsdate327.setText(date2)}
                R.id.admin_shipp_termsdate328 ->{admin_shipp_termsdate328.setText(date2)}
                R.id.admin_shipp_termsdate329 ->{admin_shipp_termsdate329.setText(date2)}
                R.id.admin_shipp_termsdate3210 ->{admin_shipp_termsdate3210.setText(date2)}
                R.id.admin_delivery_termsdate331 ->{admin_delivery_termsdate331.setText(date2)}
                R.id.admin_delivery_termsdate332 ->{admin_delivery_termsdate332.setText(date2)}
                R.id.admin_delivery_termsdate333 ->{admin_delivery_termsdate333.setText(date2)}
                R.id.admin_delivery_termsdate334 ->{admin_delivery_termsdate334.setText(date2)}
                R.id.admin_delivery_termsdate335 ->{admin_delivery_termsdate335.setText(date2)}
                R.id.admin_delivery_termsdate336 ->{admin_delivery_termsdate336.setText(date2)}
                R.id.admin_delivery_termsdate337 ->{admin_delivery_termsdate337.setText(date2)}
                R.id.admin_delivery_termsdate338 ->{admin_delivery_termsdate338.setText(date2)}
                R.id.admin_delivery_termsdate339 ->{admin_delivery_termsdate339.setText(date2)}
                R.id.admin_delivery_termsdate3310 ->{admin_delivery_termsdate3310.setText(date2)}
                R.id.admin_shipp_termsdate421 ->{admin_shipp_termsdate421.setText(date2)}
                R.id.admin_shipp_termsdate422 ->{admin_shipp_termsdate422.setText(date2)}
                R.id.admin_shipp_termsdate423 ->{admin_shipp_termsdate423.setText(date2)}
                R.id.admin_shipp_termsdate424 ->{admin_shipp_termsdate424.setText(date2)}
                R.id.admin_shipp_termsdate425 ->{admin_shipp_termsdate425.setText(date2)}
                R.id.admin_shipp_termsdate426 ->{admin_shipp_termsdate426.setText(date2)}
                R.id.admin_shipp_termsdate427 ->{admin_shipp_termsdate427.setText(date2)}
                R.id.admin_shipp_termsdate428 ->{admin_shipp_termsdate428.setText(date2)}
                R.id.admin_shipp_termsdate429 ->{admin_shipp_termsdate429.setText(date2)}
                R.id.admin_shipp_termsdate4210 ->{admin_shipp_termsdate4210.setText(date2)}
                R.id.admin_delivery_termsdate431 ->{admin_delivery_termsdate431.setText(date2)}
                R.id.admin_delivery_termsdate432 ->{admin_delivery_termsdate432.setText(date2)}
                R.id.admin_delivery_termsdate433 ->{admin_delivery_termsdate433.setText(date2)}
                R.id.admin_delivery_termsdate434 ->{admin_delivery_termsdate434.setText(date2)}
                R.id.admin_delivery_termsdate435 ->{admin_delivery_termsdate435.setText(date2)}
                R.id.admin_delivery_termsdate436 ->{admin_delivery_termsdate436.setText(date2)}
                R.id.admin_delivery_termsdate437 ->{admin_delivery_termsdate437.setText(date2)}
                R.id.admin_delivery_termsdate438 ->{admin_delivery_termsdate438.setText(date2)}
                R.id.admin_delivery_termsdate439 ->{admin_delivery_termsdate439.setText(date2)}
                R.id.admin_delivery_termsdate4310 ->{admin_delivery_termsdate4310.setText(date2)}
                R.id.admin_shipp_termsdate521 ->{admin_shipp_termsdate521.setText(date2)}
                R.id.admin_shipp_termsdate522 ->{admin_shipp_termsdate522.setText(date2)}
                R.id.admin_shipp_termsdate523 ->{admin_shipp_termsdate523.setText(date2)}
                R.id.admin_shipp_termsdate524 ->{admin_shipp_termsdate524.setText(date2)}
                R.id.admin_shipp_termsdate525 ->{admin_shipp_termsdate525.setText(date2)}
                R.id.admin_shipp_termsdate526 ->{admin_shipp_termsdate526.setText(date2)}
                R.id.admin_shipp_termsdate527 ->{admin_shipp_termsdate527.setText(date2)}
                R.id.admin_shipp_termsdate528 ->{admin_shipp_termsdate528.setText(date2)}
                R.id.admin_shipp_termsdate529 ->{admin_shipp_termsdate529.setText(date2)}
                R.id.admin_shipp_termsdate5210 ->{admin_shipp_termsdate5210.setText(date2)}
                R.id.admin_delivery_termsdate531 ->{admin_delivery_termsdate531.setText(date2)}
                R.id.admin_delivery_termsdate532 ->{admin_delivery_termsdate532.setText(date2)}
                R.id.admin_delivery_termsdate533 ->{admin_delivery_termsdate533.setText(date2)}
                R.id.admin_delivery_termsdate534 ->{admin_delivery_termsdate534.setText(date2)}
                R.id.admin_delivery_termsdate535 ->{admin_delivery_termsdate535.setText(date2)}
                R.id.admin_delivery_termsdate536 ->{admin_delivery_termsdate536.setText(date2)}
                R.id.admin_delivery_termsdate537 ->{admin_delivery_termsdate537.setText(date2)}
                R.id.admin_delivery_termsdate538 ->{admin_delivery_termsdate538.setText(date2)}
                R.id.admin_delivery_termsdate539 ->{admin_delivery_termsdate539.setText(date2)}
                R.id.admin_delivery_termsdate5310 ->{admin_delivery_termsdate5310.setText(date2)}
                R.id.admin_shipp_termsdate621 ->{admin_shipp_termsdate621.setText(date2)}
                R.id.admin_shipp_termsdate622 ->{admin_shipp_termsdate622.setText(date2)}
                R.id.admin_shipp_termsdate623 ->{admin_shipp_termsdate623.setText(date2)}
                R.id.admin_shipp_termsdate624 ->{admin_shipp_termsdate624.setText(date2)}
                R.id.admin_shipp_termsdate625 ->{admin_shipp_termsdate625.setText(date2)}
                R.id.admin_shipp_termsdate626 ->{admin_shipp_termsdate626.setText(date2)}
                R.id.admin_shipp_termsdate627 ->{admin_shipp_termsdate627.setText(date2)}
                R.id.admin_shipp_termsdate628 ->{admin_shipp_termsdate628.setText(date2)}
                R.id.admin_shipp_termsdate629 ->{admin_shipp_termsdate629.setText(date2)}
                R.id.admin_shipp_termsdate6210 ->{admin_shipp_termsdate6210.setText(date2)}
                R.id.admin_delivery_termsdate631 ->{admin_delivery_termsdate631.setText(date2)}
                R.id.admin_delivery_termsdate632 ->{admin_delivery_termsdate632.setText(date2)}
                R.id.admin_delivery_termsdate633 ->{admin_delivery_termsdate633.setText(date2)}
                R.id.admin_delivery_termsdate634 ->{admin_delivery_termsdate634.setText(date2)}
                R.id.admin_delivery_termsdate635 ->{admin_delivery_termsdate635.setText(date2)}
                R.id.admin_delivery_termsdate636 ->{admin_delivery_termsdate636.setText(date2)}
                R.id.admin_delivery_termsdate637 ->{admin_delivery_termsdate637.setText(date2)}
                R.id.admin_delivery_termsdate638 ->{admin_delivery_termsdate638.setText(date2)}
                R.id.admin_delivery_termsdate639 ->{admin_delivery_termsdate639.setText(date2)}
                R.id.admin_delivery_termsdate6310 ->{admin_delivery_termsdate6310.setText(date2)}
                R.id.admin_shipp_termsdate721 ->{admin_shipp_termsdate721.setText(date2)}
                R.id.admin_shipp_termsdate722 ->{admin_shipp_termsdate722.setText(date2)}
                R.id.admin_shipp_termsdate723 ->{admin_shipp_termsdate723.setText(date2)}
                R.id.admin_shipp_termsdate724 ->{admin_shipp_termsdate724.setText(date2)}
                R.id.admin_shipp_termsdate725 ->{admin_shipp_termsdate725.setText(date2)}
                R.id.admin_shipp_termsdate726 ->{admin_shipp_termsdate726.setText(date2)}
                R.id.admin_shipp_termsdate727 ->{admin_shipp_termsdate727.setText(date2)}
                R.id.admin_shipp_termsdate728 ->{admin_shipp_termsdate728.setText(date2)}
                R.id.admin_shipp_termsdate729 ->{admin_shipp_termsdate729.setText(date2)}
                R.id.admin_shipp_termsdate7210 ->{admin_shipp_termsdate7210.setText(date2)}
                R.id.admin_delivery_termsdate731 ->{admin_delivery_termsdate731.setText(date2)}
                R.id.admin_delivery_termsdate732 ->{admin_delivery_termsdate732.setText(date2)}
                R.id.admin_delivery_termsdate733 ->{admin_delivery_termsdate733.setText(date2)}
                R.id.admin_delivery_termsdate734 ->{admin_delivery_termsdate734.setText(date2)}
                R.id.admin_delivery_termsdate735 ->{admin_delivery_termsdate735.setText(date2)}
                R.id.admin_delivery_termsdate736 ->{admin_delivery_termsdate736.setText(date2)}
                R.id.admin_delivery_termsdate737 ->{admin_delivery_termsdate737.setText(date2)}
                R.id.admin_delivery_termsdate738 ->{admin_delivery_termsdate738.setText(date2)}
                R.id.admin_delivery_termsdate739 ->{admin_delivery_termsdate739.setText(date2)}
                R.id.admin_delivery_termsdate7310 ->{admin_delivery_termsdate7310.setText(date2)}
                R.id.admin_shipp_termsdate821 ->{admin_shipp_termsdate821.setText(date2)}
                R.id.admin_shipp_termsdate822 ->{admin_shipp_termsdate822.setText(date2)}
                R.id.admin_shipp_termsdate823 ->{admin_shipp_termsdate823.setText(date2)}
                R.id.admin_shipp_termsdate824 ->{admin_shipp_termsdate824.setText(date2)}
                R.id.admin_shipp_termsdate825 ->{admin_shipp_termsdate825.setText(date2)}
                R.id.admin_shipp_termsdate826 ->{admin_shipp_termsdate826.setText(date2)}
                R.id.admin_shipp_termsdate827 ->{admin_shipp_termsdate827.setText(date2)}
                R.id.admin_shipp_termsdate828 ->{admin_shipp_termsdate828.setText(date2)}
                R.id.admin_shipp_termsdate829 ->{admin_shipp_termsdate829.setText(date2)}
                R.id.admin_shipp_termsdate8210 ->{admin_shipp_termsdate8210.setText(date2)}
                R.id.admin_delivery_termsdate831 ->{admin_delivery_termsdate831.setText(date2)}
                R.id.admin_delivery_termsdate832 ->{admin_delivery_termsdate832.setText(date2)}
                R.id.admin_delivery_termsdate833 ->{admin_delivery_termsdate833.setText(date2)}
                R.id.admin_delivery_termsdate834 ->{admin_delivery_termsdate834.setText(date2)}
                R.id.admin_delivery_termsdate835 ->{admin_delivery_termsdate835.setText(date2)}
                R.id.admin_delivery_termsdate836 ->{admin_delivery_termsdate836.setText(date2)}
                R.id.admin_delivery_termsdate837 ->{admin_delivery_termsdate837.setText(date2)}
                R.id.admin_delivery_termsdate838 ->{admin_delivery_termsdate838.setText(date2)}
                R.id.admin_delivery_termsdate839 ->{admin_delivery_termsdate839.setText(date2)}
                R.id.admin_delivery_termsdate8310 ->{admin_delivery_termsdate8310.setText(date2)}
                R.id.admin_shipp_termsdate921 ->{admin_shipp_termsdate921.setText(date2)}
                R.id.admin_shipp_termsdate922 ->{admin_shipp_termsdate922.setText(date2)}
                R.id.admin_shipp_termsdate923 ->{admin_shipp_termsdate923.setText(date2)}
                R.id.admin_shipp_termsdate924 ->{admin_shipp_termsdate924.setText(date2)}
                R.id.admin_shipp_termsdate925 ->{admin_shipp_termsdate925.setText(date2)}
                R.id.admin_shipp_termsdate926 ->{admin_shipp_termsdate926.setText(date2)}
                R.id.admin_shipp_termsdate927 ->{admin_shipp_termsdate927.setText(date2)}
                R.id.admin_shipp_termsdate928 ->{admin_shipp_termsdate928.setText(date2)}
                R.id.admin_shipp_termsdate929 ->{admin_shipp_termsdate929.setText(date2)}
                R.id.admin_shipp_termsdate9210 ->{admin_shipp_termsdate9210.setText(date2)}
                R.id.admin_delivery_termsdate931 ->{admin_delivery_termsdate931.setText(date2)}
                R.id.admin_delivery_termsdate932 ->{admin_delivery_termsdate932.setText(date2)}
                R.id.admin_delivery_termsdate933 ->{admin_delivery_termsdate933.setText(date2)}
                R.id.admin_delivery_termsdate934 ->{admin_delivery_termsdate934.setText(date2)}
                R.id.admin_delivery_termsdate935 ->{admin_delivery_termsdate935.setText(date2)}
                R.id.admin_delivery_termsdate936 ->{admin_delivery_termsdate936.setText(date2)}
                R.id.admin_delivery_termsdate937 ->{admin_delivery_termsdate937.setText(date2)}
                R.id.admin_delivery_termsdate938 ->{admin_delivery_termsdate938.setText(date2)}
                R.id.admin_delivery_termsdate939 ->{admin_delivery_termsdate939.setText(date2)}
                R.id.admin_delivery_termsdate9310 ->{admin_delivery_termsdate9310.setText(date2)}
                R.id.admin_shipp_termsdate1021 ->{admin_shipp_termsdate1021.setText(date2)}
                R.id.admin_shipp_termsdate1022 ->{admin_shipp_termsdate1022.setText(date2)}
                R.id.admin_shipp_termsdate1023 ->{admin_shipp_termsdate1023.setText(date2)}
                R.id.admin_shipp_termsdate1024 ->{admin_shipp_termsdate1024.setText(date2)}
                R.id.admin_shipp_termsdate1025 ->{admin_shipp_termsdate1025.setText(date2)}
                R.id.admin_shipp_termsdate1026 ->{admin_shipp_termsdate1026.setText(date2)}
                R.id.admin_shipp_termsdate1027 ->{admin_shipp_termsdate1027.setText(date2)}
                R.id.admin_shipp_termsdate1028 ->{admin_shipp_termsdate1028.setText(date2)}
                R.id.admin_shipp_termsdate1029 ->{admin_shipp_termsdate1029.setText(date2)}
                R.id.admin_shipp_termsdate10210 ->{admin_shipp_termsdate10210.setText(date2)}
                R.id.admin_delivery_termsdate1031 ->{admin_delivery_termsdate1031.setText(date2)}
                R.id.admin_delivery_termsdate1032 ->{admin_delivery_termsdate1032.setText(date2)}
                R.id.admin_delivery_termsdate1033 ->{admin_delivery_termsdate1033.setText(date2)}
                R.id.admin_delivery_termsdate1034 ->{admin_delivery_termsdate1034.setText(date2)}
                R.id.admin_delivery_termsdate1035 ->{admin_delivery_termsdate1035.setText(date2)}
                R.id.admin_delivery_termsdate1036 ->{admin_delivery_termsdate1036.setText(date2)}
                R.id.admin_delivery_termsdate1036 ->{admin_delivery_termsdate1036.setText(date2)}
                R.id.admin_delivery_termsdate1036 ->{admin_delivery_termsdate1036.setText(date2)}
                R.id.admin_delivery_termsdate1037 ->{admin_delivery_termsdate1037.setText(date2)}
                R.id.admin_delivery_termsdate1038 ->{admin_delivery_termsdate1038.setText(date2)}
                R.id.admin_delivery_termsdate1039 ->{admin_delivery_termsdate1039.setText(date2)}
                R.id.admin_delivery_termsdate10310 ->{admin_delivery_termsdate10310.setText(date2)}
                R.id.admin_termsdate52 ->{admin_termsdate52.setText(date2)}
                R.id.admin_termsdate62 ->{admin_termsdate62.setText(date2)}
                R.id.admin_termsdate72 ->{admin_termsdate72.setText(date2)}
                R.id.admin_termsdate82 ->{admin_termsdate82.setText(date2)}
                R.id.admin_termsdate92 ->{admin_termsdate92.setText(date2)}
                R.id.admin_termsdate102 ->{admin_termsdate102.setText(date2)}
                R.id.admin_termsdate53 ->{admin_termsdate53.setText(date2)}
                R.id.admin_termsdate63 ->{admin_termsdate63.setText(date2)}
                R.id.admin_termsdate73 ->{admin_termsdate73.setText(date2)}
                R.id.admin_termsdate83 ->{admin_termsdate83.setText(date2)}
                R.id.admin_termsdate93 ->{admin_termsdate93.setText(date2)}
                R.id.admin_termsdate54 ->{admin_termsdate54.setText(date2)}
                R.id.admin_termsdate64 ->{admin_termsdate64.setText(date2)}
                R.id.admin_termsdate74 ->{admin_termsdate74.setText(date2)}
                R.id.admin_termsdate84 ->{admin_termsdate84.setText(date2)}
                R.id.admin_termsdate94 ->{admin_termsdate94.setText(date2)}
                R.id.admin_termsdate104 ->{admin_termsdate104.setText(date2)}
                R.id.admin_termsdate55 ->{admin_termsdate55.setText(date2)}
                R.id.admin_termsdate65 ->{admin_termsdate65.setText(date2)}
                R.id.admin_termsdate75 ->{admin_termsdate75.setText(date2)}
                R.id.admin_termsdate85 ->{admin_termsdate85.setText(date2)}
                R.id.admin_termsdate95 ->{admin_termsdate95.setText(date2)}
                R.id.admin_termsdate105 ->{admin_termsdate105.setText(date2)}
                R.id.admin_termsdate56 ->{admin_termsdate56.setText(date2)}
                R.id.admin_termsdate66 ->{admin_termsdate66.setText(date2)}
                R.id.admin_termsdate76 ->{admin_termsdate76.setText(date2)}
                R.id.admin_termsdate86 ->{admin_termsdate86.setText(date2)}
                R.id.admin_termsdate96 ->{admin_termsdate96.setText(date2)}
                R.id.admin_termsdate106 ->{admin_termsdate106.setText(date2)}
                R.id.admin_termsdate57 ->{admin_termsdate57.setText(date2)}
                R.id.admin_termsdate67 ->{admin_termsdate67.setText(date2)}
                R.id.admin_termsdate77 ->{admin_termsdate77.setText(date2)}
                R.id.admin_termsdate87 ->{admin_termsdate87.setText(date2)}
                R.id.admin_termsdate97 ->{admin_termsdate97.setText(date2)}
                R.id.admin_termsdate107 ->{admin_termsdate107.setText(date2)}
                R.id.admin_termsdate58 ->{admin_termsdate58.setText(date2)}
                R.id.admin_termsdate68 ->{admin_termsdate68.setText(date2)}
                R.id.admin_termsdate78 ->{admin_termsdate78.setText(date2)}
                R.id.admin_termsdate88 ->{admin_termsdate88.setText(date2)}
                R.id.admin_termsdate98 ->{admin_termsdate98.setText(date2)}
                R.id.admin_termsdate108 ->{admin_termsdate108.setText(date2)}
                R.id.admin_termsdate59 ->{admin_termsdate59.setText(date2)}
                R.id.admin_termsdate69 ->{admin_termsdate69.setText(date2)}
                R.id.admin_termsdate79 ->{admin_termsdate79.setText(date2)}
                R.id.admin_termsdate89 ->{admin_termsdate89.setText(date2)}
                R.id.admin_termsdate99 ->{admin_termsdate99.setText(date2)}
                R.id.admin_termsdate109 ->{admin_termsdate109.setText(date2)}
                R.id.admin_termsdate510 ->{admin_termsdate510.setText(date2)}
                R.id.admin_termsdate610 ->{admin_termsdate610.setText(date2)}
                R.id.admin_termsdate710 ->{admin_termsdate710.setText(date2)}
                R.id.admin_termsdate810 ->{admin_termsdate810.setText(date2)}
                R.id.admin_termsdate910 ->{admin_termsdate910.setText(date2)}
                R.id.admin_termsdate1010 ->{admin_termsdate1010.setText(date2)}
                else -> println("Number too high")
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)) .show()
    }
}
