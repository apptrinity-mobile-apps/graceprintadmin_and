package indo.com.graceprintingadmin.Activities

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprintingadmin.Adapter.PageAdapter
import indo.com.graceprintingadmin.Fragment.PageFragment
import indo.com.graceprintingadmin.Model.APIResponse.DeptTaskListResponse
import indo.com.graceprintingadmin.Model.APIResponse.Tasklist
import indo.com.graceprintingadmin.R
import kotlinx.android.synthetic.main.activity_assigned_view_job.*
import retrofit2.Call
import retrofit2.Callback



class AssignedViewJob : AppCompatActivity() {

    internal var st_jobid: String = ""
    internal var st_deptname: String = ""
    internal var st_assigned: String = ""
    internal var position:Int = 0
    public var mTasklist: ArrayList<Tasklist> = ArrayList<Tasklist>()
    var flag = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assigned_view_job)
        readData(intent)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        toolbar.setTitle("Assigned View Job")
        departmentTaskList()

    }

    private fun readData(intent: Intent?) {
        //st_deptname = intent.getStringExtra("deptname")
        if(intent!= null && intent!!.getStringExtra("deptjobid")!=null) {
            st_jobid = intent!!.getStringExtra("deptjobid")
            st_deptname = intent!!.getStringExtra("deptname")
            if (intent!!.getStringExtra("assignedid") != null) {
                st_assigned = intent.getStringExtra("assignedid")
            }
        }
    }

    private fun departmentTaskList() {
        val apiService = ApiInterface.create()
        val call = apiService.departmentTaskList(st_jobid)
        Log.d("departmentTaskList", ""+st_jobid)
        call.enqueue(object : Callback<DeptTaskListResponse> {
            override fun onResponse(call: Call<DeptTaskListResponse>, response: retrofit2.Response<DeptTaskListResponse>?) {
                if (response != null) {
                    //my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                       // Toast.makeText(this@AssignedViewJob,""+ response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                        val list: Array<Tasklist>? = response.body()!!.tasklist!!
                        for (item: Tasklist in list!!.iterator()) {
                            mTasklist.add(item)
                        }

                    }else{
                        Toast.makeText(this@AssignedViewJob,""+ response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                    }

                }
                setTabPages()
            }

            override fun onFailure(call: Call<DeptTaskListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed"+t.message)
            }
        })
    }

    private fun setTabPages() {
        val pageAdapter = PageAdapter(supportFragmentManager)

        // create fragments from 0 to 9
        var title: String = ""
        for (i in 0 until mTasklist.size) {
            pageAdapter.add(PageFragment.newInstance(i,mTasklist,st_jobid), mTasklist.get(i).department_name!!)
            if(mTasklist.get(i).department_name!!.equals(st_deptname) && flag){
                position = i
            }
        }

        view_pager.adapter = pageAdapter
        tabs.setupWithViewPager(view_pager)
        pageAdapter.notifyDataSetChanged()
        view_pager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                view_pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
        view_pager.currentItem = position

        val linearLayout = tabs.getChildAt(0) as LinearLayout
        linearLayout.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
        val drawable = GradientDrawable()
        drawable.setColor(Color.WHITE)
        drawable.setSize(2, 0)
        linearLayout.dividerDrawable = drawable

    }


}
