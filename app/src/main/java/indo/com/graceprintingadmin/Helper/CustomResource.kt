package indo.com.graceprintingadmin.Helper

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import indo.com.graceprintingadmin.R

object CustomResource {

    var dialog_list: Dialog?=null
    var list_items_list: ListView? = null


    fun dialogInitialization(context: Context, list: List<String>, customTextView: CustomTextView) {
        dialog_list = Dialog(context)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)

        list_items_list = dialog_list!!.findViewById(R.id.list_languages)
        customTextView.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing())
                dialog_list!!.show()
            val state_array_adapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, list)
            list_items_list!!.setAdapter(state_array_adapter)
            list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                var address_type = list_items_list!!.getItemAtPosition(i).toString()
                customTextView.setText(address_type)
                dialog_list!!.dismiss()
            })
        })
    }
}