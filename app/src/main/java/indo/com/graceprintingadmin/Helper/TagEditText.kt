package indo.com.graceprintingadmin.Helper

import android.content.Context
import android.text.style.ClickableSpan
import android.graphics.drawable.BitmapDrawable
import android.graphics.Bitmap
import android.graphics.Canvas
import android.util.TypedValue
import android.widget.TextView
import android.widget.LinearLayout
import android.text.Spanned
import android.text.Spannable
import android.text.style.ImageSpan
import android.text.SpannableStringBuilder
import android.text.Editable
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import indo.com.graceprintingadmin.R


class TagEditText(context: Context, attrs: AttributeSet) : EditText(context, attrs) {

    internal lateinit var textWatcher: TextWatcher

    internal lateinit var lastString: String

    internal var separator = ","

    init {
        init()
    }


    private fun init() {
        movementMethod = LinkMovementMethod.getInstance()

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                val thisString = s.toString()
                if (thisString.length > 0 && thisString != lastString) {
                    format()

                }
            }
        }

        addTextChangedListener(textWatcher)
    }


    private fun format() {

        val sb = SpannableStringBuilder()
        val fullString = text.toString()

        val strings = fullString.split(separator.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()


        for (i in strings.indices) {

            val string = strings[i]
            sb.append(string)

            if (fullString[fullString.length - 1] != separator[0] && i == strings.size - 1) {
                break
            }

            val bd = convertViewToDrawable(createTokenView(string)) as BitmapDrawable
            bd.setBounds(0, 0, bd.intrinsicWidth, bd.intrinsicHeight)

            val startIdx = sb.length - string.length
            val endIdx = sb.length

            sb.setSpan(ImageSpan(bd), startIdx, endIdx, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val myClickableSpan = MyClickableSpan(startIdx, endIdx)
            sb.setSpan(myClickableSpan, Math.max(endIdx - 2, startIdx), endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

            if (i < strings.size - 1) {
                sb.append(separator)
            } else if (fullString[fullString.length - 1] == separator[0]) {
                sb.append(separator)
            }
        }


        lastString = sb.toString()

        setText(sb)
        setSelection(sb.length)

    }

    fun createTokenView(text: String): View {


        val l = LinearLayout(context)
        l.orientation = LinearLayout.HORIZONTAL
        l.setBackgroundResource(R.drawable.square_default)

        val tv = TextView(context)
        l.addView(tv)
        tv.text = text
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14f)

        val im = ImageView(context)
        l.addView(im)
        im.setImageResource(R.drawable.delete_icon)
        im.setScaleType(ImageView.ScaleType.FIT_CENTER)

        return l
    }

    fun convertViewToDrawable(view: View): Any {
        val spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        view.measure(spec, spec)
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight())

        val b = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888)

        val c = Canvas(b)

        c.translate((-view.getScrollX()).toFloat(), (-view.getScrollY()).toFloat())
        view.draw(c)
        view.setDrawingCacheEnabled(true)
        val cacheBmp = view.getDrawingCache()
        val viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true)
        view.destroyDrawingCache()
        return BitmapDrawable(context.resources, viewBmp)
    }

    private inner class MyClickableSpan(internal var startIdx: Int, internal var endIdx: Int) : ClickableSpan() {

        override fun onClick(widget: View) {


            val s = text.toString()

            val s1 = s.substring(0, startIdx)
            val s2 = s.substring(Math.min(endIdx + 1, s.length - 1), s.length)

            this@TagEditText.setText(s1 + s2)
        }

    }
}