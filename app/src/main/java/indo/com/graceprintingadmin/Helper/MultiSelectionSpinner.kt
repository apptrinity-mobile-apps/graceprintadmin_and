package indo.com.graceprintingadmin.Helper

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnMultiChoiceClickListener
import android.util.AttributeSet
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.SpinnerAdapter
import indo.com.graceprintingadmin.R
import kotlinx.android.synthetic.main.spinner_text_item.view.*
import java.util.*
import java.util.List
import android.text.method.TextKeyListener.clear
import android.content.ContentValues.TAG
import android.widget.Toast
import android.text.method.TextKeyListener.clear
import kotlin.collections.ArrayList


/**
 * Created by indo67 on 6/11/2018.
 */
class MultiSelectionSpinner : Spinner, OnMultiChoiceClickListener {
    private var listener: OnMultipleItemsSelectedListener? = null

    internal var _items: Array<String>? = null
    internal var _items2: Array<String>? = null
    internal var _items3: Array<String>? = null
    internal var _items4: Array<String>? = null
    internal var _items5: Array<String>? = null
    internal var _items6: Array<String>? = null
    internal var _items7: Array<String>? = null
    internal var _items8: Array<String>? = null
    internal var _items9: Array<String>? = null
    internal var _items10: Array<String>? = null
    internal var mSelection: BooleanArray? = null
    internal var mSelection2: BooleanArray? = null
    internal var mSelection3: BooleanArray? = null
    internal var mSelection4: BooleanArray? = null
    internal var mSelection5: BooleanArray? = null
    internal var mSelection6: BooleanArray? = null
    internal var mSelection7: BooleanArray? = null
    internal var mSelection8: BooleanArray? = null
    internal var mSelection9: BooleanArray? = null
    internal var mSelection10: BooleanArray? = null
    internal var mSelectionAtStart: BooleanArray? = null
    internal var mSelectionAtStart2: BooleanArray? = null
    internal var mSelectionAtStart3: BooleanArray? = null
    internal var mSelectionAtStart4: BooleanArray? = null
    internal var mSelectionAtStart5: BooleanArray? = null
    internal var mSelectionAtStart6: BooleanArray? = null
    internal var mSelectionAtStart7: BooleanArray? = null
    internal var mSelectionAtStart8: BooleanArray? = null
    internal var mSelectionAtStart9: BooleanArray? = null
    internal var mSelectionAtStart10: BooleanArray? = null
    internal var _itemsAtStart: String? = null
    internal var _itemsAtStart2: String? = null
    internal var _itemsAtStart3: String? = null
    internal var _itemsAtStart4: String? = null
    internal var _itemsAtStart5: String? = null
    internal var _itemsAtStart6: String? = null
    internal var _itemsAtStart7: String? = null
    internal var _itemsAtStart8: String? = null
    internal var _itemsAtStart9: String? = null
    internal var _itemsAtStart10: String? = null

    internal var simple_adapter: ArrayAdapter<String>
    var alertDialog: AlertDialog? = null

    var index=0
    internal var _statuslist: ArrayList<String>? = null


    val selectedStrings: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
           /* if (index == 1) {*/
                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(_items!![i])

                    }
                }
           /* } else if (index == 2) {
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(_items2!![i])
                    }
                }
            } else if (index == 3) {
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(_items3!![i])
                    }
                }
            } else if (index == 4) {
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(_items4!![i])
                    }
                }
            }*/
            return selection
        }
    val selectedStrings2: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            /*if (index == 1) {
                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(_items!![i])
                    }
                }
            } else if (index == 2) {*/
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(_items2!![i])
                    }
                }
            /*} else if (index == 3) {
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(_items3!![i])
                    }
                }
            } else if (index == 4) {
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(_items4!![i])
                    }
                }
            }*/
            return selection
        }

    val selectedStrings3: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            /*if (index == 1) {
                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(_items!![i])
                    }
                }
            } else if (index == 2) {
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(_items2!![i])
                    }
                }
            } else if (index == 3) {*/
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(_items3!![i])
                    }
                }
            /*} else if (index == 4) {
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(_items4!![i])
                    }
                }
            }*/
            return selection
        }

    val selectedStrings4: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            /*if (index == 1) {
                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(_items!![i])
                    }
                }
            } else if (index == 2) {
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(_items2!![i])
                    }
                }
            } else if (index == 3) {
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(_items3!![i])
                    }
                }
            } else if (index == 4) {*/
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(_items4!![i])
                    }
                }
           // }
            return selection
        }

    val selectedStrings5: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            for (i in _items5!!.indices) {
                if (mSelection5!![i]) {
                    selection.add(_items5!![i])
                }
            }
            // }
            return selection
        }


    val selectedStrings6: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            for (i in _items6!!.indices) {
                if (mSelection6!![i]) {
                    selection.add(_items7!![i])
                }
            }
            // }
            return selection
        }


    val selectedStrings7: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            for (i in _items7!!.indices) {
                if (mSelection7!![i]) {
                    selection.add(_items7!![i])
                }
            }
            // }
            return selection
        }
    val selectedStrings8: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            for (i in _items8!!.indices) {
                if (mSelection8!![i]) {
                    selection.add(_items8!![i])
                }
            }
            // }
            return selection
        }


    val selectedStrings9: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            for (i in _items9!!.indices) {
                if (mSelection9!![i]) {
                    selection.add(_items9!![i])
                }
            }
            // }
            return selection
        }


    val selectedStrings10: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            for (i in _items10!!.indices) {
                if (mSelection10!![i]) {
                    selection.add(_items10!![i])
                }
            }
            // }
            return selection
        }

    val selectedIndices: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            /*if (index == 1) {*/

                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(i)
                    }
                }
                /*return selection
            } else if (index == 2) {
                //val selection = LinkedList<Int>()
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 3) {
                //val selection = LinkedList<Int>()
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 4) {
                // val selection = LinkedList<Int>()
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            }*/
            return selection
        }
    val selectedIndices2: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            /*if (index == 1) {

                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 2) {*/
                //val selection = LinkedList<Int>()
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            /*} else if (index == 3) {
                //val selection = LinkedList<Int>()
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 4) {
                // val selection = LinkedList<Int>()
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            }*/
            //return selection
        }
    val selectedIndices3: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            /*if (index == 1) {

                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 2) {
                //val selection = LinkedList<Int>()
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 3) {*/
                //val selection = LinkedList<Int>()
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            /*} else if (index == 4) {
                // val selection = LinkedList<Int>()
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            }
            return selection*/
        }
    val selectedIndices4: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            /*if (index == 1) {

                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 2) {
                //val selection = LinkedList<Int>()
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 3) {
                //val selection = LinkedList<Int>()
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 4) {*/
                // val selection = LinkedList<Int>()
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(i)
                    }
                }
                return selection
        /*    }
            return selection*/
        }


    val selectedIndices5: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            // val selection = LinkedList<Int>()
            for (i in _items5!!.indices) {
                if (mSelection5!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*    }
                return selection*/
        }

    val selectedIndices6: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            // val selection = LinkedList<Int>()
            for (i in _items6!!.indices) {
                if (mSelection6!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*    }
                return selection*/
        }
    val selectedIndices7: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            // val selection = LinkedList<Int>()
            for (i in _items7!!.indices) {
                if (mSelection7!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*    }
                return selection*/
        }

    val selectedIndices8: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            // val selection = LinkedList<Int>()
            for (i in _items8!!.indices) {
                if (mSelection8!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*    }
                return selection*/
        }

    val selectedIndices9: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            // val selection = LinkedList<Int>()
            for (i in _items9!!.indices) {
                if (mSelection9!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*    }
                return selection*/
        }
    val selectedIndices10: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            // val selection = LinkedList<Int>()
            for (i in _items10!!.indices) {
                if (mSelection10!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*    }
                return selection*/
        }
    val selectedItemsAsString: String
        get() {
            val sb = StringBuilder()
            val sb2 = StringBuilder()
            val sb3 = StringBuilder()
            val sb4 = StringBuilder()
            var foundOne = false

            if (index == 1) {
                if (_items!!.size > 0) {
                    for (i in _items!!.indices) {
                        if (mSelection!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items!![i])
                        }
                    }
                }
            } /*else if (index == 2) {
                if (_items2!!.size > 0) {
                    for (i in _items2!!.indices) {
                        if (mSelection2!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb2.append(_items2!![i])
                        }
                    }
                }
            } else if (index == 3) {
                if (_items3!!.size > 0) {
                    for (i in _items3!!.indices) {
                        if (mSelection3!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb3.append(_items3!![i])
                        }
                    }
                }
            } else if (index == 4) {
                if (_items4!!.size > 0) {
                    for (i in _items4!!.indices) {
                        if (mSelection4!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb4.append(_items4!![i])
                        }
                    }
                }
            }*/

            return sb.toString()
        }
    val selectedItemsAsString2: String
        get() {
            val sb = StringBuilder()
            val sb2 = StringBuilder()
            val sb3 = StringBuilder()
            val sb4 = StringBuilder()
            var foundOne = false

            /*if (index == 1) {
                if (_items!!.size > 0) {
                    for (i in _items!!.indices) {
                        if (mSelection!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items!![i])
                        }
                    }
                }
            } else*/ //if (index == 2) {
                if (_items2!!.size > 0) {
                    for (i in _items2!!.indices) {
                        if (mSelection2!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items2!![i])
                        }
                    }
                }
            //}
                /*else if (index == 3) {
                if (_items3!!.size > 0) {
                    for (i in _items3!!.indices) {
                        if (mSelection3!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb3.append(_items3!![i])
                        }
                    }
                }
            } else if (index == 4) {
                if (_items4!!.size > 0) {
                    for (i in _items4!!.indices) {
                        if (mSelection4!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb4.append(_items4!![i])
                        }
                    }
                }
            }*/

            return sb.toString()
        }

    val selectedItemsAsString3: String
        get() {
            val sb = StringBuilder()
            val sb2 = StringBuilder()
            val sb3 = StringBuilder()
            val sb4 = StringBuilder()
            var foundOne = false

            /*if (index == 1) {
                if (_items!!.size > 0) {
                    for (i in _items!!.indices) {
                        if (mSelection!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items!![i])
                        }
                    }
                }
            } else if (index == 2) {
                if (_items2!!.size > 0) {
                    for (i in _items2!!.indices) {
                        if (mSelection2!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb2.append(_items2!![i])
                        }
                    }
                }
            } else if (index == 3) {*/
                if (_items3!!.size > 0) {
                    for (i in _items3!!.indices) {
                        if (mSelection3!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items3!![i])
                        }
                    }
                }
           /* } else if (index == 4) {
                if (_items4!!.size > 0) {
                    for (i in _items4!!.indices) {
                        if (mSelection4!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb4.append(_items4!![i])
                        }
                    }
                }
            }*/

            return sb.toString()
        }

    val selectedItemsAsString4: String
        get() {
            val sb = StringBuilder()
            val sb2 = StringBuilder()
            val sb3 = StringBuilder()
            val sb4 = StringBuilder()
            var foundOne = false

            /*if (index == 1) {
                if (_items!!.size > 0) {
                    for (i in _items!!.indices) {
                        if (mSelection!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items!![i])
                        }
                    }
                }
            } else if (index == 2) {
                if (_items2!!.size > 0) {
                    for (i in _items2!!.indices) {
                        if (mSelection2!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb2.append(_items2!![i])
                        }
                    }
                }
            } else if (index == 3) {
                if (_items3!!.size > 0) {
                    for (i in _items3!!.indices) {
                        if (mSelection3!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb3.append(_items3!![i])
                        }
                    }
                }
            } else if (index == 4) {*/
                if (_items4!!.size > 0) {
                    for (i in _items4!!.indices) {
                        if (mSelection4!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items4!![i])
                        }
                    }
                }
            //}

            return sb.toString()
        }

    val selectedItemsAsString5: String
        get() {
            val sb = StringBuilder()
            var foundOne = false

            if (_items5!!.size > 0) {
                for (i in _items5!!.indices) {
                    if (mSelection5!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items5!![i])
                    }
                }
            }

            return sb.toString()
        }

    val selectedItemsAsString6: String
        get() {
            val sb = StringBuilder()
            var foundOne = false

            if (_items6!!.size > 0) {
                for (i in _items6!!.indices) {
                    if (mSelection6!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items6!![i])
                    }
                }
            }

            return sb.toString()
        }


    val selectedItemsAsString7: String
        get() {
            val sb = StringBuilder()
            var foundOne = false

            if (_items7!!.size > 0) {
                for (i in _items7!!.indices) {
                    if (mSelection7!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items7!![i])
                    }
                }
            }

            return sb.toString()
        }

    val selectedItemsAsString8: String
        get() {
            val sb = StringBuilder()
            var foundOne = false

            if (_items8!!.size > 0) {
                for (i in _items8!!.indices) {
                    if (mSelection8!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items8!![i])
                    }
                }
            }

            return sb.toString()
        }


    val selectedItemsAsString9: String
        get() {
            val sb = StringBuilder()
            var foundOne = false

            if (_items9!!.size > 0) {
                for (i in _items9!!.indices) {
                    if (mSelection9!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items9!![i])
                    }
                }
            }

            return sb.toString()
        }
    val selectedItemsAsString10: String
        get() {
            val sb = StringBuilder()
            var foundOne = false

            if (_items10!!.size > 0) {
                for (i in _items10!!.indices) {
                    if (mSelection10!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items10!![i])
                    }
                }
            }

            return sb.toString()
        }

    interface OnMultipleItemsSelectedListener {
        fun selectedIndices(indices: LinkedList<Int>)
        fun selectedStrings(strings: LinkedList<String>)
        fun selectedIndices2(indices: LinkedList<Int>)
        fun selectedStrings2(strings: LinkedList<String>)
        fun selectedIndices3(indices: LinkedList<Int>)
        fun selectedStrings3(strings: LinkedList<String>)
        fun selectedIndices4(indices: LinkedList<Int>)
        fun selectedStrings4(strings: LinkedList<String>)
        fun selectedIndices5(indices: LinkedList<Int>)
        fun selectedStrings5(strings: LinkedList<String>)
        fun selectedIndices6(indices: LinkedList<Int>)
        fun selectedStrings6(strings: LinkedList<String>)
        fun selectedIndices7(indices: LinkedList<Int>)
        fun selectedStrings7(strings: LinkedList<String>)
        fun selectedIndices8(indices: LinkedList<Int>)
        fun selectedStrings8(strings: LinkedList<String>)
        fun selectedIndices9(indices: LinkedList<Int>)
        fun selectedStrings9(strings: LinkedList<String>)
        fun selectedIndices10(indices: LinkedList<Int>)
        fun selectedStrings10(strings: LinkedList<String>)
    }

    constructor(context: Context) : super(context) {
       /* if(index==1){

        }else if (index==2){

        }else if (index==3){

        }else if (index==4){

        }*/
        simple_adapter = ArrayAdapter(context,
                R.layout.spinner_text_item)
        super.setAdapter(simple_adapter)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

        simple_adapter = ArrayAdapter(context,
                R.layout.spinner_text_item)
        super.setAdapter(simple_adapter)
    }

    fun setListener(listener: OnMultipleItemsSelectedListener) {
        this.listener = listener
    }


    val getSelectedIndices: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            for (i in 0 until _items!!.size) {
                if (mSelection!![i]) {
                    selection.add(i)
                }
            }
            return selection

        }

    val getSelectedStrings: LinkedList<String>
        get(){
        val selection = LinkedList<String>()
        for (i in 0 until _items!!.size) {
            if (mSelection!![i]) {
                selection.add(_items!![i])
            }
        }
        return selection
    }

    val getSelectedItemsAsString: String get(){
        val sb = StringBuilder()
        var foundOne = false

        for (i in 0 until _items!!.size) {
            if (mSelection!![i]) {
                if (foundOne) {
                    sb.append(", ")
                }
                foundOne = true
                sb.append(_items!![i])
            }
        }
        return sb.toString()
    }


    override fun onClick(dialog: DialogInterface, which: Int, isChecked: Boolean) {

        if (index == 1) {
            if (mSelection != null && which < mSelection!!.size) {
              /*  Log.e("mSelection!!.size",mSelection!!.toString()+"--"+mSelection!!.size);
                for (i in mSelection!!.indices) {

                        Log.e("mSelection_inn",""+mSelectionAtStart!![i]+"--"+mSelection!![i]);
                }*/
                Log.e("mSelection!![which]",which.toString()+"--"+mSelection!![which]);
                mSelection!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemStringtest())
             /*   if(mSelectionAtStart!![which] != true && (mSelection!![which] == false || mSelection!![which] == true)) {
                    simple_adapter.add(buildSelectedItemStringtest())
                    Log.e("true_false",which.toString()+"--"+mSelection!![which]);
                }else
                {
                    setDeselection(which)
                }*/
            }
        } else if (index == 2) {
            if (mSelection2 != null && which < mSelection2!!.size) {
                mSelection2!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 3) {
            if (mSelection3 != null && which < mSelection3!!.size) {
                mSelection3!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 4) {
            if (mSelection4 != null && which < mSelection4!!.size) {
                mSelection4!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 5) {
            if (mSelection5 != null && which < mSelection5!!.size) {
                mSelection5!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 6) {
            if (mSelection6 != null && which < mSelection6!!.size) {
                mSelection6!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 7) {
            if (mSelection7 != null && which < mSelection7!!.size) {
                mSelection7!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 8) {
            if (mSelection8 != null && which < mSelection8!!.size) {
                mSelection8!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 9) {
            if (mSelection9 != null && which < mSelection9!!.size) {
                mSelection9!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 10) {
            if (mSelection10 != null && which < mSelection10!!.size) {
                mSelection10!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else {
            throw IllegalArgumentException(
                    "Argument 'which' is out of bounds.")
        }

       /* if (getSelectedIndices!!.size < 3) {
            mSelection!![which] = isChecked;
            simple_adapter.clear();
            simple_adapter.add(buildSelectedItemString());
        } else {
            Toast.makeText(getContext(),"Exceeds",Toast.LENGTH_SHORT).show();*/
            //setDeselection(which);

        //}

    }

    override fun performClick(): Boolean {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Please select!!!")
        if (index == 1) {
            builder.setMultiChoiceItems(_items, mSelection, this)
            _itemsAtStart = selectedItemsAsString
        } else if (index == 2) {
            builder.setMultiChoiceItems(_items2, mSelection2, this)
            _itemsAtStart2 = selectedItemsAsString2
        } else if (index == 3) {
            builder.setMultiChoiceItems(_items3, mSelection3, this)
            _itemsAtStart3 = selectedItemsAsString3
        } else if (index == 4) {
            builder.setMultiChoiceItems(_items4, mSelection4, this)
            _itemsAtStart4 = selectedItemsAsString4
        } else if (index == 5) {
            builder.setMultiChoiceItems(_items5, mSelection5, this)
            _itemsAtStart5 = selectedItemsAsString5
        } else if (index == 6) {
            builder.setMultiChoiceItems(_items6, mSelection6, this)
            _itemsAtStart6 = selectedItemsAsString6
        } else if (index == 7) {
            builder.setMultiChoiceItems(_items7, mSelection7, this)
            _itemsAtStart7 = selectedItemsAsString7
        } else if (index == 8) {
            builder.setMultiChoiceItems(_items8, mSelection8, this)
            _itemsAtStart8 = selectedItemsAsString8
        } else if (index == 9) {
            builder.setMultiChoiceItems(_items9, mSelection9, this)
            _itemsAtStart9 = selectedItemsAsString9
        }else if (index == 10) {
            builder.setMultiChoiceItems(_items10, mSelection10, this)
            _itemsAtStart10 = selectedItemsAsString10
        }

        builder.setPositiveButton("Submit", { dialog, which ->

            if(index==1){
                System.arraycopy(mSelection!!, 0, mSelectionAtStart!!, 0, mSelection!!.size)
                listener!!.selectedIndices(selectedIndices)
                listener!!.selectedStrings(selectedStrings)
            }else if (index==2){
                System.arraycopy(mSelection2!!, 0, mSelectionAtStart2!!, 0, mSelection2!!.size)
                listener!!.selectedIndices2(selectedIndices2)
                listener!!.selectedStrings2(selectedStrings2)
            }else if (index==3){
                System.arraycopy(mSelection3!!, 0, mSelectionAtStart3!!, 0, mSelection3!!.size)
                listener!!.selectedIndices3(selectedIndices3)
                listener!!.selectedStrings3(selectedStrings3)
            }else if (index==4){
                System.arraycopy(mSelection4!!, 0, mSelectionAtStart4!!, 0, mSelection4!!.size)
                listener!!.selectedIndices4(selectedIndices4)
                listener!!.selectedStrings4(selectedStrings4)
            }else if (index==5){
                System.arraycopy(mSelection5!!, 0, mSelectionAtStart5!!, 0, mSelection5!!.size)
                listener!!.selectedIndices5(selectedIndices5)
                listener!!.selectedStrings5(selectedStrings5)
            }else if (index==6){
                System.arraycopy(mSelection6!!, 0, mSelectionAtStart6!!, 0, mSelection6!!.size)
                listener!!.selectedIndices6(selectedIndices6)
                listener!!.selectedStrings6(selectedStrings6)
            }else if (index==7){
                System.arraycopy(mSelection7!!, 0, mSelectionAtStart7!!, 0, mSelection7!!.size)
                listener!!.selectedIndices7(selectedIndices7)
                listener!!.selectedStrings7(selectedStrings7)
            }else if (index==8){
                System.arraycopy(mSelection8!!, 0, mSelectionAtStart8!!, 0, mSelection8!!.size)
                listener!!.selectedIndices8(selectedIndices8)
                listener!!.selectedStrings8(selectedStrings8)
            }else if (index==9){
                System.arraycopy(mSelection9!!, 0, mSelectionAtStart9!!, 0, mSelection9!!.size)
                listener!!.selectedIndices9(selectedIndices9)
                listener!!.selectedStrings9(selectedStrings9)
            }else if (index==10){
                System.arraycopy(mSelection10!!, 0, mSelectionAtStart10!!, 0, mSelection10!!.size)
                listener!!.selectedIndices10(selectedIndices10)
                listener!!.selectedStrings10(selectedStrings10)
            }

        })
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
            if (index == 1) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart)
                System.arraycopy(mSelectionAtStart!!, 0, mSelection!!, 0, mSelectionAtStart!!.size)
            } else if (index == 2) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart2)
                System.arraycopy(mSelectionAtStart2!!, 0, mSelection2!!, 0, mSelectionAtStart2!!.size)
            } else if (index == 3) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart3)
                System.arraycopy(mSelectionAtStart3!!, 0, mSelection3!!, 0, mSelectionAtStart3!!.size)
            } else if (index == 4) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart4)
                System.arraycopy(mSelectionAtStart4!!, 0, mSelection4!!, 0, mSelectionAtStart4!!.size)
            }else if (index == 5) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart5)
                System.arraycopy(mSelectionAtStart5!!, 0, mSelection5!!, 0, mSelectionAtStart5!!.size)
            }else if (index == 6) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart6)
                System.arraycopy(mSelectionAtStart6!!, 0, mSelection6!!, 0, mSelectionAtStart6!!.size)
            }else if (index == 7) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart7)
                System.arraycopy(mSelectionAtStart7!!, 0, mSelection7!!, 0, mSelectionAtStart7!!.size)
            }else if (index == 8) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart8)
                System.arraycopy(mSelectionAtStart8!!, 0, mSelection8!!, 0, mSelectionAtStart8!!.size)
            }else if (index == 9) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart9)
                System.arraycopy(mSelectionAtStart9!!, 0, mSelection9!!, 0, mSelectionAtStart9!!.size)
            }else if (index == 10) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart10)
                System.arraycopy(mSelectionAtStart10!!, 0, mSelection10!!, 0, mSelectionAtStart10!!.size)
            }

        })
        alertDialog = builder.create()
        alertDialog!!.show()
        return true
    }

    override fun setAdapter(adapter: SpinnerAdapter) {
        throw RuntimeException(
                "setAdapter is not supported by MultiSelectSpinner.")
    }

    fun setItems(items: Array<String>, i: Int) {
        index = i
        if(i==1){
            _items = items
            mSelection = BooleanArray(_items!!.size)
            mSelectionAtStart = BooleanArray(_items!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection, false)
            if(items.size>0) {
                mSelection!![0] = false
                mSelectionAtStart!![0] = false
            }
        }else if(i==2){
            _items2 = items
            mSelection2 = BooleanArray(_items2!!.size)
            mSelectionAtStart2 = BooleanArray(_items2!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection2, false)
            if(items.size>0) {
                mSelection2!![0] = false
                mSelectionAtStart2!![0] = false
            }
        }
        else if(i==3){
            _items3 = items
            mSelection3 = BooleanArray(_items3!!.size)
            mSelectionAtStart3 = BooleanArray(_items3!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection3, false)
            if(items.size>0) {
                mSelection3!![0] = false
                mSelectionAtStart3!![0] = false
            }
        }
        else if(i==4){
            _items4 = items
            mSelection4 = BooleanArray(_items4!!.size)
            mSelectionAtStart4 = BooleanArray(_items4!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection4, false)
            if(items.size>0) {
                mSelection4!![0] = false
                mSelectionAtStart4!![0] = false
            }
        }

        else if(i==5){
            _items5 = items
            mSelection5 = BooleanArray(_items5!!.size)
            mSelectionAtStart5 = BooleanArray(_items5!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection5, false)
            if(items.size>0) {
                mSelection5!![0] = false
                mSelectionAtStart5!![0] = false
            }
        }


        else if(i==6){
            _items6 = items
            mSelection6 = BooleanArray(_items6!!.size)
            mSelectionAtStart6 = BooleanArray(_items6!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection6, false)
            if(items.size>0) {
                mSelection6!![0] = false
                mSelectionAtStart6!![0] = false
            }
        }

        else if(i==7){
            _items7 = items
            mSelection7 = BooleanArray(_items7!!.size)
            mSelectionAtStart7 = BooleanArray(_items7!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection7, false)
            if(items.size>0) {
                mSelection7!![0] = false
                mSelectionAtStart7!![0] = false
            }
        }

        else if(i==8){
            _items8 = items
            mSelection8 = BooleanArray(_items8!!.size)
            mSelectionAtStart8 = BooleanArray(_items8!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection8, false)
            if(items.size>0) {
                mSelection8!![0] = false
                mSelectionAtStart8!![0] = false
            }
        }

        else if(i==9){
            _items9 = items
            mSelection9 = BooleanArray(_items9!!.size)
            mSelectionAtStart9 = BooleanArray(_items9!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection9, false)
            if(items.size>0) {
                mSelection9!![0] = false
                mSelectionAtStart9!![0] = false
            }
        }

        else if(i==10){
            _items10 = items
            mSelection10 = BooleanArray(_items10!!.size)
            mSelectionAtStart10 = BooleanArray(_items10!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection10, false)
            if(items.size>0) {
                mSelection10!![0] = false
                mSelectionAtStart10!![0] = false
            }
        }

    }

 /*   fun setItems(items: List<String>) {
        _items = items.toTypedArray()
        mSelection = BooleanArray(_items!!.size)
        mSelectionAtStart = BooleanArray(_items!!.size)
        simple_adapter.clear()
        simple_adapter.add(_items!![0])
        Arrays.fill(mSelection, false)
        mSelection!![0] = true
    }*/

    fun setSelection(selection: Array<String>) {
        for (i in mSelection!!.indices) {
            mSelection!![i] = false
            mSelectionAtStart!![i] = false
        }
        for (cell in selection) {
            for (j in _items!!.indices) {
                if (_items!![j] == cell) {
                    mSelection!![j] = true
                    mSelectionAtStart!![j] = true
                }
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection(selection: List<String>) {
        for (i in mSelection!!.indices) {
            mSelection!![i] = false
            mSelectionAtStart!![i] = false
        }
        for (sel in selection) {
            for (j in _items!!.indices) {
                if (_items!![j] == sel) {
                    mSelection!![j] = true
                    mSelectionAtStart!![j] = true
                }
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    override fun setSelection(index: Int) {
        for (i in mSelection!!.indices) {
            mSelection!![i] = false
            mSelectionAtStart!![i] = false
        }
        if (index >= 0 && index < mSelection!!.size) {
            mSelection!![index] = true
            mSelectionAtStart!![index] = true
        } else {
            throw IllegalArgumentException("Index " + index
                    + " is out of bounds.")
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection(selectedIndices: IntArray) {

        for (i in mSelection!!.indices) {
            mSelection!![i] = false
            mSelectionAtStart!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection!!.size) {
                mSelection!![index] = true
                mSelectionAtStart!![index] = true

            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }
    fun setSelection1(selectedIndices: IntArray) {

        for (i in mSelection2!!.indices) {
            mSelection2!![i] = false
            mSelectionAtStart2!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection2!!.size) {
                mSelection2!![index] = true
                mSelectionAtStart2!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }
    fun setSelection2(selectedIndices: IntArray) {

        for (i in mSelection3!!.indices) {
            mSelection3!![i] = false
            mSelectionAtStart3!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection3!!.size) {
                mSelection3!![index] = true
                mSelectionAtStart3!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }
    fun setSelection3(selectedIndices: IntArray) {

        for (i in mSelection4!!.indices) {
            mSelection4!![i] = false
            mSelectionAtStart4!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection4!!.size) {
                mSelection4!![index] = true
                mSelectionAtStart4!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection5(selectedIndices: IntArray) {

        for (i in mSelection5!!.indices) {
            mSelection5!![i] = false
            mSelectionAtStart5!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection5!!.size) {
                mSelection5!![index] = true
                mSelectionAtStart5!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection6(selectedIndices: IntArray) {

        for (i in mSelection6!!.indices) {
            mSelection6!![i] = false
            mSelectionAtStart6!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection6!!.size) {
                mSelection6!![index] = true
                mSelectionAtStart6!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection7(selectedIndices: IntArray) {

        for (i in mSelection7!!.indices) {
            mSelection7!![i] = false
            mSelectionAtStart7!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection7!!.size) {
                mSelection7!![index] = true
                mSelectionAtStart7!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection8(selectedIndices: IntArray) {

        for (i in mSelection8!!.indices) {
            mSelection8!![i] = false
            mSelectionAtStart8!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection8!!.size) {
                mSelection8!![index] = true
                mSelectionAtStart8!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }
    fun setSelection9(selectedIndices: IntArray) {

        for (i in mSelection9!!.indices) {
            mSelection9!![i] = false
            mSelectionAtStart9!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection9!!.size) {
                mSelection9!![index] = true
                mSelectionAtStart9!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection10(selectedIndices: IntArray) {

        for (i in mSelection10!!.indices) {
            mSelection10!![i] = false
            mSelectionAtStart10!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection10!!.size) {
                mSelection10!![index] = true
                mSelectionAtStart10!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }


    private fun buildSelectedItemString(): String {
        val sb = StringBuilder()
        var foundOne = false
        if(index==1){
            for (i in _items!!.indices) {
                if (mSelection!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items!![i])
                }
            }
        }else if (index==2){
            for (i in _items2!!.indices) {
                if (mSelection2!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items2!![i])
                }
            }
        }else if (index==3){
            for (i in _items3!!.indices) {
                if (mSelection3!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items3!![i])
                }
            }
        }else if (index==4){
            for (i in _items4!!.indices) {
                if (mSelection4!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items4!![i])
                }
            }
        }else if (index==5){
            for (i in _items5!!.indices) {
                if (mSelection5!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items5!![i])
                }
            }
        }else if (index==6){
            for (i in _items6!!.indices) {
                if (mSelection6!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items6!![i])
                }
            }
        }else if (index==7){
            for (i in _items7!!.indices) {
                if (mSelection7!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items7!![i])
                }
            }
        }else if (index==8){
            for (i in _items8!!.indices) {
                if (mSelection8!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items8!![i])
                }
            }
        }else if (index==9){
            for (i in _items9!!.indices) {
                if (mSelection9!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items9!![i])
                }
            }
        }else if (index==10){
            for (i in _items10!!.indices) {
                if (mSelection10!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items10!![i])
                }
            }
        }

        return sb.toString()
    }

    private fun buildSelectedItemStringtest(): String {

        val sb = StringBuilder()
        var foundOne = false

            for (i in _items!!.indices) {
                if (mSelection!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items!![i])
                }
            }

        return sb.toString()
    }

    fun setDeselection(index: Int) {
        Log.d(TAG, "setDeselection() called with: index = [$index]")

        if (index >= 0 && index < mSelection!!.size) {
            mSelection!![index] = true
            mSelectionAtStart!![index] = true

        } else {
            throw IllegalArgumentException("Index " + index
                    + " is out of bounds.")
        }
        simple_adapter.clear()
        simple_adapter.notifyDataSetChanged()
        simple_adapter.add(buildSelectedItemString())

        buildDialogue()
    }


    private fun buildDialogue() {
        if (alertDialog != null && alertDialog!!.isShowing())
            alertDialog!!.dismiss()

        val builder = AlertDialog.Builder(context)
        builder.setTitle("Please select!!!")
        builder.setMultiChoiceItems(_items, mSelection, this)
        _itemsAtStart = getSelectedItemsAsString
        builder.setPositiveButton("Submit") { dialog, which ->
            System.arraycopy(mSelection, 0, mSelectionAtStart, 0, mSelection!!.size)
            if (listener != null) {
                listener!!.selectedIndices(getSelectedIndices)
                listener!!.selectedStrings(getSelectedStrings)
            }
        }
        builder.setNegativeButton("Cancel") { dialog, which ->
            simple_adapter.clear()
            simple_adapter.add(_itemsAtStart)
            System.arraycopy(mSelectionAtStart, 0, mSelection, 0, mSelectionAtStart!!.size)
        }
        alertDialog = builder.create()
        alertDialog!!.show()
    }
}