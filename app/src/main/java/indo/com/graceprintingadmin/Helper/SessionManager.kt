package indo.com.graceprintingadmin.Helper

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import indo.com.graceprintingadmin.Activities.LoginActivity
import java.util.*


/**
 * Created by indobytes21 on 2/9/2017.
 */

class SessionManager// Constructor
(// Context
        internal var _context: Context) {
    internal var pref: SharedPreferences
    // Editor for Shared preferences
    internal var editor: SharedPreferences.Editor
    // Shared pref mode
    internal var PRIVATE_MODE = 0
    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)
    val isUserId :String
        get() = pref.getString(ADMIN_ID,"")
    val isUserName :String
        get() = pref.getString(ADMIN_USERNAME,"User Name")

    // user name
    // user email id
    //user.put(PROFILE_WEB_ID, pref.getString(PROFILE_WEB_ID, null));
    // return user
    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user[ADMIN_ID] = pref.getString(ADMIN_ID, "")
            user[ADMIN_EMAIL] = pref.getString(ADMIN_EMAIL, "")
            user[ADMIN_NAME] = pref.getString(ADMIN_NAME, "")
            user[ADMIN_IMAGE] = pref.getString(ADMIN_IMAGE, "")
            user[ADMIN_USERNAME] = pref.getString(ADMIN_USERNAME, "")


            return user
        }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun createLoginSession(admin_id: String, admin_name: String, admin_email: String, admin_image: String, admin_username: String) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true)
        // Storing name in pref
        editor.putString(ADMIN_ID, admin_id)
        editor.putString(ADMIN_EMAIL, admin_email)
        editor.putString(ADMIN_NAME, admin_name)
        editor.putString(ADMIN_IMAGE, admin_image)
        editor.putString(ADMIN_USERNAME, admin_username)

        // commit changes
        editor.commit()
    }

    fun tabstatus(tab_position: String) {

        editor.putString(TAB_POSITION, tab_position)
        // commit changes
        editor.commit()
    }

    fun gettabstatus(): String {
        return pref.getString(TAB_POSITION, "")
    }

    fun checkLogin() {
        // Check login status
        if (!this.isLoggedIn) {
            // user is not logged in redirect him to Login Activity
            val i = Intent(_context, LoginActivity::class.java)
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            // Add new Flag to start new Activity
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            // Staring Login Activity
            _context.startActivity(i)
        }
    }

    fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear()
        editor.commit()
        // After logout redirect user to Loing Activity
        val i = Intent(_context, LoginActivity::class.java)
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        // Add new Flag to start new Activity
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        // Staring Login Activity
        _context.startActivity(i)
    }


    companion object {

        var ADMIN_ID = "adminid"
        var ADMIN_EMAIL = "adminemail"
        var ADMIN_NAME = "adminname"
        var ADMIN_IMAGE = "adminimage"
        var ADMIN_USERNAME = "adminusername"

        var IS_LOGIN = "IsLoggedIn"
        var PREF_NAME = "graceprint"

        var TAB_POSITION = "tab_position"
    }
}
