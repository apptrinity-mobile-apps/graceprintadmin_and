package indo.com.graceprintingadmin.Helper

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnMultiChoiceClickListener
import android.util.AttributeSet
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.SpinnerAdapter
import indo.com.graceprintingadmin.R
import kotlinx.android.synthetic.main.spinner_text_item.view.*
import java.util.*
import java.util.List
import android.text.method.TextKeyListener.clear
import android.content.ContentValues.TAG
import android.widget.Toast
import android.text.method.TextKeyListener.clear
import kotlin.collections.ArrayList


/**
 * Created by indo67 on 6/11/2018.
 */
class MultiSelectionSpinnerEdit : Spinner, OnMultiChoiceClickListener {
    private var listener: OnMultipleItemsSelectedListener? = null

    internal var _items: Array<String>? = null
    internal var _items2: Array<String>? = null
    internal var _items3: Array<String>? = null
    internal var _items4: Array<String>? = null
    internal var mSelection: BooleanArray? = null
    internal var mSelectionStatus: String? = null
    internal var mSelection2: BooleanArray? = null
    internal var mSelection3: BooleanArray? = null
    internal var mSelection4: BooleanArray? = null
    internal var mSelectionAtStart: BooleanArray? = null
    internal var mSelectionAtStart2: BooleanArray? = null
    internal var mSelectionAtStart3: BooleanArray? = null
    internal var mSelectionAtStart4: BooleanArray? = null
    internal var _itemsAtStart: String? = null
    internal var _itemsAtStart2: String? = null
    internal var _itemsAtStart3: String? = null
    internal var _itemsAtStart4: String? = null
    internal var int_ass_status: ArrayList<String>? = ArrayList()

    internal var simple_adapter: ArrayAdapter<String>
    var alertDialog: AlertDialog? = null

    var index = 0

    internal var depart_status:ArrayList<String>?=ArrayList()

    val selectedStrings: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            /* if (index == 1) {*/
            for (i in _items!!.indices) {
                if (mSelection!![i]) {
                    selection.add(_items!![i])

                }
            }
            /* } else if (index == 2) {
                 for (i in _items2!!.indices) {
                     if (mSelection2!![i]) {
                         selection.add(_items2!![i])
                     }
                 }
             } else if (index == 3) {
                 for (i in _items3!!.indices) {
                     if (mSelection3!![i]) {
                         selection.add(_items3!![i])
                     }
                 }
             } else if (index == 4) {
                 for (i in _items4!!.indices) {
                     if (mSelection4!![i]) {
                         selection.add(_items4!![i])
                     }
                 }
             }*/
            return selection
        }
    val selectedStrings2: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            /*if (index == 1) {
                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(_items!![i])
                    }
                }
            } else if (index == 2) {*/
            for (i in _items2!!.indices) {
                if (mSelection2!![i]) {
                    selection.add(_items2!![i])
                }
            }
            /*} else if (index == 3) {
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(_items3!![i])
                    }
                }
            } else if (index == 4) {
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(_items4!![i])
                    }
                }
            }*/
            return selection
        }

    val selectedStrings3: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            /*if (index == 1) {
                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(_items!![i])
                    }
                }
            } else if (index == 2) {
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(_items2!![i])
                    }
                }
            } else if (index == 3) {*/
            for (i in _items3!!.indices) {
                if (mSelection3!![i]) {
                    selection.add(_items3!![i])
                }
            }
            /*} else if (index == 4) {
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(_items4!![i])
                    }
                }
            }*/
            return selection
        }

    val selectedStrings4: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            /*if (index == 1) {
                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(_items!![i])
                    }
                }
            } else if (index == 2) {
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(_items2!![i])
                    }
                }
            } else if (index == 3) {
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(_items3!![i])
                    }
                }
            } else if (index == 4) {*/
            for (i in _items4!!.indices) {
                if (mSelection4!![i]) {
                    selection.add(_items4!![i])
                }
            }
            // }
            return selection
        }

    val selectedIndices: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            /*if (index == 1) {*/

            for (i in _items!!.indices) {
                if (mSelection!![i]) {
                    selection.add(i)
                }
            }
            /*return selection
        } else if (index == 2) {
            //val selection = LinkedList<Int>()
            for (i in _items2!!.indices) {
                if (mSelection2!![i]) {
                    selection.add(i)
                }
            }
            return selection
        } else if (index == 3) {
            //val selection = LinkedList<Int>()
            for (i in _items3!!.indices) {
                if (mSelection3!![i]) {
                    selection.add(i)
                }
            }
            return selection
        } else if (index == 4) {
            // val selection = LinkedList<Int>()
            for (i in _items4!!.indices) {
                if (mSelection4!![i]) {
                    selection.add(i)
                }
            }
            return selection
        }*/
            return selection
        }
    val selectedIndices2: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            /*if (index == 1) {

                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 2) {*/
            //val selection = LinkedList<Int>()
            for (i in _items2!!.indices) {
                if (mSelection2!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*} else if (index == 3) {
                //val selection = LinkedList<Int>()
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 4) {
                // val selection = LinkedList<Int>()
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            }*/
            //return selection
        }
    val selectedIndices3: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            /*if (index == 1) {

                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 2) {
                //val selection = LinkedList<Int>()
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 3) {*/
            //val selection = LinkedList<Int>()
            for (i in _items3!!.indices) {
                if (mSelection3!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*} else if (index == 4) {
                // val selection = LinkedList<Int>()
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            }
            return selection*/
        }
    val selectedIndices4: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            /*if (index == 1) {

                for (i in _items!!.indices) {
                    if (mSelection!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 2) {
                //val selection = LinkedList<Int>()
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 3) {
                //val selection = LinkedList<Int>()
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        selection.add(i)
                    }
                }
                return selection
            } else if (index == 4) {*/
            // val selection = LinkedList<Int>()
            for (i in _items4!!.indices) {
                if (mSelection4!![i]) {
                    selection.add(i)
                }
            }
            return selection
            /*    }
                return selection*/
        }


    val selectedItemsAsString: String
        get() {
            val sb = StringBuilder()
            val sb2 = StringBuilder()
            val sb3 = StringBuilder()
            val sb4 = StringBuilder()
            var foundOne = false

            if (index == 1) {
                if (_items!!.size > 0) {
                    for (i in _items!!.indices) {
                        if (mSelection!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items!![i])
                        }
                    }
                }
            } /*else if (index == 2) {
                if (_items2!!.size > 0) {
                    for (i in _items2!!.indices) {
                        if (mSelection2!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb2.append(_items2!![i])
                        }
                    }
                }
            } else if (index == 3) {
                if (_items3!!.size > 0) {
                    for (i in _items3!!.indices) {
                        if (mSelection3!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb3.append(_items3!![i])
                        }
                    }
                }
            } else if (index == 4) {
                if (_items4!!.size > 0) {
                    for (i in _items4!!.indices) {
                        if (mSelection4!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb4.append(_items4!![i])
                        }
                    }
                }
            }*/

            return sb.toString()
        }
    val selectedItemsAsString2: String
        get() {
            val sb = StringBuilder()
            val sb2 = StringBuilder()
            val sb3 = StringBuilder()
            val sb4 = StringBuilder()
            var foundOne = false

            /*if (index == 1) {
                if (_items!!.size > 0) {
                    for (i in _items!!.indices) {
                        if (mSelection!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items!![i])
                        }
                    }
                }
            } else*/ //if (index == 2) {
            if (_items2!!.size > 0) {
                for (i in _items2!!.indices) {
                    if (mSelection2!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items2!![i])
                    }
                }
            }
            //}
            /*else if (index == 3) {
            if (_items3!!.size > 0) {
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb3.append(_items3!![i])
                    }
                }
            }
        } else if (index == 4) {
            if (_items4!!.size > 0) {
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb4.append(_items4!![i])
                    }
                }
            }
        }*/

            return sb.toString()
        }

    val selectedItemsAsString3: String
        get() {
            val sb = StringBuilder()
            val sb2 = StringBuilder()
            val sb3 = StringBuilder()
            val sb4 = StringBuilder()
            var foundOne = false

            /*if (index == 1) {
                if (_items!!.size > 0) {
                    for (i in _items!!.indices) {
                        if (mSelection!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items!![i])
                        }
                    }
                }
            } else if (index == 2) {
                if (_items2!!.size > 0) {
                    for (i in _items2!!.indices) {
                        if (mSelection2!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb2.append(_items2!![i])
                        }
                    }
                }
            } else if (index == 3) {*/
            if (_items3!!.size > 0) {
                for (i in _items3!!.indices) {
                    if (mSelection3!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items3!![i])
                    }
                }
            }
            /* } else if (index == 4) {
                 if (_items4!!.size > 0) {
                     for (i in _items4!!.indices) {
                         if (mSelection4!![i]) {
                             if (foundOne) {
                                 sb.append(", ")
                             }
                             foundOne = true
                             sb4.append(_items4!![i])
                         }
                     }
                 }
             }*/

            return sb.toString()
        }

    val selectedItemsAsString4: String
        get() {
            val sb = StringBuilder()
            val sb2 = StringBuilder()
            val sb3 = StringBuilder()
            val sb4 = StringBuilder()
            var foundOne = false

            /*if (index == 1) {
                if (_items!!.size > 0) {
                    for (i in _items!!.indices) {
                        if (mSelection!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb.append(_items!![i])
                        }
                    }
                }
            } else if (index == 2) {
                if (_items2!!.size > 0) {
                    for (i in _items2!!.indices) {
                        if (mSelection2!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb2.append(_items2!![i])
                        }
                    }
                }
            } else if (index == 3) {
                if (_items3!!.size > 0) {
                    for (i in _items3!!.indices) {
                        if (mSelection3!![i]) {
                            if (foundOne) {
                                sb.append(", ")
                            }
                            foundOne = true
                            sb3.append(_items3!![i])
                        }
                    }
                }
            } else if (index == 4) {*/
            if (_items4!!.size > 0) {
                for (i in _items4!!.indices) {
                    if (mSelection4!![i]) {
                        if (foundOne) {
                            sb.append(", ")
                        }
                        foundOne = true
                        sb.append(_items4!![i])
                    }
                }
            }
            //}

            return sb.toString()
        }

    interface OnMultipleItemsSelectedListener {
        fun selectedIndices(indices: LinkedList<Int>)
        fun selectedStrings(strings: LinkedList<String>)
        fun selectedIndices2(indices: LinkedList<Int>)
        fun selectedStrings2(strings: LinkedList<String>)
        fun selectedIndices3(indices: LinkedList<Int>)
        fun selectedStrings3(strings: LinkedList<String>)
        fun selectedIndices4(indices: LinkedList<Int>)
        fun selectedStrings4(strings: LinkedList<String>)
    }

    constructor(context: Context) : super(context) {
        /* if(index==1){

         }else if (index==2){

         }else if (index==3){

         }else if (index==4){

         }*/
        simple_adapter = ArrayAdapter(context,
                R.layout.spinner_text_item)
        super.setAdapter(simple_adapter)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

        simple_adapter = ArrayAdapter(context,
                R.layout.spinner_text_item)
        super.setAdapter(simple_adapter)
    }

    fun setListener(listener: OnMultipleItemsSelectedListener) {
        this.listener = listener
    }


    val getSelectedIndices: LinkedList<Int>
        get() {
            val selection = LinkedList<Int>()
            for (i in 0 until _items!!.size) {
                if (mSelection!![i]) {
                    selection.add(i)
                }
            }
            return selection

        }

    val getSelectedStrings: LinkedList<String>
        get() {
            val selection = LinkedList<String>()
            for (i in 0 until _items!!.size) {
                if (mSelection!![i]) {
                    selection.add(_items!![i])
                }
            }
            return selection
        }

    val getSelectedItemsAsString: String
        get() {
            val sb = StringBuilder()
            var foundOne = false

            for (i in 0 until _items!!.size) {
                if (mSelection!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true
                    sb.append(_items!![i])
                }
            }
            return sb.toString()
        }


    override fun onClick(dialog: DialogInterface, which: Int, isChecked: Boolean) {

        if (index == 1) {
            if (mSelection != null && which < mSelection!!.size) {
                mSelection!![which] = isChecked
                simple_adapter.clear()

                for(k in 0 until depart_status!!.size){
                    if(which==k){
                        if (depart_status!!.get(k).equals("0")||depart_status!!.get(k).equals("1")) {


                            simple_adapter.add(buildSelectedItemString())
                            Log.e("true_false", which.toString() + "--" + mSelection!![which]+"---"+depart_status!!.get(k));
                        } else {
                            setDeselection(which)
                            Log.e("false_false", which.toString() + "--" + mSelection!![which]+"---"+depart_status!!.get(k));
                        }
                    }

                }


                /*if (mSelectionAtStart!![which] != true && (mSelection!![which] == false || mSelection!![which] == true)) {
                    simple_adapter.add(buildSelectedItemString())
                    Log.e("true_false", which.toString() + "--" + mSelection!![which]);
                } else {
                    setDeselection(which)
                }*/

                /*for(i in int_ass_status!!.iterator()){
                    Log.e("status_stg",int_ass_status!!.get(i).toString());
                }*/

            }
        } else if (index == 2) {
            if (mSelection2 != null && which < mSelection2!!.size) {
                mSelection2!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 3) {
            if (mSelection3 != null && which < mSelection3!!.size) {
                mSelection3!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else if (index == 4) {
            if (mSelection4 != null && which < mSelection4!!.size) {
                mSelection4!![which] = isChecked
                simple_adapter.clear()
                simple_adapter.add(buildSelectedItemString())
            }
        } else {
            throw IllegalArgumentException(
                    "Argument 'which' is out of bounds.")
        }

        /* if (getSelectedIndices!!.size < 3) {
             mSelection!![which] = isChecked;
             simple_adapter.clear();
             simple_adapter.add(buildSelectedItemString());
         } else {
             Toast.makeText(getContext(),"Exceeds",Toast.LENGTH_SHORT).show();*/
        //setDeselection(which);

        //}

    }

    override fun performClick(): Boolean {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Please select!!!")
        if (index == 1) {
            builder.setMultiChoiceItems(_items, mSelection, this)
            _itemsAtStart = selectedItemsAsString
        } else if (index == 2) {
            builder.setMultiChoiceItems(_items2, mSelection2, this)
            _itemsAtStart2 = selectedItemsAsString2
        } else if (index == 3) {
            builder.setMultiChoiceItems(_items3, mSelection3, this)
            _itemsAtStart3 = selectedItemsAsString3
        } else if (index == 4) {
            builder.setMultiChoiceItems(_items4, mSelection4, this)
            _itemsAtStart4 = selectedItemsAsString4
        }

        builder.setPositiveButton("Submit", { dialog, which ->

            if (index == 1) {
                System.arraycopy(mSelection!!, 0, mSelectionAtStart!!, 0, mSelection!!.size)
                listener!!.selectedIndices(selectedIndices)
                listener!!.selectedStrings(selectedStrings)
            } else if (index == 2) {
                System.arraycopy(mSelection2!!, 0, mSelectionAtStart2!!, 0, mSelection2!!.size)
                listener!!.selectedIndices2(selectedIndices2)
                listener!!.selectedStrings2(selectedStrings2)
            } else if (index == 3) {
                System.arraycopy(mSelection3!!, 0, mSelectionAtStart3!!, 0, mSelection3!!.size)
                listener!!.selectedIndices3(selectedIndices3)
                listener!!.selectedStrings3(selectedStrings3)
            } else if (index == 4) {
                System.arraycopy(mSelection4!!, 0, mSelectionAtStart4!!, 0, mSelection4!!.size)
                listener!!.selectedIndices4(selectedIndices4)
                listener!!.selectedStrings4(selectedStrings4)
            }

        })
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
            if (index == 1) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart)
                System.arraycopy(mSelectionAtStart!!, 0, mSelection!!, 0, mSelectionAtStart!!.size)
            } else if (index == 2) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart2)
                System.arraycopy(mSelectionAtStart2!!, 0, mSelection2!!, 0, mSelectionAtStart2!!.size)
            } else if (index == 3) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart3)
                System.arraycopy(mSelectionAtStart3!!, 0, mSelection3!!, 0, mSelectionAtStart3!!.size)
            } else if (index == 4) {
                simple_adapter.clear()
                simple_adapter.add(_itemsAtStart4)
                System.arraycopy(mSelectionAtStart4!!, 0, mSelection4!!, 0, mSelectionAtStart4!!.size)
            }

        })
        alertDialog = builder.create()
        alertDialog!!.show()
        return true
    }

    override fun setAdapter(adapter: SpinnerAdapter) {
        throw RuntimeException(
                "setAdapter is not supported by MultiSelectSpinner.")
    }

    fun setItems(items: Array<String>, i: Int) {
        index = i
        if (i == 1) {
            _items = items
            mSelection = BooleanArray(_items!!.size)
            mSelectionAtStart = BooleanArray(_items!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection, false)
            if (items.size > 0) {
                mSelection!![0] = false
                mSelectionAtStart!![0] = false
            }
        } else if (i == 2) {
            _items2 = items
            mSelection2 = BooleanArray(_items2!!.size)
            mSelectionAtStart2 = BooleanArray(_items2!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection2, false)
            if (items.size > 0) {
                mSelection2!![0] = false
                mSelectionAtStart2!![0] = false
            }
        } else if (i == 3) {
            _items3 = items
            mSelection3 = BooleanArray(_items3!!.size)
            mSelectionAtStart3 = BooleanArray(_items3!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection3, false)
            if (items.size > 0) {
                mSelection3!![0] = false
                mSelectionAtStart3!![0] = false
            }
        } else if (i == 4) {
            _items4 = items
            mSelection4 = BooleanArray(_items4!!.size)
            mSelectionAtStart4 = BooleanArray(_items4!!.size)
            simple_adapter.clear()
            //simple_adapter.add(_items!![0])
            Arrays.fill(mSelection4, false)
            if (items.size > 0) {
                mSelection4!![0] = false
                mSelectionAtStart4!![0] = false
            }
        }

    }

    /*   fun setItems(items: List<String>) {
           _items = items.toTypedArray()
           mSelection = BooleanArray(_items!!.size)
           mSelectionAtStart = BooleanArray(_items!!.size)
           simple_adapter.clear()
           simple_adapter.add(_items!![0])
           Arrays.fill(mSelection, false)
           mSelection!![0] = true
       }*/

    fun setSelection(selection: Array<String>) {
        for (i in mSelection!!.indices) {
            mSelection!![i] = false
            mSelectionAtStart!![i] = false
        }
        for (cell in selection) {
            for (j in _items!!.indices) {
                if (_items!![j] == cell) {
                    mSelection!![j] = true
                    mSelectionAtStart!![j] = true
                }
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }


    fun setSelection(selection: List<String>) {
        for (i in mSelection!!.indices) {
            mSelection!![i] = false
            mSelectionAtStart!![i] = false
        }
        for (sel in selection) {
            for (j in _items!!.indices) {
                if (_items!![j] == sel) {
                    mSelection!![j] = true
                    mSelectionAtStart!![j] = true
                }
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    override fun setSelection(index: Int) {
        for (i in mSelection!!.indices) {
            mSelection!![i] = false
            mSelectionAtStart!![i] = false
        }
        if (index >= 0 && index < mSelection!!.size) {
            mSelection!![index] = true
            mSelectionAtStart!![index] = true
        } else {
            throw IllegalArgumentException("Index " + index
                    + " is out of bounds.")
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection(selectedIndices: IntArray) {

        for (i in mSelection!!.indices) {
            mSelection!![i] = false
            mSelectionAtStart!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection!!.size) {
                mSelection!![index] = true
                mSelectionAtStart!![index] = true

            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelectionStatus(selectedIndices: IntArray) {

        for(k in 0 until selectedIndices.size){
            Log.e("int_ass_status_full_ssss", selectedIndices.get(k).toString())
            depart_status!!.add(selectedIndices.get(k).toString())
        }

    }

    fun setSelection1(selectedIndices: IntArray) {

        for (i in mSelection2!!.indices) {
            mSelection2!![i] = false
            mSelectionAtStart2!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection2!!.size) {
                mSelection2!![index] = true
                mSelectionAtStart2!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection2(selectedIndices: IntArray) {

        for (i in mSelection3!!.indices) {
            mSelection3!![i] = false
            mSelectionAtStart3!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection3!!.size) {
                mSelection3!![index] = true
                mSelectionAtStart3!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    fun setSelection3(selectedIndices: IntArray) {

        for (i in mSelection4!!.indices) {
            mSelection4!![i] = false
            mSelectionAtStart4!![i] = false
        }
        for (index in selectedIndices) {
            if (index >= 0 && index < mSelection4!!.size) {
                mSelection4!![index] = true
                mSelectionAtStart4!![index] = true
            } else {
                throw IllegalArgumentException("Index " + index
                        + " is out of bounds.")
            }
        }
        simple_adapter.clear()
        simple_adapter.add(buildSelectedItemString())
    }

    private fun buildSelectedItemString(): String {
        val sb = StringBuilder()
        var foundOne = false
        if (index == 1) {
            for (i in _items!!.indices) {
                if (mSelection!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items!![i])
                }
            }
        } else if (index == 2) {
            for (i in _items2!!.indices) {
                if (mSelection2!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items2!![i])
                }
            }
        } else if (index == 3) {
            for (i in _items3!!.indices) {
                if (mSelection3!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items3!![i])
                }
            }
        } else if (index == 4) {
            for (i in _items4!!.indices) {
                if (mSelection4!![i]) {
                    if (foundOne) {
                        sb.append(", ")
                    }
                    foundOne = true

                    sb.append(_items4!![i])
                }
            }
        }

        return sb.toString()
    }

    fun setDeselection(index: Int) {
        Log.d(TAG, "setDeselection() called with: index = [$index]")

        if (index >= 0 && index < mSelection!!.size) {
            mSelection!![index] = true
            mSelectionAtStart!![index] = true

        } else {
            throw IllegalArgumentException("Index " + index
                    + " is out of bounds.")
        }
        simple_adapter.clear()
        simple_adapter.notifyDataSetChanged()
        simple_adapter.add(buildSelectedItemString())

        buildDialogue()
    }


    private fun buildDialogue() {
        Toast.makeText(context,"Task Completed ",Toast.LENGTH_LONG).show()
        if (alertDialog != null && alertDialog!!.isShowing())
            alertDialog!!.dismiss()

        val builder = AlertDialog.Builder(context)
        builder.setTitle("Please select!!!")
        builder.setMultiChoiceItems(_items, mSelection, this)
        _itemsAtStart = getSelectedItemsAsString
        builder.setPositiveButton("Submit") { dialog, which ->
            System.arraycopy(mSelection, 0, mSelectionAtStart, 0, mSelection!!.size)
            if (listener != null) {
                listener!!.selectedIndices(getSelectedIndices)
                listener!!.selectedStrings(getSelectedStrings)
            }
        }
        builder.setNegativeButton("Cancel") { dialog, which ->
            simple_adapter.clear()
            simple_adapter.add(_itemsAtStart)
            System.arraycopy(mSelectionAtStart, 0, mSelection, 0, mSelectionAtStart!!.size)
        }
        alertDialog = builder.create()
        alertDialog!!.show()
    }
}