package indo.com.graceprintingadmin.Helper

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import indo.com.graceprintingadmin.Activities.AssignedViewJob
import indo.com.graceprintingadmin.R


class FirebaseMessageService : FirebaseMessagingService() {
    private val TAG = "FirebaseMessageService"


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

       // Log.d(TAG, "MessageNotificationBody: " + remoteMessage!!.getNotification()!!.getBody()!!)
        // Check whether the remoteMessage contains a notification payload.
        if (remoteMessage!!.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification()!!.getBody()!!)
            sendNotification(remoteMessage.getNotification()!!.getBody(),"","","","","")
        }


        //Check whether the remoteMessage contains a data payload.
        if (remoteMessage.getData().size > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString())


           /* val resultIntent = Intent(this, AssignedViewJob::class.java)
            resultIntent.putExtra("deptjobid",remoteMessage.getData().get("job_dep_id"))
            resultIntent.putExtra("deptname",remoteMessage.getData().get("department_name"))
            resultIntent.putExtra("assignedid",remoteMessage.getData().get("assigned_id"))
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.getApplication().startActivity(resultIntent);*/


            if(remoteMessage.getData().get("type").equals("authentication")){

            }

            sendNotification(remoteMessage.getData().get("job_id"),remoteMessage.getData().get("department_name"),remoteMessage.getData().get("assigned_id"),remoteMessage.getData().get("task_name"),remoteMessage.getData().get("job_dep_id"),remoteMessage.getData().get("department_id"))
            try {
                val map = remoteMessage.getData()
                handleDataMessage(map)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }


        }
    }


    private fun handleDataMessage(map: Map<String, String>) {
        Log.e(TAG, "push json: " + map.toString())

        try {

            val job_id = map["job_id"]
            val assigned_id = map["assigned_id"]
            val job_dept_id = map["job_dep_id"]
            val dept_id = map["department_id"]
            val department_name = map["department_name"]
            val used_sheets = map["used_sheets"]
            val alloted_sheets = map["alloted_sheets"]

            //Log.e(TAG, "type: " + type)
           // Log.e(TAG, "message: " + message)
            //Log.e(TAG, "id: " + id)
            /*val resultIntent = Intent(this, AssignedViewJob::class.java)

            resultIntent.putExtra("deptjobid",job_dept_id)
            resultIntent.putExtra("deptname",department_name)
            resultIntent.putExtra("assignedid",assigned_id)
            //resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            resultIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(resultIntent)*/

            /** Do the things you would like to do with the data, here **/

        } catch (e: Exception) {
            Log.e(TAG, "Exception: " + e.message)
        }


    }





    fun sendNotification(job_id: String?,department_name: String?, assigned_id: String?, task_name: String?, job_dept_id: String?,dept_id: String?) {

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = 1
        val channelId = "channel-01"
        val channelName = "Channel Name"
        val importance = NotificationManager.IMPORTANCE_HIGH

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                    channelId, channelName, importance)
            notificationManager.createNotificationChannel(mChannel)
        }
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val mBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.appicon)
                .setContentTitle("GracePrintingAdmin")
                .setSound(defaultSoundUri)
                .setContentText(department_name)
                .setAutoCancel(true)

        val resultIntent = Intent(this, AssignedViewJob::class.java)
        resultIntent.putExtra("deptjobid",job_dept_id)
        resultIntent.putExtra("deptname",department_name)
        resultIntent.putExtra("assignedid",assigned_id)
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)

            notificationManager.notify(notificationId, mBuilder.build())


    }



}